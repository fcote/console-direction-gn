﻿Imports System.Data
Imports Oracle.DataAccess.Client
Imports dotnetCHARTING
Partial Class ee
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        ' ScriptManager.GetCurrent(Page).RegisterPostBackControl(ExportToExcelButton)

        'If Not Page.IsPostBack Then
        '    Session("niveau") = Request.QueryString("niveau")
        '    Dim niveau As String = Request.QueryString("niveau")
        'End If

        If Not Page.IsPostBack Then

            'If Session("role") = "administrateur" Then
            '    If ddl_dir.SelectedValue = "" Then
            '        Session("ID_USER") = "" '"0001114710"
            '    Else
            '        Session("ID_USER") = ddl_dir.SelectedValue
            '    End If
            'Else 'If Session("role") = "directeur" Then
            '    Session("ID_USER") = Session("ID")
            'End If


            If Session("role") = "administrateur" Or Session("role") = "directeur" Then
                'SqlDataSource1.SelectCommand = " SELECT DISTINCT gne2.m_ecole as ecole, gne2.nom_ecole " & _
                '        "FROM GN_VW_ENSEIGNANTS GNE2 " & _
                '        "WHERE GNE2.M_MEMBRE_ID = :ID_USER "
                SqlDataSource1.SelectCommand = " SELECT DISTINCT gne2.m_ecole as ecole, gne2.nom_ecole " & _
                        "FROM GN_VW_ENSEIGNANTS GNE2 " & _
                        "WHERE GNE2.M_MEMBRE_ID = '" & Session("ID_USER") & "' "
            Else
                'SqlDataSource1.SelectCommand = " select ecole from SP_ACCES_CONSOLE where login = :ID_USER"
                SqlDataSource1.SelectCommand = " select ecole from SP_ACCES_CONSOLE where login  = '" & Session("ID_USER") & "' "
            End If
            ' If Session("ID_USER") <> "" Or Session("role") = "enseignant" Then

            'ddl_ecole.DataBind()
            'initialisation...
            ddl_ecole.DataBind()
			InitDdlEcole()
            'ddl_etape.Items.Clear()
            'If GetPalierEcole(ddl_ecole.SelectedValue) = "E" Then
            '    ddl_etape.Items.Add(New ListItem("1", "1"))
            '    ddl_etape.Items.Add(New ListItem("2", "2"))
            '    ddl_etape.Items.Add(New ListItem("3", "3"))
            'Else
            '    ddl_etape.Items.Add(New ListItem("00", "00"))
            'End If

            'SqlDataSource2.SelectCommand = Requete_ee()
            'RadGrid1.Rebind()

            'End If
        Else
            SqlDataSource2.SelectCommand = Requete_ee()
            'RadGrid1.Rebind()
            RadGrid1.ExportSettings.FileName = "Élèves_en_échecs_" & Date.Today & "_" & ddl_ecole.SelectedValue.ToString
            RadGrid1.ExportSettings.IgnorePaging = True
            RadGrid1.ExportSettings.OpenInNewWindow = True
            'voir exemple sur site web
            'RadGrid1.ExportSettings.Excel.FileExtension = "xls"
            'RadGrid1.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Html
            RadGrid1.MasterTableView.CommandItemSettings.RefreshText = "Rafraîchir"

            'RadGrid1.MasterTableView.ExportToExcel()
            'RadGrid1.MasterTableView.
            'RadGrid1.ExportSettings
            'RadGrid1.ExportSettings.Excel.FileExtension
            'enlever pour accèder à l'export XLS, CSV, DOCX

        End If


        'ddl_ecole.DataBind()
        'ddl_etape.Items.Clear()
        'If GetPalierEcole(ddl_ecole.SelectedValue) = "E" Then
        '    ddl_etape.Items.Add(New ListItem("1", "1"))
        '    ddl_etape.Items.Add(New ListItem("2", "2"))
        '    ddl_etape.Items.Add(New ListItem("3", "3"))
        'Else
        '    ddl_etape.Items.Add(New ListItem("00", "00"))
        'End If

        'SqlDataSource2.SelectCommand = Requete_ee()
        'RadGrid1.Rebind()


    End Sub

    Private Function Requete_ee() As String

        'Dim niveau As String = ddl_niveau.SelectedValue
        'Dim palier As String = "S"
        'If niveau = "ele" Then
        '    palier = "E"
        'End If
        'Dim ecole As String = GetEcolesByDirection(Session("ID"), palier)


        Dim ecole As String = ddl_ecole.SelectedValue
        Dim palier As String = GetPalierEcole(ddl_ecole.SelectedValue)
        Dim niveau As String = "SEC"
        If palier = "E" Then
            niveau = "ELE"
        End If

        Dim note As String = ddl_note.SelectedValue
        Dim etapeCourante = ddl_etape.SelectedValue

        Dim Sql As String = ""
        Sql &= "SELECT DISTINCT SP.M_NOM AS NOM, "
        Sql &= "SP.M_PRENOM AS PRENOM, "
        Sql &= "SP.M_MEMBRE_ID as PHOTO, "
        Sql &= "GNEL.NIVEAU AS NIVEAU, "
        Sql &= "SP.M_CLASSE_CODE AS CLASSE_CODE, "
        Sql &= "GNEN.M_PRENOM || ' ' || GNEN.M_NOM AS ENSEIGNANT, "
        'Sql &= "GN_VW_ENSEIGNANTS.M_NOM AS NOM_E, "
        'Sql &= "GN_VW_ENSEIGNANTS.M_PRENOM AS PRENOM_E, "
        Sql &= "SP.M_NOTE AS NOTE, "
        'Sql &= "GN_VW_CLASSES_SEC.DESCRIPTION AS DESCRIPTION, "
        Sql &= "SUBSTR(GNCL.DESCRIPTION,0,20) AS DESCRIPTION, "
        'Sql &= "GN_VW_ENSEIGNANTS.NOM_ECOLE AS ECOLE, "
        Sql &= "SP.M_DATECREATION AS M_DATE "
        Sql &= "FROM SP_ELEVES_RESULTATS SP, GN_VW_ENSEIGNANTS GNEN, GN_VW_ELEVES_" & niveau & " GNEL , GN_VW_CLASSES_" & niveau & " GNCL "
        Sql &= "WHERE SP.M_NOTE " & note & " and "
        Sql &= "SP.M_ANNEESCOLAIRE = fc_val_parametres('ANNEESCOLAIRE') and "
        Sql &= "SP.M_PERIODE_REF = '" & etapeCourante & "' and "
        Sql &= "SP.M_ECOLE = '" & ecole & "' and "
        Sql &= "GNCL.MEMBRE_ID = GNEN.M_MEMBRE_ID and "
        Sql &= "GNCL.CLASSE_CODE = SP.M_CLASSE_CODE and "
        Sql &= "GNCL.CODECOURS = SP.M_CODECOURS and "
        Sql &= "GNCL.ANNEESCOLAIRE = fc_val_parametres('ANNEESCOLAIRE') and "
        If niveau = "ELE" Then
            Sql &= "GNCL.STRAND_CODE = SP.M_STRAND_CODE  and "
        End If
        Sql &= "SP.M_MEMBRE_ID = GNEL.MEMBRE_ID and "
        Sql &= "SP.M_CLASSE_CODE = GNEL.CLASSE_CODE and "
        Sql &= "SP.M_CODECOURS = GNEL.CODECOURS and "
        Sql &= "GNEL.ECOLE = '" & ecole & "' and "
        Sql &= "GNEL.ANNEESCOLAIRE = fc_val_parametres('ANNEESCOLAIRE') and "
        Sql &= "GNEN.M_ECOLE = SP.M_ECOLE and "
        Sql &= "GNEN.ANNEESCOLAIRE = SP.M_ANNEESCOLAIRE "

        Return Sql
    End Function

    Private Function Chart_ee() As String

        Dim ecole As String = ddl_ecole.SelectedValue
        Dim palier As String = GetPalierEcole(ddl_ecole.SelectedValue)
        Dim niveau As String = "SEC"
        If palier = "E" Then
            niveau = "ELE"
        End If

        Dim note As String = ddl_note.SelectedValue
        Dim etapeCourante = ddl_etape.SelectedValue

        ' ''Dim Sql As String = "SELECT "
        ' ''Sql &= "SP_ELEVES_RESULTATS.M_DATECREATION AS M_DATE, "
        ' ''Sql &= "SP_ELEVES_RESULTATS.M_NOTE AS NOTE, "
        ' ''Sql &= "SP_ELEVES_RESULTATS.M_CLASSE_CODE AS CLASSE_CODE "
        ' ''Sql &= "FROM SP_ELEVES_RESULTATS, GN_VW_ENSEIGNANTS, GN_VW_ELEVES_" & niveau & ", GN_VW_CLASSES_" & niveau & "  "
        ' ''Sql &= "WHERE SP_ELEVES_RESULTATS.M_NOTE " & note & " and "
        ' ''Sql &= "SP_ELEVES_RESULTATS.M_ANNEESCOLAIRE = fc_val_parametres('ANNEESCOLAIRE') and "
        ' ''Sql &= "SP_ELEVES_RESULTATS.M_PERIODE_REF = '" & etapeCourante & "' and "
        ' ''Sql &= "SP_ELEVES_RESULTATS.M_ECOLE = '" & ecole & "' and "
        ' ''Sql &= "GN_VW_CLASSES_" & niveau & ".MEMBRE_ID = GN_VW_ENSEIGNANTS.M_MEMBRE_ID and "
        ' ''Sql &= "GN_VW_CLASSES_" & niveau & ".CLASSE_CODE = SP_ELEVES_RESULTATS.M_CLASSE_CODE and "
        ' ''Sql &= "GN_VW_CLASSES_" & niveau & ".CODECOURS = SP_ELEVES_RESULTATS.M_CODECOURS and "
        ' ''Sql &= "GN_VW_CLASSES_" & niveau & ".ANNEESCOLAIRE = fc_val_parametres('ANNEESCOLAIRE') and "
        ' ''If niveau = "ELE" Then
        ' ''    Sql &= "GN_VW_CLASSES_ELE.STRAND_CODE = SP_ELEVES_RESULTATS.M_STRAND_CODE  and "
        ' ''End If
        ' ''Sql &= "SP_ELEVES_RESULTATS.M_MEMBRE_ID = GN_VW_ELEVES_" & niveau & ".MEMBRE_ID and "
        ' ''Sql &= "SP_ELEVES_RESULTATS.M_CLASSE_CODE = GN_VW_ELEVES_" & niveau & ".CLASSE_CODE and "
        ' ''Sql &= "SP_ELEVES_RESULTATS.M_CODECOURS = GN_VW_ELEVES_" & niveau & ".CODECOURS and "
        ' ''Sql &= "GN_VW_ELEVES_" & niveau & ".ECOLE = '" & ecole & "' and "
        ' ''Sql &= "GN_VW_ELEVES_" & niveau & ".ANNEESCOLAIRE = fc_val_parametres('ANNEESCOLAIRE') and "
        ' ''Sql &= "GN_VW_ENSEIGNANTS.M_ECOLE = SP_ELEVES_RESULTATS.M_ECOLE and "
        ' ''Sql &= "GN_VW_ENSEIGNANTS.ANNEESCOLAIRE = SP_ELEVES_RESULTATS.M_ANNEESCOLAIRE "
        '' ''Sql &= "GROUP BY  "


        Dim Sql As String = " SELECT sp_eleves_resultats.m_datecreation AS m_date, "
        Sql &= " sp_eleves_resultats.m_note AS note, "
        Sql &= " sp_eleves_resultats.m_classe_code AS classe_code "
        Sql &= " FROM sp_eleves_resultats "
        Sql &= " WHERE(sp_eleves_resultats.m_note " & note & " ) "
        Sql &= " AND sp_eleves_resultats.m_anneescolaire = fc_val_parametres ('ANNEESCOLAIRE') "
        Sql &= " AND sp_eleves_resultats.m_periode_ref = '" & etapeCourante & "' "
        Sql &= " AND sp_eleves_resultats.m_ecole = '" & ecole & "' "


        Return Sql
    End Function

    Public Function GetEcolesByDirection(ByVal id_direction As String, Optional ByVal palier As String = "") As String

        Dim sql As String = " SELECT M_ECOLE FROM GN_VW_ENSEIGNANTS WHERE M_MEMBRE_ID= '" & id_direction & "' AND PALIER = '" & palier & "' "
        Dim ds As DataSet = New OracleHelperAE().getDataSet(sql.ToString)
        Return ds.Tables(0).Rows(0)("M_ECOLE").ToString

    End Function


    Public Function GetPalierEcole(ByVal ecole As String) As String

        Dim sql As String = "  SELECT SUBSTR (DECODE (o.school_type_code,'ConEd', 'Sec', o.school_type_code),1,1) palier  " & _
        " FROM organizations o, schools s " & _
        " WHERE s.school_code = '" & ecole & "' " & _
        " AND o.organization_id = s.default_school_bsid " & _
        " AND o.board_bsid = 'B67318' " & _
        " AND s.active_flag = 'x' "
        Dim ds As DataSet = New OracleHelperAE().getDataSet(sql.ToString)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)("PALIER").ToString
        End If
        Return ""


    End Function

	Public Function InitDdlEcole()
		ddl_etape.Items.Clear()
        If GetPalierEcole(ddl_ecole.SelectedValue) = "E" Then
            ddl_etape.Items.Add(New ListItem("I", "I"))
            ddl_etape.Items.Add(New ListItem("F", "F"))
        Else
            ddl_etape.Items.Add(New ListItem("00", "00"))
        End If

        ddl_etape.SelectedIndex = 0

        SqlDataSource2.SelectCommand = Requete_ee()
        RadGrid1.Rebind()
	End Function
	
    Protected Sub ddl_ecole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_ecole.SelectedIndexChanged
		InitDdlEcole()
        

    End Sub

    Protected Sub ddl_etape_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_etape.SelectedIndexChanged
        SqlDataSource2.SelectCommand = Requete_ee()
        RadGrid1.Rebind()
    End Sub

    Protected Sub ddl_note_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_note.SelectedIndexChanged
        SqlDataSource2.SelectCommand = Requete_ee()
        RadGrid1.Rebind()
    End Sub

    Protected Sub btn_chart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_chart.Click
        chart.Visible = True
        Dim de As New DataEngine()
        Dim ds As DataSet = New OracleHelperAE().getDataSet(Chart_ee().ToString)
        de.Data = ds

        '// We want to group dates by year/month
        'chart.DateGrouping = TimeInterval.Month

        '// A simple query: first column returned are the x axis values and the second column 
        '// are the y axis values. In this case the first column returns dates.

        '// Show the data as an area line.
        'chart.Series.Type = SeriesType.AreaLine
        'chart.SeriesCollection.Add(

        chart.Series.Name = "Note des élèves"
        chart.SeriesCollection.Add(de.GetSeries)

        'de.DataFields = "YAxis=NOTE,XAxis=CLASSE_CODE"
        'Dim s As Series = New de.GetSeries()
        'Dim SC As SeriesCollection = de.GetSeries()

        'chart.SeriesCollection.Add(de.GetSeries())
        'de.DataFields = "YAxis=name,XAxis=CLASSE_CODE"
        chart.SeriesCollection.Add(de.GetSeries())


    End Sub

    'Protected Sub ddl_dir_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_dir.SelectedIndexChanged
    '    Session("ID_USER") = ddl_dir.SelectedValue
    '    'ddl_ecole.DataBind()

    '    SqlDataSource2.SelectCommand = Requete_ee()
    '    RadGrid1.Rebind()

    'End Sub
End Class
