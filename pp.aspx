<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="pp.aspx.vb" Inherits="pp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <br />
        <asp:Label ID="Label1" runat="server" Font-Names="Berlin Sans FB Demi" Font-Size="XX-Large"
            Text="Portail des parents: informations des comptes" Width="100%"></asp:Label><br />
        <br />
        <br />
        <br />
    <div>
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" AllowPaging="True"
            AllowSorting="True" DataSourceID="SqlDataSource1" GridLines="None" Skin="Office2007" PageSize="20">
            <MasterTableView AutoGenerateColumns="true" DataSourceID="SqlDataSource1">
                     </MasterTableView>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CS_PortailEdu %>"
            ProviderName="<%$ ConnectionStrings:CS_PortailEdu.ProviderName %>" SelectCommand="
            SELECT DISTINCT P2.PERSON_ID, 
            P2.LEGAL_SURNAME, P2.LEGAL_FIRST_NAME, P1.LEGAL_NAME_UPPER as NOM_ELEVE, PT.EMAIL_ACCOUNT, cp.login, cptmp.motdepasse
            FROM STUDENT_CONTACTS SC, PERSONS P1, PERSONS P2, STUDENT_REGISTRATIONS SR, PERSON_TELECOM PT, compteparent cp, cpt_tmp cptmp
            WHERE(SC.STUDENT_PERSON_ID = P1.PERSON_ID) 
            AND SC.CONTACT_PERSON_ID = P2.PERSON_ID 
            AND SC.STUDENT_PERSON_ID = SR.PERSON_ID 
            AND SC.RELATIONSHIP_TYPE_NAME NOT IN ('G-AM', 'G-PM') 
            AND SR.SCHOOL_YEAR = gn.fc_val_parametres ('ANNEESCOLAIRE') 
            AND SR.STATUS_INDICATOR_CODE IN ('PreReg','Active') 
            AND SR.GRADE IN ('07', '08', '09', '10', '11', '12') 
            AND SC.ACCESS_TO_RECORDS_FLAG = 'x' 
            AND PT.PERSON_ID= P2.person_id 
            AND (PT.END_DATE IS NULL or PT.END_DATE > TO_DATE (to_char(sysdate, 'YYYY-MM-DD'), 'YYYY-MM-DD') ) 
            and cp.PERSON_ID = PT.PERSON_ID 
            and cptmp.id_compte = cp.id_compte
            ORDER BY P2.LEGAL_SURNAME "> <%--group by nom_eleve--%>
        </asp:SqlDataSource>
<%--    select distinct cp.Person_id, Login , cptmp.motdepasse, pp_courriel, p.legal_first_name, p.legal_surname
from COMPTEPARENT cp , PP_PREFERENCES_PARENT, cpt_tmp cptmp, persons p
where cp.id_compte =  cptmp.id_compte
and pp_membre_id = cp.person_id
and pp_membre_id = p.person_id
order by pp_courriel--%>
    </div>
 </asp:content>