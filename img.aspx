﻿<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        
        
        Dim imgName As String = Request.QueryString("name")
        Dim imgExt As String = Request.QueryString("ext")
        If imgExt = "" Then
            imgExt = ".png"
            imgName = "Org" & imgName
        End If
        
        Dim wight As Integer = 64
        
        Try 
       
        Dim pb As System.Windows.Forms.PictureBox = New System.Windows.Forms.PictureBox
        'pb.Image = System.Drawing.Image.FromFile("C:\Dev\Interface_trill_gn_pp\orga\" & imgName & imgExt)
        pb.Image = System.Drawing.Image.FromFile(Server.MapPath("~/orga/")  & imgName & imgExt)
        'pb.Image = System.Drawing.Image.FromFile("\orga\" & imgName & imgExt)
        
        'a finir, pour gerer tout les cas
        
        If pb.PreferredSize.Height > 64 Then
            wight = pb.PreferredSize.Width / (pb.PreferredSize.Height / 64)
        End If
       
                Catch
       imgName = "Org1"  
       imgExt = ".png"       
End Try

                
        ' This sample is used to provide a dynamic chart image for sample EmbeddedLegendCharts.aspx
        'Chart.TempDirectory = "~/temp"
        
        'Chart.Type = ChartType.Combo 'Horizontal;
        'Chart.Width = Unit.Parse(64)
        Chart.Width = Unit.Parse(wight)
        Chart.Height = Unit.Parse(64)
        Chart.ImageFormat = ImageFormat.Jpg
        Chart.FileQuality = 95
        Chart.Debug = True
        Chart.ImageFormat = ImageFormat.Png
        
        

        'Chart.Background = New Background("error2.png")
        If imgName <> "?" And imgExt <> "?" Then
            Chart.Background = New Background("orga/" & imgName & imgExt)
        End If
        
        'Chart.Background = New Background("orga/" & imgName)

        Chart.Background.Transparency = 100
        Chart.NoDataLabel.Text = ""
        
        Chart.UseFile = False 'pour le streaming
        Chart.Background.Color = Color.Transparent
        'Chart.ToolTip = "CECI EST UN TEST!!!"
     
    End Sub

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>

<%--<html>
<body>
<img src="~/orga/rp.jpg" width="64" />
</body>
</html>--%>
