﻿Imports Oracle.DataAccess.Client
Imports System.Data
'Imports CECCE.StudioPedagogique.Data

'Imports connection

Partial Class gn
    Inherits System.Web.UI.Page


    Public Chrono As System.Diagnostics.Stopwatch = New System.Diagnostics.Stopwatch()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Write("ID=" & Session("ID_Direction"))
        If Not Page.IsPostBack Then
            '    Session("ID_Enseignant") = DropDownList1.SelectedValue
        End If

        If Not Page.IsPostBack Then
            If Session("role") = "administrateur" Or Session("role") = "directeur" Then
                SqlDataSource7.SelectCommand = " SELECT DISTINCT gne2.m_ecole as ecole, gne2.nom_ecole " & _
                        "FROM GN_VW_ENSEIGNANTS GNE2 " & _
                        "WHERE GNE2.M_MEMBRE_ID = :ID_USER"
            Else
                SqlDataSource7.SelectCommand = " select ecole from SP_ACCES_CONSOLE where login = :ID_USER"
            End If
        Else
            RadGrid1.ExportSettings.FileName = "Moyenne_du_cours_" & Date.Today
            RadGrid1.ExportSettings.IgnorePaging = True
            RadGrid1.ExportSettings.OpenInNewWindow = True
            RadGrid1.MasterTableView.CommandItemSettings.RefreshText = "Rafraîchir"

            RadGrid2.ExportSettings.FileName = "Moyenne_évaluation_" & Date.Today
            RadGrid2.ExportSettings.IgnorePaging = True
            RadGrid2.ExportSettings.OpenInNewWindow = True
            RadGrid2.MasterTableView.CommandItemSettings.RefreshText = "Rafraîchir"

            RadGrid3.ExportSettings.FileName = "Notes_élèves_évaluation_" & Date.Today
            RadGrid3.ExportSettings.IgnorePaging = True
            RadGrid3.ExportSettings.OpenInNewWindow = True
            RadGrid3.MasterTableView.CommandItemSettings.RefreshText = "Rafraîchir"
        End If


    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged


        'faire un test sur ecole
        ' si sec
        'alors on cache les RG ele
        ' si ele
        'alors on cache les RG sec

        Session("ID_Enseignant") = DropDownList1.SelectedValue
        Session("ID_Evaluation") = ""
        'DropDownList2.Enabled = True

        Session("m_ecole") = ""
        Session("m_classe_code") = ""
        Session("m_codecours") = ""
        Session("m_periode_ref") = ""
        Session("m_strand_code") = ""

        'RadGrid1.DataSource = Nothing
        'RadGrid1.DataBind()


    End Sub

    Protected Sub DropDownList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList2.SelectedIndexChanged
        ' Session("ID_Enseignant") = DropDownList1.SelectedValue
        Session("ID_Evaluation") = ""
        Dim unique_id As String = DropDownList2.SelectedValue

        Dim sql As String
        sql = "select * from gn_vw_classes_sec where (ecole || codecours || cours_section || classe_code || strand_code || report_period) = '" & unique_id & "' "
        Dim ds As DataSet = New OracleHelperAE().getDataSet(sql.ToString)


        If ds.Tables(0).Rows.Count > 0 Then
            Session("m_ecole") = ds.Tables(0).Rows(0)("ECOLE").ToString
            'Dim m_ecole As String = ds.Tables(0).Rows(0)("ECOLE").ToString
            Session("m_classe_code") = ds.Tables(0).Rows(0)("CLASSE_CODE").ToString
            'Dim m_classe_code As String = ds.Tables(0).Rows(0)("CLASSE_CODE").ToString
            Session("m_codecours") = ds.Tables(0).Rows(0)("CODECOURS").ToString
            'Dim m_codecours As String = ds.Tables(0).Rows(0)("CODECOURS").ToString
            Session("m_periode_ref") = ds.Tables(0).Rows(0)("REPORT_PERIOD").ToString ' A VÉRIFIER
            'Dim m_periode_ref As String = ds.Tables(0).Rows(0)("REPORT_PERIOD").ToString ' A VÉRIFIER
            Session("m_strand_code") = ds.Tables(0).Rows(0)("STRAND_CODE").ToString
            'Dim m_strand_code As String = ds.Tables(0).Rows(0)("STRAND_CODE").ToString
        End If

    End Sub


    Protected Sub DropDownList2_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList2.DataBound
        DropDownList2_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub DropDownList3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList3.SelectedIndexChanged
        Session("ID_Evaluation") = DropDownList3.SelectedValue
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Chrono.Start()
    End Sub

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        Chrono.Stop()
        Response.Write("Temps: " & Chrono.ElapsedMilliseconds().ToString())
    End Sub
End Class
