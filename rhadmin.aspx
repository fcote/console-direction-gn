﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rhadmin.aspx.vb" Inherits="rhadmin" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        +Administration Apprécinet:<br />
        <br />
        <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDSrh" 
            GridLines="None" AllowFilteringByColumn="True" AllowSorting="True" AllowAutomaticUpdates="True" 
            AutoGenerateEditColumn="True" >
<MasterTableView datasourceid="SqlDSrh" autogeneratecolumns="False"
                editmode="InPlace" >
<CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn DataField="id" DataType="System.Int32" HeaderText="id" 
            SortExpression="id" UniqueName="id">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="login" HeaderText="login" 
            SortExpression="login" UniqueName="login">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="password" HeaderText="password" 
            SortExpression="password" UniqueName="password">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="nom_directeur" HeaderText="nom_directeur" 
            SortExpression="nom_directeur" UniqueName="nom_directeur">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="prenom_directeur" 
            HeaderText="prenom_directeur" SortExpression="prenom_directeur" 
            UniqueName="prenom_directeur">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ecole" HeaderText="ecole" 
            SortExpression="ecole" UniqueName="ecole">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="email_direction" 
            HeaderText="email_direction" SortExpression="email_direction" 
            UniqueName="email_direction">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="type" HeaderText="type" 
            SortExpression="type" UniqueName="type">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="nom_dirAdj" HeaderText="nom_dirAdj" 
            SortExpression="nom_dirAdj" UniqueName="nom_dirAdj">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="prenom_dirAdj" HeaderText="prenom_dirAdj" 
            SortExpression="prenom_dirAdj" UniqueName="prenom_dirAdj">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="email_dirAdj" HeaderText="email_dirAdj" 
            SortExpression="email_dirAdj" UniqueName="email_dirAdj">
        </telerik:GridBoundColumn>
    </Columns>
</MasterTableView>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDSrh" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CStrRh %>" 
            ProviderName="<%$ ConnectionStrings:CStrRh.ProviderName %>" 
            SelectCommand="SELECT * FROM directions order by nom_directeur"
            UpdateCommand="UPDATE directions SET email_dirAdj = @email_dirAdj WHERE id = @id"
            >
            <SelectParameters>
                <asp:Parameter Name="email_dirAdj" Type="String" DefaultValue="TADA" />
                <asp:Parameter Name="id" Type="Int32" DefaultValue="77" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="email_dirAdj" Type="String" DefaultValue="TADA" />
                <asp:Parameter Name="id" Type="Int32" DefaultValue="77" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateEditColumn="True" 
            DataSourceID="SqlDataSource1" GridLines="None">
<MasterTableView datasourceid="SqlDataSource1">
<CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
</MasterTableView>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
            SelectCommand="SELECT id, login, password, nom_directeur, prenom_directeur, ecole, email_direction, type, nom_dirAdj, prenom_dirAdj, email_dirAdj FROM directions" 
            UpdateCommand="UPDATE directions SET email_dirAdj = 'Habbba Babbba' WHERE (id = 77)">
        </asp:SqlDataSource>
        <br />
        <br />
    
    </div>
    <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
    </telerik:RadScriptManager>
    </form>
</body>
</html>
