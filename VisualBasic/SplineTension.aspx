<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Spline Tension"
   
   Chart.DefaultSeries.Type = SeriesType.Spline
   Chart.DefaultElement.Marker.Visible = False
   Chart.LegendBox.Position = New Point(50, 50)
   
   ' This sample demonstrates specifying spline tension.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   ' Get the same data for both series.
   Dim s1 As Series = getRandomData()(0)
   Dim s2 As Series = getRandomData()(0)
   
   ' Specify the tension.
   s1.SplineTensionPercent = 60
   s2.SplineTensionPercent = 20
   
   ' Some aesthetic settings.
   s1.Name = "Tension = 60%"
   s2.Name = "Tension = 20%"
   s2.DefaultElement.Marker.Visible = True
   
   Dim le As New LegendEntry()
   le.Name = "Default = 50%"
   Chart.LegendBox.ExtraEntries.Add(le)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(s1, s2)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(2)
   Dim a As Integer
   For a = 1 To 1
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 9
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
