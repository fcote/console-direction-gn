<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.DefaultSeries.Type = SeriesType.Line
   
   ' This sample will demonstrate how to use a replica shadow axis to identify series.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Get the x axis closer to the second axis.	
   Chart.XAxis.Maximum = 3
   
   ' Setup the shadow axis and add it.
   Dim shadow As Axis = Chart.YAxis.Calculate("")
   shadow.Orientation = dotnetCHARTING.Orientation.Right
   shadow.ClearValues = True
   shadow.DefaultTick.GridLine.Color = Color.Empty
   Chart.AxisCollection.Add(shadow)
   
   ' Add a tick for each series.
   Dim i As Integer
   For i = 0 To mySC.Count - 1
      Dim myTick As New AxisTick(mySC(i).Elements((mySC(i).Elements.Count - 1)).YValue)
      myTick.Label.Text = mySC(i).Name
      shadow.ExtraTicks.Add(myTick)
   Next i
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(2)
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         'e.Name = "Element " + Convert.ToString(b+1)
         e.XValue = b
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
