<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates using axis markers with polar and spider radar charts.
   ' Specify RadarMode
   Chart.XAxis.RadarMode = RadarMode.Spider
   Chart1.XAxis.RadarMode = RadarMode.Polar
   
   ' Add axis markers.
   Dim a1 As New AxisMarker("Marker", Color.Red, 35)
   Dim a2 As New AxisMarker("Marker", Color.Red, 35)
   Dim a3 As New AxisMarker("Marker", Color.FromArgb(150, Color.Orange), 55, 100)
   Dim a4 As New AxisMarker("Marker", Color.FromArgb(150, Color.Orange), 55, 100)
   a1.BringToFront = True
   a2.BringToFront = True
   a3.BringToFront = True
   a4.BringToFront = True
   
   Chart.YAxis.Markers.Add(a1)
   Chart1.YAxis.Markers.Add(a2)
   Chart.XAxis.Markers.Add(a3)
   Chart1.XAxis.Markers.Add(a4)
   
   
   ' Setup the two charts.
   Chart.Type = ChartType.Radar
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample (AxisMarker in Front)"
   
   Chart1.Type = ChartType.Radar 'Horizontal;
   Chart1.Size = "600x350"
   Chart1.TempDirectory = "temp"
   Chart1.Debug = True
   Chart1.Title = ".netCHARTING Sample (AxisMarker in Front)"
   
   
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
   Chart1.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(3)
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 9
         Dim e As New Element()
         e.YValue = myR.Next(50)
         e.XValue = 360 / 10 * b
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart1" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
