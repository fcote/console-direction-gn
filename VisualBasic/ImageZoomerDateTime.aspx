<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>dotnetCHARTING Sample</title>
</head>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	'This chart is with zooming, right click on the chart to access zooming
	Chart.TempDirectory = "temp"
	Chart.XAxis.FormatString = "MMM yyyy"
	Chart.Debug = True
	Chart.LegendBox.Position = LegendBoxPosition.None
	Chart.ShadingEffect = True
	Chart.PaletteName = dotnetCHARTING.Palette.Bright
	Chart.Type = ChartType.Combo
	Chart.Size = "800x350"
	Chart.Title = "Chart without zooming"
	Chart.DefaultSeries.Type = SeriesType.Bar
	Chart.XAxis.TimeInterval = TimeInterval.Month
	Chart.YAxis.Interval = 1
	Chart.DefaultSeries.DefaultElement.ToolTip = "%yValue"
	Chart.DefaultElement.ShowValue = True
	Dim mySC2 As SeriesCollection = getRandomData()
	Chart.SeriesCollection.Add(mySC2)

	'This chart is with initial zooming, right click on the chart to access zooming.
	Chart2.TempDirectory = "temp"
	Chart2.XAxis.FormatString = "MMM yyyy"
	Chart2.Debug = True
	Chart2.LegendBox.Position = LegendBoxPosition.None
	Chart2.ShadingEffect = True
	Chart2.ShadingEffectMode = ShadingEffectMode.Six
	Chart2.PaletteName = dotnetCHARTING.Palette.Bright
	Chart2.Type = ChartType.Combo
	Chart2.Size = "800x350"
	Chart2.Title = "A chart with initial zooming, right click on the chart for zooming options. Hover mouse for bar value."
	Chart2.DefaultSeries.Type = SeriesType.Bar
	Chart2.XAxis.TimeInterval = TimeInterval.Month
	Chart2.YAxis.Interval = 1
	Chart2.DefaultElement.ShowValue = True
	Chart2.DefaultSeries.DefaultElement.ToolTip = "%yValue"
	Dim mySC3 As SeriesCollection= getRandomData()
	Chart2.SeriesCollection.Add(mySC3)

	'set for initial zooming  
	Chart2.Zoomer.Enabled =True

	'configures the viewport to display 1 year
	Chart2.XAxis.Viewport.TimeInterval = New TimeIntervalAdvanced(New TimeSpan(365, 0, 0, 0))

	'displays starting in 2002 (if not set it would display at the first date. This in effect controls the starting scroll position.
	Chart2.XAxis.Viewport.ScaleRange = New ScaleRange(New DateTime(2002,1,1),New DateTime(2002,12,31))
End Sub
Function getRandomData() As SeriesCollection

	Dim myR As Random = New Random(3)
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim dt As DateTime = New DateTime(2000,1,1)

		Dim s As Series = New Series()
		For b As Integer = 0 To 95

			Dim e As Element = New Element()
			e.XDateTime = dt
					dt = dt.AddMonths(1)
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b

	' give each series element its own color
	s.PaletteName = Palette.Two
	SC.Add(s)

	Return SC
End Function
</script>

<body>
<center>
	 <dotnet:Chart ID="Chart" runat="server" />
   <br/>
	 <dotnet:Chart ID="Chart2" runat="server" />
   </center>

</body>
</html>
