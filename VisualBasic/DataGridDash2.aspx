<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using MicroCharts inside DataGrids declaratively.

		GridView1.DataSource = getDataSet()
		GridView1.DataBind()
	End Sub

	Function getDataSet() As DataSet
        Dim strAccessConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.MapPath("../../database/chartsample.mdb")
		Dim myAccessConn As OleDbConnection = New OleDbConnection(strAccessConn)
		myAccessConn.Open()
		Dim myDataSet As DataSet = New DataSet()
		CType(New OleDbDataAdapter(New OleDbCommand("SELECT City,Population FROM Cities ORDER BY Population DESC", myAccessConn)), OleDbDataAdapter).Fill(myDataSet, "Cities")
		myAccessConn.Close()
		Return myDataSet
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<form id="form1" runat="server">
	<div align="center">
	</div>
	<asp:GridView ID="GridView1" runat="server">
		<Columns>
			   <asp:TemplateField HeaderText="Chart" >
				<ItemTemplate>

				  <dnc:Chart ID="Chart1" TempDirectory="temp" Margin="0" runat="server"
                  LabelChart-Text="<%# String.Format(&quot;<Chart:Bar shading='7' max='10000000' value='{0}'>&quot;,  Eval(&quot;Population&quot;)) %>" />

				</ItemTemplate>
			</asp:TemplateField>     
		</Columns>
	</asp:GridView>
	</form>
</body>
</html>
