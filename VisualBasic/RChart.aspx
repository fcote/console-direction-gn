<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>

	<script runat="server">

		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
			Chart.Type = ChartType.Combo
			Chart.Width = 600
			Chart.Height = 450
			Chart.TempDirectory = "temp"
			Chart.Debug = True
			Chart.Title = "Sample Data"

			Chart.ChartAreaSpacing = 0
			Chart.ChartArea.HeightPercentage = 20
			Dim mySC As SeriesCollection = getRandomData()
			Chart.SeriesCollection.Add(mySC)
			Chart.DefaultSeries.Type = SeriesType.Spline
			Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical

			Dim rChartArea As ChartArea = New ChartArea("R Chart Example")
			rChartArea.XAxis = New Axis()
			'rChartArea.HeightPercentage = 20;
			rChartArea.TitleBox.Position = TitleBoxPosition.FullWithLegend
			Chart.ExtraChartAreas.Add(rChartArea)

			Dim rChart As Series = StatisticalEngine.RChart(mySC)
			rChartArea.SeriesCollection.Add(rChart)

			Dim rXBARChartArea As ChartArea = New ChartArea("R XBAR Chart Example")
			rXBARChartArea.XAxis = New Axis()
			'rXBARChartArea.HeightPercentage = 20;
			Chart.ExtraChartAreas.Add(rXBARChartArea)
			rXBARChartArea.TitleBox.Position = TitleBoxPosition.FullWithLegend

			Dim rXBARChart As Series = StatisticalEngine.RXBARChart(mySC)
			rXBARChartArea.SeriesCollection.Add(rXBARChart)
		End Sub

		Function getRandomData() As SeriesCollection
			Dim SC As SeriesCollection = New SeriesCollection()
			Dim myR As Random = New Random()

			For a As Integer = 0 To 9
				Dim s As Series = New Series()
				s.Name = "Series " & a
				s.LegendEntry.Visible = False
				For b As Integer = 0 To 4
					Dim e As Element = New Element()
					e.Name = "Element " & b
					e.YValue = myR.Next(50)
					s.Elements.Add(e)
				Next b
				SC.Add(s)
			Next a

			' Set Different Colors for our Series
			'SC[0].DefaultElement.Color = Color.FromArgb(255,99,49);
			'SC[1].DefaultElement.Color = Color.FromArgb(0,156,255);
			'SC[2].DefaultElement.Color = Color.FromArgb(49,255,49);
			'SC[3].DefaultElement.Color = Color.FromArgb(255,255,0);

			Return SC
		End Function
	</script>

</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" runat="server" Width="568px" Height="344px">
		</dotnet:Chart>
	</div>
</body>
</html>
