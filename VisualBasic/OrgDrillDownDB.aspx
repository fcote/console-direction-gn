<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">


	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates organizational drill-down using the Series.Trim method with live data from a DataBase.

		' Setup chart
		Chart.Size = "1100x450"
		Chart.Title = ".netCHARTING Sample"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Type = ChartType.Organizational

		' Define some different Header styles.
		Dim headerLabel1 As String = "<Chart:Spacer size='50,1'><Chart:Spacer size='90,1'><block><row> %Name <block><img:../../images/addW.png>"
		Dim headerLabel2 As String = "<Chart:Spacer size='50,1'><Chart:Spacer size='90,1'><block><row> %Name <block><block>"

		' Apply some styling settings.
		Chart.ChartArea.Background.ShadingEffectMode = ShadingEffectMode.Five
		Chart.ChartArea.Background.Color = Color.FromArgb(250, Color.FromArgb(180, 200, 224))
		Chart.ChartArea.Padding = 35
		Chart.ChartArea.CornerBottomRight = BoxCorner.Round
		Chart.ChartArea.CornerBottomLeft = BoxCorner.Round
		Chart.ChartArea.CornerTopRight = BoxCorner.Round
		Chart.ChartArea.Label.Font = New Font("Tahoma", 12)

		Chart.DefaultSeries.Line.Color = Color.Gray
		Chart.DefaultSeries.Line.Width = 5

		' TitleBox Customization
		Chart.TitleBox.Label.Text = " Acme Company's Organizational Chart"
		Chart.TitleBox.Label.Color = Color.White
		Chart.TitleBox.Label.Shadow.Color = Color.FromArgb(105, 0, 0, 0)
		Chart.TitleBox.Label.Shadow.Depth = 2
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Two
		Chart.TitleBox.Background.Color = Color.SteelBlue

		' Style the default annotation.
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Padding = 5
		Chart.DefaultElement.Annotation.DynamicSize = False
		Chart.DefaultElement.Annotation.DefaultCorner = BoxCorner.Round
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Seven
		Chart.DefaultElement.Annotation.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Two
		Chart.DefaultElement.Annotation.HeaderLabel.Shadow.Depth = 3
		Chart.DefaultElement.Annotation.HeaderBackground.Color = Color.Green
		Chart.DefaultElement.Annotation.HeaderLabel = New dotnetCHARTING.Label(headerLabel1, New Font("Arial", 11, FontStyle.Bold), Color.White)
		Chart.DefaultElement.Annotation.Label.Text = "<img:../../images/Org%picture><block halign='right'> %Department <br> %Email <br> %phone <br> Office #%office "
		Chart.DefaultElement.Annotation.URL = "?id=%id"

		Dim root As Integer = 1

		' Get the query string
		If Page.Request.QueryString.Count > 0 Then
			Dim query As String = Page.Request.QueryString("id")
			If Not query Is Nothing AndAlso query.Length > 0 Then
				root = Integer.Parse(query)
			End If
		End If

		'Get the data and trim the nodes to only show the node with ID = root and its immediate child elements.
		Dim mySC As SeriesCollection = getLiveData()
		Dim [partial] As Series = mySC(0).Trim(root, 2)

		' Get a string of breadcrumbs leading up to this element to help navigate and add it to the chart area.
		' Breadcrumb templates:
		Dim elementCrumb As String = "<block url='?id=%id' fColor='blue' >%Name"
		Dim lastElementCrumb As String = "<block>%Name"
		Dim separator As String = " <block fStyle='bold'>/ "
		Dim path As String = mySC(0).GetElementBreadcrumbs(root, elementCrumb, lastElementCrumb, separator)
		Chart.ChartArea.Label.Text = "Navigation: " & path


		' The following loop will iterate the elements and modify them based on custom attributes added by the trim method and other conditions.  
		For Each el As Element In [partial].Elements
			' If the root element of this view has parents, set the url to go up one level
			If el.CustomAttributes("NodeType") = "RootWithParents" Then
				If el.Annotation Is Nothing Then
				el.Annotation = New Annotation()
				End If
				el.Annotation.URL = "?id=%parentId"
				el.Annotation.HeaderLabel.Text = headerLabel2
			End If
			' If the element is the root node, or the node has no children to expand, don't link it or show expand/contract icon.
			If el.CustomAttributes("NodeType") = "Root" OrElse el.CustomAttributes("NodeType") = "NoChildren" Then
				If el.Annotation Is Nothing Then
				el.Annotation = New Annotation()
				End If
				el.Annotation.URL = ""
				el.Annotation.HeaderLabel.Text = headerLabel2
			End If
		Next el

		' Add the trimmed data.
		Chart.SeriesCollection.Add([partial])
	End Sub


	Function getLiveData() As SeriesCollection
		' Obtain series data from the database. Note this was created by adding PID (parentid)
		' to an existing table only, then populating the parentid based on the hiearchy in place.
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.SqlStatement = "SELECT * FROM Employees"
		de.DataFields = "InstanceID=ID,InstanceParentID=PID,Name=Name,office,Department,Email,Phone,Picture"
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
