<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using bubbles with a shading effect and smartLabels in the Navigator.

		Chart.Size = "600x350"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(0, 156, 255), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49) }
		Chart.DefaultSeries.Type = SeriesType.Bubble
		Chart.LegendBox.Position = LegendBoxPosition.ChartArea
		Chart.YAxis.Scale = Scale.Range
		Chart.ShadingEffectMode = ShadingEffectMode.Two
		Chart.MaximumBubbleSize = 70

		' Enable the Navigator
		Chart.Navigator.Enabled = True

		' Enable smart labels.
		Chart.DefaultElement.ShowValue = True
		Chart.DefaultElement.SmartLabel.Text = "%BubbleSize"
		'
		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2009, 1, 1)

		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 54
			Dim e As Element = New Element()
			dt = dt.AddDays(2)
			e.XDateTime = dt
			e.YValue = myR.Next(Math.Max(b,10))
			e.BubbleSize = myR.Next(45)
			s.Elements.Add(e)
		Next b
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
