<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates quickly getting data from a database using API shortcuts.
   Chart.Title = "Orders frequency for each month"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.DefaultSeries.PaletteName = Palette.One
   Chart.Size = "600x350"
   Chart.XAxis.Label.Text = "Months"
   
   Dim SqlStatement As String = "SELECT OrderDate,1 FROM Orders ORDER BY Orders.OrderDate"
   Chart.SeriesCollection.Add(DataEngine.GetSeries(ConfigurationSettings.AppSettings("DNCConnectionString"), SqlStatement, TimeInterval.Year))
End Sub 'Page_Load 


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
