<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates a chart with a stacked time scale.

		' This type of chart is acheved by using a stacked numeric x axis scale, then the numeric axis tick 
		' labels are replaced with a time representation. 

		' Setup the chart
		Chart.Type = ChartType.ComboHorizontal
		Chart.Size = "850x250"
		Chart.Title = "<block fSize='9'>.netCHARTING Sample"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.ShadingEffectMode = ShadingEffectMode.Two

		' Style the titlebox.
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.TitleBox.Background.Color = Color.AliceBlue ' Color.Gold;
		Chart.TitleBox.CornerTopLeft = BoxCorner.Round
		Chart.TitleBox.CornerTopRight = BoxCorner.Round
		Chart.TitleBox.CornerSize = 10
		Chart.TitleBox.Padding = 8

		Chart.DefaultElement.Transparency = 50
		Chart.YAxis.SpacingPercentage = 0
		Chart.YAxis.DefaultTick.GridLine.Color = Color.Empty

		' Setup the x axis.
		Chart.XAxis.Scale = Scale.Stacked
		Chart.XAxis.Maximum = 1440
		Chart.XAxis.Interval = 60
		Chart.XAxis.AlternateGridBackground.Color = Color.Empty

		' Add axis ticks to replace the numeric ones with time formatted versions.
		Chart.XAxis.ExtraTicks.Add(getTimeTicks(60, 1440))

		' Add a custom legend box.
		Chart.DefaultSeries.LegendEntry.Visible = False
		Chart.LegendBox.ExtraEntries.Add(New LegendEntry("Open", "", Chart.Palette(0)))
		Chart.LegendBox.ExtraEntries.Add(New LegendEntry("Occupied", "", Chart.Palette(1)))
		Chart.LegendBox.ExtraEntries.Add(New LegendEntry("Unavailable", "", Chart.Palette(2)))

		' Create some elements.

		Dim e1 As Element = New Element("Tennis Court", 500)
		Dim e2 As Element = New Element("Basketball Court", 400)

		Dim s1 As Series = New Series()
		s1.AddElements(e1, e2)

		Dim e3 As Element = New Element("Tennis Court", 100)
		Dim e4 As Element = New Element("Basketball Court", 200)

		Dim s2 As Series = New Series()
		s2.AddElements(e3, e4)

		Dim e5 As Element = New Element("Tennis Court", 600)
		Dim e6 As Element = New Element("Basketball Court", 1200)
		Dim s3 As Series = New Series()
		s3.AddElements(e5, e6)

		Dim e7 As Element = New Element("Tennis Court", 600)
		Dim s4 As Series = New Series()
		s4.AddElements(e7)

		s1.DefaultElement.Color = Chart.Palette(0)
		s2.DefaultElement.Color = Chart.Palette(1)
		s3.DefaultElement.Color = Chart.Palette(0)
		s4.DefaultElement.Color = Chart.Palette(2)

		' Add the random data.
		Chart.SeriesCollection.Add(s1, s2, s3, s4)
	End Sub

	' Method helps create a fake timeline on a numeric scale representing minutes.
	Function getTimeTicks(ByVal interval As Integer, ByVal max As Integer) As AxisTickCollection
		Dim atc As AxisTickCollection = New AxisTickCollection()
		Dim label As String = ""
		Dim i As Integer = 0
		Do While i * interval <= max
			Dim ts As TimeSpan = TimeSpan.FromMinutes(i * interval)

			If ts.Hours = 0 Then
				label = "12am"
			ElseIf ts.Hours = 12 Then
				label = "12pm"
			ElseIf ts.Hours < 12 Then
				label = ts.Hours & "am"
			Else
				label = (ts.Hours - 12) & "pm"
			End If

			Dim at As AxisTick = New AxisTick(i * interval, label)
			atc.Add(at)
			i += 1
		Loop
		Return atc
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
