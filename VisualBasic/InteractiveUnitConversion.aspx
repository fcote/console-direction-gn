<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING" %>

<script runat="server">
Sub Page_Init(sender As Object ,e As EventArgs )
  
  if(Not IsPostBack)then
	Dim str As string
	Dim np As Integer
	
	
	
	For Each str in [Enum].GetNames(GetType(dotnetCHARTING.Length))
		LengthOptions.Items.Add(str)
	next
	for np =0 To 7
		NumberPercision.Items.Add(np.ToString())
	next
  end if
	

	'set global properties
    Chart.Title="Tallest buildings in the World"
    Chart.ChartArea.XAxis.Label.Text ="Building Names"
    Chart.ChartArea.YAxis.Label.Text="Hieght"
    Chart.LegendBox.Position=LegendBoxPosition.None
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.Use3D=true
    Chart.ShadingEffect=true
    Chart.DefaultSeries.DefaultElement.ToolTip ="%yValue Feet"
    Chart.DefaultSeries.DefaultElement.ShowValue=true
    Chart.DefaultSeries.DefaultElement.ForceMarker=true
    Chart.DefaultSeries.DefaultElement.Marker.Type=ElementMarkerType.FivePointStar
    Chart.DefaultSeries.DefaultElement.SmartLabel.ForceVertical=true
    Chart.YAxis.Label.Text="Foot" 
    Chart.Size="600X500"
   
  
   'Adding series programatically
   Dim sr As Series =new Series()
   sr.Name="Building Height"
 
   Dim el As Element = new Element("CN Tower",1861)
   el.LabelTemplate = "Toronto"
   sr.Elements.Add(el)
   el = new Element("Sears Tower",1707)
   el.LabelTemplate ="Chicago"
   sr.Elements.Add(el)
   el = new Element("Ostankino Tower",1771)
   el.LabelTemplate = "Moscow"
   sr.Elements.Add(el)
   el = new Element("John Hancock Center",1476)
   el.LabelTemplate = "Chicago"
   sr.Elements.Add(el)
	el = new Element("Petronas Towers",1482)
   el.LabelTemplate = "Kuala Lumpur"
   sr.Elements.Add(el)
	el = new Element("OPB Tower",1535)
   el.LabelTemplate = "Shanghai"
   sr.Elements.Add(el)
	el = new Element("Jim Mao Building",1378)
   el.LabelTemplate = "Shanghai"
   sr.Elements.Add(el)
   el = new Element("Menara Telecom Tower",1403)
   el.LabelTemplate = "Kuala Lumpur"
   sr.Elements.Add(el)
   el = new Element("Empire State Building",1454)
   el.LabelTemplate = "New York"
   sr.Elements.Add(el)
   Chart.SeriesCollection.Add(sr)
 
End Sub
Sub ButtonConvert_Click(Sender As Object,E As EventArgs)
	Dim F As Axis 
	F =Chart.YAxis.Calculate(LengthOptions.SelectedItem.Value, Length.Foot,[Enum].Parse(GetType(dotnetCHARTING.Length),LengthOptions.SelectedItem.Value,True),RefreshScale.Checked)
	F.Orientation = dotnetCHARTING.Orientation.Right
	F.NumberPercision = Convert.ToInt32(NumberPercision.SelectedItem.Value)
	Chart.AxisCollection.Add(F)
 End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Length Conversion Options</title>
    <style type="text/css">
     body.myBody  
     {
     border-top-width:0px;
      border-left-width:0px;
     
     }
     img.myImage  
     {
     border:0px;
     
     }
     td.myTD1  
     {
      width:"1%";
        border:1px; 
        margin:0;
     }
     td.myTD2  
     {     
      width:"99%";
      background:"#BFC0DB";
      border-collapse:collapse;
       
      
     }
     table.myTable
     {
      border:1px; 
      border-style:solid;
      border-spacing:0;
      border-color:"#111111";
      border-collapse:collapse;
 
     }
     
    </style>
</head>
<body class="myBody">
    <form runat="server" action="interactiveunitconversion.aspx">
        <div style="text-align:center">
            <table class="myTable" id="AutoNumber1">
                
                <tr>
    <td class="myTD1">
<img class="myImage" alt="dotnetChartingImage" src="../../images/dotnetCharting.gif" width="230" height="94"/></td>
    <td class="myTD2">Length Options: 
 <ASP:DropDownList id="LengthOptions" runat="server" size="1">
    </ASP:DropDownList>
    <ASP:CheckBox id="RefreshScale" Text="Independent Scale" Checked="true" runat="server" size="1">
    </ASP:CheckBox>
    Decimal Places: 
    <ASP:DropDownList id="NumberPercision" runat="server" size="1">
    </ASP:DropDownList>
     <asp:Button id="ButtonConvert" onclick="ButtonConvert_Click" runat="server" Text="Convert">
    </asp:Button>
 </td>
          </tr>
            </table>
            <DOTNET:Chart id="Chart" runat="server" Visible="true" />
        </div>
    </form></body>
</html>