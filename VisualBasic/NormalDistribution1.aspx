<%@ Page Language="VB" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates the use of NormalDistribution
   ' within StatisticalEngine.
   ' The Distribution Chart
   DistributionChart.Title = "Distribution"
   DistributionChart.TempDirectory = "temp"
   DistributionChart.Debug = True
   DistributionChart.Size = "600x300"
   DistributionChart.LegendBox.Template = "%icon %name"
   DistributionChart.XAxis.ScaleRange.ValueLow = 30
   DistributionChart.XAxis.Interval = 5
   DistributionChart.YAxis.Percent = True
   DistributionChart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   DistributionChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   DistributionChart.DefaultSeries.DefaultElement.Marker.Visible = False
   
   Dim normalDistVector() As Double = {40, 55, 70, 85, 115, 130, 145, 160}
   
   
   
   Dim sampledata As New Series("Sample1")
   Dim i As Integer
   For i = 0 To normalDistVector.Length - 1
      Dim el As New Element()
      el.XValue = normalDistVector(i)
      sampledata.Elements.Add(el)
   Next i
   
   
   ' The second parameter of this method is the standard deviation of the normal probability distribution
   Dim normalDistribution As Series = StatisticalEngine.NormalDistribution(sampledata, 19)
   normalDistribution.Type = SeriesType.AreaSpline
   normalDistribution.DefaultElement.ShowValue = False
   normalDistribution.DefaultElement.SmartLabel.Text = "%PercentOfTotal"
   'normalDistribution.DefaultElement.SmartLabel.Color = Color.Black;
   normalDistribution.DefaultElement.ShowValue = True
   
   DistributionChart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
   
   'normalDistribution.
   
   'Series tmpS = new Series("",normalDistribution.Trim(new ScaleRange(50,70),ElementValue.XValue).Elements);
   Dim tmpS As New Series("", normalDistribution.Elements)
   tmpS.Trim(New ScaleRange(0, 70), ElementValue.XValue)
   ColorElements(tmpS, Color.Orange)
   
   tmpS = New Series("", normalDistribution.Elements)
   tmpS.Trim(New ScaleRange(130, 190), ElementValue.XValue)
   ColorElements(tmpS, Color.Orange)
   
   tmpS = New Series("", normalDistribution.Elements)
   tmpS.Trim(New ScaleRange(71, 85), ElementValue.XValue)
   ColorElements(tmpS, Color.Blue)
   
   tmpS = New Series("", normalDistribution.Elements)
   tmpS.Trim(New ScaleRange(115, 129), ElementValue.XValue)
   ColorElements(tmpS, Color.Blue)
   DistributionChart.SeriesCollection.Add(normalDistribution)
End Sub 'Page_Load
 

Sub ColorElements(s As Series, color As Color)
   Dim count As Integer = s.Elements.Count
   Dim i As Integer = 0
   Dim el As Element
   For Each el In  s.Elements
      'if(count/2 == i)
      el.ShowValue = True
      el.Color = color
      i += 1
   Next el
End Sub 'ColorElements
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="DistributionChart" runat="server"/>
		</div>
	</body>
</html>
