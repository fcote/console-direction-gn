<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates organizational drill-down using the Series.Trim method.

		' Setup chart
		Chart.Size = "750x350"
		Chart.Type = ChartType.Organizational
		Chart.Title = ".netCHARTING Sample"
		Chart.TempDirectory = "temp"
		Chart.Debug = True


		' Apply some styling settings.
		Chart.TitleBox.Position = TitleBoxPosition.Full
		Chart.TitleBox.Padding = 10
		Chart.TitleBox.Background.Color = Color.FromArgb(196, 215, 255)
		Chart.TitleBox.CornerTopRight = BoxCorner.Round
		Chart.TitleBox.CornerTopLeft = BoxCorner.Round
		Chart.ChartArea.Padding = 20
		Chart.ChartArea.Background.Color = Color.FromArgb(227, 236, 255)
		Chart.ChartArea.CornerBottomRight = BoxCorner.Round
		Chart.ChartArea.CornerBottomLeft = BoxCorner.Round

		Chart.DefaultSeries.Line.Width = 2
		Chart.DefaultSeries.DefaultElement.Color = Color.Gray

		' Style the default annotation.
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Size = New Size(125, 50)
		Chart.DefaultElement.Annotation.Background.Transparency = 10
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.DefaultElement.Annotation.Background.Color = Color.DeepSkyBlue 'Color.White;
		Chart.DefaultElement.Annotation.Label.Font = New Font("Tahoma", 8, FontStyle.Bold)
		Chart.DefaultElement.Annotation.DefaultCorner = BoxCorner.Round


		Dim root As Integer = 1
		Dim levels As Integer = 3

		' This sample will maintain the id of the root element shown and the number of child levels.
		' The number of levels can be changed with the 'Show x Levels' annotation buttons.
		If Page.Request.QueryString.Count > 0 Then
			Dim query As String = Page.Request.QueryString("id")
			If Not query Is Nothing AndAlso query.Length > 0 Then
				root = Integer.Parse(query)
			End If

			query = Page.Request.QueryString("levels")
			If Not query Is Nothing AndAlso query.Length > 0 Then
				levels = Integer.Parse(query)
			End If
		End If

		If levels = 4 Then
		Chart.Size = "750x500"
		End If

		' Set a default annotation URL.
		Chart.DefaultElement.Annotation.URL = "?id=%id&levels=" & levels

		'///// Setup the 'Show x Levels' annotation buttons.
		Dim b3Url As String = ""
		Dim b4Url As String = ""

		If levels = 3 Then
		b3Url &= ""
		Else
			b3Url &= "?id=" & root & "&levels=3"
		End If
		If levels = 4 Then
		b4Url &= ""
		Else
			b4Url &= "?id=" & root & "&levels=4"
		End If


		Dim defaultButtonAnnotation As Annotation = New Annotation()
		defaultButtonAnnotation.DefaultCorner = BoxCorner.Round
		defaultButtonAnnotation.CornerSize = 4
		defaultButtonAnnotation.Background.Color = Color.DeepSkyBlue
		defaultButtonAnnotation.Background.ShadingEffectMode = ShadingEffectMode.Background2

		Dim a3 As Annotation = New Annotation("Show 3 Levels ", defaultButtonAnnotation)
		a3.URL = b3Url
		If b3Url.Length = 0 Then
		a3.Label.Color = Color.Gray
		End If
		a3.Position = New Point(25, 60)
		Chart.Annotations.Add(a3)

		Dim a4 As Annotation = New Annotation("Show 4 Levels ",defaultButtonAnnotation)
		a4.URL = b4Url
		If b4Url.Length = 0 Then
		a4.Label.Color = Color.Gray
		End If
		a4.Position = New Point(90, 60)
		Chart.Annotations.Add(a4)


		'/////

		' Get the organizational nodes.
		Dim myS As Series = getOrgData()

		' The root of the element to show and the number of levels is passed to the 
		' Trim method to get the partial series of only the visible elements.
		Dim [partial] As Series = myS.Trim(root, levels)

		' The following loop will iterate the elements and modify them based on custom attributes added by the trim method and other conditions.  
		For Each el As Element In [partial].Elements
			If el.Annotation Is Nothing Then
			el.Annotation = New Annotation()
			End If

			' Give each node a color based on the organizational level. The deeper the level, the lighter the color.
			' This color array is defined below.
			If Not el.CustomAttributes("OrganizationalLevel") Is Nothing Then
				el.Annotation.Background.Color = grColor((CInt(Fix(el.CustomAttributes("OrganizationalLevel"))))-1)
			End If

			' If the root element of this view has parents, set the url to go up one level
			If el.CustomAttributes("NodeType") = "RootWithParents" Then
				el.Annotation.URL = "?id=%parentId&levels=" & levels

			' if this node is the root element of the main data set, or it has no children, get rid of the node's url so it is not clickable.
			ElseIf el.CustomAttributes("NodeType") = "Root" OrElse el.CustomAttributes("NodeType") = "NoChildren" Then
				el.Annotation.URL = ""
			End If
		Next el


		' Get a string of breadcrumbs leading up to this element to help navigate.
		Dim path As String = myS.GetElementBreadcrumbs(root, "<block url='?id=%id&levels=" & levels & "' fColor='blue' >%Name<block>", "<block  fStyle='regular' fColor='black'>%Name", " <block fStyle='bold'>/ ")

		'Chart.ChartArea.Label.Text
		Chart.Title = "<block fStyle='bold'>Navigation: " & path
		Chart.TitleBox.Label.Font = New Font("Tahoma", 8, FontStyle.Bold Or FontStyle.Underline)

		' Add the random data.
		Chart.SeriesCollection.Add([partial])



	End Sub

	Dim index As Integer = 0
	Dim ranks As String() = New String() { "General of the Army", "General", "Lieutenant General", "Major General", "Brigadier General", "Colonel", "Lieutenant Colonel", "Major", "Captan", "First Lieutenant", "Second Lieutenant" }
	Dim grColor As Color() = New Color() { Color.FromArgb(21, 67, 104), Color.FromArgb(26, 90, 128), Color.FromArgb(32, 115, 155), Color.FromArgb(38, 142, 184), Color.FromArgb(44, 170, 214), Color.FromArgb(49, 194, 240), Color.FromArgb(53, 223, 255) }

	Function getOrgData() As Series
		Dim e As Element = New Element(ranks(0))
		Dim s As Series = New Series()
		e.InstanceID = c
		c += 1
		s.Elements.Add(e)
		s.Elements.Add(getNextLevel(e, 1, 6, 2).Elements)
		Return s
	End Function
	Dim c As Integer = 1
	Function getNextLevel(ByVal parent As Element, ByVal level As Integer, ByVal maxLevel As Integer, ByVal cPerNode As Integer) As Series
		If level < maxLevel Then
			Dim s As Series = New Series()
			For i As Integer = 0 To cPerNode - 1
				Dim e As Element = New Element()
				e.InstanceID = c
				c += 1
				e.Name = ranks(level) & " " & e.InstanceID.ToString()
				e.InstanceParentID = parent.InstanceID
				s.Elements.Add(e)
				s.Elements.Add(getNextLevel(e, level + 1, maxLevel, cPerNode).Elements)
			Next i
			Return s
		Else
			Return New Series()
		End If
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
