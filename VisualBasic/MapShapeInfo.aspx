<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>

	<script runat="server">

		' This sample demonstrates adding annotations to selected shapes showing more details.

		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
			' Setup the chart.
			Chart.Type = ChartType.Map
			Chart.Size = "650x400"
			Chart.Title = "<block fSize='10'> Click on a state to see more information"
			Chart.TempDirectory = "temp"
			Chart.Debug = True
			Chart.ChartArea.Background.ShadingEffectMode = ShadingEffectMode.Background2
			Chart.LegendBox.Visible = False

			' Style the title box.
			Chart.TitleBox.Position = TitleBoxPosition.Full
			Chart.TitleBox.Padding = 5
			Chart.TitleBox.Label.Color = Color.White
			Chart.TitleBox.Label.GlowColor = Color.Black
			Chart.TitleBox.Background.Color = Color.DeepSkyBlue
			Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background1
			Chart.TitleBox.Position = TitleBoxPosition.Full
			Chart.TitleBox.CornerTopRight = BoxCorner.Round
			Chart.TitleBox.CornerTopLeft = Chart.TitleBox.CornerTopRight

			' Create a layer for the map.
			Dim layer As MapLayer = MapDataEngine.LoadLayer("../../images/MapFiles/primusa.shp")
			layer.DefaultShape.Background.Color = Color.FromArgb(240, 240, 240)

			' Make each shape clickable so it's state abbreviation can be passed to this page.
			layer.DefaultShape.Hotspot.URL = "?id=%STATE_ABBR"

			' Get the state abbreviation from the query string.
			Dim abbr As String = ""
			If Page.Request.QueryString.Count > 0 Then
				abbr = Page.Request.QueryString("id").ToString()
			End If

			' If the abbreviation was passed, add an annotation showing more detail for that state.
			If abbr.Length > 0 Then
				' Create the annotation.
				Dim an As Annotation = New Annotation("Name: <Block>%State_Name<row>Population: <Block>%Population<row>Males - Females<br>%Males - %Females<Chart:Pie values='%Males,%Females'>")
				an.DynamicSize = False
				an.DefaultCorner = BoxCorner.Round
				an.Background.ShadingEffectMode = ShadingEffectMode.Background2
				an.Background.Color = Color.DeepSkyBlue

				' Find the shape to add it to.
				For Each shape As Shape In layer.Shapes
					If CStr(shape("state_abbr")) = abbr Then
						shape.Annotation = an
						Exit For
					End If
				Next shape
			End If

			' Add the states layer to the chart.
			Chart.Mapping.MapLayerCollection.Add(layer)
		End Sub

	</script>

</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" runat="server">
		</dotnet:Chart>
	</div>
</body>
</html>
