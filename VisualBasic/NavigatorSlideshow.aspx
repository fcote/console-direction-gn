<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates creating a slideshow that includes a Silverlight Navigator.

		chart.Size = "600x300"

		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(0, 156, 255) }
		chart.DefaultSeries.Type = SeriesType.Line
		chart.LegendBox.Position = LegendBoxPosition.ChartArea
		chart.YAxis.Scale = Scale.Range

		' The control ID is specified so it can be referenced in javascript.
		chart.Navigator.ControlID = "slPlugin"
		chart.DefaultSeries.Line.Width = 2
		chart.DefaultElement.Marker.Visible = False

		chart.Navigator.Enabled = True
		chart.Navigator.NavigationBar.Visible = False
		chart.Navigator.PreviewAreaNavigationOptons = 0
		chart.DefaultSeries.LegendEntry.Value = "%PercentageChange"

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2010, 1, 1)

		Dim s As Series = New Series("Series 1")
		Dim trend As Double = 100
		Do While dt < New DateTime(2011,1,1)
			Dim e As Element = New Element()
			e.XDateTime = dt
			dt = dt.AddDays(7)
			trend += myR.Next(50)-20
			e.YValue = trend
			s.Elements.Add(e)
		Loop
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
	<style type="text/css">
	div, p,td
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: x-small;
	}
</style>
<script type="text/javascript" src="temp/javascript.js"></script>
		<script type="text/javascript">
			var chartID = "slPlugin";

			// For more information on this code and a client side API reference, see this tutorial: 
			// http://www.dotnetcharting.com/documentation/v6_0/Silverlight_Navigator_API.html

			// Define the slideshow information for each slide.
			var myTextArr = ['[ <strong>1</strong> 2 3 4 ] The first quarter was fairly flat.'
			, '[ 1 <strong>2</strong> 3 4 ] The fourth quarter was not as good as the first.'
			, '[ 1 2 <strong>3</strong> 4 ] The second and third quarters show the highest growth (158%).'
			, '[ 1 2 3 <strong>4</strong> ] April was the best month with 23% growth.'
			, 'The year of 2010 shows a great (313%) overall growth.'];
			var delayArr = [500, 500, 500, 500,500];
			var leftDTArr = [new Date(2010, 0, 0, 18, 0, 0), new Date(2010, 9, 0, 18, 0, 0), new Date(2010, 3, 0, 18, 0, 0), new Date(2010, 3, 0, 18, 0, 0), new Date(2010, 0, 0, 18, 0, 0)];
			var rightDTArr = [new Date(2010, 3, 0, 18, 0, 0), new Date(2010, 12, 0, 18, 0, 0), new Date(2010, 9, 0, 18, 0, 0), new Date(2010, 4, 0, 18, 0, 0), new Date(2010, 12, 0, 18, 0, 0)];

			var slidesPause = 3500;

			function startSlideshow() {
					playSlide(0)
			}

			// Iterates through the available slide arrays and implements delays.
			function playSlide(frame) {
				displaySlide(frame);
				var nextFrame = frame + 1;
				if (nextFrame < myTextArr.length) {
					setTimeout('playSlide(' + nextFrame + ')', delayArr[frame] + slidesPause);
				}
			}

			// Displays the specified slide
			function displaySlide(frame) {
				var control = document.getElementById(chartID);
				control.Content.Chart.NavigateToVisibleRange(leftDTArr[frame], rightDTArr[frame], delayArr[frame])
				var tText = myTextArr[frame];
				setTimeout('displayText("' + tText + '");', delayArr[frame]);
			}

			function displayText(myText) {
				document.getElementById('InfoDiv').innerHTML = myText;
			}
		</script>
</head>
<body>
<table align="center" width="600px">
   <tr>
   <td><a href="Javascript:startSlideshow();">Start Demo</a><br />This sample demonstrates creating a slideshow that includes a navigator chart.<hr />
   </td>
   </tr><tr>
   <td> <div id="InfoDiv" >The year of 2010 shows a great (313%) overall growth.</div>
   </td>
   </tr></table>
	<div align="center">
		<dnc:Chart ID="chart" runat="server" />
	</div>
</body>
</html>
