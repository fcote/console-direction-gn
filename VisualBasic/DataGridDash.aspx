<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Import Namespace="System.Threading" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using MicroCharts inside DataGrids using a GetImage method.

		GridView1.DataSource = getDataSet()
		GridView1.DataBind()
	End Sub

	Protected Function GetChartImageTag(ByVal dataItem As Object) As String
		Dim val As Integer = Int32.Parse(DataBinder.Eval(dataItem, "Population").ToString())

		Dim c As Chart = New Chart()
		c.TempDirectory = "temp"
		c.Margin = "0"
		c.ObjectChart = New dotnetCHARTING.Label("<Chart:Bullet max='10000000' values='" & val & ",5000000,2000000,5000000'> ")

		Return "<img src='" & c.FileManager.SaveImage() & "'>"

	End Function


	Function getDataSet() As DataSet
		Dim strAccessConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.MapPath(ConfigurationSettings.AppSettings("DNCConnectionString"))
		Dim myAccessConn As OleDbConnection = New OleDbConnection(strAccessConn)

		myAccessConn.Open()
		Dim myDataSet As DataSet = New DataSet()
		CType(New OleDbDataAdapter(New OleDbCommand("SELECT City,Population FROM Cities", myAccessConn)), OleDbDataAdapter).Fill(myDataSet, "Cities")
		myAccessConn.Close()

		Return myDataSet
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<form id="form1" runat="server">
	<div align="center">
	</div>
	<asp:GridView ID="GridView1" runat="server">
		<Columns>
			   <asp:TemplateField HeaderText="Chart" >
				<ItemTemplate>
				   <%#GetChartImageTag(Container.DataItem)%>
				</ItemTemplate>
			</asp:TemplateField>     
		</Columns>
	</asp:GridView>
	</form>
</body>
</html>
