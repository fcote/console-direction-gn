<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using MicroCharts as pie labels to give more detail about the data.

		Chart.Type = ChartType.Pies
		Chart.Size = "600x350"
		Chart.Title = "January 2009 Sales"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.Use3D = True

		Chart.DefaultElement.ShowValue = True
		Chart.YAxis.FormatString = "Currency"
		Chart.LegendBox.Visible = False

		' Get the same data as for the first chart.        
		Dim mySC As SeriesCollection = getRandomData()

		' Transpose it to get a sum series (3 bars).
		mySC.Transpose()

		Dim sum As Series = mySC.Calculate("", Calculation.Sum)
		' Color each bar separately.
		sum.Palette = Chart.Palette
		Chart.SeriesCollection.Add(sum)

		' Transpose it back.
		mySC.Transpose()

		' Set the label for each bar manualy getting the daily values from the original mySC collection.
		Dim i As Integer = 0
		For Each s As Series In mySC
			sum(i).SmartLabel.Text = "<Chart:Column color='DarkGray' values='" & s.GetYValueList() & "' width='100'><row><block fStyle='Bold' hAlign='Center'>%Name - %YValue"
			i += 1
		Next s

	End Sub
	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(2)
		Dim SC As SeriesCollection = New SeriesCollection()
		For a As Integer = 1 To 4
			Dim s As Series = New Series("Product " & a.ToString())
			For b As Integer = 1 To 29
				Dim e As Element = New Element()
				e.YValue = myR.Next(50)
				If a = 1 Then
				e.YValue = myR.Next(20)
				End If
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
