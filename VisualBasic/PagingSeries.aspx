<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="vb" Debug="true" Trace="false" EnableSessionState="true" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Paging series</title>
		<script runat="server">
Dim currentPage As Integer = 1
Dim pageSize As Integer=5
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	'set global properties
	Chart.Width = 600
	Chart.Height = 300
	Chart.TempDirectory="temp"
	Chart.Debug=True
	Chart.Title="Item sales"
	Chart.TitleBox.Position = TitleBoxPosition.Right
	Chart.TitleBox.ClearColors()
	Chart.XAxis.Label.Text="Year"



	' Get the page from the query string.
	If Not Request.QueryString("Page") Is Nothing Then
		currentPage = Convert.ToInt32(Request.QueryString("Page"))
	End If

	Chart.PaletteName=CType(currentPage, Palette)
	'set global database properties
	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
	Chart.DateGrouping = TimeInterval.Years

	 'Add a series
	Chart.Series.StartDate = New System.DateTime(2002,1,1,0,0,0)
	Chart.Series.EndDate = New System.DateTime(2002,12,31,23,59,59)
	Chart.Series.SqlStatement= "SELECT OrderDate,Sum(Quantity),Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate,Name ORDER BY Orders.OrderDate"
	Chart.SeriesCollection.Add()

	Chart.DefaultSeries.SplitByLimit= currentPage.ToString() & "x" & pageSize.ToString()
	Chart.DefaultSeries.LimitMode = dotnetCHARTING.LimitMode.Top
	Chart.DefaultSeries.ShowOther=False


	' Create an annotation button          
	Dim an2 As Annotation = New Annotation(" < Previous")
	setAnnotation(an2)

	an2.Position = New Point(49,20)
	an2.CornerTopLeft = BoxCorner.Cut
	an2.CornerTopRight = BoxCorner.Square
	an2.Size = New Size(85,16)
	Chart.Annotations.Add(an2)

	' Activate it
	If currentPage > 1 Then
		an2.URL = "Pagingseries.aspx?page=" & Convert.ToString(currentPage-1)
	Else
		an2.Label.Color = Color.Gray
	End If







AddHandler Chart.PostDataProcessing, AddressOf OnPostDataProcessing

End Sub

Sub setAnnotation(ByVal an As Annotation)
	' Set default annotation properties
	an.DefaultCorner = BoxCorner.Square
	an.Size = New Size(66,16)
	an.Background = New Background(Color.FromArgb(241,250,180), Color.White,90)
	an.Label.Font = New Font("Verdana",7.7f,FontStyle.Bold)
	an.CornerTopRight = BoxCorner.Cut
	an.Padding = 2

End Sub

Sub OnPostDataProcessing(ByVal sender As Object)
	Dim totalPage As Integer
	If Chart.TotalSeries Mod pageSize=0 Then
		totalPage = Chart.TotalSeries/pageSize
	Else
		totalPage = (Chart.TotalSeries/pageSize)+1
	End If

	'Create the next button annotation
	Dim an As Annotation = New Annotation("Next >")
	setAnnotation(an)
	an.Position = New Point(224,20)

	' Activate it
	If totalPage>currentPage Then
		an.URL = "Pagingseries.aspx?page=" & Convert.ToString(currentPage+1)
	Else
		an.Label.Color = Color.Gray
	End If

	' 	
	Dim an2 As Annotation = New Annotation("(Page " & currentPage & " of " & totalPage & ")")


	setAnnotation(an2)
	an2.Size = New Size(90,16)
	an2.Position = New Point(134,20)
	an2.CornerTopRight = BoxCorner.Square
	an2.Label.Font = New Font("Arial",7.7f)
	an2.Label.Color = Color.DarkBlue

	Chart.Annotations.Add(an,an2)

End Sub

		</script>
	<style type="text/css">
	  body.myBody  
	  {
	   border-top-width:0px;
	   border-left-width:0px;

	 }
	 </style>
	</head>
	<body class="myBody">
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" />
		</div>
	</body>
</html>
