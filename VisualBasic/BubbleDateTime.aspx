<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs)

	'set global properties
    Chart.Title="Bubble sample"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.Type = ChartType.Bubble
    Chart.Use3D=true
    Chart.XAxis.Scale = Scale.Time
    Chart.DefaultSeries.DefaultElement.ShowValue=true
    Chart.DefaultSeries.DefaultElement.LabelTemplate="%Xvalue,%Yvalue"
    Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
        
      
   'Adding series programatically
   Chart.Series.Name="Vancouver"
   Chart.Series.Element = new Element("",New System.DateTime(2003,6,2,0,0,0),3,1)
   Chart.Series.Elements.Add()
   Chart.Series.Element = new Element("",New System.DateTime(2003,6,3,0,0,0),6,2)
   Chart.Series.Elements.Add()
   Chart.Series.Name="Vancouver"
   Chart.Series.Element = new Element("",New System.DateTime(2003,6,4,0,0,0),4,1)
   Chart.Series.Elements.Add()
   Chart.Series.Element = new Element("",New System.DateTime(2003,6,5,0,0,0),2,2)
   Chart.Series.Elements.Add()
   Chart.SeriesCollection.Add()
  
  
   
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Bubble sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
