<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Aixs Caps</title>
		<script runat="server">



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Scatter
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.DefaultSeries.Type = SeriesType.Line
   
   
   ' This sample will demonstrate axis line caps.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim mySC As SeriesCollection = getRandomData()
   
   ' We'll make the axis line a translucent black color and add some width so the caps can be seen better.
   Chart.DefaultAxis.Line.Color = Color.FromArgb(255, 0, 0, 0)
   Chart.DefaultAxis.Line.Width = 5
   
   
   ' The axis lines are drawn from origin. If the origin is in the middle of the chart, two axis lines are drawn.
   ' Axis lines can be marked with caps like so:
   Chart.DefaultAxis.Line.StartCap = LineCap.RoundAnchor
   Chart.DefaultAxis.Line.EndCap = LineCap.ArrowAnchor
   
   ' This will mark all the lines in the same manner.
   
   ' Add the some random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 



Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         
         e.YValue = - 25 + myR.Next(50)
         e.XValue = - 25 + myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
