<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates styling the dynamic legend entries in the chart area.

		chart.Size = "600x350"

		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		chart.DefaultSeries.Type = SeriesType.Line

		chart.YAxis.Scale = Scale.Range

		chart.Navigator.Enabled = True

		chart.LegendBox.Position = LegendBoxPosition.ChartArea
		' Specify legend entry text style.
		chart.DefaultSeries.LegendEntry.LabelStyle.Font = New Font("Verdana", 9, FontStyle.Bold)
		chart.DefaultSeries.LegendEntry.LabelStyle.Color = Color.DarkBlue

		' Text representing ranges.
		chart.DefaultSeries.LegendEntry.Value = "%Sum Max(%High)"

		' Text representing a single value.
		chart.DefaultElement.SmartLabel.Text = "%YValue (%PercentOfSeries)"

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2010, 1, 1)

		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 14
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="chart" runat="server" />
	</div>
</body>
</html>
