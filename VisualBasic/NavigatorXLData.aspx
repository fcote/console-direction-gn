<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates the navigator with a very large dataSet.

		Chart.Size = "600x350"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.DefaultSeries.Type = SeriesType.Line
		Chart.DefaultElement.Marker.Visible = False

		Chart.LegendBox.Position = LegendBoxPosition.ChartArea
		Chart.ChartArea.Padding = 6

		Chart.YAxis.Scale = Scale.Range

		' Enable the Navigator
		Chart.Navigator.Enabled = True

		' Specify the start date range.
		Chart.XAxis.Viewport.ScaleRange = New ScaleRange(New DateTime(DateTime.Now.Year, 1, 1), DateTime.Now)

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

		Chart.ChartArea.Label.Text = "<br>This chart contains over " & mySC(0).Elements.Count & " elements representing daily values since " & mySC(0).Elements(0).XDateTime.Year

	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		'DateTime dt = new DateTime(1950, 1, 1);
		Dim dt As DateTime = New DateTime(1915, 1, 1)
		Dim yVal As Double = 50
		Dim s As Series = New Series("Series 1")
		Do While dt < DateTime.Now
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			Dim change As Double = (-5 + myR.Next(10))
			If yVal < 0 AndAlso change < 0 Then
			change = -change
			End If
			yVal = yVal + change
			e.YValue = yVal
			s.Elements.Add(e)
		Loop
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
