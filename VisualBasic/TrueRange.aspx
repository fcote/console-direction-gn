<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Financial TrueRange Sample</title>
		<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of the TrueRange and DirectionalMotion indicators
   ' First we declare a chart of type FinancialChart	
   FinancialChart.Title = "Financial Chart"
   FinancialChart.TempDirectory = "temp"
   FinancialChart.Debug = True
   FinancialChart.ShadingEffect = True
   FinancialChart.LegendBox.Template = "%icon %name"
   FinancialChart.Size = "800X600"
   FinancialChart.XAxis.Scale = Scale.Time
   
   FinancialChart.XAxis.FormatString = "MMM d"
   FinancialChart.XAxis.TimeInterval = TimeInterval.Day
   ' For financial indicators the time scale is inverted (i.e. the first element of the series is the newest)
   FinancialChart.XAxis.InvertScale = True
   FinancialChart.YAxis.Label.Text = "Price (USD)"
   FinancialChart.YAxis.FormatString = "currency"
   FinancialChart.YAxis.ScaleRange.ValueLow = 24
   FinancialChart.YAxis.ScaleRange.ValueHigh = 34
   
   ' Here we load data samples from the FinancialDELL table from within chartsample.mdb
   Dim priceDataEngine As New DataEngine()
   priceDataEngine.ChartType = ChartType.Financial
   priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   priceDataEngine.DateGrouping = TimeInterval.Day
   priceDataEngine.StartDate = New DateTime(2001, 11, 1)
   priceDataEngine.EndDate = New DateTime(2001, 12, 31)
   priceDataEngine.SqlStatement = "SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate "
   priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice"
   
   Dim sc As SeriesCollection = priceDataEngine.GetSeries()
   Dim prices As Series = Nothing
   If sc.Count > 0 Then
      prices = sc(0)
   Else
      Return
   End If 
   prices.DefaultElement.ToolTip = "L:%Low-H:%High"
   prices.DefaultElement.SmartLabel.Font = New Font("Arial", 6)
   prices.DefaultElement.SmartLabel.Text = "O:%Open-C:%Close"
   prices.Type = SeriesTypeFinancial.CandleStick
   
   Dim cp As New CalendarPattern(TimeInterval.Day, TimeInterval.Week, "1000001")
   cp.AdjustmentUnit = TimeInterval.Day
   prices.Trim(cp, ElementValue.XDateTime)
   prices.Name = "Prices"
   FinancialChart.SeriesCollection.Add(prices)
   
   FinancialChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   FinancialChart.DefaultSeries.Type = SeriesType.Spline
   
   ' Create a new chart area for DM and TrueRange indicators.
   Dim dmChartArea As New ChartArea("Directional Motion")
   dmChartArea.HeightPercentage = 20
   dmChartArea.YAxis = New Axis()
   FinancialChart.ExtraChartAreas.Add(dmChartArea)
   
   ' Create a new chart area for DM+ and DM- indicators.
   Dim dmMovementChartArea As New ChartArea("Directional Movement")
   dmMovementChartArea.HeightPercentage = 20
   dmMovementChartArea.YAxis = New Axis()
   dmMovementChartArea.YAxis.ScaleRange.ValueLow = 0.0
   dmMovementChartArea.YAxis.ScaleRange.ValueHigh = 0.8
   
   FinancialChart.ExtraChartAreas.Add(dmMovementChartArea)
   
   Dim trChartArea As New ChartArea("True Range")
   trChartArea.HeightPercentage = 20
   trChartArea.YAxis = New Axis()
   trChartArea.YAxis.ScaleRange.ValueLow = 0.0
   trChartArea.YAxis.ScaleRange.ValueHigh = 2.5
   FinancialChart.ExtraChartAreas.Add(trChartArea)
   
   
   ' Here we display the TrueRange
   Dim trueRange As Series = dotnetCHARTING.FinancialEngine.TrueRange(prices)
   trueRange.Type = SeriesType.Spline
   trueRange.DefaultElement.Color = Color.FromArgb(0, 255, 0)
   If trueRange.Elements.Count > 0 Then
      trChartArea.SeriesCollection.Add(trueRange)
   Else
      Response.Write("The series trueRange is empty")
   End If 
   
   ' Here we display the DM
   Dim sDM As Series = dotnetCHARTING.FinancialEngine.DirectionalMotion(prices)
   sDM.Type = SeriesType.Spline
   sDM.DefaultElement.Color = Color.FromArgb(0, 0, 255)
   If sDM.Elements.Count > 0 Then
      dmChartArea.SeriesCollection.Add(sDM)
   Else
      Response.Write("The series sDM is empty")
   End If 
   
   ' Here we display the DM+
   Dim sDMPlus As Series = dotnetCHARTING.FinancialEngine.PlusDirectionalMovement(prices)
   sDMPlus.Type = SeriesType.Spline
   sDMPlus.DefaultElement.Color = Color.FromArgb(0, 255, 255)
   If sDMPlus.Elements.Count > 0 Then
      dmMovementChartArea.SeriesCollection.Add(sDMPlus)
   Else
      Response.Write("The series sDMPlus is empty")
   End If 
   
   ' Here we display the DM-
   Dim sDMinus As Series = dotnetCHARTING.FinancialEngine.MinusDirectionalMovement(prices)
   sDMinus.Type = SeriesType.Spline
   sDMinus.DefaultElement.Color = Color.FromArgb(255, 0, 255)
   If sDMinus.Elements.Count > 0 Then
      dmMovementChartArea.SeriesCollection.Add(sDMinus)
   Else
      Response.Write("The series sDMinus is empty")
   End If 
End Sub 'Page_Load

</script>
	</head>
	<body>
		<center>
			<dotnet:Chart id="FinancialChart" runat="server"/>
		</center>
	</body>
</html>
