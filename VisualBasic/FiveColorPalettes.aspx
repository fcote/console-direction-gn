<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates the five color palettes.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.Mentor = False
		Chart.Type = ChartType.Pies
		Chart.Size = "1450x2050"
		Chart.LegendBox.Visible = False
		Chart.ChartArea.ClearColors()
		Chart.ChartArea.Line.Color = Color.Black

		Chart.XAxis.Scale = Scale.FullStacked
		Chart.YAxis.SpacingPercentage = 60

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

		For i As Integer = 0 To 45
			Chart.PaletteName = CType(i, Palette) + 20
			mySC(i)(0).Color = Chart.Palette(0)
			mySC(i).Name = Chart.PaletteName.ToString()
			mySC(i)(1).Color = Chart.Palette(1)
			mySC(i)(2).Color = Chart.Palette(2)
			mySC(i)(3).Color = Chart.Palette(3)
			mySC(i)(4).Color = Chart.Palette(4)
		Next i
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 46
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 5
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = 5
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
