<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   ' This sample will demonstrates using a custom legend entry with additional custom columns.
   ' First we create the new box.
   Dim newLegend As New LegendBox()
   
   ' Now we'll set the columns for the box to use. The first 3 columns refer to legend entry properties
   ' LegendEntry.Name, LegendEntry.Marker, and LegendEntry.Value. The 4th column is a custom attribute column.
   newLegend.Template = "%Name %Icon %Value %customToken"
   
   ' instantiate the legend entries with a name, value, and background color (LegendEntry.Marker will use this background).
   Dim entry1 As New LegendEntry("Entry 1", "No Value", New Background(Color.Orange))
   Dim entry2 As New LegendEntry("Entry 2", "No Value", New Background(Color.Purple))
  ' Dim entry3 As New LegendEntry("Entry 3", "No Value", New Background(Color.LightBlue))
   
   ' Now we'll add custom attributes to populate the customToken column of the legend box by adding custom attributes.
   entry1.CustomAttributes.Add("customToken", "custom string 1")
   entry2.CustomAttributes.Add("customToken", "custom string 2")
   
  newLegend.ExtraEntries.Add(entry1)
   newLegend.ExtraEntries.Add(entry2)

   
   ' Set an orientation for the custom legend.
   newLegend.Orientation = dotnetCHARTING.Orientation.Top
   ' The the new legend to the chart.
   Chart.ExtraLegendBoxes.Add(newLegend)
   
   ' Add some data.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " & b
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="519px" Height="400px"></dotnet:Chart>
		</div>
	</body>
</html>
