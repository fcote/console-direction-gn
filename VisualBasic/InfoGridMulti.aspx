<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using multiple DB driven infoGrids inside annotations.

		Chart.Size = "500x380"
		Chart.TempDirectory = "temp"
		Chart.Debug = True

		Dim mySC As SeriesCollection = getOrders()

		Dim an As Annotation = New Annotation(getGridText(mySC(0)))
		an.HeaderLabel.Text = "Sales by User"
		an.Position = New Point(10, 10)

		Dim an2 As Annotation = New Annotation(getGridTextMontly(getItemsMonthly()(0)))
		an2.Position = New Point(200, 10)
		an2.HeaderLabel.Text = "Monthly Sales"

		setAnStyling(an)
		setAnStyling(an2)

		Chart.Annotations.Add(an, an2)

	End Sub

	' Settings common to both annotations.
	Sub setAnStyling(ByVal an As Annotation)
		an.Padding = 10
		an.DynamicSize = False
		an.HeaderBackground.Color = Color.LightSteelBlue
		an.Label.Font = New Font("Arial", 8)
		an.Background.Color = Color.White
		an.HeaderLabel.Font = New Font("Arial", 11, FontStyle.Bold)
		an.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Seven
		an.DefaultCorner = BoxCorner.Round
	End Sub


	Function getGridText(ByVal s As Series) As String
		Dim sb As StringBuilder = New StringBuilder()
		sb.Append("<block fStyle='Bold' vAlign='Center'>User<block fStyle='Bold' vAlign='Center'>Qty.<Chart:Scale min='0' max='600'> <hr>")

		Dim i As Integer = 0
		For Each e As Element In s.Elements
			sb.Append("<block vAlign='Center'>" & e.Name & "<block hAlign='Center' vAlign='Center'>" & e.YValue & "<Chart:Bar  shading='7'  min='0' max='600' value='" & e.YValue & "'>")
            If i < s.Elements.Count - 1 Then
                sb.Append("<hr>")
            End If
            i = i + 1
		Next e
		Return sb.ToString()
	End Function

	Function getGridTextMontly(ByVal s As Series) As String
		Dim sb As StringBuilder = New StringBuilder()
		sb.Append("<block fStyle='Bold' vAlign='Center'>Month<block fStyle='Bold' vAlign='Center'>Qty.<Chart:Scale min='0' max='6000'><block> Daily<hr>")

		Dim i As Integer = 0
		For Each e As Element In s.Elements
			Dim daily As String = getMonthValues(i + 1)
			sb.Append("<block vAlign='Center'>" & e.Name & "<block hAlign='Center' vAlign='Center'>" & e.YValue & "<Chart:Bar shading='7' min='0' max='6000' value='" & e.YValue & "'><Chart:Sparkline values='" & daily & "' colors='Gray,Red'>")
            If i < s.Elements.Count - 1 Then
                sb.Append("<hr>")
            End If
            i = i + 1
		Next e
		Return sb.ToString()
	End Function

	Function getOrders() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.SqlStatement = "SELECT Name,Sum(1) FROM Orders GROUP BY Name ORDER BY Name"
		Return de.GetSeries()
	End Function

	Function getItemsMonthly() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = New System.DateTime(2002, 1, 1, 0, 0, 0)
		de.EndDate = New System.DateTime(2002, 12, 31, 23, 59, 59)
		de.DateGrouping = TimeInterval.Year
		de.SqlStatement = "SELECT OrderDate,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
		Return de.GetSeries()
	End Function

	Function getMonthValues(ByVal month As Integer) As String
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = New System.DateTime(2002, month, 1, 0, 0, 0)
		de.EndDate = de.StartDate.AddMonths(1)
		de.DateGrouping = TimeInterval.Month
		de.SqlStatement = "SELECT OrderDate,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"

		Dim sc As SeriesCollection = de.GetSeries()
		Return sc(0).GetYValueList()
	End Function


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
