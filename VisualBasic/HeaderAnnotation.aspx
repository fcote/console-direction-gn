<%@ Page Language="vb"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Header Annotation</title>
		<script runat="server">


Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	Chart.Type = ChartType.Combo 'Horizontal;
	Chart.Width = 600
	Chart.Height = 350
	Chart.TempDirectory = "temp"
	Chart.Debug = True


	' This sample will demonstrate how to use annotations to create a custom company header on your charts.

	' First we get our data, if you would like to get the data from a database you need to use
	' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	Dim mySC As SeriesCollection = getRandomData()


	' Our company logo is 40 pixels high so we'll start by making some space at the top of the chart.
	Chart.MarginTop = 40

	' Next we place the logo on the chart using an annotation.
	Dim a As Annotation = New Annotation(New Background("../../images/dncLogo.jpg"))
	a.Shadow.Color = Color.Empty
	a.Position = New Point(27,0)
	Chart.Annotations.Add(a)


	' Now we'll make an annotation for the text part.
	Dim a2 As Annotation = New Annotation("Image and Text Header. Copyright .netCHARTING. Click for details.")

	' We'll add a link so then this text is clicked it takes us to the site.
	a2.URL = "http://www.dotnetcharting.com"

	' The logo size is 103x40. We'll place the text on the right side and center the text vertically with the logo
	' my moving it down 15 pixels.
	a2.Position = New Point(130,15)

	' Make sure the annotation doesn't create a rectangle for the text and wrap it.
	a2.DynamicSize = False

	' In order for the annotation not to draw the background, outline and so on... We'll use a shortcut to clear the colors.
	a2.ClearColors()

	' Add the second annotation.
	Chart.Annotations.Add(a2)


	' Add some random data.
	Chart.SeriesCollection.Add(mySC)


End Sub


Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random()
	For a As Integer = 1 To 4
		Dim s As Series = New Series()
		s.Name = "Series " & a
		For b As Integer = 1 To 4
			Dim e As Element = New Element()
			e.Name = "Element " & b
			'e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	Return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
