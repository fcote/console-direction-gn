<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates plotting a mathematical function. In this case, y = sin(x).
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Palette = New Color() {Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255)}
   
   Chart.Type = ChartType.Combo
   Chart.Size = "600x350"
   Chart.Title = ".netCHARTING Sample"
   
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Help File > Getting Started > Data Tutorials
   ' - DataEngine Class in the help file	
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 


Function mathFunction(x As Double) As Double
   Return Math.Sin(x)
End Function 'mathFunction


Function getRandomData() As SeriesCollection
   Dim s As New Series("Series ")
   Dim b As Integer
   For b = 1 To 99
      
      Dim e As New Element()
      e.YValue = mathFunction((b / 10.0))
      e.XValue = b / 10.0
      s.Elements.Add(e)
   Next b
   Return New SeriesCollection(s)
End Function 'getRandomData

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
