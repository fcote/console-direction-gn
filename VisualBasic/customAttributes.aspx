<%@ Page Language="VB" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.Title = " Employees' Report"
   Chart.ChartArea.Label.Text = "MouseOver John's column to see a tooltip."
   Chart.XAxis.Label.Text = "Employee's Name"
   Chart.YAxis.Label.Text = "Annual Salary"
   Chart.YAxis.FormatString = "currency"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
        Chart.Use3D = True
        Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   Chart.DefaultSeries.DefaultElement.URL = "customAttributeTarget.aspx?name=%name&salary=%yvalue&id=%EmployeeID&department=%Department&email=%email&phone=%PhoneNumber"
   
   'Create a series  
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   de.SqlStatement = "SELECT * FROM Employees"
   'Set predefined data fields and some custom fields as attributes for x-Axis. (Employee�s name)
   de.DataFields = "xAxis=name,yAxis=salary,id=EmployeeID,department=Department,email,phone=PhoneNumber"
   Dim sc As SeriesCollection = de.GetSeries()
   If sc.Count > 0 Then
      Dim ser1 As Series = sc(0)
      ser1.Name = "Employees"
      Dim i As Integer
      For i = 0 To 2
         Select Case i
            Case 0
               'set employees' information to annotation 
               ' we can use all the element tokens in the text, toolTip and even url of this annotation.
               Dim a As New Annotation("Employee No: %EmployeeID" + ControlChars.Lf + "Department: %Department" + ControlChars.Lf + "Email: %email" + ControlChars.Lf + "Phone: %PhoneNumber")
               a.Background.Bevel = True
               a.Background.Color = Color.FromArgb(150, 180, 180, 220)
               ' Assign the annotation to an element in the sc collection.
               ser1.Elements(0).Annotation = a
            Case 2
               'set employees' information to Tooltip 
               ser1.Elements(2).ToolTip = "Employee No: %EmployeeID" + ControlChars.Lf + "Department: %Department" + ControlChars.Lf + "Email: %email" + ControlChars.Lf + "Phone: %PhoneNumber"
            Case Else
         End Select
      Next i
   End If
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Custom Attributes</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart" runat="server"/>

</div>
</body>
</html>
