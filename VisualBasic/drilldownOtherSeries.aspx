<%@ Page Language="vb" Debug="true" Trace="false" EnableSessionState="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	 If (Not IsPostBack) Then
		DropDownLimitMode.Items.Add("Top")
		DropDownLimitMode.Items.Add("Bottom")
		DropDownLimitMode.Items.Add("ExcludeTop")
		DropDownLimitMode.Items.Add("ExcludeBottom")
		If Not Request.QueryString("limitMode") Is Nothing Then
		  DropDownLimitMode.SelectedIndex = CInt(Fix(System.Enum.Parse(GetType(dotnetCHARTING.LimitMode),Request.QueryString("limitMode"),True)))
		End If



	 End If
	'set global properties
	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
	Chart.Title="Item sales"
	Chart.XAxis.Label.Text="Years"
	Chart.TempDirectory="temp"
	Chart.Debug=True
	Chart.DefaultSeries.DefaultElement.ToolTip="%yvalue"

	Chart.DateGrouping = TimeInterval.Years
	'"OtherAll", shows all other elements in the second level.
	'"Other" shows the next number of elements has been set in limit propertes.
	   Chart.DrillDownChain="other"


	'Add a series

	Chart.Series.StartDate = New System.DateTime(2002,1,1,0,0,0)
	Chart.Series.EndDate = New System.DateTime(2002,12,31,23,59,59)
	Chart.Series.SqlStatement= "SELECT OrderDate,Sum(Quantity),Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate,Name ORDER BY Orders.OrderDate"
	Chart.SeriesCollection.Add()


			Chart.DefaultSeries.SplitByLimit=TextBoxLimit.Text
			Chart.DefaultSeries.LimitMode = CType(System.Enum.Parse(GetType(dotnetCHARTING.LimitMode),DropDownLimitMode.SelectedItem.Value,True), dotnetCHARTING.LimitMode)
			Chart.DefaultSeries.ShowOther=True
			Chart.DefaultSeries.OtherElementText ="Other"




End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Drill down on other with SplitByLimit</title>
<style type="text/css">
	 body.myBody  
	 {
	 border-top-width:0px;
	  border-left-width:0px;

	 }
	 img.myImage  
	 {
	 border:0px;

	 }
	 td.myTD1  
	 {
	  width:"1%";
		border:1px; 
		margin:0;
	 }
	 td.myTD2  
	 {     
	  width:"99%";
	  background:"#BFC0DB";
	  border-collapse:collapse;


	 }
	 table.myTable
	 {
	  border:1px; 
	  border-style:solid;
	  border-spacing:0;
	  border-color:"#111111";
	  border-collapse:collapse;

	 }

	</style>
</head>
<body class="myBody">
	<form runat="server" action="drilldownOtherSeries.aspx">
		<div style="text-align:center">
			<table class="myTable" id="AutoNumber1">

				<tr>
	<td class="myTD1">
<img class="myImage" alt="dotnetChartingImage" src="../../images/dotnetCharting.gif" width="230" height="94"/></td>
	<td class="myTD2">Limit: 

 <ASP:DropDownList id="DropDownLimitMode" runat="server">
	</ASP:DropDownList>
	<ASP:TextBox id="TextBoxLimit" runat="server"/>
	 <asp:Button id="ButtonSet" runat="server" Text="Set">
	</asp:Button>
 </td>
		  </tr>
			</table>
			</div>
			</form>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>

</div>
</body>
</html>
