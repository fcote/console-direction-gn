<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>

<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<html>
<head>
	<title>Gallery Sample (Time Gantt Chart in 2D)</title>

	<script runat="server">

		' This sample demonstrates how element values can be inserted into the legend box beside the series.

		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

			Chart.Type = ChartType.Combo 'Horizontal;
			Chart.Width = 600
			Chart.Height = 350
			Chart.TempDirectory = "temp"
			Chart.Debug = True
			Chart.DefaultSeries.LegendEntry.Visible = False
			Dim sc As SeriesCollection = getRandomData(1)
			' Add the random data.
			Chart.SeriesCollection.Add(sc)

			For i As Integer = 0 To sc.Count - 1
				Chart.LegendBox.ExtraEntries.Add(getEntry(sc(i), i))
			Next i
			Chart.LegendBox.Template = "%Name%Icon%Value"

		End Sub

		''' <summary>
		''' This method processes a populated series legend entry and inserts the element values into the LegendEntry 'Value' property.
		''' </summary>
		Function getEntry(ByVal s As Series, ByVal index As Integer) As LegendEntry
			Dim le As LegendEntry = New LegendEntry()
			le.Name = s.Name
			le.Value = ""
			le.SeriesType = SeriesType.Column
			le.Background.Color = Chart.Palette(index)
			For i As Integer = 0 To s.Elements.Count - 1
				Dim el As Element = s.Elements(i)
				le.Value += el.YValue.ToString()
				If i < s.Elements.Count - 1 Then
				le.Value &= ", "
				End If
			Next i

			Return le
		End Function

		Function getRandomData(ByVal seed As Integer) As SeriesCollection
			Dim SC As SeriesCollection = New SeriesCollection()
			Dim myR As Random = New Random(seed)
			For a As Integer = 0 To 3
				Dim s As Series = New Series()
				s.Name = "Series " & a
				For b As Integer = 0 To 3
					Dim e As Element = New Element()
					e.Name = "Element " & b
					'e.YValue = -25 + myR.Next(50);
					e.YValue = myR.Next(1000)
					s.Elements.Add(e)
				Next b
				SC.Add(s)
			Next a
			Return SC
		End Function

		Function getLiveData() As SeriesCollection
			Dim de As DataEngine = New DataEngine("ConnectionString goes here")
			de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
			de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
			Return de.GetSeries()
		End Function

	</script>

</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
