<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using Series.GetInterpolated_Value() to find midpoints between two elements.
		' FullScreen
		Dim Chart As Chart = New Chart()
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.LegendBox.Visible = False
		Chart.DefaultSeries.Type = SeriesType.Line
		Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month)

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

		Dim bp As Bitmap = Chart.GetChartBitmap()

		' Process chart clicks and create zoom chart area.
		If Page.Request.QueryString.Count > 0 Then
			Dim query As String = Page.Request.QueryString(0)

			' From the query string, get the values of the axes where clicked.
			Dim xval As Object = Chart.XAxis.GetValueAtX(query)
			Dim yval As Object = Chart.YAxis.GetValueAtY(query)

			If Not xval Is Nothing Then
				' Here the interpolated y value is calculated.
				Dim yValue As Object = mySC(0).GetInterpolatedYValue(CDate(xval))

				If Not yValue Is Nothing Then
					Dim s2 As Series = New Series()
					Dim el As Element = New Element()
					el.XDateTime = CDate(xval)

					If TypeOf yValue Is DateTime Then
					el.YDateTime = CDate(yValue)
					Else
					el.YValue = CDbl(yValue)
					End If
					el.Marker.Color = Color.Red
					s2.Elements.Add(el)
					Chart.SeriesCollection.Add(s2)
					Dim an As Annotation = New Annotation()
					an.Padding = 6
					an.Label.Text = "%YValue"
					an.HeaderLabel.Text = "<%XValue,d>"
					an.HeaderLabel.Color = Color.White
					an.HeaderLabel.Font = New Font("arial", 8, FontStyle.Bold)
					an.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Two
					an.HeaderBackground.Color = Color.Red
					an.Background.Color = Color.White
					'an.Orientation = dotnetCHARTING.Orientation.BottomRight;
					el.Annotation = an
					xval = el.XDateTime
				End If

				'// Add axis markers at click positions.
				If Not xval Is Nothing Then
					Chart.XAxis.Markers.Add(New AxisMarker("", Color.Orange, CDate(xval)))
				End If
			End If

			' Generate the chart once more to show the axis markers that were added above.
			bp = Chart.GetChartBitmap()
		End If

		Dim fileName As String = Chart.FileManager.SaveImage(bp)
		imageLabel.Text = "<img border = 0 src=""" & fileName & """ ISMAP>"
	End Sub


	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim startVal As Integer = 50
		Dim dt As DateTime = DateTime.Now
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 9
				Dim e As Element = New Element()
				dt = dt.AddDays(10)
				e.XDateTime = dt
				'e.YDateTime = DateTime.Now.AddDays(myR.Next(10));
				startVal = startVal + (-7+myR.Next(20))
				e.YValue = startVal
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
			<a href="">
			<asp:Label ID="imageLabel" Runat="server" /></a>

	</div>
</body>
</html>
