<%@ Page Language="VB" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs )

	'set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.XAxis.Label.Text="Customers"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.Use3D=true
    
    
    'Set TitleBox
    Chart.TitleBox.Background = new Background(System.Drawing.Color.SkyBlue)
    Chart.TitleBox.CornerBottomLeft = BoxCorner.Square
    Chart.TitleBox.CornerBottomRight = BoxCorner.Square
	Chart.TitleBox.CornerTopLeft = BoxCorner.Cut
	Chart.TitleBox.CornerTopRight = BoxCorner.Cut
    Chart.TitleBox.IconPath="../../Images/icon.gif"
    Chart.TitleBox.Label =  new dotnetCHARTING.Label("Orders By Customers",new Font("Arial",10),Color.DarkSlateBlue)
    Chart.TitleBox.Line = new Line(Color.Blue)
    Chart.TitleBox.Position = TitleBoxPosition.Center
    Chart.TitleBox.Shadow.Color = Color.LightBlue
    Chart.TitleBox.Shadow.Depth=40
   	   	
    'Add a series
    Chart.Series.Name="Orders"
    Chart.Series.SqlStatement= "SELECT Name,Sum(1) FROM Orders GROUP BY Name ORDER BY Sum(1) DESC"
    Chart.SeriesCollection.Add()
    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Orders Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
