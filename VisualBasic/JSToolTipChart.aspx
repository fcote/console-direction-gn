<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>JavaScript ToolTip Chart</title>

	<script runat="server">

		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
			' This sample demonstrates using a dynamic chart inside tooltips.

			Chart.Type = ChartType.Combo
			Chart.Width = 500
			Chart.Height = 250
			Chart.TempDirectory = "temp"
			Chart.Debug = True
			Chart.Title = "Hover your mouse over a column to reveal the months breakdown."
	        Chart.ShowDateInTitle = False
	        Chart.DefaultElement.DefaultSubValue.Visible = False

			'Disable the legend box, we will use the tooltips to view information typically shown there.
			Chart.LegendBox.Visible = False

			'Add a shading effect to give the columns some more visual polish.
			Chart.ShadingEffect = True
			Chart.ShadingEffectMode = ShadingEffectMode.Three

			'This is the bulk of the work forthis sample
	        Chart.DefaultElement.ToolTip = "%Name %YValue<row><Chart:Pie values='%SubValueList' Colors='Red,Yellow,Green' Shading='4' width='100' height='100'>"
			
			' y axis label.
			Chart.YAxis.Label.Text = "GDP (Millions)"
			Chart.YAxis.SmartScaleBreak = True

			Chart.SeriesCollection.Add(getLiveData())

		End Sub
		Function getLiveData() As SeriesCollection
			Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
	        de.ChartObject = Chart
	        de.StartDate = New System.DateTime(2002, 1, 1, 0, 0, 0)
	        de.EndDate = New System.DateTime(2002, 12, 31, 23, 59, 59)
	        de.SqlStatement = "SELECT OrderDate,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
	        de.DateGrouping = TimeInterval.Quarter
	        de.SubValueDateGrouping = TimeInterval.Months
	        Return de.GetSeries()
		End Function
	</script>

</head>
<body>
	<br/>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" Width="568px" Height="344px">
		</dotnet:Chart>
	</div>
</body>
</html>
