<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + CStr(a)
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + CStr(b)
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample demonstrates how you can create dynamic axis markers based on the data.
   ' We'll set the default type to line so it can be viewed better.
   Chart.DefaultSeries.Type = SeriesType.Line
   
   
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx.
   Dim sc As SeriesCollection = getRandomData()
   
   ' We now calculate and add the axis markers to the y axis.
   ' First we'll get single series calculations the highs and lows from the series collection.
   Dim highS As Series = sc.Calculate("", Calculation.Maximum)
   Dim lowS As Series = sc.Calculate("", Calculation.Minimum)
   
   
   'Now from each series we can calculate high and low values as single elements.
   ' Because series.Calculate returns an element we use the yValue of the result
   Dim high As Double = highS.Calculate("", Calculation.Maximum).YValue
   Dim low As Double = lowS.Calculate("", Calculation.Minimum).YValue
   
   
   ' Add the markers to the y axis.
   Chart.YAxis.Markers.Add(New AxisMarker("Low", New Line(Color.Red, 4), low))
   Chart.YAxis.Markers.Add(New AxisMarker("High", New Line(Color.Green, 4), high))
   
   
   ' This code will add a range marker
   Chart.YAxis.Markers.Add(New AxisMarker("Range", New Background(Color.Orange), low, high))
   Chart.YAxis.Markers(2).Label.Alignment = StringAlignment.Near
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load 
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Dynamic Axis Marker</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
