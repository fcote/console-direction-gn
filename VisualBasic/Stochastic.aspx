<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Financial Stochastic Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of the K% Stochastic and D% Stochastic
   Dim connection As String = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + Server.MapPath("../../database/chartsample.mdb")
   
   ' First we declare a chart of type FinancialChart	
   ' The Financial Chart
   FinancialChart.Title = "Financial Chart"
   FinancialChart.TempDirectory = "temp"
   FinancialChart.Debug = True
   FinancialChart.ShadingEffect = True
   FinancialChart.LegendBox.Template = "%icon %name"
   FinancialChart.Size = "800X600"
   FinancialChart.XAxis.Scale = Scale.Time
   FinancialChart.XAxis.FormatString = "MMM d"
   FinancialChart.XAxis.TimeInterval = TimeInterval.Day
   FinancialChart.YAxis.Label.Text = "Price (USD)"
   FinancialChart.YAxis.FormatString = "currency"
   FinancialChart.YAxis.ScaleRange.ValueLow = 11
   FinancialChart.PaletteName=Palette.Three
   
   ' Here we load data samples from the FinancialCompany table from within chartsample.mdb
   Dim priceDataEngine As New DataEngine()
   priceDataEngine.ChartType = ChartType.Financial
   priceDataEngine.ConnectionString = connection
   priceDataEngine.DateGrouping = TimeInterval.Day
   priceDataEngine.StartDate = New DateTime(2001, 8, 1)
   priceDataEngine.EndDate = New DateTime(2001, 9, 30)
   priceDataEngine.SqlStatement = "SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate "
   priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice"
   
   Dim sc As SeriesCollection = priceDataEngine.GetSeries()
   Dim prices As Series = Nothing
   If sc.Count > 0 Then
      prices = sc(0)
   Else
      Return
   End If
   'prices.DefaultElement.ShowValue = True
   prices.DefaultElement.ToolTip = "L:%Low-H:%High"
   prices.DefaultElement.SmartLabel.Font = New Font("Arial", 6)
   prices.DefaultElement.SmartLabel.Text = "O:%Open-C:%Close"
   prices.Type = SeriesTypeFinancial.CandleStick
   
   Dim cp As New CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0000001")
   cp.AdjustmentUnit = TimeInterval.Day
   prices.Trim(cp, ElementValue.XDateTime)
   prices.Name = "Prices"
   FinancialChart.SeriesCollection.Add(prices)
   
   ' Create the second chart area 
   Dim volumeChartArea As New ChartArea()
   volumeChartArea.Label.Text = "Stock Volume"
   volumeChartArea.YAxis.Label.Text = "Volumes"
   volumeChartArea.Series.Name = "Stock Volume"
   volumeChartArea.HeightPercentage = 17
   volumeChartArea.Series.DefaultElement.ToolTip = "%YValue"
   FinancialChart.ExtraChartAreas.Add(volumeChartArea)
   
   ' Add a volume series to the chart area
   Dim volumeDataEngine As New DataEngine()
   volumeDataEngine.ConnectionString = connection
   volumeDataEngine.DateGrouping = TimeInterval.Days
   volumeDataEngine.StartDate = New DateTime(2001, 8, 1)
   volumeDataEngine.EndDate = New DateTime(2001, 9, 30)
   volumeDataEngine.SqlStatement = "SELECT TransDate,volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate"
   volumeDataEngine.DataFields = "xAxis=TransDate,yAxis=Volume"
   
   Dim volumes As Series = volumeDataEngine.GetSeries()(0)
   volumes.Trim(cp, ElementValue.XDateTime)
   volumes.Name = "Volumes"
   volumes.Type = SeriesType.Bar
   volumeChartArea.SeriesCollection.Add(volumes)
   
   FinancialChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   FinancialChart.DefaultSeries.Type = SeriesType.Spline
   
   ' Here we display the financial exponentially moving average for the prices series over five days
   FinancialChart.SeriesCollection.Add(FinancialEngine.ExponentiallyWeightedMovingAverage(prices, ElementValue.High, 0.2, 5))
   ' Here we display the statistical exponentially moving average for the Volumes Series over five days
   volumeChartArea.SeriesCollection.Add(StatisticalEngine.ExponentiallyWeightedMovingAverage(volumes, 0.2, 5))
   
   ' Create a new chart area for KFastStochastic and DStochastic.
   Dim stochasticChartArea As New ChartArea("Stochastic")
   stochasticChartArea.HeightPercentage = 20
   stochasticChartArea.YAxis = New Axis()
   FinancialChart.ExtraChartAreas.Add(stochasticChartArea)
   
   ' Here we display the kFastStochastic over a period of ten days for the Prices Series
   Dim kFast As Series = FinancialEngine.KFastStochastic(prices, 10)
   kFast.Type = SeriesType.Spline
   kFast.DefaultElement.Color = Color.FromArgb(255, 99, 49)
   If kFast.Elements.Count > 0 Then
      stochasticChartArea.SeriesCollection.Add(kFast)
   Else
      Console.WriteLine("The series kfast is empty")
   End If 
   ' Here we display the DStochastic over a period of ten days.
   ' For the calculation of DStochastic we use ExponentiallyMovingAverage over 5 days.
   Dim dStochastic As Series = FinancialEngine.DStochastic(prices, 10, 4, 5)
   dStochastic.Type = SeriesType.Spline
   dStochastic.DefaultElement.Color = Color.FromArgb(0, 255, 0)
   If dStochastic.Elements.Count > 0 Then
      stochasticChartArea.SeriesCollection.Add(dStochastic)
   Else
      Console.WriteLine("The series dStochastic is empty")
   End If 
End Sub 'Page_Load
		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="FinancialChart" runat="server"/>
			
			
		</div>
	</body>
</html>
