<%@ Page Language="vb"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">





Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   
   ' Rate charts can be used to show many interesting stats. For this sample we will make a chart that will show
   ' click through rates for banner campaigns.
   ' Lets get our data first. The methods below will generate some random data for us to use.
   Dim views As Series = getRandomViewsData()
   Dim clicks As Series = getRandomClicksData()
   
   ' Now to get a rate is as easy as this:
   Dim rate As Series = Series.Divide(clicks, views)
   
   ' We need a new name for the series
   rate.Name = "CTR"
   
   
   ' If we want to show the clicks, views, and rates then we need to create a new y axis because the CTR is so 
   ' small that it will not be readable.
   Dim ctrAxis As New Axis("Click Through Ratio (CTR)")
   
   ' A new number precision has be to supplied so the numbers can be seen. Otherwise we will see only 0s on the axis.
   ctrAxis.NumberPrecision = 4
   ' Lets move it on the right side also 
   ctrAxis.Orientation = dotnetCHARTING.Orientation.Right
   
   ' Now to assign the ctr axis to the ctr series:
   rate.YAxis = ctrAxis
   
   
   ' In the legend the values are sums by default and that does not make sense for CTR so lets 
   ' make the value in the legend an average for the ctr series.
   rate.LegendEntry.Value = "Average: %YAverage"
   
   
   ' Show values on bars
   Chart.DefaultSeries.DefaultElement.ShowValue = True
   
   
   ' Add the data.
   Chart.SeriesCollection.Add(views)
   Chart.SeriesCollection.Add(clicks)
   Chart.SeriesCollection.Add(rate)
End Sub 'Page_Load
 


Function getRandomViewsData() As Series ' This method will generate a series representing views for our banners
   
   Dim myR As New Random()
   
   Dim s As New Series()
   s.Name = "Views"
   Dim b As Integer
   For b = 1 To 4
      Dim e As New Element()
      e.Name = "Banner " + b.ToString()
      e.YValue = 50 + myR.Next(500)
      s.Elements.Add(e)
   Next b
   
   
   Return s
End Function 'getRandomViewsData


Function getRandomClicksData() As Series ' This method will generate a series representing clicks for our banners
   
   Dim myR As New Random()
   
   Dim s As New Series()
   s.Name = "Clicks"
   Dim b As Integer
   For b = 1 To 4
      Dim e As New Element()
      e.Name = "Banner " + b.ToString()
      e.YValue = myR.Next(50)
      s.Elements.Add(e)
   Next b
   
   
   Return s
End Function 'getRandomClicksData

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Rate chart</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
