<%@ Page Language="vb" Debug="true" Trace="false" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>

<script runat="server">
	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

		' This sample demonstrates usage of an organizational chart using custom attributes populated from an existing user table by adding instance and parent IDs to establish the org hierarchy.

		' Set global properties
		Chart.Type = ChartType.Organizational
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Size = "550X650"

		Chart.ChartArea.Background.ShadingEffectMode = ShadingEffectMode.Five
		Chart.ChartArea.Background.Color = Color.FromArgb(250, Color.FromArgb(180, 200, 224))

		' TitleBox Customization
		Chart.TitleBox.Label.Text = " Acme Company's Organizational Chart"
		Chart.TitleBox.Label.Color = Color.White
		Chart.TitleBox.Label.Shadow.Color = Color.FromArgb(105, 0, 0, 0)
		Chart.TitleBox.Label.Shadow.Depth = 2
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Two
		Chart.TitleBox.Background.Color = Color.SteelBlue

		' Set the org line style
		Chart.DefaultSeries.Line.Width = 5
		Chart.DefaultSeries.Line.Color = Color.DarkGray

		' Set the org annotation visual appearance, shading and layout
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Label.Alignment = StringAlignment.Center
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Seven
		Chart.DefaultElement.Annotation.Background.Color = Chart.Palette(2)
		Chart.DefaultElement.Annotation.Padding = 15
		Chart.DefaultElement.Annotation.Label.Text = "<block fStyle='bold' fSize='15'>%name<row><img:../../images/Org%picture><block halign='right'>%Department<br>%Email<br>%phone<br>Office #%office"

		' Controls the padding of the annotations within the chart area
		Chart.ChartArea.Padding = 30

		' Obtain series data from the database.  Note this was created by adding PID (parentid)
		' to an existing table only, then populating the parentid based on the hiearchy in place.
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.SqlStatement = "SELECT * FROM Employees"
		'Select only some users
		de.SqlStatement &= " WHERE Name LIKE 'Aida' OR Name LIKE 'Ain' OR Name LIKE 'John' OR Name LIKE 'George' OR Name LIKE 'Lori' "

		de.DataFields = "InstanceID=ID,InstanceParentID=PID,Name=Name,office,Department,Email,Phone,Picture"
		Chart.SeriesCollection.Add(de.GetSeries())


	End Sub
</script>

<html>
<head>
	<title>Database Driven Organizational Chart</title>
</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
