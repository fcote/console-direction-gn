<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">




Sub  Page_Load(sender as Object ,e as EventArgs )


	Chart.TempDirectory = "temp"
	Chart.Debug = true
	Chart.Title = "Tick affects the axis range."
	
	Chart2.TempDirectory = "temp"
	Chart2.Debug = true
	Chart2.Title = "Axis marker affects the axis range."	
	
	' * INTRO *
	' This sample will demonstrate how axis ticks and axis markers can force an axis scale to expand so they can be seen.

	' 1. CREATE A AXIS TICK ( For Chart1 )
	' Create a marker with the value of 550, well beyond the data we will chart.
	Dim at As AxisTick  = new AxisTick()
	at.Value = 550
	at.Label.Color = Color.Red
	at.Label.Text = "Show On Scale at 550"	
	
	' This property will indicate we want the scale where the tick is shown to expand.
	at.IncludeInAxisScale = true	
	' Add the tick to the y axis.
	Chart.YAxis.ExtraTicks.Add(at)
		
	' 2. CREATE A AXIS MARKER ( For Chart2 )
	' Create an axis marker with the value of 550, well beyond the data we will chart.
	Dim am as AxisMarker = new AxisMarker()
	am.Label.Text = "Quota"
	am.Value = 550
	am.Line.Color = Color.Red
	' This property will indicate we want the scale where the marker is shown to expand.
	am.IncludeInAxisScale = true
	' Add the marker to the y axis.	
	Chart2.YAxis.Markers.Add(am)

	
	' 3. CREATE A AXIS MARKER ( For Chart2 )
	' Create an axis marker with the value of November 2005, well beyond the data we will chart.
	Dim am2 as AxisMarker = new AxisMarker()
	am2.Label.Text = "Deadline"
	' Notice the alignment is changed because the default would draw on top of another marker.
	am2.Label.Alignment = StringAlignment.Far
	am2.Value = new DateTime(2005,11,1)
	am2.Line.Color = Color.Green
	
	' This property will indicate we want the scale where the marker is shown to expand.
	am2.IncludeInAxisScale = true
	' Add the marker to the x axis .
	Chart2.XAxis.Markers.Add(am2)


	' 4. ADD DATA
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx

	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData3())
	Chart2.SeriesCollection.Add(getRandomData())

    
End Sub


Function  getRandomData() As SeriesCollection

	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR AS Random  = new Random()
	Dim a,b as Integer
	Dim s as Series 
	Dim e As Element
	
		
	Dim dt as DateTime  = new DateTime(2005,1,1)
	
	For  a = 0 To 1
	
		s = new Series()
		s.Name = "Series " & a
		
		For b = 0  To 3
		
			e = new Element()
			e.YValue = myR.Next(50)
			dt = dt.AddMonths(1)
			e.XDateTime = dt
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	return SC
End Function



	
Function  getRandomData3() As SeriesCollection

	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR AS Random  = new Random()
	Dim a,b as Integer
	Dim s as Series 
	Dim e As Element	
	
	For a = 0 To 0
	
		s = new Series()
		s.Name = "Series " & a
		For  b = 0 to 3
		
			e = new Element()
			e.Name = "Element " & b
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>

		</div>
	</body>
</html>
