<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(300)
   Chart.Height = Unit.Parse(80)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   ' This sample will demonstrate how to create a dashboard like chart.
   Chart.LegendBox.Visible = False
   
   ' we'll setup the ranges of the chart.
   Chart.XAxis.ScaleRange = New ScaleRange(0, 10)
   Chart.YAxis.ScaleRange = New ScaleRange(0, 1)
   ' Clear the y axis values.
   Chart.YAxis.ClearValues = True
   
   ' Now we'll create the multi color background by using an area line with different colors. 
   ' Each element will be added to change the colors the area line.
   Dim back As New Series("")
   ' The markers will be eliminated because we dont need them.
   back.DefaultElement.Marker.Type = ElementMarkerType.None
   back.Type = SeriesType.AreaLine
   Dim e1 As New Element("", 1)
   e1.XValue = 0
   Dim e2 As New Element("", 1)
   e2.XValue = 2
   Dim e3 As New Element("", 1)
   e3.XValue = 4
   Dim e4 As New Element("", 1)
   e4.XValue = 6
   Dim e5 As New Element("", 1)
   e5.XValue = 8
   Dim e6 As New Element("", 1)
   e6.XValue = 10
   ' Add the elements to the back series.
   back.Elements.Add(e1)
   back.Elements.Add(e2)
   back.Elements.Add(e3)
   back.Elements.Add(e4)
   back.Elements.Add(e5)
   back.Elements.Add(e6)
   
   ' Set the different element colors.
   back.Elements(0).Color = Color.DarkGreen
   back.Elements(1).Color = Color.DarkGreen
   back.Elements(2).Color = Color.Green
   back.Elements(3).Color = Color.Yellow
   back.Elements(4).Color = Color.Orange
   back.Elements(5).Color = Color.Red
   
   ' Our data  will be a triangle marker series with one element
   Dim theData As New Series()
   theData.Type = SeriesType.Marker
   
   ' The y value is .12 so the marker appers closer to the bottom.
   Dim dataE As New Element()
   dataE.YValue = 0.12
   dataE.XValue = 8
   
   ' Specify the exact marker appearance.
   dataE.Marker.Type = ElementMarkerType.Triangle
   dataE.Marker.Size = 12
   ' Add the marker.
   theData.Elements.Add(dataE)
   
   ' This code will override the 0 and 10 tick on the x axis with text.
   Chart.XAxis.ExtraTicks.Add(New AxisTick(0, "Safe"))
   Chart.XAxis.ExtraTicks.Add(New AxisTick(10, "Danger"))
   
   ' Add the 2 series.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses static data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   ' Add the random data.
   Chart.SeriesCollection.Add(back)
   Chart.SeriesCollection.Add(theData)
End Sub 'Page_Load 

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
