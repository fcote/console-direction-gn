<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   
   
   ' This sample demonstrates how to label the total values of column stacks.
   Chart.DefaultElement.ShowValue = True
   Chart.YAxis.Scale = Scale.Stacked
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Clear the secondary alignment options of element labels.
   Dim ser As Series
   For Each ser In  mySC
      ser.DefaultElement.SmartLabel.AlignmentSecondary.Clear()
      Dim el As Element
      For Each el In  ser.Elements
         el.SmartLabel.AlignmentSecondary.Clear()
      Next el
   Next ser
   
   
   ' Create a total series that is invisible. Just used to place the totals labels.
   Dim total As Series = mySC.Calculate("Total", Calculation.Sum)
   total.Type = SeriesType.Marker
   
   total.DefaultElement.Marker.Type = ElementMarkerType.None
   total.DefaultElement.SmartLabel.Alignment = LabelAlignment.Top
   total.DefaultElement.SmartLabel.Font = New Font("Arial Black", 8, FontStyle.Bold)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
   Chart.SeriesCollection.Add(total)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(7)
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(20,50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
