<%@ Page Language="VB" Debug="true" Trace="false" Description="dotnetChart Component"%>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<script runat="server">

    Sub Page_Load(sender As [Object], e As EventArgs)
   
   If Not IsPostBack Then
      Dim dt As DateTime = DateTime.Today
      Dim myListItem As ListItem
      
      DropDownShow.Items.Add("Sales")
      DropDownShow.Items.Add("Orders")
      DropDownShow.Items.Add("Items")
      
      DropDownBy.Items.Add("Hours")
      DropDownBy.Items.Add(New ListItem("Day/Hour", "Day"))
      DropDownBy.Items.Add("Days")
      DropDownBy.Items.Add(New ListItem("Week/Day", "Week"))
      DropDownBy.Items.Add("Weeks")
      DropDownBy.Items.Add(New ListItem("Month/Day", "Month"))
      DropDownBy.Items.Add("Months")
      DropDownBy.Items.Add("Quarters")
      DropDownBy.Items.Add(New ListItem("Year/Month", "Year"))
      DropDownBy.Items.Add(New ListItem("Year/Quarter", "Quarter"))
      DropDownBy.Items.Add("Years")
      DropDownBy.Items.Add("Customer")
      DropDownBy.SelectedIndex = 8
      
      
      myListItem = New ListItem("None", "")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Jan", "1")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Feb", "2")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Mar", "3")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Apr", "4")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("May", "5")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Jun", "6")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Jul", "7")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Aug", "8")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Sep", "9")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Oct", "10")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Nov", "11")
      Month.Items.Add(myListItem)
      myListItem = New ListItem("Dec", "12")
      Month.Items.Add(myListItem)
      Month.SelectedIndex = dt.Month
      
      myListItem = New ListItem("None", "")
      Day.Items.Add(myListItem)
      Dim d As Integer
      For d = 1 To 31
         Day.Items.Add(d.ToString())
      Next d
      Day.SelectedIndex = dt.Day
      
      myListItem = New ListItem("None", "")
      Year.Items.Add(myListItem)
      Year.Items.Add("2001")
      Year.Items.Add("2002")
      Year.Items.Add("2003")
      Year.Items.Add("2004")
      Year.SelectedIndex = 2
      
      'Set FromDate
      myListItem = New ListItem("", "")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Jan", "1")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Feb", "2")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Mar", "3")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Apr", "4")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("May", "5")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Jun", "6")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Jul", "7")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Aug", "8")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Sep", "9")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Oct", "10")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Nov", "11")
      MonthFrom.Items.Add(myListItem)
      myListItem = New ListItem("Dec", "12")
      MonthFrom.Items.Add(myListItem)
      
      
      myListItem = New ListItem("", "")
      DayFrom.Items.Add(myListItem)
      
      For d = 1 To 31
         DayFrom.Items.Add(d.ToString())
      Next d 
      
      myListItem = New ListItem("", "")
      YearFrom.Items.Add(myListItem)
      YearFrom.Items.Add("2001")
      YearFrom.Items.Add("2002")
      YearFrom.Items.Add("2003")
      YearFrom.Items.Add("2004")
      
      
      'Set ToDate
      myListItem = New ListItem("", "")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Jan", "1")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Feb", "2")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Mar", "3")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Apr", "4")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("May", "5")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Jun", "6")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Jul", "7")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Aug", "8")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Sep", "9")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Oct", "10")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Nov", "11")
      MonthTo.Items.Add(myListItem)
      myListItem = New ListItem("Dec", "12")
      MonthTo.Items.Add(myListItem)
      
      
      myListItem = New ListItem("", "")
      DayTo.Items.Add(myListItem)
      
      For d = 1 To 31
         DayTo.Items.Add(d.ToString())
      Next d 
      
      myListItem = New ListItem("", "")
      YearTo.Items.Add(myListItem)
      YearTo.Items.Add("2001")
      YearTo.Items.Add("2002")
      YearTo.Items.Add("2003")
      YearTo.Items.Add("2004")
      
      
      myListItem = New ListItem("None", "")
      SplitByDropdown.Items.Add(myListItem)
      SplitByDropdown.Items.Add("Customer")
      
      DropDownChartType.Items.Add("Combo")
      DropDownChartType.Items.Add("ComboSideBySide")
      DropDownChartType.Items.Add("ComboHorizontal")
      myListItem = New ListItem("Single Pie", "Pie")
      DropDownChartType.Items.Add(myListItem)
      myListItem = New ListItem("Multi Pie", "Pies")
      DropDownChartType.Items.Add(myListItem)
      myListItem = New ListItem("Single Donut", "Donut")
      DropDownChartType.Items.Add(myListItem)
      myListItem = New ListItem("Multi Donut", "Donuts")
      DropDownChartType.Items.Add(myListItem)
      myListItem = New ListItem("Single Radar", "radar")
      DropDownChartType.Items.Add(myListItem)
      myListItem = New ListItem("Multi Radar", "radars")
      DropDownChartType.Items.Add(myListItem)
      DropDownChartType.Items.Add("Scatter")
      DropDownChartType.Items.Add("Bubble")
      
      DropDownScale.Items.Add("Normal")
      DropDownScale.Items.Add("Range")
      DropDownScale.Items.Add("Logarithmic")
      DropDownScale.Items.Add("Stacked")
      DropDownScale.Items.Add("FullStacked")
      DropDownScale.Items.Add("LogarithmicStacked")
      
      myListItem = New ListItem("3D", "true")
      Chart3D.Items.Add(myListItem)
      myListItem = New ListItem("2D", "false")
      Chart3D.Items.Add(myListItem)
      
      DropDownSeriesType.Items.Add("Column")
      DropDownSeriesType.Items.Add("Cylinder")
      DropDownSeriesType.Items.Add("Marker")
      DropDownSeriesType.Items.Add("Line")
      DropDownSeriesType.Items.Add("AreaLine")
      DropDownSeriesType.Items.Add("Spline")
      
      
      myListItem = New ListItem("None", "")
      DropDownSeriesAggregation.Items.Add(myListItem)
      DropDownSeriesAggregation.Items.Add("Average")
      DropDownSeriesAggregation.Items.Add("Sum")
      DropDownSeriesAggregation.Items.Add("Mode")
      DropDownSeriesAggregation.Items.Add("Median")
      DropDownSeriesAggregation.Items.Add("Maximum")
      DropDownSeriesAggregation.Items.Add("Minimum")
      DropDownSeriesAggregation.Items.Add("RunningAverage")
      DropDownSeriesAggregation.Items.Add("RunningSum")
      DropDownSeriesAggregation.Items.Add("RunningMode")
      DropDownSeriesAggregation.Items.Add("RunningMedian")
      DropDownSeriesAggregation.Items.Add("RunningMaximum")
      DropDownSeriesAggregation.Items.Add("RunningMinimum")
      
      
      
      SummerySeriesType.Items.Add("Column")
      SummerySeriesType.Items.Add("Cylinder")
      SummerySeriesType.Items.Add("Marker")
      SummerySeriesType.Items.Add("Line")
      SummerySeriesType.Items.Add("AreaLine")
      SummerySeriesType.Items.Add("Spline")
      SummerySeriesType.SelectedIndex = 3
      
      DropDownLimitMode.Items.Add("Top")
      DropDownLimitMode.Items.Add("Bottom")
      DropDownLimitMode.Items.Add("ExcludeTop")
      DropDownLimitMode.Items.Add("ExcludeBottom")
   End If 
End Sub 'Page_Load
 


Sub ButtonDisplay_Click(sender As [Object], e As EventArgs)
   Dim myChartType As dotnetCHARTING.ChartType = CType([Enum].Parse(GetType(dotnetCHARTING.ChartType), DropDownChartType.SelectedItem.Value, True), dotnetCHARTING.ChartType)
   Dim myAxisScale As dotnetCHARTING.Scale = CType([Enum].Parse(GetType(dotnetCHARTING.Scale), DropDownScale.SelectedItem.Value, True), dotnetCHARTING.Scale)
   Dim mySeriesType As dotnetCHARTING.SeriesType = CType([Enum].Parse(GetType(dotnetCHARTING.SeriesType), DropDownSeriesType.SelectedItem.Value, True), dotnetCHARTING.SeriesType)
   Dim show As String = DropDownShow.SelectedItem.Value
   Dim yearFromDate As String = YearFrom.SelectedItem.Value
   Dim monthFromDate As String = MonthFrom.SelectedItem.Value
   Dim dayFromDate As String = DayFrom.SelectedItem.Value
   
   Dim yearToDate As String = YearTo.SelectedItem.Value
   Dim monthToDate As String = MonthTo.SelectedItem.Value
   Dim dayToDate As String = DayTo.SelectedItem.Value
   
   Dim myDategrouping As dotnetCHARTING.TimeInterval = TimeInterval.Year
   Dim ByCustomer As String = ""
   If DropDownBy.SelectedItem.Value = "Customer" Then
      ByCustomer = "Customer"
   Else
      myDategrouping = CType([Enum].Parse(GetType(dotnetCHARTING.TimeInterval), DropDownBy.SelectedItem.Value, True), dotnetCHARTING.TimeInterval)
   End If 
   Dim splitBy As String = SplitByDropdown.SelectedItem.Value
   Dim splitByLimit As String = SplitByLimitTextbox.Text
   Dim seriesAggregation As String = DropDownSeriesAggregation.SelectedItem.Value
   Dim limit As String = TextBoxLimit.Text
   Dim show3D As String = Chart3D.SelectedItem.Value
   Dim reportType As String = show + "by"
   If ByCustomer <> "" Then
      reportType += ByCustomer
   Else
      reportType += myDategrouping.ToString()
   End If 
   Dim SqlSelect As StringBuilder = Nothing
   Dim where As StringBuilder = Nothing
   If yearFromDate <> "" And monthFromDate <> "" And dayFromDate <> "" Or(yearToDate <> "" And monthToDate <> "" And dayToDate <> "") Then
      where = New StringBuilder(" WHERE ")
      If yearFromDate <> "" And monthFromDate <> "" And dayFromDate <> "" Then
         Chart.DefaultSeries.StartDate = New DateTime(Convert.ToInt32(yearFromDate), Convert.ToInt32(monthFromDate), Convert.ToInt32(dayFromDate), 0, 0, 0)
         where.Append("OrderDate >= #STARTDATE#")
         
         If yearToDate <> "" And monthToDate <> "" And dayToDate <> "" Then
            Chart.DefaultSeries.EndDate = New DateTime(Convert.ToInt32(yearToDate), Convert.ToInt32(monthToDate), Convert.ToInt32(dayToDate), 23, 59, 59)
            where.Append("AND OrderDate <= #ENDDATE#")
         End If 
      Else
         Chart.DefaultSeries.EndDate = New DateTime(Convert.ToInt32(yearToDate), Convert.ToInt32(monthToDate), Convert.ToInt32(dayToDate), 23, 59, 59)
         where.Append("AND OrderDate <= #ENDDATE#")
      End If
   
   Else
      Dim daySelection As String = Day.SelectedItem.Value
      Dim monthSelection As String = Month.SelectedItem.Value
      Dim yearSelection As String = Year.SelectedItem.Value
      If yearSelection <> "" And monthSelection <> "" And daySelection <> "" Then
         
         Dim d As Integer = DateTime.DaysInMonth(Integer.Parse(yearSelection), Integer.Parse(monthSelection))
         If Integer.Parse(daySelection) > d Then
            daySelection = d.ToString()
            Day.SelectedIndex = d
         End If
         Do
            If myDategrouping = TimeInterval.Day Or myDategrouping = TimeInterval.Hours Then
               Chart.DefaultSeries.StartDate = New DateTime(Convert.ToInt32(yearSelection), Convert.ToInt32(monthSelection), Convert.ToInt32(daySelection), 0, 0, 0)
               Chart.DefaultSeries.EndDate = New DateTime(Convert.ToInt32(yearSelection), Convert.ToInt32(monthSelection), Convert.ToInt32(daySelection), 23, 59, 59)
               
               where = New StringBuilder(" WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ")
               
               
               Exit Do
            Else
               If myDategrouping = TimeInterval.Month Or myDategrouping = TimeInterval.Days Or myDategrouping = TimeInterval.Weeks Then
                  Chart.DefaultSeries.StartDate = New DateTime(Convert.ToInt32(yearSelection), Convert.ToInt32(monthSelection), 1, 0, 0, 0)
                  Chart.DefaultSeries.EndDate = New DateTime(Convert.ToInt32(yearSelection), Convert.ToInt32(monthSelection), d, 23, 59, 59)
                  
                  
                  where = New StringBuilder(" WHERE OrderDate >= #STARTDATE# ")
                  where.Append("AND OrderDate <= #ENDDATE#")
                  Exit Do
               Else
                  If myDategrouping = TimeInterval.Year Or myDategrouping = TimeInterval.Quarter Or myDategrouping = TimeInterval.Weeks Then
                     Chart.DefaultSeries.StartDate = New DateTime(Convert.ToInt32(yearSelection), 1, 1, 0, 0, 0)
                     Chart.DefaultSeries.EndDate = New DateTime(Convert.ToInt32(yearSelection), 12, 31, 23, 59, 59)
                     
                     where = New StringBuilder(" WHERE OrderDate >= #STARTDATE# ")
                     where.Append("AND OrderDate <= #ENDDATE#")
                     Exit Do
                  End If
               End If 
            End If 
         Loop While False
      End If 
   End If
   
   'General settings
   Chart.Visible = True
   Chart.Type = myChartType
   Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   Chart.Height = Unit.Parse(480)
   Chart.Width = Unit.Parse(640)
   Chart.UseFile = True
   Chart.TempDirectory = "temp"
   Chart.TitleBox.IconPath = "../../Images/icon.gif"
   Chart.DefaultSeries.Limit = limit
   Chart.DefaultSeries.LimitMode = CType([Enum].Parse(GetType(dotnetCHARTING.LimitMode), DropDownLimitMode.SelectedItem.Value, True), dotnetCHARTING.LimitMode)
   Chart.Debug = True
   Chart.DonutHoleSize = 50
   Chart.LegendBox.Template = "%icon%name"
   If ShowValues.Checked Then
      Chart.DefaultSeries.DefaultElement.ShowValue = True
   End If 
   If show3D = "true" Then
      Chart.Use3D = True
   Else
      Chart.Use3D = False
   End If 
   If Transpose.Checked Then
      Chart.Transpose = True
   End If 
   If ShowOther.Checked Then
      Chart.DefaultSeries.ShowOther = True
   Else
      Chart.DefaultSeries.ShowOther = False
   End If
   Chart.DefaultSeries.Type = mySeriesType
   
   Chart.DefaultAxis.Scale = myAxisScale
   
   If ByCustomer = "" And myChartType <> ChartType.Scatter And myChartType <> ChartType.Bubble Then
      Chart.DateGrouping = myDategrouping
      If DropDownShow.SelectedItem.Value = "Sales" Then
         Chart.YAxis.FormatString = "currency"
      Else
         Chart.DefaultAxis.NumberPercision = 1
      End If 
   End If 
   
   reportType = reportType.ToLower()
   If myChartType.Equals(ChartType.Bubble) Then
      'set global properties
      Chart.Title = "Sales comparison for 3 days"
      Chart.YAxis.Label.Text = "Sales"
      Chart.XAxis.Label.Text = "No of Items"
      Chart.Palette = New Color() {Color.Blue, Color.Red, Color.Yellow}
      Chart.DefaultSeries.DefaultElement.Transparency = 50
      Chart.XAxis.NumberPercision = 2
      Chart.LegendBox.Template = "%icon %name"
      
      
      'Add a series
      Chart.Series.Name = "Dec 6,2002"
      Chart.Series.StartDate = New DateTime(2002, 12, 6, 0, 0, 0)
      Chart.Series.EndDate = New DateTime(2002, 12, 6, 23, 59, 59)
      Chart.Series.SqlStatement = "SELECT Orders.ItemNo,Sum(Orders.Total), Sum(Orders.Quantity) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo"
      Chart.SeriesCollection.Add()
      
      'Add a series
      Chart.Series.Name = "Dec 7,2002"
      Chart.Series.StartDate = New DateTime(2002, 12, 7, 0, 0, 0)
      Chart.Series.EndDate = New DateTime(2002, 12, 7, 23, 59, 59)
      Chart.Series.SqlStatement = "SELECT  Orders.ItemNo,Sum(Orders.Total), Sum(Orders.Quantity) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo"
      Chart.SeriesCollection.Add()
      
      'Add a series
      Chart.Series.Name = "Dec 8,2002"
      Chart.Series.StartDate = New DateTime(2002, 12, 8, 0, 0, 0)
      Chart.Series.EndDate = New DateTime(2002, 12, 8, 23, 59, 59)
      
      Chart.Series.SqlStatement = "SELECT  Orders.ItemNo,Sum(Orders.Total), Sum(Orders.Quantity) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo"
      Chart.SeriesCollection.Add()
   
   
   Else
      If myChartType.Equals(ChartType.Scatter) Then
         Chart.Title = show + " By item no"
         Chart.YAxis.Label.Text = show
         Chart.XAxis.Label.Text = "Item No"
         Chart.DefaultSeries.Name = "Dec 6-8,2002"
         Chart.DefaultSeries.StartDate = New DateTime(2002, 12, 6, 0, 0, 0)
         Chart.DefaultSeries.EndDate = New DateTime(2002, 12, 8, 23, 59, 59)
         Chart.DefaultSeries.DefaultElement.LabelTemplate = "(%xvalue, %yvalue)"
         Chart.XAxis.NumberPercision = 2
         
         
         Select Case show.ToLower()
            Case "sales"
               'Add a series
               Chart.Series.SqlStatement = "SELECT Orders.ItemNo,Sum(Orders.Total) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo"
               Chart.SeriesCollection.Add()
            Case "orders"
               'Add a series
               Chart.Series.SqlStatement = "SELECT Orders.ItemNo,Count(1) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo"
               Chart.SeriesCollection.Add()
            Case "items"
               'Add a series
               Chart.Series.SqlStatement = "SELECT  Orders.ItemNo,Sum(Orders.Quantity) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo"
               Chart.SeriesCollection.Add()
         End Select
      Else
         If Sum.Checked Then
            Chart.Series.Elements.Add(Calculation.Sum, "Total")
         End If
         If Average.Checked Then
            Chart.Series.Elements.Add(Calculation.Average, "Average")
         End If
         If Mode.Checked Then
            Chart.Series.Elements.Add(Calculation.Mode, "Mode")
         End If
         If Median.Checked Then
            Chart.Series.Elements.Add(Calculation.Median, "Median")
         End If
         If Min.Checked Then
            Chart.Series.Elements.Add(Calculation.Minimum, "Min")
         End If
         If Max.Checked Then
            Chart.Series.Elements.Add(Calculation.Maximum, "Max")
         End If 
         
         If ByCustomer <> "" Then
            Chart.XAxis.Label.Text = "Customers"
         Else
            If myChartType = dotnetCHARTING.ChartType.Combo Then
               Chart.XAxis.Label.Text = myDategrouping.ToString()
            End If
         End If
         Chart.Series.Name = DropDownShow.SelectedItem.Value
         Chart.DefaultSeries.SplitByLimit = splitByLimit
         
         Select Case reportType.ToLower()
            Case "salesbyday", "salesbyday/hour", "salesbyhours", "salesbyweek", "salesbyweeks", "salesbyweek/day", "salesbymonth/day", "salesbymonth", "salesbydays", "salesbyyear", "salesbyquarter", "salesbyyear/month", "salesbymonths", "salesbyquarters", "salesbyyears"
               If (True) Then
                  Select Case splitBy.ToLower()
                     Case "customer"
                        Chart.Title = "Sales By customer"
                        SqlSelect = New StringBuilder("SELECT  OrderDate,Total, Name FROM Orders ", 128)
                        If Not (where Is Nothing) Then
                           SqlSelect.Append(where)
                        End If
                        SqlSelect.Append(" ORDER BY Orders.OrderDate")
                        Chart.Series.SqlStatement = SqlSelect.ToString()
                        Chart.SeriesCollection.Add()
                     Case Else
                        Chart.Title = "Sales"
                        SqlSelect = New StringBuilder("SELECT OrderDate,Total FROM Orders ")
                        If Not (where Is Nothing) Then
                           SqlSelect.Append(where)
                        End If
                        SqlSelect.Append(" ORDER BY OrderDate")
                        Chart.Series.SqlStatement = SqlSelect.ToString()
                        Chart.SeriesCollection.Add()
                  End Select
               End If
            Case "ordersbyday/hour", "ordersbyday", "ordersbyhours", "ordersbyweek", "ordersbyweek/day", "ordersbymonth/day", "ordersbymonth", "ordersbydays", "ordersbyyear", "ordersbyquarter", "ordersbyyear/month", "ordersbymonths", "ordersbyweeks", "ordersbyyears", "ordersbyquarters"
               If (True) Then
                  Select Case splitBy.ToLower()
                     
                     Case "customer"
                        Chart.Title = "Orders by customer"
                        SqlSelect = New StringBuilder("SELECT  OrderDate,1 AS q, Name FROM Orders ", 128)
                        If Not (where Is Nothing) Then
                           SqlSelect.Append(where)
                        End If
                        SqlSelect.Append(" ORDER BY Orders.OrderDate")
                        Chart.Series.SqlStatement = SqlSelect.ToString()
                        Chart.SeriesCollection.Add()
                     Case Else
                        Chart.Title = "Orders"
                        SqlSelect = New StringBuilder("SELECT OrderDate,1 AS q FROM Orders ")
                        If Not (where Is Nothing) Then
                           SqlSelect.Append(where)
                        End If
                        SqlSelect.Append(" ORDER BY OrderDate")
                        Chart.Series.SqlStatement = SqlSelect.ToString()
                        Chart.SeriesCollection.Add()
                  End Select
               End If
            
            Case "itemsbyday/hour", "itemsbyday", "itemsbyhours", "itemsbyweek/day", "itemsbyweek", "itemsbymonth/day", "itemsbymonth", "itemsbyweeks", "itemsbydays", "itemsbyyear", "itemsbyquarter", "itemsbyyear/month", "itemsbymonths", "itemsbyyears", "itemsbyquarters"
               If (True) Then
                  Select Case splitBy.ToLower()
                     
                     Case "customer"
                        Chart.Title = "Items by customer"
                        SqlSelect = New StringBuilder("SELECT  OrderDate,Sum(Quantity) AS CountOfQuantity, Name FROM Orders", 128)
                        If Not (where Is Nothing) Then
                           SqlSelect.Append(where)
                        End If
                        SqlSelect.Append(" GROUP BY Orders.OrderDate, Orders.Name ORDER BY Orders.OrderDate")
                        Chart.Series.SqlStatement = SqlSelect.ToString()
                        Chart.SeriesCollection.Add()
                     Case Else
                        Chart.Title = "Items"
                        SqlSelect = New StringBuilder("SELECT  OrderDate,Sum(Quantity) FROM Orders ", 128)
                        If Not (where Is Nothing) Then
                           SqlSelect.Append(where)
                        End If
                        SqlSelect.Append(" GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate")
                        Chart.Series.SqlStatement = SqlSelect.ToString()
                        Chart.SeriesCollection.Add()
                  End Select
               End If
            
            Case "salesbycustomer"
               Chart.Title = "Sales by customer"
               SqlSelect = New StringBuilder("SELECT  Orders.Name,Sum(Orders.Total) AS TotalOrders FROM Orders ")
               If Not (where Is Nothing) Then
                  SqlSelect.Append(where)
               End If
               SqlSelect.Append(" GROUP BY Orders.Name ORDER BY Count(Orders.Total) DESC")
               Chart.Series.SqlStatement = SqlSelect.ToString()
               Chart.SeriesCollection.Add()
            Case "itemsbycustomer"
               Chart.Title = "Items by customer"
               SqlSelect = New StringBuilder("SELECT Name,Sum(Quantity) FROM Orders ")
               If Not (where Is Nothing) Then
                  SqlSelect.Append(where)
               End If
               SqlSelect.Append(" GROUP BY Name ORDER BY Sum(Quantity) DESC")
               Chart.Series.SqlStatement = SqlSelect.ToString()
               Chart.SeriesCollection.Add()
            
            Case "ordersbycustomer"
               Chart.Title = "Orders by customer"
               SqlSelect = New StringBuilder("SELECT Name,Count(1) FROM Orders ", 128)
               If Not (where Is Nothing) Then
                  SqlSelect.Append(where)
               End If
               SqlSelect.Append(" GROUP BY Name ORDER BY Count(1) DESC")
               Chart.Series.SqlStatement = SqlSelect.ToString()
               Chart.SeriesCollection.Add()
         End Select
      End If 
   End If '**************************************************************************    
   If seriesAggregation <> "" Then
      
      Chart.Series.Name = seriesAggregation
      Chart.Series.Type = CType([Enum].Parse(GetType(dotnetCHARTING.SeriesType), SummerySeriesType.SelectedItem.Value, True), dotnetCHARTING.SeriesType)
      Chart.SeriesCollection.Add(CType([Enum].Parse(GetType(dotnetCHARTING.Calculation), seriesAggregation, True), dotnetCHARTING.Calculation))
   End If 
End Sub 'ButtonDisplay_Click


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Report Options</title>
     <style type="text/css">
     body.myBody  
     {
     border-top-width:0px;
     
     }
     img.myImage  
     {
     border:0px;
     
     }
     td.myTD1  
     {
      width:"1%";
     }
     td.myTD2  
     {
      width:"99%";
      background:"#BFC0DB";
     }
     
    </style>
</head>
<body class="myBody">
    <form action="sample.aspx" runat="server">
        <div style="text-align:center">
            <table border="1" cellpadding="3" cellspacing="0" style="border-collapse: collapse; border-color: #111111" id="AutoNumber1">
                <tr>
    <td class="myTD1">
<img class="myImage" src="../../images/dotnetCharting.gif" width="230" alt="dotnetChartingImage" height="94"/></td>
    <td class="myTD2">Report: 
 <ASP:DropDownList id="DropDownShow" runat="server">
    </ASP:DropDownList>&nbsp;By:&nbsp; 
<ASP:DropDownList id="DropDownBy" runat="server">
    </ASP:DropDownList>
<ASP:DropDownList id="Month" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="Day" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="Year" runat="server">
    </ASP:DropDownList>
Split By: 
    <ASP:DropDownList id="SplitByDropdown" runat="server"/>
 Limit Series:
 <asp:TextBox id="SplitByLimitTextbox" width="25" runat="server" /> 
        <br/>From Date: 
    <ASP:DropDownList id="MonthFrom" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="DayFrom" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="YearFrom" runat="server">
    </ASP:DropDownList>
&nbsp;To Date: 
      <ASP:DropDownList id="MonthTo" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="DayTo" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="YearTo" runat="server">
    </ASP:DropDownList>
&nbsp;Limit Elements: <asp:TextBox id="TextBoxLimit" width="25" runat="server" ></asp:TextBox>
Limit Mode:<ASP:DropDownList id="DropDownLimitMode" runat="server"> 
    </ASP:DropDownList>
	<asp:CheckBox id="ShowOther" Text="Show other" runat="server"/>
      <asp:CheckBox id="ShowValues" Text="Show Values" runat="server"/>
        <asp:CheckBox id="Average" Text="Average" runat="server"/>
      	<asp:CheckBox id="Sum" Text="Sum" runat="server"/>
      	<asp:CheckBox id="Mode" Text="Mode" runat="server"/>
		<asp:CheckBox id="Median" Text="Median" runat="server"/>
		<asp:CheckBox id="Min" Text="Minimum" runat="server"/>
		<asp:CheckBox id="Max" Text="Maximum" runat="server"/>
	
of series. Chart Type: 
    <ASP:DropDownList id="DropDownChartType" runat="server">
    </ASP:DropDownList>&nbsp; 
     Axis Scale:<ASP:DropDownList id="DropDownScale" runat="server">
    </ASP:DropDownList>&nbsp;
    Transpose:
		<asp:CheckBox id="Transpose" runat="server"/>

<ASP:DropDownList id="Chart3D" runat="server">
    </ASP:DropDownList>&nbsp; 
<ASP:DropDownList id="DropDownSeriesType" runat="server">
    </ASP:DropDownList>
    &nbsp;Summary series: 
    <ASP:DropDownList id="DropDownSeriesAggregation" runat="server">
    </ASP:DropDownList>
    <ASP:DropDownList id="SummerySeriesType" runat="server">
    </ASP:DropDownList>
    <asp:Button id="ButtonDisplay" onclick="ButtonDisplay_Click" runat="server" Text="Display">
    </asp:Button>
                  </td>
                </tr>
            </table>
            <dotnet:Chart id="Chart" runat="server" TempDirectory="Temp" Visible="false" />
        </div>
    </form></body>
</html>