<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        ' This sample demonstrates showing MicroChart tooltips based on SubValues created using SubValueDateGrouping.

		' Setup the chart.
		Chart.Size = "600x350"
		Chart.Title = "Hover over the bars for details"
		Chart.ShowDateInTitle = False
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.DefaultElement.DefaultSubValue.Visible = False
		Chart.ShadingEffectMode = ShadingEffectMode.Three
		Chart.YAxis.FormatString = "Currency"

		' Style the title box
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.Padding = 8
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.TitleBox.Background.Color = Color.LightSteelBlue

		' Tooltip definition.
		Chart.DefaultElement.ToolTip = "<block fStyle='bold'>%Name 2002 Daily<hr><Chart:Sparkline values='%SubValueList' width='100' height='50' colors='blue,Black'><hr>"

		' Get data for each sale that is grouped by MONTHS for a YEAR and add subvalues grouped by DAYS to each month element.
		Dim myS As Series = getLiveData()

		Chart.SeriesCollection.Add(myS)

	End Sub


	Function getLiveData() As Series
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.StartDate = New DateTime(2002, 1, 1, 0, 0, 0)
		de.EndDate = New DateTime(2002, 12, 31, 23, 59, 59)
		de.SqlStatement = "SELECT OrderDate,Total,Name,Quantity FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#"
		de.DataFields = "xAxis=OrderDate,yAxis=Total,Quantity"
		de.SubValueDateGrouping = TimeInterval.Days
		de.DateGrouping = TimeInterval.Year
		Return de.GetSeries()(0)
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
