<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs) 

	'set global properties
    Chart.Title="Item sales report"
    Chart.XAxis.Label.Text="Customers"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    
    
    'Adding series programatically
   	Chart.Series.Name = "Item sales"
   	Chart.Series.Data = CreateDataSet()
    Chart.SeriesCollection.Add()
                       
   
End Sub
Function  CreateDataSet() As DataSet

	Dim ds As  DataSet = new DataSet()
	Dim dt As DataTable = new DataTable()
	
	Dim connString As String = "Provider=Microsoft.Jet.OLEDB.4.0;user id=admin;password=;data source=" & Server.MapPath("../../database/chartsample.mdb")  
    Dim  conn As OleDbConnection = new OleDbConnection(connString)
    Dim  adapter As OleDbDataAdapter = new OleDbDataAdapter()
    Dim  selectQuery As String = "SELECT Name,Sum(Quantity) FROM Orders GROUP BY Orders.Name ORDER BY Orders.Name" 
    adapter.SelectCommand = new OleDbCommand(selectQuery, conn)
    adapter.Fill(dt)
    ds.Tables.Add(dt)
    return ds
End Function
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Data Set Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
