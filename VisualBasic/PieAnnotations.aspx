<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using annotations instead of labels on a pie chart.

		Chart.Type = ChartType.Pies
		Chart.Size = "500x350"
		Chart.TempDirectory = "temp"
		Chart.ShadingEffectMode = ShadingEffectMode.Four
		Chart.Debug = True
		Chart.PaletteName = Palette.Four

		'Modify the legendbox
		Chart.LegendBox.ClearColors()
		Chart.LegendBox.Position = LegendBoxPosition.Middle

		' Clear the chart area.
		Chart.ChartArea.ClearColors()
		Chart.ChartArea.Padding = 38

		' Set annotation properties for all elements.
		Chart.DefaultElement.Annotation = New Annotation("%Name<br>%Value (%PercentOfSeries)")
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Background1
		Chart.DefaultElement.Annotation.Background.Color = Color.FromArgb(120,190,255)
		Chart.DefaultElement.Annotation.DynamicSize = False
		Chart.DefaultElement.Annotation.Label.Font = New Font("Arial", 9, FontStyle.Bold)
		Chart.DefaultElement.Annotation.DefaultCorner = BoxCorner.Round
		Chart.DefaultElement.Annotation.CornerSize = 15

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(3)
		Dim SC As SeriesCollection = New SeriesCollection()
		For a As Integer = 1 To 1
			Dim s As Series = New Series()
			For b As Integer = 1 To 6
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
