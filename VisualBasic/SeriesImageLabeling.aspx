<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	Chart.Type = ChartType.Combo 'Horizontal;
	Chart.Size = "600x350"
	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Title = ".netCHARTING Sample"

	Chart.DefaultSeries.Type = SeriesType.AreaSpline
	Chart.YAxis.Scale = Scale.Stacked

	' This sample demonstrates using a single label per series to indicate the series name.	

	' Some dofault label settigns
	Chart.DefaultElement.SmartLabel.Font = New Font("Arial",11)
	Chart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center

	' Make element labels show up.
	Chart.DefaultElement.ShowValue = True

	' Set the default label to be empty
	Chart.DefaultElement.SmartLabel.Text = ""

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Dim mySC As SeriesCollection = getRandomData()

	' Set the label text for the 3rd element of each series.

	mySC(0).Elements(2).SmartLabel.Text = "<img:../../images/browserO.png>"
	mySC(1).Elements(2).SmartLabel.Text = "<img:../../images/browserIE.png>"
	mySC(2).Elements(2).SmartLabel.Text = "<img:../../images/browserS.png>"
	mySC(3).Elements(2).SmartLabel.Text = "<img:../../images/browserN.png>"




	' Add the random data.
	Chart.SeriesCollection.Add(mySC)

End Sub

Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random(5)
	For a As Integer = 1 To 4
		Dim s As Series = New Series()
		s.Name = "Series " & a.ToString()
		For b As Integer = 1 To 5
			Dim e As Element = New Element()
			e.Name = "Element " & b.ToString()
			e.YValue = 30 + myR.Next(30)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	Return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
