<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using line caps on 2D line series.

		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.LegendBox.Visible = False

		' Set series type.
		Chart.DefaultSeries.Type = SeriesType.Line
		Chart.DefaultElement.Marker.Visible = False

		' Set line properties.
		Chart.DefaultSeries.Line.Width = 10
		Chart.DefaultSeries.Line.AnchorCapScale = 2
		Chart.DefaultSeries.Line.EndCap = LineCap.ArrowAnchor

		Chart.SeriesCollection.Add(Series.FromYValues(10,20,15,30))
	End Sub



</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
