<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs )

	'set global properties
    Chart.Title="Item sales"
    Chart.ChartArea.XAxis.Label.Text="%name"
    Chart.ChartArea.YAxis.Label.Text="Orders"
    Chart.Type = ChartType.Pies
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.TitleBox.Position=TitleBoxPosition.Full
        
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
 
    'Add Jan series
    Chart.Series.Name="Jan"
    Chart.Series.StartDate= New System.DateTime(2002,1,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,1,31,23,59,59)
    Chart.Series.SqlStatement= "SELECT Name, Sum(1) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Name"
    Chart.SeriesCollection.Add()
	
	'Add Feb series
    Chart.Series.Name="Feb"
    Chart.Series.StartDate= New System.DateTime(2002,2,1,0,0,0 )
    Chart.Series.EndDate = New System.DateTime(2002,2,28,23,59,59)
    Chart.Series.SqlStatement= "SELECT  Name, Sum(1) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Name"
    Chart.SeriesCollection.Add()

	'Add Mar series
    Chart.Series.Name="Mar"
    Chart.Series.StartDate= New System.DateTime(2002,3,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,3,31,23,59,59)
    Chart.Series.SqlStatement= "SELECT  Name, Sum(1) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Name"
    Chart.SeriesCollection.Add()

	'Add Apr series
    Chart.Series.Name="Apr"
    Chart.Series.StartDate= New System.DateTime(2002,4,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,4,30,23,59,59)
    Chart.Series.SqlStatement= "SELECT  Name, Sum(1) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Name"
    Chart.SeriesCollection.Add()
	
	'Add May series
    Chart.Series.Name="May"
    Chart.Series.StartDate= New System.DateTime(2002,5,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,5,31,23,59,59)
    Chart.Series.SqlStatement= "SELECT  Name, Sum(1) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Name"
    Chart.SeriesCollection.Add()

    'Add June series
    Chart.Series.Name="Jun"
    Chart.Series.StartDate= New System.DateTime(2002,6,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,6,30,23,59,59)
    Chart.Series.SqlStatement= "SELECT  Name, Sum(1) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Name"
    Chart.SeriesCollection.Add()     
    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Orders Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
