<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs )

	'set global properties
    Chart.Title="Item sales"
    Chart.ChartArea.XAxis.Label.Text="Hours"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DateGrouping = TimeInterval.Hours
    
	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
	Chart.DefaultSeries.StartDate= new System.DateTime(2002,3,10,0,0,0)
    Chart.DefaultSeries.EndDate = new System.DateTime(2002,3,10,20,59,59)
    Chart.OverlapFooter=false
    
    'Add a series
    Chart.Series.Name="Items"
    Chart.Series.SqlStatement= "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
    Chart.SeriesCollection.Add()
    
   
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Date Grouping By Hours Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
