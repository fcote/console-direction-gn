<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using smart label padding and maximum label distance from element settings.
		' FullScreen

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.ChartArea.Label.Text = "Label Padding: 2" & Constants.vbLf & "Label Maximum Distance: 400"
		Chart.Type = ChartType.Combo
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.DefaultSeries.Type = SeriesType.Marker
		Chart.DefaultElement.ShowValue = True
		Chart.XAxis.ScaleRange = New ScaleRange(0, 60)
		Chart.YAxis.ScaleRange = New ScaleRange(0, 60)
		Chart.DefaultElement.SmartLabel.Padding = 2
		Chart.DefaultElement.SmartLabel.DistanceMaximum = 400

		Chart1.TempDirectory = "temp"
		Chart1.Debug = True
		Chart1.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart1.ChartArea.Label.Text = "Label Padding: 5" & Constants.vbLf & "Label Maximum Distance: 50"
		Chart1.Type = ChartType.Combo
		Chart1.Size = "600x350"
		Chart1.Title = ".netCHARTING Sample"
		Chart1.DefaultSeries.Type = SeriesType.Marker
		Chart1.DefaultElement.ShowValue = True
		Chart1.XAxis.ScaleRange = New ScaleRange(0, 60)
		Chart1.YAxis.ScaleRange = New ScaleRange(0, 60)
		Chart1.DefaultElement.SmartLabel.Padding = 5
		Chart1.DefaultElement.SmartLabel.DistanceMaximum = 50

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
		Chart1.SeriesCollection.Add(getRandomData())
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 9
				Dim e As Element = New Element()
				e.YValue = 35 + myR.Next(5)
				e.XValue = 35+myR.Next(5)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
		<dotnet:Chart ID="Chart1" runat="server" />
	</div>
</body>
</html>
