<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs)

	'set global properties
    Chart.Title="Orders By Customers"
    Chart.XAxis.Label.Text="Customers"
    Chart.Palette= new Color () {Color.Blue,Color.Yellow}
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.ChartArea.Shadow.Depth=10
    
	'Add sum series
    Chart.Series.Name = "sum"
    Chart.Series.DefaultElement.ShowValue=true
    Chart.Series.Type = SeriesType.AreaLine
    Chart.SeriesCollection.Add(Calculation.RunningSum)
 
    
    'Add a series
    Chart.Series.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.Series.Name="Orders"
    Chart.Series.Limit ="5"
    Chart.Series.SqlStatement= "SELECT Name,Sum(1) FROM Orders GROUP BY Name ORDER BY Sum(1) DESC"
    Chart.Series.Type = SeriesType.Cylinder
    
    Chart.SeriesCollection.Add()
    
    
   

    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Orders Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
