<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using compounded gauges.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.DarkBlue }
		Chart.Type = ChartType.Gauges
		Chart.Size = "500x350"
		Chart.OverlapFooter = true
		Chart.Title = ".netCHARTING Sample"
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.ClearColors()
		Chart.ChartArea.ClearColors()

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

		mySC(0).Background.Color = Color.WhiteSmoke

		' Create a second series to embed into the circular gauge
		Dim digital As Series = Series.FromYValues(mySC(0)(0).YValue)
		digital.GaugeType = GaugeType.DigitalReadout
		digital.GaugeBorderBox.Padding = 2
		digital.GaugeBorderBox.Background.Color = Color.FromArgb(100, Color.Black)
		digital.GaugeBorderBox.Shadow.Color = Color.Empty
		digital.GaugeBorderBox.Position = New Rectangle(195, 240, 100, 40)
		digital.LegendEntry.Visible = False

		' Specify the digital readout font size
		digital.DefaultElement.SmartLabel.Font = New Font("Arial", 30)

		' Get rid of digital gauge labels
		digital.XAxis = New Axis()
		digital.XAxis.DefaultTick.Label.Text = ""

		' Add the digital readout series to the chart.
		Chart.SeriesCollection.Add(digital)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 1
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(14150)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
