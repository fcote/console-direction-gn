<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
  Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Synchronizing two differnt axes."
   
   
   ' This sample demonstrates how to synchronize two axis scales.
   ' The following code will synchronize the new axis with the default y axis.
   Dim newAxis As New Axis()
   Chart.YAxis.SynchronizeScale.Add(newAxis)
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Change the values of elements to demonstrate ensure the axis scales would not be the same.
   mySC(0)(0).YValue = 100
   mySC(3)(0).YValue = - 100
   
   ' Specify the new axis for two of the series and add them to a different chart area.
   mySC(2).YAxis = newAxis
   mySC(3).YAxis = newAxis
   Chart.ExtraChartAreas.Add(New ChartArea(New SeriesCollection(mySC(2), mySC(3))))
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC(0), mySC(1))
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " +Convert.ToString(b + 1)
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
