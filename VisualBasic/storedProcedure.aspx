<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs)

	'set global properties
        Chart.DefaultSeries.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;user id=admin;password=;data source=" + Server.MapPath("../../database/chartsample.mdb")
    Chart.Title="Item sales"
    Chart.XAxis.Label.Text="Hours"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DateGrouping = TimeInterval.Day
    Chart.EmptyElementText="N/A"
   
    
    'Add a series
    Chart.Series.Name="Items"
    Chart.Series.StoredProcedure = "spSalesLimit"
    'Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
    Chart.Series.AddParameter("@STARTDATE","3/10/02 00:00:00",FieldType.Date)
    Chart.Series.AddParameter("@ENDDATE","3/10/02 23:59:59",FieldType.Date)
    Chart.SeriesCollection.Add()
    
    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Orders Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
