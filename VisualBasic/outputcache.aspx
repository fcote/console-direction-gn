<%@ Page Language="VB" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Outputcache Duration = "10" Varybyparam = "none"%>


<script runat="server">


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 5
         Dim e As New Element()
         e.Name = "E " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   
   
   
   'Set file name 
   Chart.FileName = "outputcachesample"
   
   ' Set the title to the current time
   Dim Dt As DateTime = DateTime.Now
   Chart.Title = "This chart was created at " + Dt.ToString()
   Response.Write(("This page was created at " + Dt.ToString()))
   
   ' Set the x axis label
   Chart.ChartArea.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   Chart.ChartArea.YAxis.Label.Text = "Y Axis Label"
   
   ' Set the directory where the images will be stored.
   Chart.TempDirectory = "temp"
   
   ' Set the bar shading effect
   Chart.ShadingEffect = True
   
   ' Set he chart size.
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   
   Chart.Debug = True
   
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Output Cache Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 <font size="2" face="Arial">Refresh this chart, it will be cached for 10 seconds then a new chart will be created. You will also see that page render and chart render are both cached and syncronized.  See <a href="fragmentcache.aspx">fragmentcache</a> for a sample where the chart image is cached but the page contents are not.
 </font> 
</div>
</body>
</html>
