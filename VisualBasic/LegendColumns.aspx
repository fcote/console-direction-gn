<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates adding a column microChart into a legend box value column.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.Use3D = True
		Chart.Type = ChartType.Donut
		Chart.DonutHoleSize = 85
		Chart.Size = "600x350"
		Chart.ChartArea.DefaultCorner = BoxCorner.Round
		Chart.ChartArea.Background.ShadingEffectMode = ShadingEffectMode.Five

		Chart.LegendBox.Position = New Point(210, 120)
		Chart.LegendBox.Template = "%Name%Value%Icon"
		Chart.LegendBox.DefaultEntry.Value = "<Chart:Column height='15' values='%YValueList' > (%YSum)"
		Chart.LegendBox.DefaultCorner = BoxCorner.Round

		' Add the random data.
		Chart.SeriesCollection.Add(getRandomData())
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			Dim c As Integer = 5+myR.Next(10)
			For b = 1 To c - 1
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(80)*a
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
