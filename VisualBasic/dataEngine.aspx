<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs )

	'set global properties
    Chart.Title="Item sales"
    Chart.XAxis.Label.Text ="months"
    Chart.YAxis.FormatString="currency"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.ShadingEffect = true
    Chart.Size="800X500"
    Chart.LegendBox.Position = LegendBoxPosition.None
    Chart.DefaultSeries.DefaultElement.ToolTip = "%yValue"
     
    'Add a series
    Dim de As DataEngine
    de = new DataEngine()
    de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
    de.StartDate=new DateTime (2002,1,1,0,0,0)
    de.EndDate = new DateTime (2002,12,31,23,59,59)
    de.DateGrouping = TimeInterval.Year
	de.SqlStatement= "SELECT OrderDate,Total, Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"

	
	Dim sc As SeriesCollection 
    sc = de.GetSeries()
   'customize element settings
   'set the color for different customers in each month based on their total sale.
   Dim sr As dotnetCHARTING.Series
   Dim el As dotnetCHARTING.Element
	for each sr in sc  
		for each el in sr.Elements
      		if (el.YValue < 3500)
				el.Color = Color.Red      		
      		else if (el.YValue < 5000)
      			el.Color = Color.Yellow
      		else
      			el.Color = Color.Green    		
      		end if
      	Next el
	Next sr

    Chart.SeriesCollection.Add(sc)
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Data Engine Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>

</html>
