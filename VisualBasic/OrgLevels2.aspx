<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using the Series.Trim method to figure out the organizational level each node is on.

		Chart.Size = "900x300"
		Chart.TempDirectory = "temp"
		Chart.Type = ChartType.Organizational
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255), Color.Orange }
		Chart.LegendBox.Background.Color = Color.FromArgb(240, 240, 240)

		' Set styling defaults.
		Chart.ChartArea.ClearColors()
		Chart.DefaultSeries.Line.Color = Color.Gray
		Chart.DefaultSeries.Line.Width = 3
		Chart.DefaultSeries.LegendEntry.Visible = False

		' Style the default annotation.
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Padding = 5
		Chart.DefaultElement.Annotation.Size = New Size(75, 30)
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Background2

		' Get Data
		Dim mySC As SeriesCollection = getLiveData()

		' Trim the data to get organizational levels and other attributes.
		Dim result As Series = mySC(0).Trim(20)

		For Each el As Element In result.Elements
			' Each node takes a color set based on the organizational level.
			If Not el.CustomAttributes("OrganizationalLevel") Is Nothing Then
				If el.Annotation Is Nothing Then
				el.Annotation = New Annotation()
				End If
				el.Annotation.Background.Color = Chart.Palette((CInt(Fix(el.CustomAttributes("OrganizationalLevel")))) - 1)
			End If
		Next el

		' Create legendbox
		Chart.LegendBox.Visible = True
		Chart.LegendBox.Position = LegendBoxPosition.Middle
		Chart.LegendBox.Padding = 5
		For i As Integer = 0 To 4
			Chart.LegendBox.ExtraEntries.Add(New LegendEntry("Level " & (i + 1), "", Chart.Palette(i)))
		Next i

		' Add the data
		Chart.SeriesCollection.Add(result)
	End Sub

	Function getLiveData() As SeriesCollection
		' Obtain series data from the database. Note this was created by adding PID (parentid)
		' to an existing table only, then populating the parentid based on the hiearchy in place.
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.SqlStatement = "SELECT * FROM Employees"
		de.DataFields = "InstanceID=ID,InstanceParentID=PID,Name=Name,office,Department,Email,Phone,Picture"
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
