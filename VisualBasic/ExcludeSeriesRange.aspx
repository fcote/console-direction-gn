<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates excluding a series from the axis range.
		' Fullscreen
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.ChartArea.Label.Text = "Series 1 is not used " & Constants.vbLf & "in the Y axis range."

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

		mySC(0).ExcludeFromAxisRange = True
		mySC(0)(0).YValue = 200
		mySC(0)(1).YValue = 40
		mySC(0)(2).YValue = 100
		mySC(0)(3).YValue = 40
		mySC(0).Type = SeriesType.Line


		Chart1.TempDirectory = "temp"
		Chart1.Debug = True
		Chart1.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart1.Type = ChartType.Combo
		Chart1.Size = "600x350"
		Chart1.Title = ".netCHARTING Sample"
		Chart1.ChartArea.Label.Text = "Series 1 is used " & Constants.vbLf & "in the Y axis range."
		Dim mySC2 As SeriesCollection = getRandomData()

		' Add the random data.
		Chart1.SeriesCollection.Add(mySC2)

		mySC2(0)(0).YValue = 200
		mySC2(0)(1).YValue = 40
		mySC2(0)(2).YValue = 100
		mySC2(0)(3).YValue = 40
		mySC2(0).Type = SeriesType.Line
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
		<dotnet:Chart ID="Chart1" runat="server" />
	</div>
</body>
</html>
