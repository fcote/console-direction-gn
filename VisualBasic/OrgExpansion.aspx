<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates interactive organizational chart expansion using the AJAX zoomer.

		' Apply some styling settings.
		Chart.Size = "850x550"
		Chart.Type = ChartType.Organizational
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Zoomer.Enabled = True
		Chart.ChartArea.Background.Color = Color.FromArgb(227, 236, 255)
		Chart.ChartArea.Padding = 25

		' Node connecting line styling.
		Chart.DefaultSeries.Line.Width = 2
		Chart.DefaultSeries.DefaultElement.Color = Color.Gray

		' Style the default annotation.
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Size = New Size(145, 50)
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.DefaultElement.Annotation.Background.Color = Color.DeepSkyBlue
		Chart.DefaultElement.Annotation.Label.Font = New Font("Tahoma", 8, FontStyle.Bold)
		Chart.DefaultElement.Annotation.DefaultCorner = BoxCorner.Round

		Dim lastClicked As Integer = 1

		' This sample will maintain a comma delimited list of node IDs that are expanded.
		' By default the main node is expanded showing 3
		Dim expandedList As String = "1"

		' Get the query string
		If Page.Request.QueryString.Count > 0 Then
			Dim query As String = Page.Request.QueryString("id")
			If Not query Is Nothing AndAlso query.Length > 0 Then
				lastClicked = Integer.Parse(query)
			End If

			query = Page.Request.QueryString("list")
			If Not query Is Nothing AndAlso query.Length > 0 Then
				expandedList = query.ToString()
			End If
		End If

		' Each node's URL will append its ID to the expandedList.
		' The ID of the clicked node is also passed so the last clicked node can be highlighted.
		Chart.DefaultElement.Annotation.URL = "?id=%id&list=" & expandedList & ",%id"
		Chart.DefaultElement.Annotation.Label.Text = "<Chart:Image src='../../images/add.png'>  %Name "

		' Get the organizational nodes.
		Dim myS As Series = getOrgData()

		' By passing the comma delimited list of IDs to the trim method, it will return only the nodes that should appear on the chart.
		' When the addAttributes parameter is true, the trim method will also mark some nodes with custom attributes describing them.
		Dim [partial] As Series = myS.Trim(expandedList, True)

		Dim listArray As String() = expandedList.Split(New String() { "," }, StringSplitOptions.RemoveEmptyEntries)

		' The following loop will iterate the elements and modify them based on custom attributes added by the trim method and other conditions.  
		For Each el As Element In [partial].Elements

			'If node was clicked last, set a different color
			If lastClicked = el.InstanceID AndAlso lastClicked <> 1 Then
				If el.Annotation Is Nothing Then
				el.Annotation = New Annotation()
				End If
				el.Annotation.Background.Color = Color.Aqua
			End If
			' If node is expanded, use a remove icon and pass an expansion list without this element's ID.
			If isInList(el.InstanceID, listArray) AndAlso el.InstanceID <> 1 Then
				If el.Annotation Is Nothing Then
				el.Annotation = New Annotation()
				End If
				el.Annotation.URL = "?id=%id&list=" & removeFromList(el.InstanceID, listArray)
				el.Annotation.Label.Text = "<Chart:Image src='../../images/remove.png'> %Name "
			End If
			' If it's the root node, or the node has no children to expand, don't link it or show expand/contract icon.
			If el.CustomAttributes("NodeType") = "Root" OrElse el.CustomAttributes("NodeType") = "NoChildren" Then
				If el.Annotation Is Nothing Then
				el.Annotation = New Annotation()
				End If
				el.Annotation.URL = ""
				el.Annotation.Label.Text = "%Name "
			End If
		Next el

		' Add the random data.
		Chart.SeriesCollection.Add([partial])
	End Sub


	Function removeFromList(ByVal id As Integer, ByVal list As String()) As String
		Dim newList As String = ""
		For i As Integer = 0 To list.Length - 1
			Dim lid As String = list(i)
			If Integer.Parse(lid) <> id Then
				newList &= lid
				If i < list.Length - 1 Then
				newList &= ","
				End If
			End If
		Next i
		Return newList
	End Function

	Function isInList(ByVal id As Integer, ByVal list As String()) As Boolean
		For Each lid As String In list
			If Integer.Parse(lid) = id Then
			Return True
			End If
		Next lid
		Return False
	End Function

	Function getOrgData() As Series
		Dim e As Element = New Element(ranks(0))
		Dim s As Series = New Series()
		e.InstanceID = c
		c += 1
		s.Elements.Add(e)
		s.Elements.Add(getNextLevel(e, 1, 6, 2).Elements)
		Return s
	End Function

	Dim c As Integer = 1
	Dim lIndexes As Integer() = New Integer() { 1, 1, 1, 1, 1, 1, 1 }
	Dim index As Integer = 0
	Dim ranks As String() = New String() { "General of the Army", "General", "Lieutenant General", "Major General", "Brigadier General", "Colonel", "Lieutenant Colonel", "Major", "Captan", "First Lieutenant", "Second Lieutenant" }

	Function getNextLevel(ByVal parent As Element, ByVal level As Integer, ByVal maxLevel As Integer, ByVal cPerNode As Integer) As Series
		If level < maxLevel Then
			Dim s As Series = New Series()
			For i As Integer = 0 To cPerNode - 1
				Dim e As Element = New Element()
				e.InstanceID = c
				c += 1
				e.Name = ranks(level) & " " & lIndexes(level)
				lIndexes(level) += 1
				e.InstanceParentID = parent.InstanceID
				s.Elements.Add(e)
				s.Elements.Add(getNextLevel(e, level + 1, maxLevel, cPerNode).Elements)
			Next i
			Return s
		Else
			Return New Series()
		End If
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
