<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs )

	'set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
    Chart.Title="Orders By Customers"
    Chart.XAxis.Label.Text="Customers"
    Chart.TempDirectory="temp"
    Chart.Debug=true
           
    'set backgrounds 
    Chart.ChartArea.Background = new Background("../../Images/background.gif",BackgroundMode.ImageStretch)  
     
    
    'Add a series
    Chart.Series.Name="Orders"
    Chart.Series.SqlStatement= "SELECT Name,Sum(1) FROM Orders GROUP BY Name ORDER BY Sum(1) DESC"
    Chart.SeriesCollection.Add()
    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Background Image Stretch Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
