<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates the effect of AxisMarkers with IncludeInAxisScale = true. It also shows the chart used as slideshow like movie.

		chart.Size = "600x450"

		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255), Color.FromArgb(255, 255, 0) }
		chart.DefaultSeries.Type = SeriesType.Line
		chart.DefaultElement.Marker.Visible = False
		chart.LegendBox.Visible = False
		chart.YAxis.Scale = Scale.Range
		chart.DefaultSeries.LegendEntry.Value = ""
		chart.ChartArea.Label.Text = "<row>The <block fColor='Red'>red axis marker<block> at Y value of 200 will always be included in the dynamic y range" '<row> not a new row.";
		chart.Navigator.ControlID = "slPlugin"
		chart.Navigator.Enabled = True

		Dim am As AxisMarker = New AxisMarker("", Color.Red, 200)
		am.IncludeInAxisScale = True
		chart.YAxis.Markers.Add(am)


		Dim ca2 As ChartArea = New ChartArea()
		ca2.YAxis = New Axis()
		ca2.YAxis.Scale = Scale.Range
		ca2.SeriesCollection.Add(getRandomData())

		chart.ExtraChartAreas.Add(ca2)

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx
		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2010, 1, 1)


		Dim currentValue As Double = 100
		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 104
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			currentValue += myR.Next(5)
			e.YValue = currentValue
			s.Elements.Add(e)
			' e.CustomAttributes.Add("phone", "2345-543");
		Next b
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
	<style type="text/css">
	div, p,td
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: x-small;
	}
</style>
	<script type="text/javascript">
		var chartID = "slPlugin";

		// For more information on this code and a client side API reference, see this tutorial: 
		// http://www.dotnetcharting.com/documentation/v6_0/Silverlight_Navigator_API.html

		function PlayMovie() {
			var control = document.getElementById(chartID);
			control.Content.Chart.NavigateToVisibleRangePerc(0, .2, 2000);
			document.getElementById('InfoDiv').innerHTML = "The top chart will encompass the green line and the red axis marker.";
			setTimeout("MoveToMiddle()", 4000);

		}

		function MoveToMiddle() {
			var control = document.getElementById(chartID);
			document.getElementById('InfoDiv').innerHTML = "Both chart's scales will be similar.";
			control.Content.Chart.NavigateToVisibleRangePerc(.4, .6, 3000);
			setTimeout("MoveToRight()", 5000);
		}

		function MoveToRight() {
			var control = document.getElementById(chartID);
			document.getElementById('InfoDiv').innerHTML = "The top chart will encompass the green line and the red axis marker below it.";
			control.Content.Chart.NavigateToVisibleRangePerc(.8, 1, 3000);
		}

	</script>
</head>
<body>
   <table align="center" width="600px">
   <tr>
   <td><a href="Javascript:PlayMovie();">Start Demo</a>
   </td>
   </tr><tr>
   <td> <div id="InfoDiv" > The entire Y scale range can be seen.</div>
   </td>
   </tr></table>
	<div align="center">
		<dnc:Chart ID="chart" runat="server" />


	</div>


</body>
</html>
