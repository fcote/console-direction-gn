<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates the different behavior between normal and range scales.

		chart.Size = "600x650"

		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255), Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0) }
		chart.DefaultSeries.Type = SeriesType.Line
		chart.LegendBox.Position = LegendBoxPosition.ChartArea
		chart.YAxis.Scale = Scale.Normal
		chart.DefaultElement.Marker.Visible = False
		chart.ChartArea.Label.Text = "Normal Scale will always snap to 0."
		chart.YAxis.Label.Text = "Normal Scale"
		chart.Navigator.Enabled = True
		chart.DefaultChartArea.Padding = 6
		chart.DefaultSeries.Line.Width = 2

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		chart.SeriesCollection.Add(mySC)


		Dim ca1 As ChartArea = getNewCA()
		ca1.Label.Text = "Range Scale will zoom in on the data."
		ca1.YAxis.Scale = Scale.Range
		ca1.YAxis.Label.Text = "Range Scale"
		chart.ExtraChartAreas.Add(ca1)

		Dim ca2 As ChartArea = getNewCA()
		ca2.Label.Text = "Range Scale with bars will snap to 0 to encompas the entire bar."
		ca2.YAxis.Scale = Scale.Range
		ca2.YAxis.Label.Text = "Range Scale"
		ca2.SeriesCollection(0).Type = SeriesType.Column
		chart.ExtraChartAreas.Add(ca2)

		chart.XAxis.Viewport.ScaleRange = New ScaleRange(1, 2)
	End Sub

	Function getNewCA() As ChartArea
		Dim ca As ChartArea = New ChartArea()
		ca.SeriesCollection.Add(getRandomData())
		Return ca
	End Function

	Function mathFunction(ByVal x As Double) As Double
		Return Math.Sin(x)
	End Function

	Function getRandomData() As SeriesCollection
		Dim s As Series = New Series("Series ")
		For b As Integer = 1 To 199

			Dim e As Element = New Element()
			e.YValue = mathFunction(b / 10R)*100
			e.XValue = b / 10R
			s.Elements.Add(e)
		Next b
		Return New SeriesCollection(s)
	End Function


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="chart" runat="server" />
	</div>
</body>
</html>
