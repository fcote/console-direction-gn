<%@ Page Language="vb" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	'set global properties
	Chart.Size="1400X500"
	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
	Chart.Title=".netCHARTING Version Releases"
	Chart.TempDirectory="temp"
	Chart.Type=ChartType.Scatter
	Chart.XAxis.Scale = Scale.Time
	Chart.Debug=True
	Chart.LegendBox.Template = "%Icon %Name"
	Chart.ChartArea.Label.Text = "Click a version to view the version history at the .netCHARTING web site."
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend

	'Set Axis Interval and Minimum values
	Chart.YAxis.Interval = 1
	Chart.YAxis.Minimum = 1

	'Set xAxis Minimum and Maximum values
	Chart.XAxis.Minimum = New DateTime(2002,11,1)
        Chart.XAxis.Maximum = New DateTime(2010, 7, 30)
	Chart.XAxis.FormatString="MMM, yyyy"

	'Format date display on X Axis
	Chart.XAxis.TimeInterval = TimeInterval.Months

	'Show values for all elements across all series and set marker size to 15
	Chart.DefaultSeries.DefaultElement.Marker.Size=7
	Chart.DefaultSeries.DefaultElement.ShowValue=True

	'Use custom attribute pulled from db, version, and inline format the date in the template
	Chart.DefaultSeries.DefaultElement.SmartLabel.Text = "%Version Released <%XValue,MM/dd/yy>"

	'Link all elements to the version history page and set a tooltip for them as well
	Chart.DefaultSeries.DefaultElement.URL = "http://www.dotnetcharting.com/versionhistory.aspx"
	Chart.DefaultSeries.DefaultElement.ToolTip = "View Version History Page"

	'Add a series, define a custom attribute for version and splitby for version creating a series for each
	Chart.Series.Type = SeriesType.Marker
	Chart.Series.SqlStatement= "SELECT ReleaseDate,ReleaseNumber,Version FROM Versions"
	Chart.Series.DataFields="YValue=ReleaseNumber,XValue=ReleaseDate,SplitBy=Version,Version"
	Chart.SeriesCollection.Add()
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>.netCHARTING Releases</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
