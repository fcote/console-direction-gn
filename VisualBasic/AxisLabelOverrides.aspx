<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   Chart.Title = "Item sales"
   Chart.XAxis.Label.Text = "months"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Use3D = True
   Chart.ShadingEffect = True
   
   Chart.DateGrouping = TimeInterval.Year
   
   'Chart.YAxis.ClearValues = true;
   Chart.YAxis.AddLabelOverride("3000", "Low season")
   Chart.YAxis.AddLabelOverride("4000", "High season")
   Dim am As New AxisMarker("Normal", New Background(Color.Pink))
   am.Value = 3500
   am.Line = New Line(Color.Green)
   
   Chart.YAxis.Markers.Add(am)
   
   'Add a series
   Chart.Series.Name = "Items"
   Chart.Series.Type = SeriesType.Line
   Chart.Series.StartDate = New System.DateTime(2002, 1, 1, 0, 0, 0)
   Chart.Series.EndDate = New System.DateTime(2002, 12, 31, 23, 59, 59)
   Chart.Series.SqlStatement = "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
   Chart.SeriesCollection.Add()
End Sub 'Page_Load

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Y Axis Label Overrides</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
