<%@ Page Language="VB" Description="dotnetCHARTING Component" debug = "true" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   'View available attributes for a given shape file
   Chart.Type = ChartType.Map
   Chart.TempDirectory = "temp"
   Dim layer As MapLayer = MapDataEngine.LoadLayer("../../Images/MapFiles/canada.shp")
   Dim attributename As String
   For Each attributename In  layer.AttributeNames
      Response.Write((attributename + "<br>"))
   Next attributename
   
   Chart.Mapping.MapLayerCollection.Add(layer)
   Chart.Mapping.ZoomPercentage = 90
End Sub 'Page_Load
</script>
	</head>
	<body>
		<div style="text-align:center">
				<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>