<%@ Page Language="VB" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   Dim customerName As String = Request.QueryString("name")
   'set global properties
   Chart.Title = "Orders by " + customerName
   Chart.ChartArea.XAxis.Label.Text = "Months"
   
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.ChartArea.XAxis.FormatString = "MMM"
   Chart.DateGrouping = TimeInterval.Year
   
   
   Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString") 
   
   Chart.DefaultSeries.StartDate = New System.DateTime(2002, 1, 1, 0, 0, 0)
   Chart.DefaultSeries.EndDate = New System.DateTime(2002, 12, 31, 23, 59, 59)
   
   
   'Add a series
   Chart.Series.Name = "Orders"
   Chart.Series.SqlStatement = "SELECT OrderDate, Sum(1) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# AND Name='" + customerName + "' GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate" 
   
   Chart.SeriesCollection.Add()
End Sub 'Page_Load 
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Orders Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
