<%@ Page Language="vb" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">


Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	'Demonstrates streaming a chart image directly to the browser with all HTML code.
	'Since streaming turns the page you include the control on into the actual
	' image file return, the browser will not also allow HTML within the same http
	'request. As a result url, tooltip and drilldown options will not be
	'enabled. Setting UseSession to true works around this issue by returning an
	'image reference to the same aspx page for image streaming. 

	Chart.Title="My Chart"
	Chart.TempDirectory="temp"
	Chart.DefaultSeries.DefaultElement.Hotspot.ToolTip="%Name: %Value"

	'Enable streaming
	Chart.UseFile =False
	Chart.UseSession=True

	' Change the shading mode
	Chart.ShadingEffectMode = ShadingEffectMode.Two

	' Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label"

	' Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label"

	' Set he chart size.
	Chart.Size = "600x350"


	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData())

End Sub

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 5
         Dim e As New Element()
         e.Name = "E " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   Return SC
End Function 'getRandomData
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Use session Sample</title></head>
<body>
<div style="text-align:center">
<dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
