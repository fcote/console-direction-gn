<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates a number of background styling options which used to fill boxes and other objects.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Type = ChartType.Combo
		Chart.Size = "500x480"
		Chart.ChartArea.ClearColors()
		Chart.ChartArea.Line.Color = Color.Black
		Chart.Palette = New Color() { Color.Transparent }
		Chart.DefaultShadow.Color = Color.Empty
		Chart.LegendBox.Visible = False
		Chart.DefaultAxis.ClearValues = True
		Chart.DefaultElement.Outline.Color = Color.Transparent

		Chart.DefaultBox.CornerBottomRight = BoxCorner.Round
		Chart.DefaultBox.CornerTopLeft = BoxCorner.Round

		Dim a1 As Annotation = getAnnotation("Shading One")
		Dim a2 As Annotation = getAnnotation("Shading Two")
		Dim a3 As Annotation = getAnnotation("Shading Three")
		Dim a4 As Annotation = getAnnotation("Shading Four")
		Dim a5 As Annotation = getAnnotation("Shading Five")
		Dim a6 As Annotation = getAnnotation("Bevel")
		Dim a7 As Annotation = getAnnotation("Brush")
		Dim a8 As Annotation = getAnnotation("Gradient")
		Dim a9 As Annotation = getAnnotation("Hatch")
		Dim a10 As Annotation = getAnnotation("Glass Effect")
		Dim a11 As Annotation = getAnnotation("Solid Color")
		Dim a12 As Annotation = getAnnotation("Image Background")
		Dim a13 As Annotation = getAnnotation("Shading Six")
		Dim a14 As Annotation = getAnnotation("Shading Seven")

		Dim a15 As Annotation = getAnnotation("Background 1")
		Dim a16 As Annotation = getAnnotation("Background 2")

		a1.Position = New Point(50, 40)
		a2.Position = New Point(50, 90)
		a3.Position = New Point(50, 140)
		a4.Position = New Point(50, 190)
		a5.Position = New Point(50, 240)
		a6.Position = New Point(250, 40)
		a7.Position = New Point(250, 90)
		a8.Position = New Point(250, 140)
		a9.Position = New Point(250, 190)
		a10.Position = New Point(250, 240)
		a11.Position = New Point(50, 290)
		a12.Position = New Point(250, 290)
		a13.Position = New Point(50, 340)
		a14.Position = New Point(250, 340)
		a15.Position = New Point(50, 390)
		a16.Position = New Point(250, 390)

		Chart.Annotations.Add(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12,a13,a14,a15,a16)

		a1.Background.ShadingEffectMode = ShadingEffectMode.One
		a2.Background.ShadingEffectMode = ShadingEffectMode.Two
		a3.Background.ShadingEffectMode = ShadingEffectMode.Three
		a4.Background.ShadingEffectMode = ShadingEffectMode.Four
		a5.Background.ShadingEffectMode = ShadingEffectMode.Five
		a13.Background.ShadingEffectMode = ShadingEffectMode.Six
		a14.Background.ShadingEffectMode = ShadingEffectMode.Seven
		a15.Background.ShadingEffectMode = ShadingEffectMode.Background1
		a16.Background.ShadingEffectMode = ShadingEffectMode.Background2

		a15.Background.Color = Color.BlanchedAlmond
		a16.Background.Color = Color.Azure

		a6.Background.Bevel = True

		Dim pgb As PathGradientBrush = New PathGradientBrush(New Point() { New Point(250, 90), New Point(130, 90), New Point(400, 130), New Point(250, 130) })
		pgb.SurroundColors = Chart.Palette
		pgb.CenterColor = Color.FromArgb(myRand.Next(120), myRand.Next(220), myRand.Next(255))
		pgb.CenterPoint = New PointF(275, 120)
		a7.Background = New Background(pgb)

		a8.Background = New Background(Color.FromArgb(myRand.Next(120), myRand.Next(220), myRand.Next(255)), Color.White, 90) 'SecondaryColor = Color.White;
		a9.Background.HatchColor = Color.White
		a9.Background.HatchStyle = HatchStyle.BackwardDiagonal
		a10.Background.GlassEffect = True
		a12.Background = New Background("../../images/back038.jpg")

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Dim myRand As Random = New Random(4)

	Function getAnnotation(ByVal text As String) As Annotation
		Dim an As Annotation = New Annotation()
		an.Label.Text = text
		an.Label.Font = New Font("Arial", 12, FontStyle.Bold)
		an.Label.OutlineColor = Color.White
		an.Background.Color = Color.FromArgb(myRand.Next(120), myRand.Next(220), myRand.Next(255))
		an.Size = New Size(190, 40)
		Return an
	End Function

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 1
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
