<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(500)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   ' This sample will demonstrate how grid positions can be specified for multiple chart areas.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' First we create a new chart area and add it.
   Dim ca1 As New ChartArea()
   Chart.ExtraChartAreas.Add(ca1)
   
   ' In order to use grid positions the custom layout mode must be specified.
   Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Custom
   
   ' The main chart area will be in position 0,0 (this is the default position)
   Chart.ChartArea.GridPosition = New Point(0, 0)
   ' The second chart area's position will be 1,1
   ca1.GridPosition = New Point(1, 1)
   
   
   ' Add the random a series each the custom chart area and main chart area.
   ca1.SeriesCollection.Add(mySC(0))
   Chart.SeriesCollection.Add(mySC(1))
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 1
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " & b
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
