<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<script runat="server">

		
Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
  
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx.
   Dim sc As SeriesCollection = getRandomData()
   
   'The data has one series, we want to sort it in ascending order by yValues.
   sc(0).Sort(ElementValue.YValue, "ASC")
   
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection ' Generate some random data.
   Dim myR As New Random()
   Dim SC As New SeriesCollection()
   
   Dim s As New Series()
   s.Name = "mySeries"
   Dim b As Integer
   For b = 1 To 4
      Dim e As New Element()
      e.Name = "Element " + CStr(b)
      e.YValue = myR.Next(50)
      s.Elements.Add(e)
   Next b
   
   SC.Add(s)
   Return SC
End Function 'getRandomData

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Sort Elements</title>

	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
            </dotnet:Chart>
		</div>
	</body>
</html>
