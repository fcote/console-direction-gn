<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Pareto Chart"
   
   Chart.DefaultSeries.DefaultElement.ShowValue = True
   
   ' This sample will demonstrate how create a pareto chart.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' First we need to sort the element in descending order by the y value.
   mySC(0).Sort(ElementValue.YValue, "DESC")
   
   ' For this sample to work correctly we need to specify an axis for our series and then pass it to the 
   ' getParetoSeries method.
   mySC(0).YAxis = New Axis()
   
   ' Change the column label alignment.
   mySC(0).DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC(0))
   Chart.SeriesCollection.Add(getParetoSeries(mySC(0), mySC(0).YAxis))
End Sub 'Page_Load
 
Function getParetoSeries(s As Series, a As Axis) As SeriesCollection
   ' This method will return 2 series in order to create the pareto chart.
   Dim result As New Series()
   Dim result2 As New Series()
   
   Dim sum As Double = s.Calculate("", Calculation.Sum).YValue
   Dim runningSum As Double = 0
   
   ' The first series will be the visible line, it's y values will be the running sum but we will set the labels
   ' to represent the percentages. The reason the series needs the running sum values is because we want it to start
   ' at the top of the first column and so it also needs to be on the same y axis as the original series.
   ' The second series will be there to relate to the percentage labels of the first series to a percent axis so we set those
   ' element values to the percent values.
   Dim el As Element
   For Each el In  s.Elements
      runningSum += el.YValue
      Dim curEl As New Element(el.Name, runningSum)
      curEl.SmartLabel.Text =(runningSum * 100 / sum).ToString("0") + "%"
      result.Elements.Add(curEl)
      result2.Elements.Add(New Element(el.Name, runningSum * 100 / sum))
   Next el 
   
   result.Type = SeriesType.Line
   ' The visible series needs to be on the specified axis.
   result.YAxis = a
   
   
   ' Add the percentage axis.
   result2.YAxis = New Axis()
   result2.YAxis.Percent = True
   result2.YAxis.Orientation = dotnetCHARTING.Orientation.Right
   
   ' In order for the two axes to match the first series y axis will need to end at the running sum of the values.
   result.YAxis.Maximum = sum
   
   ' We dont really want to show the series on the chart so we will set the element colors to empty and get rid of the
   ' legend entry.
   result2.Type = SeriesType.Marker
   result2.DefaultElement.Color = Color.Empty
   result2.LegendEntry.Visible = False
   
   ' Add the series to a collection and return it.
   Dim sc As New SeriesCollection()
   sc.Add(result)
   sc.Add(result2)
   
   Return sc
End Function 'getParetoSeries


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " & b
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
