<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Map 'Horizontal;
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   Chart.Visible = False
   Chart.ChartArea.Label.Text = "Please click on a point on this map"
   Chart.ChartArea.Background = New Background(Color.FromArgb(142, 195, 236), Color.FromArgb(63, 137, 200), 90)
   Chart.Mapping.DefaultShape.Background.Color = Color.LightGray
   
   
   ' This sample demonstrates adding a point to the chart based on lat/long coordinates of the click.
   ' FullScreen
   Chart.Mapping.MapLayerCollection.Add("../../images/MapFiles/primusa.shp")
   
   Dim bp As Bitmap = Nothing
   
   If Not (Page.Request.Params("y") Is Nothing) And Not (Page.Request.Params("x") Is Nothing) Then
      ' Generate the chart in order to get the correct coordinates from the click position.
      bp = Chart.GetChartBitmap()
      
      ' Get x and y points
      Dim x As Integer = Convert.ToInt32(Page.Request.Params("x"))
      Dim y As Integer = Convert.ToInt32(Page.Request.Params("y"))
      Dim p As PointF = Chart.Mapping.GetLatLongCoordinates((x.ToString() + "," + y.ToString()))
      
      ' Create a layer with a point at the clicked position.
      Dim layer2 As New MapLayer()
      layer2.AddLatLongPoint(p, New ElementMarker(ElementMarkerType.Circle, 8, Color.Red))
      Chart.Mapping.MapLayerCollection.Add(layer2)
      iLabel.Text += "<BR>Latitude: " + p.X.ToString() + " Longitude: " + p.Y.ToString()
   End If 
   
   ' Generate the chart again to show the added point.
   bp = Chart.GetChartBitmap()
   Dim fileName As String = Chart.FileManager.SaveImage(bp)
   
   imageLabel.Text += "<input type=image value=""submit"" border=0 src=""" + fileName + """ ISMAP>"
   bp.Dispose()
End Sub 'Page_Load

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
		<form method =get >
		<asp:Label ID=imageLabel Runat=server/>
			</form>
			<dotnet:Chart id="Chart" runat="server"/>
			<asp:Label ID="iLabel" Runat=server></asp:Label>
		</div>
	</body>
</html>
