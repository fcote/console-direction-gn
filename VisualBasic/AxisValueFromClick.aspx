<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	Dim Chart As Chart = New Chart()

	Chart.Type = ChartType.Combo
	Chart.Width = 800
	Chart.Height = 450
	Chart.TempDirectory = "temp"
	Chart.ChartArea.Label.Text = "Click a point on this chart."
	Chart.YAxis.Label.Text = "Numeric Axis"
	Chart.XAxis.Label.Text = "Time Axis"

	' This sample demonstrates getting the value of an axis when the chart is clicked.
	' This method does not use the chart as a control, it's used as a library to generate the image and displaying it on the page is handled in manually.

	' IMPORTANT NOTES:
	' 1. Chart must be generated in order to use GetValueAt_ methods.
	' 2. The chart must be in an image tag with ISMAP in the tag.
	' 3. If the chart area size changes between the chart that was clicked and the one that is generated in order to get the axis values, the results will be inaccurate.

	Chart.DefaultSeries.Type = SeriesType.Line
	Chart.DefaultElement.Marker.Visible = False
	Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom


	' Add the random data.	
	Dim sc As SeriesCollection = getRandomData()
	Chart.SeriesCollection.Add(sc)


	' IMPORTANT: In order for the Axis.GetValueAt_ Methods to work, the chart must be generated first.
	' Generate the chart.
	Dim bp As Bitmap = Chart.GetChartBitmap()

	' Process chart clicks and create zoom chart area.
	If Page.Request.QueryString.Count > 0 Then
		Dim query As String = Page.Request.QueryString(0)
		If query.IndexOf("?") > -1 Then
		query = query.Substring(query.LastIndexOf("?")+1)
		End If

		' From the query string, get the values of the axes where clicked.
		Dim xval As Object = Chart.XAxis.GetValueAtX(query)
		Dim yval As Object = Chart.YAxis.GetValueAtY(query)

		clickLabel.Text = "<BR>X Axis Value = " & xval & "<BR>Y Axis Value = " & yval

		' Add axis markers at click positions.
		If Not xval Is Nothing Then
			Chart.XAxis.Markers.Add(New AxisMarker("",Color.Orange,CDate(xval)))
		End If
		If Not yval Is Nothing Then
			Chart.YAxis.Markers.Add(New AxisMarker("",Color.Red,CDbl(yval)))
		End If


		' Generate the chart once more to show the axis markers that were added above.
		  bp = Chart.GetChartBitmap()
	End If

	Dim fileName As String = Chart.FileManager.SaveImage(bp)
	imageLabel.Text = "<img border = 0 src=""" & fileName & """ ISMAP>"

End Sub



Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random(1)
	Dim dt As DateTime = New DateTime(2006,1,1)
	For a As Integer = 0 To 0
		Dim s As Series = New Series()
		s.Name = "Series " & a
		For b As Integer = 0 To 199
			Dim e As Element = New Element()
			'e.Name = "Element " + b;
			'e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(20)+20
			s.Elements.Add(e)
			dt = dt.AddDays(1)
			e.XDateTime = dt

		Next b
		SC.Add(s)
	Next a


	Return SC
End Function
		</script>
	</head>
	<body bgcolor=#ffffff>
	<a href="">
			<asp:Label ID="imageLabel" Runat="server" /></a>
			<asp:Label ID="clickLabel" Runat="server" />
	</body>
</html>
