<%@ Page Language="vb" Debug="true" Trace="false" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>

<script runat="server">
	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

		' This sample demonstrates manual drilldown with previews using the same chart page.

		Dim sc As SeriesCollection = Nothing

		'set global properties
		Chart.Size = "650x400"
		Chart.Title = "Item sales"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.ShadingEffectMode = ShadingEffectMode.Two
		Chart.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.Padding = 6
		Chart.TitleBox.Background.Color = Color.Chocolate
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background1
		Chart.TitleBox.Label.Color = Color.White
		Chart.LegendBox.DefaultEntry.LabelStyle.Color = Color.White
		Chart.LegendBox.DefaultEntry.LabelStyle.Font = New Font("Arial", 12, FontStyle.Bold)
		Chart.LegendBox.Template = "%Value"
		Chart.LegendBox.DefaultEntry.Value = "%YSum"

		' Style/Format XAxis
		Chart.XAxis.Label.Alignment = StringAlignment.Near
		Chart.YAxis.FormatString = "Currency"
		Chart.XAxis.DefaultTick.Line.Length = 6
		Chart.XAxis.DefaultTick.Line.Width = 2

		' Element defaults.
		Chart.DefaultElement.ShowValue = True
		Chart.DefaultElement.SmartLabel.Text = "<block fStyle='bold' fSize='12' >%YValue"
		Chart.DefaultElement.SmartLabel.Alignment = LabelAlignment.OutsideTop
		Chart.DefaultElement.SmartLabel.AlignmentSecondary.Insert(0, LabelAlignment.Top)
		Chart.DefaultElement.DefaultSubValue.Visible = False

		' Initial Drilldown position
		Dim curTimeInterval As dotnetCHARTING.TimeInterval = TimeInterval.Quarters

		' Get QueryString values
		Dim dateGrouping As String = HttpContext.Current.Request.QueryString("dategrouping")
		If dateGrouping Is Nothing OrElse dateGrouping = "" Then
			dateGrouping = "Years"
		ElseIf Not dateGrouping Is Nothing AndAlso dateGrouping <> "" Then
			curTimeInterval = CType(System.Enum.Parse(GetType(dotnetCHARTING.TimeInterval), dateGrouping, True), dotnetCHARTING.TimeInterval)
		End If

		Dim startDateString As String = HttpContext.Current.Request.QueryString("startDate")
		Dim startDate As DateTime = New DateTime(2002, 1, 1, 0, 0, 0)
		If Not startDateString Is Nothing AndAlso startDateString <> "" Then
			startDate = DateTime.Parse(startDateString)
		End If

		Dim endDate As DateTime = New DateTime(2002, 12, 31, 23, 59, 59)
		Dim ser As Series
		Dim groups As Series

		' If this page load is streaming for a tooltip preview, specify some extra options.
		Dim stream As String = HttpContext.Current.Request.QueryString("stream")
		If Not stream Is Nothing AndAlso stream = "true" Then
			Chart.UseFile = False
			Chart.Background.Color = Color.Transparent
			Chart.TitleBox.Visible = False
			Chart.LegendBox.Visible = False
			Chart.TitleBox.Position = TitleBoxPosition.None
			Chart.DefaultAxis.DefaultTick.Label.Font = New Font("Arial", 12)
			Chart.DefaultElement.SmartLabel.Text = "<block fStyle='bold' fSize='13' >%YValue"
		End If

		' Get Data and Create a chart based on the drilldown parameters.
		Select Case curTimeInterval
			Case TimeInterval.Years

				' Setup a title and labels.
				Chart.Title = "<block fStyle='bold' fSize='11' hAlign='left'> Showing All<row><block fSize='7'> "
				Chart.XAxis.DefaultTick.Label.Text = "<block fStyle='bold' fSize='12'>%Name"

				' Get live data based on date range.
				ser = getLiveData(startDate, endDate)

				' Break the elements down into years.
				groups = ser.SplitRegroupCalculate(TimeIntervalAdvanced.Year, TimeIntervalAdvanced.Year, Calculation.Sum, "yyyy", False)
				sc = New SeriesCollection(groups)
				If Not sc Is Nothing AndAlso sc.Count > 0 Then
					For Each el As Element In sc(0).Elements
						' Set drilldown URL for each element.
						el.URL = "?dategrouping=quarters&startDate=" & startDate.ToString()
						' Set the drilldown preview tooltip.
						el.ToolTip = "<Chart:Image src='DrillDownPreview2.aspx" & el.URL & "&stream=true' size='325x200'>"
						startDate = startDate.AddYears(1)
					Next el
				End If
			Case TimeInterval.Quarters

				' Setup a title and labels.
				Chart.Title = "<block fStyle='bold' fSize='11' hAlign='left'> " & startDate.Year & "<row><block fSize='7'> "
				Chart.DefaultElement.SmartLabel.Text = "<block fStyle='bold' fSize='11' >%YValue"
				Chart.XAxis.DefaultTick.Label.Text = "<block fStyle='bold' fSize='10'>%Name"

				' Get live data based on date range.
				ser = getLiveData(startDate, endDate)

				' Break the elements down into Quarters.
				groups = ser.SplitRegroupCalculate(TimeIntervalAdvanced.Year, TimeIntervalAdvanced.Quarters, Calculation.Sum, "", False)
				sc = New SeriesCollection(groups)

				If Not sc Is Nothing AndAlso sc.Count > 0 Then
					Dim i As Integer = 1
					For Each el As Element In sc(0).Elements
						' Set drilldown URL for each element.
						el.URL = "?dategrouping=months&startDate=" & startDate.ToString()
						' Set the drilldown preview tooltip.
						el.ToolTip = "<Chart:Image src='DrillDownPreview2.aspx" & el.URL & "&stream=true' size='300x200'>"

						el.Name = "(Q" & i.ToString() & ") " & startDate.ToString("MMM") & " - " & startDate.AddMonths(2).ToString("MMM")
						startDate = startDate.AddMonths(3)
						i += 1
					Next el
				End If
			Case TimeInterval.Months

				' Setup a title and labels.
				Chart.Title = "<block fStyle='bold' fSize='11' hAlign='left'> " & startDate.Year & "<row><block fSize='7'> "
				Chart.XAxis.DefaultTick.Label.Text = "<block fStyle='bold' fSize='10'>%Name"

				' Get live data based on date range.
				endDate = startDate.AddMonths(3).AddSeconds(-1)
				ser = getLiveData(startDate, endDate)

				' Break the elements down into Months.
				groups = ser.SplitRegroupCalculate(TimeIntervalAdvanced.Quarters, TimeIntervalAdvanced.Months, Calculation.Sum, "MMMM", False)
				sc = New SeriesCollection(groups)

				If Not sc Is Nothing AndAlso sc.Count > 0 Then
					For Each el As Element In sc(0).Elements
						' Set drilldown URL for each element.
						el.URL = "?dategrouping=days&startDate=" & startDate.ToString()
						' Set the drilldown preview tooltip.
						el.ToolTip = "<Chart:Image src='DrillDownPreview2.aspx" & el.URL & "&stream=true' size='300x200'>"
						startDate = startDate.AddMonths(1)
					Next el
				End If
			Case TimeInterval.Days

				' Setup a title and labels.
				Chart.XAxis.Label.Text = "<Chart:Spacer size='5,10'><row><block fStyle='bold' fSize='12'>" & startDate.ToString("MMMM") & "  " & startDate.Year
				Chart.Title = "<block fStyle='bold' fSize='11' hAlign='left'> " & startDate.ToString("MMMM") & "  " & startDate.Year & "<row><block fSize='7'> "

				' Setup a time x axis scale.
				Chart.XAxis.TimePadding = New TimeSpan(10, 0, 0)
				Chart.XAxis.Scale = Scale.Time
				Chart.XAxis.DefaultTick.Line.Width = 1
				Chart.XAxis.DefaultTick.Line.Length = 3
				Chart.XAxis.FormatString = "d "
				Chart.XAxis.TimeInterval = TimeInterval.Day

				' Style the element appearance.
				Chart.DefaultElement.ShowValue = False
				Chart.DefaultSeries.Type = SeriesType.AreaLine
				Chart.DefaultElement.Marker.Size = 10
				Chart.DefaultElement.Transparency = 20
				Chart.DefaultSeries.Line.Width = 9

				' Get live data based on date range.
				endDate = startDate.AddMonths(1).AddSeconds(-1)
				ser = getLiveData(startDate, endDate)

				' Break the elements down into Days.
				groups = ser.SplitRegroupCalculate(TimeIntervalAdvanced.Months, TimeIntervalAdvanced.Days, Calculation.Sum, "d", False)
				sc = New SeriesCollection(groups)

				If Not sc Is Nothing AndAlso sc.Count > 0 Then
					For Each el As Element In sc(0).Elements
						' Normalize the elements x date value.
						el.XDateTime = New DateTime(el.XDateTime.Year, el.XDateTime.Month, el.XDateTime.Day)
						' Set drilldown URL for each element.
						el.URL = "?dategrouping=hours&startDate=" & startDate.ToString()
						' Set the drilldown preview tooltip.
						el.ToolTip = "<Chart:Image src='DrillDownPreview2.aspx" & el.URL & "&stream=true' size='300x200'>"
						startDate = startDate.AddDays(1)
					Next el
				End If
			Case TimeInterval.Hours

				' Setup a title and labels.
				Chart.Title = "<block fStyle='bold' fSize='11' hAlign='left'> " & startDate.ToString("D") & "<row><block fSize='7'> "
				Chart.XAxis.Label.Text = "<Chart:Spacer size='5,10'><row><block fStyle='bold' fSize='12'>Hours"

				' Style the element appearance.
				Chart.DefaultSeries.Type = SeriesType.AreaLine
				Chart.DefaultElement.Marker.Size = 10
				Chart.DefaultElement.Transparency = 20
				Chart.DefaultSeries.Line.Width = 9
				Chart.DefaultElement.SmartLabel.Line.Width = 3
				Chart.DefaultElement.SmartLabel.Line.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor
				Chart.DefaultElement.SmartLabel.Line.AnchorCapScale = 2
				Chart.DefaultElement.SmartLabel.Line.Color = Color.FromArgb(150, 101, 39, 19)
				Chart.DefaultElement.SmartLabel.Padding = 4
				Chart.DefaultSeries.EmptyElement.Mode = EmptyElementMode.TreatAsZero

				' Style the x axis.
				Chart.XAxis.DefaultTick.Line.Width = 1
				Chart.XAxis.DefaultTick.Line.Length = 3

				' Get live data based on date range.
				Dim de As DataEngine = getDE(curTimeInterval, startDate)
				endDate = startDate.AddDays(1)
				endDate = endDate.AddSeconds(-1)
				de.EndDate = endDate
				sc = de.GetSeries()

				If Not sc Is Nothing AndAlso sc.Count > 0 Then
					For Each el As Element In sc(0).Elements
						' Dont label 0 value elements.
						If Double.IsNaN(el.YValue) Then
							el.SmartLabel.Text = ""
						End If
					Next el
				End If
		End Select
		Chart.SeriesCollection.Add(sc)
	End Sub

	Function getDE(ByVal dg As TimeInterval, ByVal st As DateTime) As DataEngine
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"), "SELECT OrderDate,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.DateGrouping = dg
		de.StartDate = st
		Return de
	End Function

	Function getLiveData(ByVal start As DateTime, ByVal [end] As DateTime) As Series
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.StartDate = start
		de.EndDate = [end]
		de.SqlStatement = "SELECT OrderDate,Total,Name,Quantity FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# "
		de.DataFields = "xAxis=OrderDate,yAxis=Total"
		Return de.GetSeries()(0)
	End Function


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Drill Down Manual Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
