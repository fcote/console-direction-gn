<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Size = "600X300"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   
   
   ' This sample will demonstrate how to align text inside annotations.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim mySC As SeriesCollection = getData()
   
   ' Create an annotation.
   Dim a1 As New Annotation("Top-Right Aligned Text")
   
   ' Set the horizontal alignment
		        a1.Label.Alignment = StringAlignment.Far
   
   ' Set the Vertical Alignment alignment
		        a1.Label.LineAlignment = StringAlignment.Near
   
   mySC(0).Elements(0).Annotation = a1
   
   
   ' And do the same for two others
   Dim a2 As New Annotation("Bottom-Right Aligned Text")
		        a2.Label.Alignment = StringAlignment.Far
		        a2.Label.LineAlignment = StringAlignment.Far
   mySC(0).Elements(1).Annotation = a2
   
   Dim a3 As New Annotation("Center Aligned Text")
		        a3.Label.Alignment = StringAlignment.Center
		        a3.Label.LineAlignment = StringAlignment.Center
   mySC(0).Elements(2).Annotation = a3
   
   
   Dim a4 As New Annotation("Top-Left Aligned Text")
		        a4.Label.Alignment = StringAlignment.Near
		        a4.Label.LineAlignment = StringAlignment.Near
   mySC(0).Elements(3).Annotation = a4
   
   Dim a5 As New Annotation("Bottom-Left Aligned Text")
		        a5.Label.Alignment = StringAlignment.Near
		        a5.Label.LineAlignment = StringAlignment.Far
   mySC(0).Elements(4).Annotation = a5
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 


Function getData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
		        ' Dim a As Integer
  
      Dim s As New Series()
      s.Name = "Series 1" 
      Dim e As New Element()
      e.Name = "Element 1"
      e.YValue = 5
      s.Elements.Add(e)
      
      e = New Element()
      e.Name = "Element 2"
      e.YValue = 7
      s.Elements.Add(e)
      
      e = New Element()
      e.Name = "Element 3"
      e.YValue = 3
      s.Elements.Add(e)
      
      e = New Element()
      e.Name = "Element 4"
      e.YValue = 6
      s.Elements.Add(e)
      
      e = New Element()
      e.Name = "Element 5"
      e.YValue = 3
      s.Elements.Add(e)
      
      SC.Add(s)
   
   Return SC
End Function 'getData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
