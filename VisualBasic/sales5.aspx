<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs )

	'set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.Title="Sales"
    Chart.XAxis.Label.Text="Months"
   Chart.YAxis.Label.Text="Sales (USD)"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.XAxis.ReverseSeries =true
    
    Chart.DateGrouping = TimeInterval.Year
    Chart.Use3D=true
    
     Dim b As Axis
     b=Chart.YAxis.Calculate("Sales (CAD)", New ChangeValueDelegate(AddressOf MyFunction))
	b.Orientation = dotnetCHARTING.Orientation.Right
	Chart.AxisCollection.Add(b)


    'Add a series
    Chart.Series.Name="sales"
    Chart.Series.StartDate= New System.DateTime(2002,1,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,12,31,23,59,59)
    Chart.Series.SqlStatement= "SELECT OrderDate, Total FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
    Chart.Series.Type = SeriesType.Cylinder
    Chart.DefaultSeries.DefaultElement.Transparency=40
    Chart.Series.DefaultElement.ShowValue=true
    Chart.SeriesCollection.Add()
    
    'Add average series
    Chart.Series.Name = "Average"
    Chart.Series.Type = SeriesType.AreaLine
    Chart.SeriesCollection.Add(Calculation.RunningAverage)
    
    'Add sum series
    Chart.Series.Name = "sum"
    Chart.Series.Type = SeriesType.AreaLine
    Chart.SeriesCollection.Add(Calculation.RunningSum)
End Sub
public Shared Function  MyFunction( value As String) As String
	Dim dValue As double
	dValue = Convert.ToDouble(value)
	dValue = dValue*1.36
	return Convert.ToString(dValue)
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Sales Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
