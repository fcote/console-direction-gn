<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Adding axis ticks at arbitrary positions."
   
   ' * INTRO *
   ' This sample will demonstrate how how ticks can be added at any position on an axis scale.
   ' A numeric tick is added to the y axis and a DateTime value tick is added to the x axis.
   ' 1. CREATE AN AXIS TICK WITH VALUE OF 15
   Dim at As New AxisTick(15)
   at.Label.Color = Color.Red
   ' Add it to the axis.
   Chart.YAxis.ExtraTicks.Add(at)
   ' Interval is set for demo purposes so the automatic ticks dont interfere with the custom tick.
   Chart.YAxis.Interval = 10
   
   
   ' 2. CREATE AN AXIS TICK WITH VALUE OF Jan 17 2005
   Dim at2 As New AxisTick(New DateTime(2005, 1, 17))
   at2.Label.Color = Color.Red
   Chart.XAxis.ExtraTicks.Add(at2)
   ' Set a format string to include the day part of the date.
   Chart.XAxis.FormatString = "MMM d"
   
   ' 3. GET DATA
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. For information on acquiring 
   ' database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2005, 1, 1)
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.YValue = myR.Next(40)
         e.XDateTime = dt
         dt = dt.AddMonths(1)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
