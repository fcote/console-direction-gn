<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of one-value financial indicators arithmeticMean ,
   ' geometric mean, standard deviation, mean deviation, range, median.
   ' The Financial Chart
   FinancialChart.Title = "Financial Chart"
   FinancialChart.TempDirectory = "temp"
   FinancialChart.Debug = True
   FinancialChart.ShadingEffect = True
   FinancialChart.LegendBox.Template = "%icon %name"
   FinancialChart.Size = "800X600"
   FinancialChart.YAxis.Label.Text = "Price (USD)"
   FinancialChart.YAxis.FormatString = "currency"
   FinancialChart.YAxis.Scale = Scale.Range
   FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   FinancialChart.DefaultElement.ShowValue = True
   FinancialChart.DefaultElement.Marker.Visible = False
   
   ' Modify the x axis labels.
   FinancialChart.XAxis.Scale = Scale.Time
   FinancialChart.XAxis.TimeInterval = TimeInterval.Day
   FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o"
   FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month)
   FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM"
   
   Dim priceDataEngine As New DataEngine()
   priceDataEngine.ChartObject = FinancialChart
   priceDataEngine.ChartType = ChartType.Financial
   priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   priceDataEngine.DateGrouping = TimeInterval.Day
   priceDataEngine.StartDate = New DateTime(2001, 2, 1)
   priceDataEngine.EndDate = New DateTime(2001, 2, 28)
   ' Here we import financial data sample from the FinancialCompany table from within chartsample.mdb
   priceDataEngine.SqlStatement = "SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate"
   priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice"
   
   Dim sc As SeriesCollection = priceDataEngine.GetSeries()
   Dim prices As Series = Nothing
   If sc.Count > 0 Then
      prices = sc(0)
   Else
      Return
   End If 
   prices.DefaultElement.ToolTip = "L:%Low | H:%High"
   prices.DefaultElement.SmartLabel.Text = "O:%Open | C:%Close"
   prices.Type = SeriesTypeFinancial.CandleStick
   
   Dim cp As New CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0000001")
   prices.Trim(cp, ElementValue.XDateTime)
   prices.Name = "Prices"
   FinancialChart.SeriesCollection.Add(prices)
   
   FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   
   
	' One-Value Indicators

	 
   Dim indicatorsChartArea As New ChartArea("Calculations")
   indicatorsChartArea.TitleBox.Position = TitleBoxPosition.FullWithLegend
   indicatorsChartArea.XAxis = New Axis()
   indicatorsChartArea.YAxis = New Axis()
   indicatorsChartArea.HeightPercentage = 40
   indicatorsChartArea.DefaultElement.DefaultSubValue.Type = SubValueType.Line
   indicatorsChartArea.DefaultElement.DefaultSubValue.Line.Length = 10
   FinancialChart.ExtraChartAreas.Add(indicatorsChartArea)
   
   ' Mean - calculates the arithmetic mean of the currently registered data set. 
   Dim mean As New Series("Mean")
   ' If this option is setted true it will allow the user to place all the source values
   ' into the elementís subValues collection
   FinancialEngine.Options.PopulateSubValues = True
   mean.Elements.Add(FinancialEngine.Mean(prices, ElementValue.High))
   mean.Elements.Add(FinancialEngine.Mean(prices, ElementValue.Low))
   mean.Elements.Add(FinancialEngine.Mean(prices, ElementValue.Open))
   mean.Elements.Add(FinancialEngine.Mean(prices, ElementValue.Close))
   mean.Type = SeriesType.Bar
   indicatorsChartArea.SeriesCollection.Add(mean)
   
   ' GeometricMean - calculates the geometric mean of the currently registered data set. 
   Dim GeometricMean As New Series("GeometricMean")
   GeometricMean.Elements.Add(FinancialEngine.GeometricMean(prices, ElementValue.High))
   GeometricMean.Elements.Add(FinancialEngine.GeometricMean(prices, ElementValue.Low))
   GeometricMean.Elements.Add(FinancialEngine.GeometricMean(prices, ElementValue.Open))
   GeometricMean.Elements.Add(FinancialEngine.GeometricMean(prices, ElementValue.Close))
   GeometricMean.Type = SeriesType.Bar
   indicatorsChartArea.SeriesCollection.Add(GeometricMean)
   
   ' StdDev - calculates the sample standard variance of the currently registered data set.
   Dim stdDev As New Series("StdDev")
   ' Here we set the value of the option  WithSubValues to false.
   FinancialEngine.Options.Reset()
   stdDev.Elements.Add(FinancialEngine.StandardDeviation(prices, ElementValue.High))
   stdDev.Elements.Add(FinancialEngine.StandardDeviation(prices, ElementValue.Low))
   stdDev.Elements.Add(FinancialEngine.StandardDeviation(prices, ElementValue.Open))
   stdDev.Elements.Add(FinancialEngine.StandardDeviation(prices, ElementValue.Close))
   stdDev.Type = SeriesType.Bar
   indicatorsChartArea.SeriesCollection.Add(stdDev)
   
   ' MeanDev - calculates the mean deviation of the currently registered data set.
   Dim meanDev As New Series("MeanDev")
   meanDev.Elements.Add(FinancialEngine.MeanDeviation(prices, ElementValue.High))
   meanDev.Elements.Add(FinancialEngine.MeanDeviation(prices, ElementValue.Low))
   meanDev.Elements.Add(FinancialEngine.MeanDeviation(prices, ElementValue.Open))
   meanDev.Elements.Add(FinancialEngine.MeanDeviation(prices, ElementValue.Close))
   meanDev.Type = SeriesType.Bar
   indicatorsChartArea.SeriesCollection.Add(meanDev)
   
   ' Range - calculates the range of the currently registered data set.
   Dim range As New Series("Range")
   range.Elements.Add(FinancialEngine.Range(prices, ElementValue.High))
   range.Elements.Add(FinancialEngine.Range(prices, ElementValue.Low))
   range.Elements.Add(FinancialEngine.Range(prices, ElementValue.Open))
   range.Elements.Add(FinancialEngine.Range(prices, ElementValue.Close))
   range.Type = SeriesType.Bar
   indicatorsChartArea.SeriesCollection.Add(range)
   
   ' Median - calculates the median of the currently registered data set. 
   Dim median As New Series("Median")
   median.Elements.Add(FinancialEngine.Median(prices, ElementValue.High))
   median.Elements.Add(FinancialEngine.Median(prices, ElementValue.Low))
   median.Elements.Add(FinancialEngine.Median(prices, ElementValue.Open))
   median.Elements.Add(FinancialEngine.Median(prices, ElementValue.Close))
   median.Type = SeriesType.Bar
   indicatorsChartArea.SeriesCollection.Add(median)
End Sub 'Page_Load 

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="FinancialChart" runat="server"/>
		</div>
	</body>
</html>
