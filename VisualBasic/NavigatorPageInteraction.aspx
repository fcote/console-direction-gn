<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates how the navigator can interact with the page through JavaScript.

		Chart.Size = "600x350"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(0, 156, 255) }
		Chart.DefaultSeries.Type = SeriesType.Column

		Chart.LegendBox.Position = LegendBoxPosition.ChartArea

		' Enable the Navigator
		Chart.Navigator.Enabled = True
		Chart.Navigator.NavigationBar.Visible = False
		Chart.XAxis.Viewport.ScaleRange = New ScaleRange(New DateTime(2009, 1, 1), New DateTime(2009, 2, 15))

		' The control ID is specified so it can be referenced in javascript.
		Chart.Navigator.ControlID = "slPlugin"

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2009, 1, 1)

		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 44
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))

		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
	<style type="text/css">
		.newStyle1
		{
			font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;
			font-weight: bold;
			background-color: #C4D2FF;
			border-width: 1px;
			border-color: #666666;
			padding: 1px;
			margin: 1px;
			height: 18px;
			color: #414141;
		}
		.newStyle2
		{
			border-style: 0;
			background-color: #F7F7F7;
			padding: 0px;
			border-width: 1px;
			position: relative;
			bottom: 1px;
			height: 18px;
			font-family: Arial, Helvetica, sans-serif;
			font-size: x-small;
		}
		.style1
		{
			border: 1px solid #8C8C8C;
			background-color: #F7F7F7;
			padding: 0px;
			position: relative;
			bottom: 2px;
			height: 18px;
			font-family: Arial, Helvetica, sans-serif;
			font-size: x-small;
		}
		.style2
		{
			font-size: 5pt;
		}
		.style3
		{
			font-size: xx-small;
			font-family: Arial, Helvetica, sans-serif;
		}
	</style>
</head>
<script type="text/javascript">

	var chartID = "slPlugin";

	// For more information on this code and a client side API reference, see this tutorial: 
	// http://www.dotnetcharting.com/documentation/v6_0/Silverlight_Navigator_API.html

	// These methods refer to methods of the navigator.
	function pageLeft() {
		var control = document.getElementById(chartID);
		control.Content.Chart.PageLeft();
	}
	function pageRight() {
		var control = document.getElementById(chartID);
		control.Content.Chart.PageRight();
	}
	function goback() {
		var control = document.getElementById(chartID);
		control.Content.Chart.NavigateBack();
	}
	function goforward() {
		var control = document.getElementById(chartID);
		control.Content.Chart.NavigateForward();
	}
	function maxslow() {
		var control = document.getElementById(chartID);
		control.Content.Chart.NavigateToVisibleRangePerc(0, 1, 2000);
	}
	function maxfast() {
		var control = document.getElementById(chartID);
		control.Content.Chart.NavigateToVisibleRangePerc(0, 1, 100);
	}
	function getRange() {
		var control = document.getElementById(chartID);
		var range = control.Content.Chart.GetVisibleRange();
		document.getElementById('textBox1').value = range.toString();
	}

	function leftten() {
		var control = document.getElementById(chartID);
		var range = control.Content.Chart.GetVisibleRange();
		var leftDate = range.ValueHigh;
		var tmpDate = range.ValueHigh;
		leftDate.setDate(leftDate.getDate() - 10);

		control.Content.Chart.NavigateToVisibleRange(leftDate, tmpDate, 300);
	}

	function centerHere() {
		var control = document.getElementById(chartID);
		var centerDate = (document.getElementById('centerDate').value);
		control.Content.Chart.NavigateToCenter(centerDate, 300);
	}

	function initEvents() {
		var control = document.getElementById("slPlugin");
		if (control) {
			control.Content.Chart.SetHelpMessageHost("helpDiv.innerHTML");
			clearInterval(initID);
		}
	}

	// Try to register the help message handler with the chart until it is succeeds.
	var initID = setInterval("initEvents()", 1000);

</script>
<body>
	<div align="center">
		<br />
		<table style="width: 600">
			<tr>
				<td style="width: 17px">
					&nbsp;
				</td>
				<td>
					<button onclick="goback()" class="newStyle1">
						&lt;</button>
					<button onclick="goforward()" class="newStyle1">
						&gt;</button>&nbsp;
					<button onclick="maxslow()" class="newStyle1">
						Max Slow</button>
					<button onclick="maxfast()" class="newStyle1">
						Max Fast</button>
					<button onclick="leftten()" class="newStyle1">
						Left 10 Days</button>
					<input name="centerDate" value="1/25/2009" size="8" class="newStyle2" style="width: 165px;
						left: 0px; bottom: 2px;" />
					<button onclick="centerHere()" class="newStyle1">
						Center Here</button>
				</td>
			</tr>
			<tr>
				<td style="width: 17px">
					&nbsp;
				</td>
				<td>
					<button onclick="getRange()" class="newStyle1">
						GetRange</button>
					<input name="textBox1" size="50" class="newStyle2" style="width: 373px; left: 0px;
						bottom: 1px;" />
					<button onclick="pageLeft()" class="newStyle1">
						&lt; Page</button>
					<button onclick="pageRight()" class="newStyle1">
						Page &gt;</button>
				</td>
			</tr>
			<tr>
				<td style="width: 17px" class="style2">
					&nbsp;&nbsp;
				</td>
				<td class="style3">
					<strong>&nbsp;&nbsp;Help Messages</strong>
				</td>
			</tr>
			<tr>
				<td style="width: 17px">
					&nbsp;
				</td>
				<td>
					<div id="helpDiv" class="style1" style="width: 530;">
					</div>
				</td>
			</tr>
		</table>
		<dnc:Chart ID="Chart" runat="server" />
		<br />
		<br />
	</div>
</body>
</html>
