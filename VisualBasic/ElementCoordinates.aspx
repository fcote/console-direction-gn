<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates getting the coordinates of elements after the chart is rendered and doing some drawing before the chart is saved.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Combo
	Chart.Size = "600x350"
	Chart.Title = ".netCHARTING Sample"


	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)

	' Get a chart bitmap
	Dim bmp As Bitmap = Chart.GetChartBitmap()
	Dim g As Graphics = Graphics.FromImage(bmp)

	' Draw the element polygons.


	Dim pen As Pen = New Pen(Color.Red,2)
	Try
		Dim el As Element
		For Each el In mySC(0).Elements
			g.DrawPolygon(pen,el.GetPolygon())
		Next el
	Finally
		CType(pen, IDisposable).Dispose()
	End Try

	g.Dispose()
	Chart.FileManager.SaveImage(bmp)


End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(1)
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim a As Integer
	For a = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())
		Dim b As Integer
		For b = 1 To 4
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
