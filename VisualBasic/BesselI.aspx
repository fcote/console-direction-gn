<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates the bessel I function.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Combo
	Chart.Size = "600x350"
	Chart.Title = "Special Functions"


	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()
	Dim besselISC As SeriesCollection = New SeriesCollection()
	' Calculate the BesselI function of the order 0, 1, 2, 3
	Dim i As Integer
	For i = 0 To 3
		Dim besselI As Series = ForecastEngine.Advanced.BesselI(mySC(i),i)
		besselI.Type = SeriesType.Line
		besselISC.Add(besselI)
	Next i

	' Bessel chart area
	Dim besselChartArea As ChartArea = New ChartArea ()
	besselChartArea.HeightPercentage = 40
	besselChartArea.YAxis.Label.Text = "BesselI"
	Chart.ExtraChartAreas.Add (besselChartArea)


	' Add the random data.
	Chart.SeriesCollection.Add(mySC)
	besselChartArea.SeriesCollection.Add(besselISC)

End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(1)
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim a As Integer
	For a = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())
		Dim b As Integer
		For b = 1 To 4
			Dim e As Element = New Element()
			e.YValue = myR.Next(5)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
