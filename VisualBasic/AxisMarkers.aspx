<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING" %>

<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs )

	
	'set global properties
   Chart.Title="Average Seasons Temperature"
   Chart.ChartArea.XAxis.Label.Text ="Seasons"
   Chart.ChartArea.YAxis.Label.Text="Tempature"
   Chart.TempDirectory="temp"
   Chart.Debug=true
    
   Chart.YAxis.NumberPercision=0
   Chart.YAxis.Label.Text="Celsius"
   

   'Creating a single line axis marker with a label on the chart and legend entry
   Dim M1 As AxisMarker 
   M1=new AxisMarker()
   M1.LegendEntry.Name = "High"
   M1.Label.Text = "High Temperature Boundary"
   M1.Line.Color = Color.Red
   M1.Line.Width = 2
   M1.Value = 27
   Chart.YAxis.Markers.Add(M1)

   'Creating a single line axis marker with legend entry
   Dim M2 As AxisMarker
   M2=new AxisMarker()
   M2.Label.Text = ""
   M2.LegendEntry.Name = "Low"
   M2.Line.Color = Color.Blue
   M2.Line.Width = 2
   M2.Value = 3
   Chart.YAxis.Markers.Add(M2)

   'Creating a ranged axis marker with legend entry
   Dim M3 As AxisMarker
   M3=new AxisMarker()
   M3.Label.Text = ""
   M3.LegendEntry.Name = "Comfort Zone"
   M3.Background = new Background(Color.Green)
   M3.ValueHigh = 22
   M3.ValueLow = 15
   Chart.YAxis.Markers.Add(M3)

    
   'Adding series programatically
   Dim sr As Series 
   Dim el As Element
   
   sr=new Series()
   sr.Name="Vancouver"
   el = new Element("Spring",10)
   sr.Elements.Add(el)
   el = new Element("Summer",20)
   sr.Elements.Add(el)
   el = new Element("Autumn",13)
   sr.Elements.Add(el)
   el = new Element("Winter",5)
   sr.Elements.Add(el)
   Chart.SeriesCollection.Add(sr)
   
   sr=new Series()
   sr.Name="Houston"
   el = new Element("Spring",20)
   sr.Elements.Add(el)
   el = new Element("Summer",32)
   sr.Elements.Add(el)
   el = new Element("Autumn",18)
   sr.Elements.Add(el)
   el = new Element("Winter",10)
   sr.Elements.Add(el)
   Chart.SeriesCollection.Add(sr)
 
End Sub

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Axis Marker Sample</title></head>
<body>
<p>
 <dotnet:Chart id="Chart"  runat="server"/>
</p>
</body>
</html>
