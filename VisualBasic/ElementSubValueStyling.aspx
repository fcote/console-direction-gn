<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Element SubValue Styling"
   
   ' This sample will demonstrate how to style element sub values such as error bars.
   ' Set a default error offset value.
   Chart.DefaultElement.ErrorOffset = 5
   
   ' Default Line Styling
   Chart.DefaultElement.DefaultSubValue.Line.DashStyle = DashStyle.Dash
   Chart.DefaultElement.DefaultSubValue.Line.Length = 55
   Chart.DefaultElement.DefaultSubValue.Line.Width = 2
   Chart.DefaultElement.DefaultSubValue.Line.Color = Color.Black
   
   ' Default Marker Styling
   Chart.DefaultElement.DefaultSubValue.Marker.Type = ElementMarkerType.FivePointStar
   Chart.DefaultElement.DefaultSubValue.Marker.Size = 12
   Chart.DefaultElement.DefaultSubValue.Marker.Color = Color.Orange
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
   
   ' Specify SubValue Types
   mySC(0).Elements(0).DefaultSubValue.Type = SubValueType.ErrorBar
   mySC(0).Elements(1).DefaultSubValue.Type = SubValueType.Line
   mySC(0).Elements(2).DefaultSubValue.Type = SubValueType.Marker
   
   ' Name the elements
   mySC(0).Elements(0).Name = "ErrorBar"
   mySC(0).Elements(1).Name = "Line"
   mySC(0).Elements(2).Name = "Marker"
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(2)
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 2
         Dim e As New Element()
         e.Name = "Element " +Convert.ToString(b + 1)
         'e.YValue = -25 + myR.Next(50);
         e.YValue = 20 + myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   Return SC
End Function 'getRandomData

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
