<%@ Page Language="vb" Debug="true" Trace="false" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>

<script runat="server">
	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using custom element attributes inside dataGrids.

		'set global properties
		Chart.Title = " Employees' Report"

		Chart.YAxis.FormatString = "currency"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Use3D = True

		Chart.XAxis.Label.Text = "Employee's Name"
		Chart.YAxis.Label.Text = "Annual Salary"

		Chart.DataGrid = "chartdatagrid"

		Chart.DataGridSeriesHeader = "Name"
		Chart.DataGridFormatString = "C"
		Chart.DataGridCustomAttributes = "Employee No=%EmployeeID<row>Department=%Department<row>Email=%email<row>Phone=%PhoneNumber"

		' Transpose switches the rows and columns.  To visualize this 
		' difference, please change this property value and run the sample again.
		Chart.DataGridTranspose = True

		'Create a series  
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.SqlStatement = "SELECT * FROM Employees"

		'Set predefined data fields and some custom fields as attributes for x-Axis. (Employee�s name)
        de.DataFields = "xAxis=name,yAxis=salary,id=EmployeeID,department=Department,email,phone=PhoneNumber"
        de.Limit = "5"
        de.ShowOther = False
		Dim sc As SeriesCollection = de.GetSeries()
		sc(0).Name = "Salary"

		Dim a As Annotation = New Annotation("Employee No: %EmployeeID" & Constants.vbLf & "Department: %Department" & Constants.vbLf & "Email: %email" & Constants.vbLf & "Phone: %PhoneNumber")
		a.Background.Bevel = True
		a.Background.Color = Color.FromArgb(150, 180, 180, 220)
		' Assign the annotation to an element in the sc collection.
		sc(0).Elements(0).Annotation = a

		Chart.SeriesCollection.Add(sc)
	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Datagrid Custom Attributes</title>
</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" TitleBox-Position="FullWithLegend" runat="server" />
	</div>
	<br />
	<center>
		<asp:DataGrid ID="chartdatagrid" Width="50%" Font-Name="Arial" HeaderStyle-BackColor="skyblue" CellPadding ="5" BackColor="lightblue" runat="server" />
	</center>
</body>
</html>
