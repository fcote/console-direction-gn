<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs)

	'set global properties
    'set global properties
    Chart.Title="Sales For January"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.Mentor=false
    'Chart.YAxis.FormatString = "currency"
    Chart.Use3D = true
    Chart.DefaultSeries.DefaultElement.Transparency = 20
    Chart.XAxis.ClusterColumns = false
    Chart.XAxis.ReverseSeries=true
    Chart.Depth=Unit.Parse(5)
    Chart.DataGrid ="chartdatagrid"
    'Chart.DataGridSeriesHeader="none"
    Chart.DataGridTranspose=true
    'Chart.LabelTemplate="yvalue"
  

        'Add a series
    Chart.Series.Name = "Product 1"
    Chart.Series.Elements.Add(new Element("Bill",7))
    Chart.Series.Elements.Add(new Element("Joe",10))
    Chart.Series.Elements.Add(new Element("Tim",9))
    Chart.Series.Elements.Add(new Element("Henry",12))
    Chart.SeriesCollection.Add()

    'Add a series
    Chart.Series.Name = "Product 2"
    Chart.Series.Elements.Add(new Element("Bill",18))
    Chart.Series.Elements.Add(new Element("Joe",15))
    Chart.Series.Elements.Add(new Element("Tim",19))
    Chart.Series.Elements.Add(new Element("Henry",15))
    Chart.SeriesCollection.Add()


    'Add a series
    Chart.Series.Name = "Product 3"
    Chart.Series.Elements.Add(new Element("Bill",25))
    Chart.Series.Elements.Add(new Element("Joe",21))
    Chart.Series.Elements.Add(new Element("Tim",23))
    Chart.Series.Elements.Add(new Element("Henry",28))
    Chart.SeriesCollection.Add()

    'Add a series
    Chart.Series.Name = "Product 4"
    Chart.Series.Elements.Add(new Element("Bill",31))
    Chart.Series.Elements.Add(new Element("Joe",35))
    Chart.Series.Elements.Add(new Element("Tim",39))
    Chart.Series.Elements.Add(new Element("Henry",34))
    Chart.SeriesCollection.Add()
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Sales Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 <asp:DataGrid id="chartdatagrid" Width="50%" Font-Name="Arial" HeaderStyle-BackColor="skyblue"
BackColor="lightblue" runat="server"/>
</div>
</body>

</html>
