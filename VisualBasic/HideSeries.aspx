<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates making series invisible based on a legend entry click.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Combo
	Chart.Size = "600x350"
	Chart.Title = ".netCHARTING Sample"

	Dim an As Annotation = New Annotation("Click the legend entries to toggle series visibility.")
	an.Position = New Point(500,115)
	Chart.Annotations.Add(an)

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)

	' This will ensure the series always have the same colors based on their name.
	Chart.SmartPalette = mySC.GetSmartPalette(Chart.Palette)

	Chart.DefaultSeries.LegendEntry.URL = "?name=%Name"

	' By enumerating the legned entry positions in the box, the custom entry added below can replace the position of the original.
	Dim i As Integer
	Dim s As Series
	For i = 0 To 3
		For Each s In mySC
			s.LegendEntry.SortOrder = i
			i = i+1
		Next s
	Next

	Dim query As String = ""
	If Not Request.QueryString("name") Is Nothing Then
		query = Request.QueryString("name")
		For Each s In mySC
			If s.Name = query AndAlso Request.QueryString("show") Is Nothing Then
				s.Visible = False
				Dim le As LegendEntry = New LegendEntry()
				le.Name = query
				le.URL = "?name=" & query & "&show=true"
				le.SortOrder = s.LegendEntry.SortOrder
				le.Background.Color = Color.FromArgb(1,Color.Transparent)
				Chart.LegendBox.ExtraEntries.Add(le)
				Exit For
			End If
		Next s
	End If
End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(1)
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim a As Integer
	For a = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())
		Dim b As Integer
		For b = 1 To 4
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
