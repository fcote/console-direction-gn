<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(700)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.DefaultSeries.Type=SeriesType.AreaLine
   Chart.Title = "TimeScaleLabelRangeMode.Dynamic time labels."
   
   ' This sample will demonstrate how to use axis time label automation and how to specify different 
   ' properties for time labels representing different instances in time.
   ' Specify the mode.
   Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Hidden
   Chart.XAxis.TimeScaleLabels.RangeMode = TimeScaleLabelRangeMode.Dynamic
   
   ' Specify a format string for day labels.
   Chart.XAxis.TimeScaleLabels.DayFormatString = "ddd d"
   
   
   ' Specify a label template and font for month labels.
   Chart.XAxis.TimeScaleLabels.MonthTick.Label.Font = New Font("Arial", 8, FontStyle.Bold)
   Chart.XAxis.TimeScaleLabels.MonthTick.Line.Color = Color.Red
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2005, 1, 28)
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.YValue = myR.Next(100)
         e.XDateTime =dt.AddDays(2)
         dt = dt.AddDays(2)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData
</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
