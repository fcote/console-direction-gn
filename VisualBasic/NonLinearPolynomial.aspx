<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates the use of NonLinear Forecasting model in order to
   ' find the function of best fit. The data used for which the fuctions are fit is a set 
   ' of data which represents a FX exchange rate over a given period of time.
   ' The Forecast Chart
   ForecastChart.Title = "Exchange"
   ForecastChart.TempDirectory = "temp"
   ForecastChart.Debug = True
   ForecastChart.Size = "1000x800"
   ForecastChart.LegendBox.Template = "%icon %name"
   
   ForecastChart.YAxis.ScaleRange.ValueLow = 220
   'ForecastChart.XAxis.Scale = Scale.Normal;
   ' The Forecast data
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   de.DateGrouping = TimeInterval.Days  
	de.StartDate = new DateTime(1982,8,7,0,0,0)
	de.EndDate = new DateTime(1982,12,10,0,0,0)
	de.SqlStatement= "SELECT ID, Value AS q FROM Exchange WHERE Data >= #STARTDATE# AND Data <= #ENDDATE# ORDER BY Data "
   
   'Add a series
   Dim scForecast As SeriesCollection = de.GetSeries()
   ForecastChart.SeriesCollection.Add(scForecast)
   
   scForecast(0).Name = "Exchange"
   scForecast(0).Type = SeriesType.Spline
   

	 ' Takes off the marker off the line and spline series.

	 
   ForecastChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   ForecastChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   
   ' Generate a series of standard deviation for the given points
   Dim deviation As New Series()
   Dim i As Integer
   For i = 0 To (scForecast(0).Elements.Count) - 1
      Dim el As New Element()
      el.XValue = scForecast(0).Elements(i).XValue
      el.YValue = 1E-10
      deviation.Elements.Add(el)
   Next i
   
   ' Note that this line is necessary in order to clear the function basis set by previous
   ' example.
   '
   ForecastEngine.Options.Reset()
   
   ' Set the first model function
   '
   ' The first basis element: a1*x^a1` + a2*x^a2` + a3*x^a3`(1)
   'ForecastEngine.Options.AddSumOfNonLinearPowerTerms(New Double() {1, 1, 1}, New Double() {0, 1, 2})
   ForecastEngine.Options.AddSumOfNonLinearPowerTerms(New Double(){1}, New Double(){1}, New Double() {0}, New Double(){1})
	ForecastEngine.Options.AddSumOfNonLinearPowerTerms(New Double(){1}, New Double(){1}, New Double() {0}, New Double() {2})

   ' Generate a new series which will draw the best fitline according with the model function which we just set
   Dim nonLinearModel1 As New Series()
   'nonLinearModel1 = ForecastEngine.Advanced.NonLinearModel(scForecast(0), deviation, New Double() {100.0, 0.0, 1.0, 1.0, 0.001, 2.0}, New Boolean() {True, True, True, True, True, True})
   nonLinearModel1 = ForecastEngine.Advanced.NonLinearModel(scForecast(0), deviation, New Double() {0.09, 1.0, 1.0, 1.0, 0.0001, 1.0, 1.0, 1.0}, New Boolean() {true, true, true, true, true, true, true, true})

   nonLinearModel1.Name = "2nd Degree Polynomial"
   nonLinearModel1.Type = SeriesType.Spline
   
   'The next two lines display on to the chart the function used to fit the curve
   'nonLinearModel1.Elements(230).SmartLabel.Text = "Function 1: %Function"
   'nonLinearModel1.Elements(230).ShowValue = True
   
   ForecastChart.SeriesCollection.Add(nonLinearModel1)
End Sub 'Page_Load 

		</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Forecasting Sample</title>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="ForecastChart" runat="server"/>			
		</div>
	</body>
</html>
