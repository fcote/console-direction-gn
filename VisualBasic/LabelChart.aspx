<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates creating a chart that draws only a Label.

		' Main Settings
		Chart.TempDirectory = "temp"
		Chart.Debug = True

		' Create the Label.
		Dim l As dotnetCHARTING.Label = New dotnetCHARTING.Label(getText())
		l.Font = New Font("Arial", 10, FontStyle.Bold)

		' Use the label as the chart content.
		Chart.ObjectChart = l

	End Sub

	Function getText() As String
		Dim s As String = "<Chart:Spacer size='1x1'><Chart:Spacer size='30x1'><Chart:Spacer size='60x1'><Chart:Spacer size='60x1'><Chart:Spacer size='60x1'><row>"
		s &= "<block><block><block>Jan<block>Feb<block>Mar<row>"
		s &= "<Chart:Spacer size='1x70'> Ford<Chart:Image src='../../images/car.png' size='20'><Chart:Image src='../../images/car.png' size='30'><Chart:Image src='../../images/car.png' size='26'><hr>"
		s &= "<Chart:Spacer size='1x70'>GM<Chart:Image src='../../images/car.png' size='40'><Chart:Image src='../../images/car.png' size='50'><Chart:Image src='../../images/car.png' size='20'><hr>"
		s &= "<Chart:Spacer size='1x70'>Toyota<Chart:Image src='../../images/car.png' size='20'><Chart:Image src='../../images/car.png' size='80'><Chart:Image src='../../images/car.png' size='40'><row>"
		Return s
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
