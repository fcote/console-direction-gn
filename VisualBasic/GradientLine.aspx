<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   
   ' This sample demonstrates using a multi-color 2D line.
   ' Set some chart properties.
   Chart.ChartArea.Label.Text = "Notice the line color change between points of different colors."
   Chart.DefaultSeries.Type = SeriesType.Line
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Iterate the elements and set the color to red if an elements YValue is outside the 20-40 range.
   Dim myE As Element
   For Each myE In  mySC(0).Elements
      If myE.YValue > 40 Or myE.YValue < 20 Then
         myE.Color = Color.Red
      End If 
   Next myE ' Add a couple axis markers.
   Chart.YAxis.Markers.Add(New AxisMarker("", Color.FromArgb(50, Color.Red), 40, 100))
   Chart.YAxis.Markers.Add(New AxisMarker("", Color.FromArgb(50, Color.Red), 0, 20))
   Chart.YAxis.Markers(0).LegendEntry.Visible = False
   Chart.YAxis.Markers(1).LegendEntry.Visible = False
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 1 To 1
      Dim s As New Series()
      s.Name = "Sample Data " + a.ToString()
      Dim b As Integer
      For b = 1 To 14
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   'SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
   'SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
   'SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
   'SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
