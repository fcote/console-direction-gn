<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates how to control the chart and page content simultaneously.

		chart.Size = "600x350"
		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		chart.DefaultSeries.Type = SeriesType.Line
		chart.LegendBox.Position = LegendBoxPosition.ChartArea
		chart.YAxis.Scale = Scale.Range

		' Set chart area label.
		chart.ChartArea.Label.Text = "Click a link above to control the chart position."
		chart.ChartArea.Label.Color = Color.DarkRed
		chart.ChartArea.Padding = 5

		' Enable the Navigator
		chart.Navigator.Enabled = True

		' The control ID is specified so it can be referenced in javascript.
		chart.Navigator.ControlID = "slPlugin"

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		chart.DefaultElement.Annotation = New Annotation()
		chart.DefaultElement.Annotation.Background.Transparency = 50
		chart.DefaultElement.Annotation.Background.Color = Color.LightBlue


		Dim mySC As SeriesCollection = getRandomData()

		Dim a1 As Annotation = New Annotation("Event A<br> Occured")
		a1.Orientation = dotnetCHARTING.Orientation.TopRight
		a1.URL = "Javascript:updateDisplay(1);"
		mySC(0)(25).Annotation = a1

		Dim a2 As Annotation = New Annotation("Event B<br> Occured")
		a2.URL = "Javascript:updateDisplay(2);"
		mySC(0)(65).Annotation = a2

		Dim a3 As Annotation = New Annotation("Event C<br> Occured")
		a3.URL = "Javascript:updateDisplay(3);"
		mySC(0)(95).Annotation = a3

		' Add the random data.
		chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2010, 1, 1)

		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 114
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(b)
			s.Elements.Add(e)
		Next b
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
	<style type="text/css">
		div, p
		{
			font-family: Arial, Helvetica, sans-serif;
			font-size: x-small;
		}
	</style>
</head>
<script type="text/javascript">
	var chartID = "slPlugin";

	function updateDisplay(disp) {

		var control = document.getElementById(chartID);
		if (disp == "1") {
			document.getElementById('dispDiv').innerHTML = "Details for Event A<br/> &nbsp;&nbsp;A list of details here.";
			leftten();
			control.content.Chart.NavigateToCenter(new Date(2010, 0, 27), 500);
		}
		if (disp == "2") {
			document.getElementById('dispDiv').innerHTML = "Details for Event B<br/> &nbsp;&nbsp;A larger list of details here.";
			leftten();
			control.content.Chart.NavigateToCenter(new Date(2010, 2, 7), 500);
		}
		if (disp == "3") {
			document.getElementById('dispDiv').innerHTML = "Details for Event C<br/> &nbsp;&nbsp;A final list of details for event C.";
			leftten();
			control.content.Chart.NavigateToCenter(new Date(2010, 3, 7), 500);
		}
	}


	function leftten() {
		var control = document.getElementById(chartID);
		var range = control.content.Chart.GetVisibleRange();
		var leftDate = range.ValueLow;

		var tmpDate = range.ValueLow;
		tmpDate.setDate(tmpDate.getDate() + 10);

		control.content.Chart.NavigateToVisibleRange(leftDate, tmpDate, 0);
	}

</script>
<body>
	<div align="center">
		<span>Click on an event to control the chart ad details below.<br />
			<a href="Javascript:updateDisplay(1);">Event A</a></span> <span><a href="Javascript:updateDisplay(2);">
				Event B</a></span> <span><a href="Javascript:updateDisplay(3);">Event C</a></span>
	</div>
	<div align="center">
		<dnc:Chart ID="chart" runat="server" />
	</div>
	<div id="dispDiv">
	</div>
</body>
</html>
