<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(400)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Run Chart"
   Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Top
   Dim mySC As SeriesCollection = getRandomData()
   'Chart.SeriesCollection.Add(mySC);
   Chart.DefaultSeries.Type = SeriesType.Spline
   
   Dim runChart As Series = StatisticalEngine.RunChart(mySC(0))
   Chart.SeriesCollection.Add(runChart)
End Sub 'Page_Load


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2005, 1, 1)
   Dim a As Integer
   For a = 0 To 1
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 0 To 29
         Dim e As New Element()
         'e.Name = "Element " + b
         'e.XValue = b;
         e.YValue = myR.Next(50)
         e.XDateTime = dt.AddDays(b)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   'SC[2].DefaultElement.Color = Color.FromArgb(49,255,49);
   'SC[3].DefaultElement.Color = Color.FromArgb(255,255,0);
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
