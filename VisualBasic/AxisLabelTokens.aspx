<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender as Object ,e as EventArgs )


	Chart.Type = ChartType.Combo 'Horizontal
	Chart.Width = new Unit(600)
	Chart.Height = new Unit(350)
	Chart.TempDirectory = "temp"
	Chart.Debug = true
	
	' This sample will demonstrate how dynamic tokens can be used by axis labels.
	' The axis label uses it's ScaleRange to process tokens and hence, ScaleRange type tokens are allowed.
	
	
	' 1.SPECIFY A LABEL WITH TOKENS
	Chart.YAxis.Label.Text = "Low(%Low) High(%High) Range(%Range)"
	
	' Included for demo purposes, not necessary.
	Chart.YAxis.Scale = Scale.Range

	' 2. ADD DATA
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData())
    
End Sub

Function  getRandomData() As SeriesCollection

	Dim SC as SeriesCollection = new SeriesCollection()
	Dim myR As Random  = new Random()
	Dim a,b As Integer
	Dim s As Series
	Dim e As Element
	
	For  a = 0 To 3
	
		s = new Series()
		s.Name = "Series " & a
		For  b = 0 To 3
		
			e = new Element()
			e.Name = "Element " & b
			'e.YValue = -25 & myR.Next(50)
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
