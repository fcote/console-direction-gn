<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of ExponentialDistribution method from the StatisticalEngine. This method return the exponential distribution for a series when the standard deviation is known. 
   
   ' The Probability Chart
   ProbabilityChart.Title = "Exponential distribution"
   ProbabilityChart.TempDirectory = "temp"
   ProbabilityChart.Debug = True
   ProbabilityChart.Size = "800x300"
   ProbabilityChart.LegendBox.Template = "%icon %name"
   'ProbabilityChart.XAxis.Scale = Scale.Normal;
   ProbabilityChart.DefaultSeries.DefaultElement.Marker.Visible = True
   ProbabilityChart.DefaultSeries.Type = SeriesType.Spline
   ProbabilityChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   
   Dim sprobability As New SeriesCollection()
   
   ' Generate the sample data.
   Dim sampledata1 As New Series("Sample Data")
   sampledata1.Elements.Add(New Element("", 0.1, 1.0))
   sampledata1.Elements.Add(New Element("", 0.2, 2.0))
   sampledata1.Elements.Add(New Element("", 0.3, 3.0))
   sampledata1.Elements.Add(New Element("", 0.4, 4.0))
   sampledata1.Elements.Add(New Element("", 0.5, 5.0))
   sampledata1.Elements.Add(New Element("", 0.6, 6.0))
   sampledata1.Elements.Add(New Element("", 0.7, 7.0))
   sampledata1.Elements.Add(New Element("", 0.8, 8.0))
   sampledata1.Elements.Add(New Element("", 0.9, 9.0))
   sprobability.Add(sampledata1)
   
   ' Add the series collection to the chart
   ProbabilityChart.SeriesCollection.Add(sprobability)
   
   
   ' The second parameter of this method is the standard deviation of the exponential probability distribution
   Dim expDistribution As Series = StatisticalEngine.ExponentialDistribution(sampledata1, 10)
   expDistribution.Type = SeriesType.Spline
   expDistribution.DefaultElement.Marker.Visible = False
   expDistribution.Line.Width = 2
   expDistribution.DefaultElement.ToolTip = "Y:%YValue"
   expDistribution(0).SmartLabel.Text = "Exponential Distribution"
   expDistribution(0).ShowValue = True
   expDistribution(0).SmartLabel.Alignment = LabelAlignment.Top
   expDistribution(0).SmartLabel.Line.EndCap = LineCap.ArrowAnchor
   
   ProbabilityChart.SeriesCollection.Add(expDistribution)
End Sub 'Page_Load
		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="ProbabilityChart" runat="server"/>
			
			
		</div>
	</body>
</html>
