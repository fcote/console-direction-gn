<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using a score comparison chart.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.ComboHorizontal
		Chart.Size = "300x250"
		Chart.Title = "Score Comparison"
		Chart.DefaultElement.DefaultSubValue.Line.Length = 1
		Chart.DefaultElement.DefaultSubValue.Marker.Size = 13
		Chart.LegendBox.Visible = False
		Chart.XAxis.Label.Text = "Score"

		Chart.YAxis.SpacingPercentage = 0

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()
		mySC(0)(0).Name = "My Score"
		mySC(0)(1).Name = "Managers"
		mySC(0)(2).Name = "Subordinates"
		mySC(0)(3).Name = "Customers"
		mySC(0)(4).Name = "Others"

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(2)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		Dim dt As DateTime = DateTime.Now
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 5
				Dim e As Element = New Element()
				e.YValue = 20

				e.YValueStart = 19.5R
				s.Elements.Add(e)

				If b > 1 Then
					Dim val As Integer = myR.Next(40)
					e.SubValues.Add(SubValue.FromValue(val))
					e.SubValues.Add(SubValue.FromValue(val))
					e.SubValues(1).Type = SubValueType.Marker
					e.SubValues(1).Marker.Color = Color.White
				End If
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
