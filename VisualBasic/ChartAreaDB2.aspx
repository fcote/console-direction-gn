<%@ Page Language="VB" Description="dotnetCHARTING Component" Debug="true" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   'set global properties
   Chart.Title = "Item sales (mouseover any bar for its value)"
   Chart.ChartArea.XAxis.Label.Text = "Months"
   Chart.Size = "800X500"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.DefaultAxis.FormatString = "c"
   
   'Set a tooltip with series name and value
   Chart.DefaultElement.ToolTip = "%SeriesName: %Value"
   
   'Add a series
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   de.StartDate = New DateTime(2002, 1, 1, 0, 0, 0)
   de.EndDate = New DateTime(2002, 12, 31, 23, 59, 59)
   de.DateGrouping = TimeInterval.Year
   
   ' Chart Area 1
   Chart.ChartArea.Label.Text = "Top 5 Producers"
   de.SplitByLimit = "5"
   de.ShowOther = False
   de.SqlStatement = "SELECT OrderDate,Total, Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY Orders.OrderDate"
   
   Chart.LegendBox.HeaderEntry.Visible = True
   Chart.LegendBox.HeaderEntry.Value = "Top 5"
   Dim sc As SeriesCollection = de.GetSeries()
   Chart.SeriesCollection.Add(sc)
   
   ' Chart Area 2
   Dim ca2 As New ChartArea()
   de.LimitMode = LimitMode.ExcludeTop
   Dim sc2 As SeriesCollection = de.GetSeries()
   Dim s As Series
   For Each s In  sc2
      s.LegendEntry.SortOrder = 2
   Next s
   ca2.SeriesCollection.Add(sc2)
   ca2.Label.Text = "Others"
   
   de.SqlStatement = "SELECT OrderDate, Sum(Total) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
   de.Limit = ""
   
   Chart.ExtraChartAreas.Add(ca2)
   
   ' Chart Area 3
   Dim ca3 As New ChartArea()
   Dim sc3 As SeriesCollection = de.GetSeries()
   sc3(0).LegendEntry.SortOrder = 4
   sc3(0).Name = "Total"
   sc3(0).DefaultElement.ToolTip = "%ElementName Total: %Value"
   ca3.SeriesCollection.Add(sc3)
   ca3.Label.Text = "Total"
   
   
   Chart.ExtraChartAreas.Add(ca3)
   
   
   ' Legend Entry Headers
   Dim head2 As New LegendEntry("", "Other", "")
   head2.SortOrder = 1
   head2.PaddingTop = 8
   head2.LabelStyle.Font = New Font("Arial", 8, FontStyle.Bold)
   head2.DividerLine.Color = Color.Black
   Chart.LegendBox.ExtraEntries.Add(head2)
   
   Dim head3 As New LegendEntry("", "Total", "")
   head3.PaddingTop = 8
   head3.LabelStyle.Font = New Font("Arial", 8, FontStyle.Bold)
   head3.SortOrder = 3
   head3.DividerLine.Color = Color.Black
   Chart.LegendBox.ExtraEntries.Add(head3)
End Sub 'Page_Load 



</script>

	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
