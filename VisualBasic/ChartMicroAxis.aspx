<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        ' This sample demonstrates using MicroCharts on axis tick labels to give more detail about the data.

        Chart.TempDirectory = "temp"
        Chart.Debug = True
        Chart.Palette = New Color() {Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255)}

        Chart.Size = "600x350"
        Chart.Title = "1st Week of 2009 Sales"
        Chart.DefaultElement.ShowValue = True
        Chart.YAxis.FormatString = "Currency"
        Chart.LegendBox.Visible = False
        Chart.Background.Color = Color.White
        Chart.Background.ShadingEffectMode = ShadingEffectMode.Seven

        ' Get the same data as for the first chart.        
        Dim mySC As SeriesCollection = getRandomData()

        ' Transpose it to get a sum series (3 bars).
        mySC.Transpose()

        Dim sum As Series = mySC.Calculate("", Calculation.Sum)
        ' Color each bar separately.
        sum.Palette = Chart.Palette
        Chart.SeriesCollection.Add(sum)

        ' Transpose it back.
        mySC.Transpose()


        ' Set the name for each bar manualy getting the daily values from the original mySC collection.
        Dim i As Integer = 0
        For Each s As Series In mySC
            sum(i).Name = "<Chart:Column color='" & getHTMLColor(Chart.Palette(i)) & "' values='" & s.GetYValueList() & "' width='100'><row><block  hAlign='Center'>Mon - Sun<row><block fStyle='Bold'>" & sum(i).Name
            i = i + 1
        Next s
        ' The X Axis ticks will by default show the element names which will carry the markup for the micro charts.

    End Sub

    Function getRandomData() As SeriesCollection
        Dim myR As Random = New Random(2)
        Dim SC As SeriesCollection = New SeriesCollection()
        For a As Integer = 1 To 4
            Dim s As Series = New Series("Product " & a.ToString())
            For b As Integer = 0 To 6
                Dim e As Element = New Element(b.ToString())
                e.YValue = myR.Next(50)
                s.Elements.Add(e)
            Next b
            SC.Add(s)
        Next a
        Return SC
    End Function

    Function getLiveData() As SeriesCollection
        Dim de As DataEngine = New DataEngine("ConnectionString goes here")
        de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
        Return de.GetSeries()
    End Function
    ''' <summary>
    ''' Helper method to turn a color object into HTML color format like #A0187E
    ''' </summary>
    Function getHTMLColor(ByVal c As Color) As String
        Dim col As String = c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2")
        Return "#" & col
    End Function
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dnc:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
