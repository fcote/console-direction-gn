<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Label Truncation</title>
		<script runat="server">




Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(400)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.LegendBox.Position = LegendBoxPosition.None
   
   
   ' This sample will demonstrate how strings can be truncated.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Setup the chart
   Chart.DefaultSeries.Type = SeriesType.Marker
   Chart.YAxis.Maximum = 40
   Chart.DefaultSeries.DefaultElement.ShowValue = True
   
   ' Specify a default string to trim.
   Chart.DefaultSeries.DefaultElement.SmartLabel.Text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
   
   ' Set the default trimming length to 10 characters.
   Chart.DefaultSeries.DefaultElement.SmartLabel.Truncation.Length = 10
   
   ' Specify a trimming mode and name for each element.
   mySC(0).Elements(0).Name = "TruncationMode.End"
   mySC(0).Elements(0).SmartLabel.Truncation.Mode = TruncationMode.End
   
   mySC(0).Elements(1).Name = "TruncationMode.Middle"
   mySC(0).Elements(1).SmartLabel.Truncation.Mode = TruncationMode.Middle
   
   mySC(0).Elements(2).Name = "TruncationMode.StartEnd"
   mySC(0).Elements(2).SmartLabel.Truncation.Mode = TruncationMode.StartEnd
   
   mySC(0).Elements(3).Name = "TruncationMode.Start"
   mySC(0).Elements(3).SmartLabel.Truncation.Mode = TruncationMode.Start
   
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 1
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = 20
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
