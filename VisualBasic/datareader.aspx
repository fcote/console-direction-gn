<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs )

	'set global properties
    Chart.Title="Item sales report"
	Chart.ChartArea.XAxis.Label.Text="Customers"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    
    
    'Adding series programatically
   	Chart.Series.Name = "Item sales"
   	SetData()
    Chart.SeriesCollection.Add()
                       
   
End sub
    Sub SetData()
        Dim connString As String = "Provider=Microsoft.Jet.OLEDB.4.0;user id=admin;password=;data source=" & Server.MapPath("../../database/chartsample.mdb")
        Dim conn As OleDbConnection = New OleDbConnection(connString)
        Dim selectQuery As String = "SELECT Name,Sum(Quantity) FROM Orders GROUP BY Orders.Name ORDER BY Orders.Name"
        Dim myCommand As OleDbCommand = New OleDbCommand(selectQuery, conn)
        conn.Open()
        Dim myReader As OleDbDataReader = myCommand.ExecuteReader()
   	
        'set dataReader to data property
        Chart.Series.Data = myReader
   	
        'Cleanup
        myReader.Close()
        conn.Close()
        
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Data Reader Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
