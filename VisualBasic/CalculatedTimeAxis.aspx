<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample will demonstrate how to use calculated time axes.
   Chart.DefaultLegendBox.Orientation = dotnetCHARTING.Orientation.Bottom
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Set some properties for the main x axis.
   Chart.XAxis.TimeInterval = TimeInterval.Days
   Chart.XAxis.Label.Text = "Days"
   
   ' Calculate an axis from the x axis and add it with 'weeks' intervals.
   Chart.AxisCollection.Add(Chart.XAxis.Calculate("Weeks", TimeInterval.Weeks))
   
   ' Calculate an axis from the x axis and add it with an advanced time interval (3 Days) intervals.
   Dim tia As TimeIntervalAdvanced = TimeIntervalAdvanced.Days
   tia.Multiplier = 3
   Dim calculatedXAxis As Axis = Chart.XAxis.Calculate("TimeIntervalAdvanced: Every 3 Days", tia)
   calculatedXAxis.Orientation = dotnetCHARTING.Orientation.Top
   Chart.AxisCollection.Add(calculatedXAxis)
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim dt As New DateTime(2005, 1, 1)
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 0 To 22
         dt = dt.AddDays(1)
         Dim e As New Element()
         'e.Name = "Element " + b.ToString()
         e.XDateTime = dt
         'e.YValue = -25 + myR.Next(50)
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   
   
   Return SC
End Function 'getRandomData

        </script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
