<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates is used with NavigatorSelectorControl

		Chart.Size = "620x350"

		Chart.TempDirectory = "temp"
		Chart.Debug = True
	   ' Chart.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
		Chart.DefaultSeries.Type = SeriesType.Line
		Chart.LegendBox.Position = LegendBoxPosition.ChartArea
		Chart.ShowDateInTitle = False
		Chart.YAxis.Scale = Scale.Range
		Chart.XAxis.Scale = Scale.Time

		' Set empty element handling.
		Chart.DefaultSeries.EmptyElement.Mode = EmptyElementMode.Fill
		Chart.DefaultSeries.EmptyElement.Line.Color = Color.LightGray

		'Chart.Navigator.Enabled = true;
		Chart.DefaultElement.Marker.Visible = False
		Chart.DefaultSeries.LegendEntry.Value = "%Sum -"
		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx



		If Not Page.Request.Params("low") Is Nothing AndAlso Not Page.Request.Params("high") Is Nothing Then

			low = DateTime.Parse(Page.Request.Params("low"))
			high = DateTime.Parse(Page.Request.Params("high"))
			' Get x and y points

		End If

		Dim mySC As SeriesCollection = getLiveData(low,high)

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)


		SetupPie()
	End Sub
 Dim low As DateTime = New DateTime(2002, 1, 1)
		Dim high As DateTime = New DateTime(2003, 1, 20)

	Sub SetupPie()
		Chart1.Type = ChartType.Pie
		Chart1.Size = "620x150"
		Chart1.TempDirectory = "temp"
		Chart1.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart1.Margin = "0"
		Dim mySC As SeriesCollection = getLiveData(low, high)
		Chart1.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2010, 1, 1)

		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 14
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData(ByVal low As DateTime, ByVal high As DateTime) As SeriesCollection
        Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = low
		de.EndDate = high
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.DateGrouping = TimeInterval.Days
		de.SqlStatement = "SELECT OrderDate,Total,Name FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#"
		de.DataFields = "XDateTime=OrderDate,YValue=Total,SplitBy=Name"
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
	<dnc:Chart ID="Chart1" runat="server" />
		<dnc:Chart ID="Chart" runat="server" /> 
	</div>
</body>
</html>
