	<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Forecast Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of TrendLinePower from  within Forecasting 	
   ' which find the best fitting power curve to sets of data
   ' The Forecast Chart
   ForecastChart.Title = "Forecast Power"
   ForecastChart.TempDirectory = "temp"
   ForecastChart.Debug = True
   ForecastChart.Size = "1000x400"
   ForecastChart.LegendBox.Template = "%icon %name"
   ForecastChart.XAxis.Scale = Scale.Normal
   
   Dim spower As New SeriesCollection()
   Dim sampledata1 As New Series("Sample 1")
   Dim i As Integer
   For i = 1 To 9
      Dim el As New Element()
      el.YValue = 2 * Math.Pow(i, 3)
      el.XValue = i
      sampledata1.Elements.Add(el)
   Next i
   
   spower.Add(sampledata1)
   spower(0).Type = SeriesType.Marker
   
   ForecastChart.SeriesCollection.Add(spower)
   
   ' Here we create a series which will hold the Y values calculated with the  PowerFit indicator
   Dim trendLinePower As New Series()
   trendLinePower = ForecastEngine.TrendLinePower(sampledata1)
   
   'The next two lines display no to the chart the power function used
   ' to fit the curve
   trendLinePower.Elements(0).SmartLabel.Text = "Function: %Function"
   trendLinePower.Elements(0).ShowValue = True
   trendLinePower.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   
   trendLinePower.DefaultElement.Color = Color.FromArgb(255, 99, 49)
   trendLinePower.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(trendLinePower)
End Sub 'Page_Load 

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="ForecastChart" runat="server"/>
			
			
		</div>
	</body>
</html>
