<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		'This sample demonstrates using a simple moving average over an n number of days.

		Chart.Type = ChartType.Combo
		Chart.Width = 600
		Chart.Height = 350
		Chart.TempDirectory = "temp"
		Chart.Debug = True

		' Set some styling properties.
		Chart.ShadingEffectMode = ShadingEffectMode.Four
		Chart.LegendBox.Position = New Point(40, 15)
		Chart.LegendBox.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.LegendBox.DefaultCorner = BoxCorner.Round
		Chart.ChartArea.DefaultCorner = BoxCorner.Round
		Chart.ChartArea.CornerSize = 20
		Chart.MarginTop = 25
		Chart.DefaultElement.Marker.Size = 15
		Chart.DefaultSeries.Line.Width = 3

		Chart.XAxis.TimeInterval = TimeInterval.Day

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx
		Dim sc As SeriesCollection = getRandomData(3)

		' Because the simple moving average calculation takes elements starting with the latest, the series is reversed using the sort method.
		sc.Sort(ElementValue.XDateTime, "Desc")

		' Get the moving average
		Dim ma As SeriesCollection = FinancialEngine.SimpleMovingAverage(sc, ElementValue.YValue, 5)

		ma(0).Type = SeriesType.Line
		ma(0).Name = "Simple Moving Average (5 Days)"

		' Add the random data.    
		Chart.SeriesCollection.Add(sc)

		' Add the moving average.
		Chart.SeriesCollection.Add(ma)
	End Sub

	Function getRandomData(ByVal seed As Integer) As SeriesCollection
		Dim myR As Random = New Random(seed)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2009, 1, 1)
		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 14
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function
</script>

<html>
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
