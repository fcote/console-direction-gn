<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates replacing labels with images anywhere a label can go.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "600x350"
		Chart.Title = "  Country Annotation Images  "
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Three
		Chart.TitleBox.Label.Font = New Font("Arial", 12,FontStyle.Bold)
	Chart.TitleBox.Label.Color = Color.White
		'Chart.TitleBox.Position = TitleBoxPosition.Full;
		Chart.TitleBox.Background.Color = Color.LightGray
		Chart.LegendBox.Visible = False
	Chart.ShadingEffectMode = ShadingEffectMode.Seven


		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		mySC(0)(0).Annotation.Label.Text = "<img:../../images/us.png>"
		mySC(0)(1).Annotation.Label.Text = "<img:../../images/ca.png>"
		mySC(0)(2).Annotation.Label.Text = "<img:../../images/uk.png>"
		mySC(0)(3).Annotation.Label.Text = "<img:../../images/mx.png>"

		mySC(0)(0).Name = "US"
		mySC(0)(1).Name = "Canada"
		mySC(0)(2).Name = "UK"
		mySC(0)(3).Name = "Mexico"
	mySC(0).DefaultElement.Color = Color.Gray

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(2)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)

				e.Annotation = New Annotation()
				e.Annotation.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Three
				e.Annotation.HeaderBackground.Color = Color.LightSkyBlue
				e.Annotation.HeaderLabel.Font = New Font("Tahoma", 10,FontStyle.Bold)
				e.Annotation.Size = New Size(80,60)
				e.Annotation.HeaderLabel.Text = "%Name"
		e.Annotation.DefaultCorner = BoxCorner.Square
				e.Annotation.Background.Color = Color.White
				e.Annotation.Padding = 5

			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
