<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Financial Moving Average Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Dim connection As String = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + Server.MapPath("../../database/chartsample.mdb")
   
   
   ' This sample demonstrates the use of the SimpleMovingAverages and GeometricalMovingAverage
   ' First we declare a chart of type FinancialChart	
   ' The Financial Chart
   FinancialChart.Title = "Financial Chart"
   FinancialChart.TempDirectory = "temp"
   FinancialChart.Debug = True
   FinancialChart.ShadingEffect = True
   FinancialChart.LegendBox.Template = "%icon %name"
   FinancialChart.Size = "800X600"
   FinancialChart.PaletteName = Palette.Three
   FinancialChart.YAxis.Label.Text = "Price (USD)"
   FinancialChart.YAxis.FormatString = "currency"
   FinancialChart.YAxis.Scale = Scale.Range
   FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   FinancialChart.DefaultSeries.DefaultElement.Marker.Visible = False
   FinancialChart.DefaultSeries.Type = SeriesType.Spline
   
   ' Modify the x axis labels.
   FinancialChart.XAxis.Scale = Scale.Time
   FinancialChart.XAxis.TimeInterval = TimeInterval.Day
   FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o"
   FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month)
   FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM"
   
   ' Here we load data samples from the FinancialCompany table from within chartsample.mdb
   Dim priceDataEngine As New DataEngine(connection)
   priceDataEngine.ChartObject = FinancialChart
   priceDataEngine.ChartType = ChartType.Financial
   priceDataEngine.DateGrouping = TimeInterval.Day
   priceDataEngine.StartDate = New DateTime(2001, 4, 1)
   priceDataEngine.EndDate = New DateTime(2001, 6, 30)
   priceDataEngine.SqlStatement = "SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate"
   priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice"
   
   Dim sc As SeriesCollection = priceDataEngine.GetSeries()
   Dim prices As Series = Nothing
   If sc.Count > 0 Then
      prices = sc(0)
   Else
      Return
   End If 
   prices.DefaultElement.ToolTip = "L:%Low-H:%High"
   prices.DefaultElement.SmartLabel.Font = New Font("Arial", 6)
   prices.DefaultElement.SmartLabel.Text = "O:%Open-C:%Close"
   prices.Type = SeriesTypeFinancial.CandleStick
   
   Dim cp As New CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0000001")
   prices.Trim(cp, ElementValue.XDateTime)
   prices.Name = "Prices"
   FinancialChart.SeriesCollection.Add(prices)
   
   ' Create the second chart area 
   Dim volumeChartArea As New ChartArea()
   volumeChartArea.Label.Text = "Stock Volume"
   volumeChartArea.YAxis.Label.Text = "Volumes"
   volumeChartArea.Series.Name = "Stock Volume"
   volumeChartArea.HeightPercentage = 30
   volumeChartArea.Series.DefaultElement.ToolTip = "%YValue"
   FinancialChart.ExtraChartAreas.Add(volumeChartArea)
   
   ' Add a volume series to the chart area
   Dim volumeDataEngine As New DataEngine(connection)
   volumeDataEngine.DateGrouping = TimeInterval.Days
   volumeDataEngine.StartDate = New DateTime(2001, 4, 1)
   volumeDataEngine.EndDate = New DateTime(2001, 6, 30)
   volumeDataEngine.SqlStatement = "SELECT TransDate,volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate"   
   volumeDataEngine.DataFields = "xAxis=TransDate,yAxis=Volume"
   
   Dim volumes As Series = volumeDataEngine.GetSeries()(0)
   volumes.Trim(cp, ElementValue.XDateTime)
   volumes.Name = "Volumes"
   volumes.Type = SeriesType.Bar
   volumeChartArea.SeriesCollection.Add(volumes)
   
   'Financial Series Moving Average Indicators
   
   ' Here we display the financial simple moving average over a period of three days for the Prices Series
   FinancialChart.SeriesCollection.Add(FinancialEngine.SimpleMovingAverage(prices, ElementValue.High, 3))
   
   ' Here we display the statistical simple moving average for the Volumes Series
   volumeChartArea.SeriesCollection.Add(StatisticalEngine.SimpleMovingAverage(volumes, 3))
   
   ' Here we display the financial geometrical moving avereage over a period of three days for the Prices Series
   FinancialChart.SeriesCollection.Add(FinancialEngine.GeometricMovingAverage(prices, ElementValue.Low, 3))
   
   ' NOTE: The other moving average indicators can be displayed in a similar way
   ' Kairi indicator - measures as a percentage of the price the divergence between the a moving 
   ' average (generally the simple moving average) of the price and the price itself. 
   Dim kairiChartArea As New ChartArea()
   kairiChartArea.YAxis.Label.Text = "Kairi"
   kairiChartArea.HeightPercentage = 10
   FinancialChart.ExtraChartAreas.Add(kairiChartArea)
   
   Dim kairi As Series = FinancialEngine.Kairi(prices, ElementValue.High, FinancialEngine.SimpleMovingAverage(prices, ElementValue.High, 3))
   kairi.Type = SeriesType.Cylinder
   kairiChartArea.SeriesCollection.Add(kairi)
End Sub 'Page_Load 

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="FinancialChart" runat="server"/>
		</div>
	</body>
</html>
