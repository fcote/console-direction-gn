<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Forecasting Sample - ExponentialFit</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of TrendLineExponential from within ForecatEngine 
   ' which find the best fitting exponential curve to sets of data
   ' The Forecast Chart
   ForecastChart.Title = "Forecast Exponential"
   ForecastChart.TempDirectory = "temp"
   ForecastChart.Debug = True
   ForecastChart.Size = "1000x400"
   ForecastChart.LegendBox.Template = "%icon %name"
   ForecastChart.XAxis.Scale = Scale.Normal
   
   Dim exponential As New SeriesCollection()
   ' Generate a exponential data sample 
   Dim sampledata1 As New Series("Sample 1")
   Dim i As Integer
   For i = 1 To 11
      Dim el As New Element()
      el.YValue = Math.Exp(i) + 10
      el.XValue = i
      sampledata1.Elements.Add(el)
   Next i
   
   exponential.Add(sampledata1)
   exponential(0).Type = SeriesType.Marker
   
   ' Add the exponetial SeriesCollection to the chart
   ForecastChart.SeriesCollection.Add(exponential)
   
   ' Here we create a series which will hold the Y values calculated with the ExponentialFit 
   Dim trendLineExponential As New Series()
   trendLineExponential = ForecastEngine.TrendLineExponential(sampledata1)
   
   'The next two lines display on to the chart the exponential function used
   ' to fit the curve
   trendLineExponential.Elements(0).SmartLabel.Text = "Function: %Function"
   trendLineExponential.Elements(0).ShowValue = True
   
   trendLineExponential.DefaultElement.Color = Color.FromArgb(255, 99, 49)
   trendLineExponential.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(trendLineExponential)
End Sub 'Page_Load 
		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="ForecastChart" runat="server"/>
			
			
		</div>
	</body>
</html>
