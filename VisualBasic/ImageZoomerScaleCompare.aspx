<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>dotnetCHARTING Sample</title>
</head>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		 ' This sample demonstrates using zooming.
		Chart.Title = "A chart with zooming, right click on the chart to access zooming"
		Chart.Debug = True
		Chart.YAxis.Label.Text = "Health cost (USD)"
		Chart.YAxis.FormatString = "currency"
		Chart.XAxis.Label.Text = "Age (Years)"
		Chart.ShadingEffect = True
		Chart.DefaultSeries.PaletteName = dotnetCHARTING.Palette.Bright
		Chart.Type = ChartType.Combo
		Chart.Size = "1000x350"
		Chart.LegendBox.Position = LegendBoxPosition.None
		Chart.DefaultSeries.Type = SeriesType.Line
		Chart.DefaultSeries.DefaultElement.ToolTip = "Year:%XValue: %YValue"
		Chart.XAxis.Interval =1
		Dim mySC As SeriesCollection = getRandomData()
		Chart.SeriesCollection.Add(mySC)
		'Enable zooming with just one property
		Chart.Zoomer.Enabled =True


		Chart2.Title = "A chart with initial zooming, right click on the chart for zooming options"
		Chart2.Debug = True
		Chart2.TempDirectory = "temp"
		Chart2.YAxis.Label.Text = "Health cost (USD)"
		Chart2.YAxis.FormatString = "currency"
		Chart2.XAxis.Label.Text = "Age (Years)"
		Chart2.ShadingEffect = True
		Chart2.DefaultSeries.PaletteName = dotnetCHARTING.Palette.Bright
		Chart2.LegendBox.Position = LegendBoxPosition.None
		Chart2.DefaultElement.ShowValue=True
		Chart2.Type = ChartType.Combo
		Chart2.Size = "1000x350"
		Chart2.DefaultSeries.Type = SeriesType.Line
		Chart2.XAxis.Interval = 1
		Chart2.DefaultElement.SmartLabel.Text = "%XValue: %YValue"
		Chart2.DefaultSeries.DefaultElement.ToolTip = "Year:%XValue: %YValue"
		Dim mySC2 As SeriesCollection = getRandomData()
		Chart2.SeriesCollection.Add(mySC2)

		'set for initial zooming  
		Chart2.Zoomer.Enabled =True
		Chart2.XAxis.Viewport.ScaleRange = New ScaleRange(0,25)
		'Chart2.YAxis.Viewport.ScaleRange = new ScaleRange(0,600);

	End Sub

	Private Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Health care cost/ Age")
			For b = 0 To 79
				Dim e As Element = New Element()
				e.XValue = b 'myR.Next(1,100);// b * a;
				If b=0 Then
					e.YValue=6500
				ElseIf b=1 Then
					e.YValue=3100
				ElseIf b=2 Then
					e.YValue=2400
				Else
					e.YValue = 100*(Math.Pow(1.045,b)) 'myR.Next(5,500);
				End If
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function
</script>

<body>
<center>
	 <dotnet:Chart ID="Chart" runat="server" />
   <br/>
	 <dotnet:Chart ID="Chart2" runat="server" />
   </center>
</body>
</html>
