<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using the chart background box.

		Chart.Size = "600x350"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.ShadingEffectMode = ShadingEffectMode.Three
		Chart.ChartArea.Background.Color = Color.FromArgb(200,Color.White)
		Chart.ChartArea.Shadow.Visible = False

		'Style the chart background box.
		Chart.Box.Background.Color = Color.Azure
		Chart.Box.DefaultCorner = BoxCorner.Round
		Chart.Box.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.Box.HeaderLabel.Text = "<block fStyle='bold' fSize='11'>.netCHARTING Sample - Using Chart.Box Header"
		Chart.Box.HeaderLabel.Color = Color.White
		Chart.Box.HeaderLabel.GlowColor = Color.Black
		Chart.Box.HeaderBackground.Color = Color.DeepSkyBlue
		Chart.Box.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Background1
		Chart.Box.Padding = 5

		Chart.DefaultAxis.DefaultTick.Label.Font = New Font("Verdana", 10)
		Chart.DefaultAxis.DefaultTick.GridLine.Color = Color.DarkGray
		Chart.YAxis.AlternateGridBackground.Color = Color.Empty
		Chart.MarginTop = 35

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		' Add the random data.
		Chart.SeriesCollection.Add(getRandomData())

		Chart1.Size = "600x350"
		Chart1.TempDirectory = "temp"
		Chart1.Debug = True
		Chart1.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart1.ShadingEffectMode = ShadingEffectMode.Three
		Chart1.ChartArea.Background.Color = Color.White ' Color.FromArgb(200, Color.White);
		Chart1.ChartArea.Shadow.Visible = False


		'Style the chart title box.
		Chart1.TitleBox.Label.Text = "<block fStyle='bold' fSize='11'>.netCHARTING Sample - Using TitleBox"
		Chart1.TitleBox.Label.Color = Color.White
		Chart1.TitleBox.Label.GlowColor = Color.Black
		Chart1.TitleBox.Background.Color = Color.DeepSkyBlue
		Chart1.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background1
		Chart1.TitleBox.Position = TitleBoxPosition.Full
		Chart1.TitleBox.CornerTopRight = BoxCorner.Round
		Chart1.TitleBox.CornerTopLeft = Chart1.TitleBox.CornerTopRight

		'Style the Chart1 background box.
		Chart1.Box.Background.Color = Color.Azure
		Chart1.Box.DefaultCorner = BoxCorner.Round
		Chart1.Box.Background.ShadingEffectMode = ShadingEffectMode.Background2

		Chart1.Box.HeaderBackground.Color = Color.DeepSkyBlue
		Chart1.Box.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Background1
		Chart1.Box.Padding = 5

		Chart1.DefaultAxis.DefaultTick.Label.Font = New Font("Verdana", 10)
		Chart1.DefaultAxis.DefaultTick.GridLine.Color = Color.DarkGray
		Chart1.YAxis.AlternateGridBackground.Color = Color.Empty

		' Add the random data.
		Chart1.SeriesCollection.Add(getRandomData())

	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		For a As Integer = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			For b As Integer = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
		<dnc:Chart ID="Chart1" runat="server" />
	</div>
</body>
</html>
