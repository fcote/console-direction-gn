<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Use3D = True
   Chart.Debug = True
   Chart.Title = "Ford vs. GM Performance Vehicle Sales"
   Chart.DefaultSeries.DefaultElement.ShowValue = True
   Chart.DefaultSeries.DefaultElement.SmartLabel.Text = "%Name " + ControlChars.Lf + "%YValue"
   Chart.ChartArea.Label.Text = "A hidden axis is used to" + ControlChars.Lf + "stack the GM vehicle " + ControlChars.Lf + "columns."
   Chart.Palette = New Color() {Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49)}
   
   ' This sample will demonstrate how to use a second y axis to show a different scale.
   ' *STATIC DATA NOTE* 
   ' This sample uses static data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   
   ' The Ford series and element	
   Dim fs As New Series("Ford (Mustang)")
   fs.Elements.Add(New Element("1999", 5000))
   
   ' Two GM Series.
   Dim gms As New Series("GM (Covette)")
   Dim gms2 As New Series("GM (Firebird)")
   gms.Elements.Add(New Element("1999", 2134))
   gms2.Elements.Add(New Element("1999", 2134))
   
   ' In order for the gm series to be stacked we need a new axis with a stacked scale. 
   gms.YAxis = New Axis()
   gms.YAxis.Scale = Scale.Stacked
   
   ' Use the same axis for both series.
   gms2.YAxis = gms.YAxis
   
   ' Ensure scales match
   gms.YAxis.Maximum = 7000
   Chart.YAxis.Maximum = 7000
   
   ' Make the new axis invisible.
   gms.YAxis.Clear()
   
   
   ' Add the data.
   Chart.SeriesCollection.Add(fs)
   Chart.SeriesCollection.Add(gms)
   Chart.SeriesCollection.Add(gms2)
End Sub 'Page_Load 



		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server">
			</dotnet:Chart>
		</div>
	</body>
</html>
