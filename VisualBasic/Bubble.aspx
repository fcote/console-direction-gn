<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs )

	'set global properties
    Chart.Title="Bubble sample"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.Type = ChartType.Bubble
    Chart.Use3D=true
    Chart.DefaultAxis.NumberPercision=1
    Chart.DefaultSeries.DefaultElement.ShowValue=true
    Chart.DefaultSeries.DefaultElement.LabelTemplate="(%Xvalue,%Yvalue)"
    Chart.DefaultSeries.DefaultElement.ForceMarker=false
    Chart.Palette = new Color () {Color.Blue,Color.Red,Color.Yellow}
    
    'Add a series
    Chart.Series.Name="series 1"
    Chart.Series.Element = new Element("",0.4,7.5,1)
    Chart.Series.Elements.Add()
    Chart.Series.Element = new Element("",2.5,4,1.5)
    Chart.Series.Elements.Add()
    Chart.SeriesCollection.Add()
   
  	'Add a series
    Chart.Series.Name="series 2"
    Chart.Series.Element = new Element("",1.2,11,1)
    Chart.Series.Elements.Add()
    Chart.Series.Element = new Element("",2.5,-6,0.6)
    Chart.Series.Elements.Add()
    Chart.SeriesCollection.Add()
    
    'Add a series
    Chart.Series.Name="series 3"
    Chart.Series.Element = new Element("",1,-6,1)
    Chart.Series.Elements.Add()
    Chart.Series.Element = new Element("",3,14,1.3)
    Chart.Series.Elements.Add()
    Chart.SeriesCollection.Add()

   
    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Bubble sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
