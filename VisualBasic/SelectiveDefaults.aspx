<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates applying element settings to a set of elements of a series that meet a specific criteria.

		Chart.TempDirectory = "temp"
		Chart.Debug = True

		Chart.ChartArea.Label.Text = "Applies the hatched style to elements with " & Constants.vbLf & "yValues outside the 30-50 range."

		Chart.Type = ChartType.Pies
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.DefaultElement.Outline.Width = 2

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		Dim myDefaultElement As Element = New Element()
	   ' myDefaultElement.Color = Color.Orange;
		myDefaultElement.ShowValue = True
		myDefaultElement.Outline.Color = Color.White
		myDefaultElement.HatchStyle = HatchStyle.BackwardDiagonal

		myDefaultElement.HatchColor = Color.Azure
		myDefaultElement.SmartLabel.Font = New Font("Arial", 18,FontStyle.Bold)
		myDefaultElement.SmartLabel.GlowColor = Color.White

		mySC.SelectiveElementDefaults(New ScaleRange(30, 50),ElementValue.YValue,myDefaultElement)

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(2)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 7
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
