<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using an InfoGrid inside a legend box header.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Type = ChartType.Combo
		Chart.Size = "450x350"
		Chart.Title = "Server Traffic"
		Chart.XAxis.DefaultTick.Label.Font = New Font("Arial", 9, FontStyle.Bold)

		Chart.YAxis.Markers.Add(New AxisMarker("", Color.Crimson, 40, 100))
		Chart.YAxis.Markers(0).Background.Transparency = 30
		Chart.YAxis.Markers(0).LegendEntry.Visible = False

		Chart.DefaultSeries.LegendEntry.Visible = False
		Chart.LegendBox.Shadow.Visible = False
		Chart.LegendBox.HeaderBackground.Color = Color.GhostWhite
		Chart.LegendBox.HeaderBackground.ShadingEffectMode = ShadingEffectMode.One
		Chart.LegendBox.HeaderBackground = New Background(Color.FromArgb(150, Color.Gainsboro), Color.White, 90)
		Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		Dim s As String = generateMarkup(mySC)
		Chart.LegendBox.HeaderLabel.Text = s

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function generateMarkup(ByVal sc As SeriesCollection) As String
		' If all series dont have the same number of of elements it may break the InfoGrid so grouping them can help.
		sc.GroupByElements()

		' Define some block styles
		Dim headerStyle As String = "<block fStyle='bold' fSize='9'>"
		Dim headerStyle2 As String = "<block fStyle='bold' fSize='9' fColor='blue'>"
		Dim totalsStyle As String = "<block fStyle='bold' fSize='10'>"

		Dim sb As StringBuilder = New StringBuilder()

		' Add header titles matching the final number of columns.
		sb.Append("<block><block>")
		For Each e As Element In sc(0).Elements
			sb.Append(headerStyle & e.Name)
		Next e
		sb.Append(headerStyle2 & "  Total")
		sb.Append(headerStyle2 & "  Average")

		' Horizontal Rule
		sb.Append("<hr>")

		' Now each row is populated with data.
		Dim bg As String = ""
		Dim i As Integer = 0
		For Each s As Series In sc
			' A block using the series color as the background and the series name. (2 blocks)
			sb.Append(headerStyle & s.Name & "<block bgColor='" & getHTMLColor(Chart.Palette(i)) & "'> ")

			' If odd column use a background color
			If ((i And 1) <> 0) Then
			bg = " bgColor='LightGray'"
			Else
				bg = ""
			End If

			' A block for each element
			For Each e As Element In s.Elements
				' Differ the styling based on the element value.
				If e.YValue > 40 Then
					sb.Append("<block fColor='red' fStyle='bold' " & bg & ">")
				Else
					sb.Append("<block" & bg & ">")
				End If

				sb.Append(e.YValue)
			Next e

			' define style for calculation label
			Dim calculationStyle As String = "<block fStyle='italic' fSize='9' " & bg & ">"
			sb.Append(calculationStyle & s.Calculate("", Calculation.Sum).YValue.ToString())
			sb.Append(calculationStyle & s.Calculate("", Calculation.Average).YValue.ToString())

			' Indicate that a new row should start but omit the last row which would start a new row with 0 blocks and cause the grid to misalign.
            If i < sc.Count - 1 Then
                sb.Append("<row>")
            End If
            i = i + 1
            
		Next s

		' Add a row of totals
		Dim sum As Series = sc.Calculate("", Calculation.Sum)

		sb.Append("<hr>")
		sb.Append(totalsStyle & "Totals:<block>")

		' For each calculated element.
		For Each e As Element In sum.Elements
			sb.Append(totalsStyle & e.YValue)
		Next e

		' Even our the block count.
		sb.Append("<block><block>")

		Return sb.ToString()

	End Function

	''' <summary>
	''' Helper method to turn a color object into HTML color format like #A0187E
	''' </summary>
	Function getHTMLColor(ByVal c As Color) As String
		Dim col As String = c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2")
		Return "#" & col
	End Function

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 4
			Dim s As Series = New Series("Week " & a.ToString())
			For b = 1 To 5
				Dim e As Element = New Element("Server " & b.ToString())
				e.YValue = myR.Next(50) + (a * 5)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function



</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
