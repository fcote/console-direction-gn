<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Gauges
   Chart.Size = "600X350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample will demonstrate how multiple needles behave on a guage chart.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim mySC As SeriesCollection = getRandomData()
   
   ' By Setting the palette name on a series, 
   mySC(0).PaletteName = Palette.Pastel
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim s As New Series()
   s.Name = "Series 1"
   Dim b As Integer
   For b = 1 To 4
      Dim e As New Element()
      e.Name = "Element " + b.ToString()
      'e.YValue = -25 + myR.Next(50);
      e.YValue = myR.Next(50)
      s.Elements.Add(e)
   Next b 
   SC.Add(s)
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   
   
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
