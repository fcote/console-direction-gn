<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Save Legend Image</title>
<script runat="server">

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   Dim Chart As New dotnetCHARTING.Chart()
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
   
   ' Save the legend as a separate image file
   ImageLegend.ImageUrl = Chart.FileManager.SaveImage(Chart.GetLegendBitmap())
   
   ' Remove the legend from the chart and save the chart as a separate image file
   Chart.LegendBox.Position = LegendBoxPosition.None
   ImageChart.ImageUrl = Chart.FileManager.SaveImage(Chart.GetChartBitmap())
End Sub 'Page_Load

</script>
</head>

<body>
<font size="2" face="Arial">This chart image can be placed freely in your page 
and treated as a regular image file:<br>
<asp:Image id="ImageChart" runat="server"/><br><br>
The legend below is a separate image file which can be independently placed in 
the page.<br><br></font>
         

              

<table border="0" style="border-collapse: collapse" id="table1" cellpadding="4">
	<tr>
		<td><asp:Image id="ImageLegend" runat="server"/></td>
		<td><font size="2" face="Arial">This feature is useful for custom layouts but it 
is also a powerful addition for multiple chart alignment as chart width is not 
affected by differing legend size requirements on the chart surface.&nbsp; In 
		this sample the legend is added to a table so this text can adjoin it.</font></td>
	</tr>
</table>
              

<p>&nbsp;</p>
</body>
</html>
