<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.Title = "Financial chart"
   Chart.Type = ChartType.Financial
   Chart.XAxis.Scale = Scale.Time
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.ShadingEffect = True
   Chart.LegendBox.Position = LegendBoxPosition.None
   Chart.Size = "600X500"
   
   ' Set the financial chart type
   Chart.DefaultSeries.Type = SeriesTypeFinancial.CandleStick
   Chart.DefaultSeries.DefaultElement.ShowValue = True
   Chart.DateGrouping = TimeInterval.Days
   
   
    
   ' Set the format and label
        Chart.ChartArea.XAxis.FormatString = "d"
        Chart.ChartArea.XAxis.Label.Text = "Days"
   
   ' Set the time padding for the x axis.
        Chart.ChartArea.XAxis.TimePadding = New TimeSpan(2, 0, 0, 0)
   
   
   Chart.Series.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   Chart.Series.StartDate = New DateTime(2003, 6, 1, 0, 0, 0)
   Chart.Series.EndDate = New DateTime(2003, 6, 30, 23, 59, 59)
   
   Chart.Series.SqlStatement = "SELECT TransDate,volume,price FROM Financial WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate"
   Chart.Series.DataFields = "xAxis=TransDate,price=price,yAxis=volume"
   Chart.SeriesCollection.Add()
End Sub 'Page_Load 
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>.netCHARTING Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
