<%@ Page Language="VB" Debug="false" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender as Object,e as EventArgs)

	

	If Not IsPostBack Then
    
    	DropDownLimitMode.Items.Add("Top")
        DropDownLimitMode.Items.Add("Bottom")
		DropDownLimitMode.Items.Add("ExcludeTop")
		DropDownLimitMode.Items.Add("ExcludeBottom")
		If Not (Request.QueryString("limitMode") Is Nothing) Then
	 	 DropDownLimitMode.SelectedIndex = CInt([Enum].Parse(GetType(dotnetCHARTING.LimitMode), Request.QueryString("limitMode"), True))
        End If
    End If
    
   

	'set global properties
	Chart.Title="Sales"
    Chart.ChartArea.YAxis.Label.Text="Sales"
    Chart.ChartArea.YAxis.FormatString="Currency"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DefaultSeries.DefaultElement.Transparency=15
    Chart.Use3D=true
    Chart.Size="500x450"
    Chart.TitleBox.Position=TitleBoxPosition.FullWithLegend

    'Add a series
    Chart.Series.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
    Chart.Series.StartDate= New System.DateTime(2002,1,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,12,31,23,59,59)
    Chart.Series.Name="Sales"
    Chart.Series.SqlStatement= "SELECT Name,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.Name ORDER BY Sum(Quantity) DESC"
    Chart.SeriesCollection.Add()

    



'This is the same chart with the ShowOther property set
'this will add an element representing the total sales
'for other customers not in the top five.

    'set global properties
    Chart2.Title="Sales"
    Chart2.ChartArea.YAxis.Label.Text="Sales"
    Chart2.ChartArea.YAxis.FormatString="Currency"
    Chart2.TempDirectory="temp"
    Chart2.Debug=true
    Chart2.DefaultSeries.DefaultElement.Transparency=15
    Chart2.Use3D=true
    '"OtherAll", shows all other elements in the second level.
    '"Other" shows the next number of elements has been set in limit propertes.
    Chart2.DrillDownChain="other" 
    Chart2.Series.DefaultElement.ToolTip="%yvalue"
    Chart2.Size="500x450"
    Chart2.TitleBox.Position=TitleBoxPosition.FullWithLegend
    Chart2.YAxis.SmartScaleBreak = true





    'Add a series
    Chart2.Series.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
    Chart2.Series.StartDate = New System.DateTime(2002,1,1,0,0,0)
    Chart2.Series.EndDate = New System.DateTime(2002,12,31,23,59,59)
    Chart2.Series.Name = "Sales"
    Chart2.Series.SqlStatement= "SELECT Name,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.Name  ORDER BY Sum(Quantity) DESC"
    Chart2.Series.Limit=TextBoxLimit.Text       
    Chart2.Series.LimitMode = CType([Enum].Parse(GetType(dotnetCHARTING.LimitMode), DropDownLimitMode.SelectedItem.Value, True), dotnetCHARTING.LimitMode)
	
	Chart2.Series.ShowOther=true
    Chart2.Series.OtherElementText ="Other"
    Chart2.SeriesCollection.Add()
    
       
End Sub
Sub ButtonSet_Click(sender as object, e as  EventArgs ) 



End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Drill down on other element</title>
 <style type="text/css">
     body.myBody  
     {
     border-top-width:0px;
      border-left-width:0px;
     
     }
     img.myImage  
     {
     border:0px;
     
     }
     td.myTD1  
     {
      width:"1%";
      border:1px; 
      margin:0;
     }
     td.myTD2  
     {     
      width:"99%";
      background:"#BFC0DB";
      border-collapse:collapse;
       
      
     }
     table.myTable
     {
      border:1px; 
      border-style:solid;
      border-spacing:0;
      border-color:"#111111";
      border-collapse:collapse;
 
     }
     
    </style>
</head>
<body class="myBody">
    <form runat="server" action="drilldownOtherElement.aspx">
        <div style="text-align:center">
            <table class="myTable"    id="AutoNumber1">
                
                <tr>
    <td class="myTD1">
<img class="myImage" alt="dotnetChartingImage" src="../../images/dotnetCharting.gif" width="230" height="94"/></td>
    <td class="myTD2">Limit: 
    
 <ASP:DropDownList id="DropDownLimitMode" runat="server">
    </ASP:DropDownList>
    <ASP:TextBox id="TextBoxLimit" width="30" runat="server"/>
     <asp:Button id="ButtonSet" OnClick="ButtonSet_Click" runat="server" Text="Set">
    </asp:Button>
 </td>
          </tr>
            </table>
            </div>
            </form>
 
<div style="text-align:center">
 <table>
<tbody>
<tr>
<td>
 <dotnet:Chart id="Chart"  runat="server"/>
 </td><td>
 <dotnet:Chart id="Chart2"  runat="server"/>
</td>
</tr>
</tbody>
</table>
 
</div>
</body>
</html>
