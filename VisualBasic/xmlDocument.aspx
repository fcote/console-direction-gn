<%@ Page Language="vb" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml" %>

<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   'set Title 
   Chart.Title = "sales report"
   
   ' Set the x axis label
   Chart.ChartArea.XAxis.Label.Text = "X Axis Label"
   
   Chart.XAxis.FormatString = "MMM d"
   
   ' Set the y axis label
   Chart.ChartArea.YAxis.Label.Text = "Y Axis Label"
   
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   'Adding series programatically
   Chart.Series.Name = "Sales"
   Chart.Series.Data = GetXmlDocument()
   Chart.Series.DataFields = "xaxis=Name,yaxis=Total"
   Chart.SeriesCollection.Add()
End Sub 'Page_Load
 

Function GetXmlDocument() As XmlDocument
   Dim doc As New XmlDocument()
   doc.Load(Server.MapPath("../../database/Orders.xml"))
   Return doc
End Function 'GetXmlDocument 
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>XmlDocument Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
