<%@ Page Language="VB" debug="false" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>

<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs)

	'set global properties
    Chart.Title="Progress Report"

    Chart.YAxis.Label.Text="Developers"
   
    Chart.XAxis.Label.Text="Weeks"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.Type=ChartType.Gantt
    Chart.Use3D=true
    Chart.ShadingEffect=true
    Chart.OverlapFooter=true
    
    Chart.DefaultSeries.DefaultElement.ToolTip="%yValueStart,%yvalue"
    Chart.DefaultSeries.DefaultElement.Transparency = 20
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.DefaultSeries.DataFields="ganttstartdate=start,ganttenddate=end,ganttname=name,ganttcomplete=completePercentage"

    
    'Add a series
    Chart.Series.Name="Research"
    Chart.Series.SqlStatement= "SELECT [start],[end],[name],[completePercentage] FROM ganttSample Where job='Research'"
    Chart.SeriesCollection.Add()
    
    'Add a series
    Chart.Series.Name="Design"
    Chart.Series.SqlStatement= "SELECT [start],[end],[name],[completePercentage] FROM ganttSample Where job='Design'"
    Chart.SeriesCollection.Add()
    
    'Add a series
    Chart.Series.Name="Implementation"
    Chart.Series.SqlStatement= "SELECT [start],[end],[name],[completePercentage] FROM ganttSample Where job='Implementation'"
    Chart.SeriesCollection.Add()
    
    'Add a series
    Chart.Series.Name="Test"
    Chart.Series.SqlStatement= "SELECT [start],[end],[name],[completePercentage] FROM ganttSample Where job='Test'"
    Chart.SeriesCollection.Add()
    
    'Add a series
    Chart.Series.Name="Package"
    Chart.Series.SqlStatement= "SELECT [start],[end],[name],[completePercentage] FROM ganttSample Where job='Package'"
    Chart.SeriesCollection.Add()

   
  
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Orders Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
