<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		 ' This sample demonstrates adding a sparkline microChart into a legend box value column.

		Chart.Size = "700x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.PaletteName = Palette.MidTones

		' Set Chart Type properties.
		Chart.DefaultSeries.Type = SeriesType.AreaLine
		Chart.YAxis.Scale = Scale.Stacked
		Chart.DefaultElement.Marker.Visible = False

		' This line will replace the default label with a microchart.
		Chart.LegendBox.DefaultEntry.Value = "<Chart:Sparkline values='%YValueList' highlight='10,30' colors='Gray,Red'>"

		' Add the random data.
		Chart.SeriesCollection.Add(getRandomData())
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(2)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 9
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 14
				Dim e As Element = New Element()
				e.XValue = b
				e.YValue = 10 + myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
