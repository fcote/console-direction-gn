<%@ Page Language="vb" Debug="true" Trace="false" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>

<script runat="server">
	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		Dim de As DataEngine
		Dim sc As SeriesCollection = Nothing
		Dim seriesObj As Series = Nothing

		'set global properties
		Chart.Title = "Item sales"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.DefaultSeries.DefaultElement.ToolTip = "%yvalue"

		Dim curTimeInterval As dotnetCHARTING.TimeInterval = TimeInterval.Years

		Dim dateGrouping As String = HttpContext.Current.Request.QueryString("dategrouping")
		If dateGrouping Is Nothing OrElse dateGrouping = "" Then
			dateGrouping = "Years"
		ElseIf Not dateGrouping Is Nothing AndAlso dateGrouping <> "" Then
			curTimeInterval = CType(System.Enum.Parse(GetType(dotnetCHARTING.TimeInterval), dateGrouping, True), dotnetCHARTING.TimeInterval)
		End If
		Dim startDateString As String = HttpContext.Current.Request.QueryString("startDate")
		Dim startDate As DateTime = New DateTime(2002, 1, 1, 0, 0, 0)
		If Not startDateString Is Nothing AndAlso startDateString <> "" Then
			startDate = DateTime.Parse(startDateString)
		End If


		Dim endDate As DateTime = New DateTime()

		Dim sqlStatement As String = "SELECT OrderDate,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"

		Select Case curTimeInterval
			Case TimeInterval.Years
				Chart.XAxis.Label.Text = "Years"
				de = getDE(curTimeInterval, startDate)
				de.EndDate = New System.DateTime(2002, 12, 31, 23, 59, 59)
				sc = de.GetSeries()
				If Not sc Is Nothing AndAlso sc.Count > 0 Then
					seriesObj = sc(0)
					For Each el As Element In seriesObj.Elements
						el.URL = "?dategrouping=quarters&startDate=" & startDate.ToString()
						startDate = startDate.AddYears(1)
					Next el
				End If
			Case TimeInterval.Quarters
				Chart.XAxis.Label.Text = "Quarters"
				de = getDE(curTimeInterval, startDate)
				de.EndDate = New System.DateTime(2002, 12, 31, 23, 59, 59)
				sc = de.GetSeries()
				If Not sc Is Nothing AndAlso sc.Count > 0 Then
					seriesObj = sc(0)
					For Each el As Element In seriesObj.Elements
						el.URL = "?dategrouping=months&startDate=" & startDate.ToString()
						startDate = startDate.AddMonths(3)
					Next el
				End If
			Case TimeInterval.Months
				Chart.XAxis.Label.Text = "Months"
				de = getDE(curTimeInterval, startDate)
				endDate = startDate.AddMonths(3)
				endDate = endDate.AddSeconds(-1)
				de.EndDate = endDate
				sc = de.GetSeries()
				If Not sc Is Nothing AndAlso sc.Count > 0 Then
					seriesObj = sc(0)
					For Each el As Element In seriesObj.Elements
						el.URL = "?dategrouping=days&startDate=" & startDate.ToString()
						startDate = startDate.AddMonths(1)
					Next el
				End If
			Case TimeInterval.Days
				Chart.XAxis.Label.Text = "Days"
				de = getDE(curTimeInterval, startDate)
				endDate = startDate.AddMonths(1)
				endDate = endDate.AddSeconds(-1)
				de.EndDate = endDate

				sc = de.GetSeries()
				If Not sc Is Nothing AndAlso sc.Count > 0 Then
					seriesObj = sc(0)
					For Each el As Element In seriesObj.Elements
						el.URL = "?dategrouping=hours&startDate=" & startDate.ToString()
						startDate = startDate.AddDays(1)
					Next el
				End If
			Case TimeInterval.Hours
				Chart.XAxis.Label.Text = "hours"
				de = getDE(curTimeInterval, startDate)
				endDate = startDate.AddDays(1)
				endDate = endDate.AddSeconds(-1)
				de.EndDate = endDate
				sc = de.GetSeries()
		End Select

		Chart.SeriesCollection.Add(sc)

	End Sub

	Function getDE(ByVal dg As TimeInterval, ByVal st As DateTime) As DataEngine
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"), "SELECT OrderDate,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate")
		de.DateGrouping = dg
		de.StartDate = st
		Return de
	End Function
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Drill Down Manual Sample</title>
</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
