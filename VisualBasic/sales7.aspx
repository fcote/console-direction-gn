<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs)

	'set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.Title="Sales"
    Chart.XAxis.Label.Text="Customers"
    Chart.YAxis.Label.Text="Orders"
    Chart.Type = ChartType.Radars
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.YAxis.Minimum = 0
	Chart.YAxis.Interval = 10

 	Chart.DefaultSeries.Limit="7"
 	Chart.DefaultSeries.DefaultElement.ShowValue=true
 	Chart.DefaultSeries.DefaultElement.Transparency=60

   'Add Apr series
    Chart.Series.Name="Apr"
    Chart.Series.StartDate= New System.DateTime(2002,4,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,4,30,23,59,59)
    Chart.Series.SqlStatement= "SELECT Name,Sum(1) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Name"
    Chart.SeriesCollection.Add()
 
    'Add May series
    Chart.Series.Name="May"
    Chart.Series.StartDate= New System.DateTime(2002,5,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,5,31,23,59,59)
    Chart.Series.SqlStatement= "SELECT Name,Sum(1)  FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Name"
    Chart.SeriesCollection.Add()
    
    'Add June series
    Chart.Series.Name="Jun"
    Chart.Series.StartDate= New System.DateTime(2002,6,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,6,30,23,59,59)
    Chart.Series.SqlStatement= "SELECT Name,Sum(1)  FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Name"
    Chart.SeriesCollection.Add()  
    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Sales Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
