<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using the Navigator with a financial bar series type.

		chart.Size = "600x350"

		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		chart.LegendBox.Position = LegendBoxPosition.ChartArea
		chart.Navigator.Enabled = True

		chart.YAxis.Scale = Scale.Range
		chart.YAxis.FormatString = "Currency"
		chart.XAxis.Viewport.ScaleRange = New ScaleRange(New DateTime(2010, 1, 1), New DateTime(2010, 6, 1))

		Dim mySC As SeriesCollection = getLiveData()
		mySC(0).Type = SeriesTypeFinancial.Bar
		mySC(0).Name = "MSFT"

		' Add the random data.
		chart.SeriesCollection.Add(mySC)
	End Sub

	Function getLiveData() As SeriesCollection
        Dim sqlStatement As String = "SELECT TransDate,OpenPrice,ClosePrice,HighPrice,LowPrice FROM MSFT WHERE TransDate >= #STARTDATE# "
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"), sqlStatement)
		de.StartDate = New DateTime(2000, 1, 1)
		de.ChartObject = chart ' Necessary to view any errors the dataEngine may throw.
        de.DataFields = "Xvalue=TransDate,Open=OpenPrice,Close=ClosePrice,High=HighPrice,Low=LowPrice"
		Return de.GetFinancialSeries()
	End Function


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="chart" runat="server" />
	</div>
</body>
</html>
