<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates creating a chart that draws only an annotation.

		Chart.TempDirectory = "temp"
		Chart.Debug = True

		' Create the annotation.
		Dim an As Annotation = New Annotation("This is an annotation chart <Chart:Column values='5,4,3,5,6,5,7'>")
		an.DynamicSize = False
		an.Size = New Size(200, 50)
		an.Background.Color = Color.GreenYellow
		an.Background.ShadingEffectMode = ShadingEffectMode.Background2

		' Use the annotation to create the chart.
		Chart.ObjectChart = an

	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
