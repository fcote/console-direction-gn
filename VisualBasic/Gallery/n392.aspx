<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	' Set the size
	Chart.Width = 600
	Chart.Height = 350
	' Set the temp directory
	Chart.TempDirectory = "temp"
	' Debug mode. ( Will show generated errors if any )
	Chart.Debug = True

	' Enable the Navigator.
	Chart.Navigator.Enabled = True

	Chart.DefaultSeries.Type = SeriesType.Line


	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Dim mySC As SeriesCollection = getRandomData()

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)

End Sub

Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random()
	For a As Integer = 0 To 0
		Dim s As Series = New Series()
		s.Name = "Series " & (a+1).ToString()
		For b As Integer = 0 To 14
			Dim e As Element = New Element()
			e.Name = "Element " & (b+1).ToString()
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).PaletteName = Palette.Two

	Return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
		</div>
	</body>
</html>
