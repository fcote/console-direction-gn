<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates several features of the ProgressGauge microChart.

		Chart.TempDirectory = "temp"
		Chart.Background.Color = Color.FromArgb(244, 244, 244)
		Chart.LabelChart.Alignment = StringAlignment.Near

		Dim s As String = "<block fStyle='Bold'>ProgressGauge Type Options<block><hr>"
		s &= "Plain BarGauge: <Chart:ProgressGauge value='10'>"

		' Setting a scale maximum value
		s &= "<row>BarGauge & scale max <Chart:ProgressGauge value='10' max='20'><hr>"

		' Specifying a color
		s &= "<row>BarGauge with Color <Chart:ProgressGauge value='10' color='LightSteelBlue' ><hr>"

		' Bar Gauge Shading Effect Modes.
		s &= "<row>Bar Shading 1 <Chart:ProgressGauge value='10' shading='1' color='LightSteelBlue'>"
		s &= "<row>Bar Shading 2 <Chart:ProgressGauge value='10' shading='2' color='PeachPuff'>"
		s &= "<row>Bar Shading 3 <Chart:ProgressGauge value='10' shading='3' color='DarkGreen'>"
		s &= "<row>Bar Shading 4 <Chart:ProgressGauge value='10' shading='4' color='DarkRed'>"
		s &= "<row>Bar Shading 5 <Chart:ProgressGauge value='10' shading='5' color='DodgerBlue'>"
		s &= "<row>Bar Shading 6 <Chart:ProgressGauge value='10' shading='6' color='LightSteelBlue'>"
		s &= "<row>Bar Shading 7 <Chart:ProgressGauge value='10' shading='7' color='LightSteelBlue'><hr>"

		' Specifying a size
		s &= "<row>ProgressGauge with size set<Chart:ProgressGauge value='10' max='20' width='150' height='15' color='Bisque'>"
		Chart.ObjectChart = New dotnetCHARTING.Label(s)
	End Sub


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
