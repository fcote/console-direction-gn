<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates several features of the Marker microChart.

		Chart.TempDirectory = "temp"
		Chart.Background.Color = Color.FromArgb(244, 244, 244)
		Chart.LabelChart.Alignment = StringAlignment.Near

		Dim s As String = "<block fStyle='Bold'>Marker Type Options<block><hr>"

		' The types of markers available.
		s &= "Plain Marker (Circle): <Chart:Marker>"
		s &= "<row>Arrow Up Marker : <Chart:Marker type='ArrowUp' color='LightSteelBlue'>"
		s &= "<row>Arrow Down Marker: <Chart:Marker type='ArrowDown' color='PeachPuff'>"
		s &= "<row>Square Marker: <Chart:Marker type='Square'color='Lavender'>"
		s &= "<row>Triangle Marker: <Chart:Marker type='Triangle'color='Aquamarine'>"
		s &= "<row>TriangleUpsideDown Marker: <Chart:Marker type='TriangleUpsideDown' color='Maroon'>"
		s &= "<row>Diamond Marker: <Chart:Marker type='Diamond' color='Peru'>"
		s &= "<row>FourPointStar Marker: <Chart:Marker type='FourPointStar' color='DeepSkyBlue'>"
		s &= "<row>FivePointStar Marker: <Chart:Marker type='FivePointStar' color='Plum'>"
		s &= "<row>SixPointStar Marker: <Chart:Marker type='SixPointStar' color='DarkSlateBlue'>"
		s &= "<row>SevenPointStar Marker: <Chart:Marker type='SevenPointStar' color='DarkSeaGreen'>"
		s &= "<row>Split Marker: <Chart:Marker type='Split' color='Sienna'>"
		s &= "<row>ReverseSplit Marker: <Chart:Marker type='ReverseSplit' color='IndianRed'>"
		s &= "<row>Merger Marker: <Chart:Marker type='Merger' color='Indigo'>"
		s &= "<row>Dividend Marker: <Chart:Marker type='Dividend' color='Tomato'>"


		' The circle marker also supports shading effect modes.
		s &= "<hr><block fStyle='Bold'>Circle Marker Shading<block><hr>"
		s &= "Shading 1: <Chart:Marker shading='1' size='30' color='chartreuse'>"
		s &= "<row>Shading 2: <Chart:Marker shading='2' size='30' color='OrangeRed'>"
		s &= "<row>Shading 3: <Chart:Marker shading='3' size='30' color='darkturquoise'>"
		s &= "<row>Shading 4: <Chart:Marker shading='4' size='30' color='gold'>"
		s &= "<row>Shading 6: <Chart:Marker shading='6' size='30' color='Navy'>"
		s &= "<row>Shading 7: <Chart:Marker shading='7' size='30' color='Yellow'>"
		Chart.ObjectChart = New dotnetCHARTING.Label(s)
	End Sub


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
