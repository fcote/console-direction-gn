<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

		<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	Chart.Type = ChartType.Combo 'Horizontal;
	Chart.Size = "600x350"
	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Title = ".netCHARTING Gallery Sample"


	' This sample demonstrates using the carside ImageBar.

	' The following line sets the image template.
	Chart.DefaultSeries.ImageBarTemplate = "../../../Images/ImageBarTemplates/carside"


	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Dim mySC As SeriesCollection = getRandomData()

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)

End Sub

Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random()
	Dim a As Integer 
 For a = 1 To 1
		Dim s As Series = New Series()
		s.Name = "Series " & a.ToString()
		Dim b As Integer 
 For b = 1 To 6
			Dim e As Element = New Element()
			e.Name = "Element " & b.ToString()
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	Return SC
End Function
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Gallery Sample</title>    </head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server">
			</dotnet:Chart>
		</div>
	</body>
</html>
