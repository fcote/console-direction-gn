<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	  ' This sample demonstrates bubbles with ShadingEffectMode.Seven.

	  Chart.Type = ChartType.Combo 'Horizontal;
	  Chart.Size = "600x350"
	  Chart.TempDirectory = "temp"
	  Chart.Debug = True
	  Chart.Title = ".netCHARTING Gallery Sample"
	  Chart.DefaultSeries.Type = SeriesType.Bubble

	  ' Bubble using ShadingEffectMode.Four
	  Chart.ShadingEffectMode = ShadingEffectMode.Seven


	  ' *DYNAMIC DATA NOTE* 
	  ' This sample uses random data to populate the chart. To populate 
	  ' a chart with database data see the following resources:
	  ' - Classic samples folder
	  ' - Help File > Data Tutorials
	  ' - Sample: features/DataEngine.aspx
	  Dim mySC As SeriesCollection = getRandomData()

	  ' Add the random data.
	  Chart.SeriesCollection.Add(mySC)

End Sub

Function getRandomData() As SeriesCollection
	  Dim SC As SeriesCollection = New SeriesCollection()
	  Dim myR As Random = New Random(1)
	  For a As Integer = 1 To 4
			Dim s As Series = New Series()
			s.Name = "Series " & a.ToString()
			For b As Integer = 1 To 4
				  Dim e As Element = New Element()

				  e.YValue = myR.Next(50)
				  e.BubbleSize = myR.Next(50)
				  e.XValue = myR.Next(50)
				  s.Elements.Add(e)
			Next b
			SC.Add(s)
	  Next a

	  ' Set Different Colors for our Series
	  SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	  SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	  SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	  SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	  Return SC
End Function
</script>



<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
<dotnet:Chart id="Chart" runat="server"/>
</div>
</body>
</html>
