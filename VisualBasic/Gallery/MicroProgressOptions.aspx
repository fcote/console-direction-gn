<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates several features of the Progress microChart.

		Chart.TempDirectory = "temp"
		Chart.Background.Color = Color.FromArgb(244, 244, 244)
		Chart.LabelChart.Alignment = StringAlignment.Near

		Dim s As String = "<block fStyle='Bold'>Progress Bar Type Options<block><hr>"
		s &= "Progress Bar: <Chart:Progress value='10' >"

		' Specifying a scale maximum/minimum and negative values
		s &= "<row>Bar with scale max set <Chart:Progress value='10' max='20'>"
		s &= "<row>Bar with scale max & min <Chart:Progress value='10' max='20' min='-20'>"
		s &= "<row>Bar with negative values <Chart:Progress value='-10' max='0'><hr>"

		' Specifying a color and background color
		s &= "<row>Bar Fully Colored <Chart:Progress value='10' colors='LightSteelBlue,Silver' >"

		' Using a transparent background color
		s &= "<row>Transparent Background <Chart:Progress value='10' colors='LightSteelBlue,Transparent' ><hr>"

		' Shading effect modes.
		s &= "<row>Bar Shading 1 <Chart:Progress value='10' shading='1' color='LightSteelBlue'>"
		s &= "<row>Bar Shading 2 <Chart:Progress value='10' shading='2' color='PeachPuff'>"
		s &= "<row>Bar Shading 3 <Chart:Progress value='10' shading='3' color='Lavender'>"
		s &= "<row>Bar Shading 4 <Chart:Progress value='10' shading='4' color='LightSteelBlue'>"
		s &= "<row>Bar Shading 5 <Chart:Progress value='10' shading='5' color='Aquamarine'>"
		s &= "<row>Bar Shading 6 <Chart:Progress value='10' shading='6' color='LightSteelBlue'>"
		s &= "<row>Bar Shading 7 <Chart:Progress value='10' shading='7' color='LightSteelBlue'><hr>"

		' Specifying a new size
		s &= "<row>Progress Bar with specified size <Chart:Progress value='10' max='20' width='150' height='15' color='Bisque'>"
		Chart.ObjectChart = New dotnetCHARTING.Label(s)
	End Sub


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
