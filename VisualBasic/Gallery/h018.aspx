<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates gauge indicator lights with shading effect mode five and overlayed markers.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Gauges
	Chart.DefaultSeries.GaugeType = GaugeType.IndicatorLight
	Chart.Size = "160x280"

   Chart.ShadingEffectMode = ShadingEffectMode.Three
   Chart.Margin = "0"
	Chart.DefaultElement.Marker.Size = 35
	Chart.LegendBox.Visible = False
	Chart.ChartArea.ClearColors()
	Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox
	Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round
	Chart.DefaultSeries.GaugeBorderBox.Background.Color = Color.FromArgb(20,Color.Green)
	'Chart.DefaultSeries.Background.Color = Color.Blue;

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	' Create smart palette.
	Dim sp As SmartPalette = New SmartPalette()

	' Create smart color with the range and color to use.
	Dim sc As SmartColor = New SmartColor(Color.Green, New ScaleRange(0, 30))
	Dim sc2 As SmartColor = New SmartColor(Color.Yellow, New ScaleRange(31, 60))
	Dim sc3 As SmartColor = New SmartColor(Color.Red, New ScaleRange(61, 100))



   ' sc.LegendEntry.Visible = false;

	' Add the color to the palette, and the palette to the chart.
	sp.Add("*", sc)
	sp.Add("*", sc2)
	sp.Add("*", sc3)
	Chart.SmartPalette = sp


	Chart.DefaultElement.SmartLabel.Color = Color.Black

	'Chart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center;
	Chart.DefaultElement.SmartLabel.Font = New Font("Arial", 10, FontStyle.Bold)
	Chart.XAxis.Orientation = dotnetCHARTING.Orientation.Top

	Dim mySC As SeriesCollection = getRandomData()
	Chart.DefaultElement.ForceMarker = True

	mySC(0)(0).Marker = New ElementMarker("../../../images/icon_gas.png")
	mySC(0)(1).Marker = New ElementMarker("../../../images/icon_oil.png")
	mySC(0)(2).Marker = New ElementMarker("../../../images/icon_battery.png")
	mySC(0)(3).Marker = New ElementMarker("../../../images/icon_temp.png")

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)


End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(1)
        Dim SC As SeriesCollection = New SeriesCollection()
        Dim a As Integer
        Dim b As Integer
	For a = 1 To 1
		Dim s As Series = New Series("")
		For b = 1 To 4
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = myR.Next(100)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
