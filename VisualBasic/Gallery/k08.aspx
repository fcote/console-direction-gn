<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates the multiple chart type with pies horizontal linear gauges and bars gauge.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Multiple
	Chart.Use3D = True
	Chart.Size = "600x350"
	Chart.Title = ".netCHARTING Sample"

	Chart.DefaultSeries.Background.Color = Color.White


	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()

	mySC(0).Type = SeriesTypeMultiple.Pie
	mySC(1).Type = SeriesTypeMultiple.Gauge

	mySC(1).GaugeType = GaugeType.Bars

	mySC(2).Type = SeriesTypeMultiple.Gauge
	mySC(2).GaugeType = GaugeType.Horizontal

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)


End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random()
        Dim SC As SeriesCollection = New SeriesCollection()
        Dim a As Integer
        Dim b As Integer
	For a = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())
		For b = 1 To 4
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = myR.Next(50)
			e.Length = myR.Next(20)
			e.Height = myR.Next(20)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
