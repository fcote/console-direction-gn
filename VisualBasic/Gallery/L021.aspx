<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates shaded BubbleShape series with shading effect mode two.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Combo
	Chart.Size = "600x350"
	Chart.Title = ".netCHARTING Sample"

	Chart.DefaultSeries.Type = SeriesType.BubbleShape
	Chart.ShadingEffectMode = ShadingEffectMode.Two
	Chart.DefaultElement.ShapeType = ShapeType.None
	Chart.YAxis.ClearValues = True
	Chart.XAxis.ClearValues = True
	Chart.YAxis.ScaleRange = New ScaleRange(-1, 5)

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()
	Chart.MaximumBubbleSize = 50
	Chart.LegendBox.Visible = False

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)
	mySC(0).PaletteName = Palette.Three

	Dim shapeNames As String() = New String() {"Triangle", "Cube", "Pentagon", "Hexagon", "Heptagon", "Octagon", "Nonagon", "Decagon", "FivePointStar", "SixPointStar", "SevenPointStar", "EightPointStar", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight", "Refresh", "Heart", "Ribbon", "Zap", "Trophy", "Chat", "Peace", "ThumbsUp", "ThumbsDown", "PointLeft", "PointRight", "PointUp", "PointDown", "Hand", "Flag", "Plane", "Drop", "Flake", "Hourglass", "Mouse", "Folder", "Candle", "Quote", "Bell", "Clover", "X", "Check" }

		mySC(0)(0).Name = ""
	For i As Integer = 1 To mySC(0).Elements.Count-2
		mySC(0)(i).ShapeType = CType(ShapeType.Parse(GetType(ShapeType),shapeNames(i-1)), ShapeType)
		mySC(0)(i).Name = shapeNames(i-1)
	Next i


End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(1)
	Dim SC As SeriesCollection = New SeriesCollection()
	For a As Integer = 1 To 1
		Dim s As Series = New Series("Series " & a.ToString())
		For b As Integer = 1 To 45
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = b Mod 5
			s.Elements.Add(e)
			e.BubbleSize = 5
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
