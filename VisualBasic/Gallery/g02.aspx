<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

		
Private Function getData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim i As Integer
   For i = 0 To 0
      
      
      Dim s As New Series()
      s.Name = "X Company"
      Dim startPrice As Double = 50
      
      Dim startDT As New DateTime(2000, 1, 1)
      Dim b As Integer
      For b = 0 To 74
         Dim e As New Element()
         e.XDateTime = startDT
         startDT = startDT.AddDays(1)
         
         If myR.Next(10) > 5 Then
            startPrice += myR.Next(5)
         Else
            startPrice -= myR.Next(3)
         End If 
         e.Close = startPrice
         
         e.Volume = myR.Next(100)
         
         If myR.Next(10) > 5 Then
            e.Open = startPrice + myR.Next(6)
         Else
            e.Open = startPrice - myR.Next(6)
         End If 
         If e.Open > e.Close Then
            e.High = e.Open + myR.Next(6)
            e.Low = e.Close - myR.Next(6)
         Else
            e.High = e.Close + myR.Next(6)
            e.Low = e.Open - myR.Next(6)
         End If
         
         
         
         s.Elements.Add(e)
      Next b 
      
      SC.Add(s)
   Next i 
   Return SC
End Function 'getData
 



Sub Page_Load(sender As [Object], e As EventArgs)
   ' Set the title.
   Chart.Title = "My Chart"
   
   ' Set 3D
   Chart.Use3D = True
   
   ' Set the chart Type
   Chart.Type = ChartType.Financial
   
   ' Label the chart areas
        Chart.ChartArea.Label.Text = "Stock Price for X Company"
        Chart.ChartArea.Label.Text = "Volume"
   
   ' Set the financial chart type
   Chart.DefaultSeries.Type = SeriesTypeFinancial.Bar
   
   ' Set the legend template
   Chart.LegendBox.Template = "IconName"
   
   
   ' Set the directory where the images will be stored.
   Chart.TempDirectory = "temp"
   
   ' Set the format
        Chart.XAxis.FormatString = "d"
   
   ' Set the time padding for the x axis.
        Chart.XAxis.TimePadding = New TimeSpan(5, 0, 0, 0)
   
   ' Set he chart size.
   Chart.Size = "600x350"
   
   ' Add the random data.
   Chart.SeriesCollection.Add(getData())
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
