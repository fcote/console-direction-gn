<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' Set the chart type
   Chart.Type = ChartType.Combo
   ' Set the size
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   ' Set the temp directory
   Chart.TempDirectory = "temp"
   ' Debug mode. ( Will show generated errors if any )
   Chart.Debug = True
   Chart.Title = "My Chart"
   
   Chart.DefaultSeries.Type = SeriesType.Column
   Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom ' = false;
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   Chart.DefaultElement.Marker.Visible = False
   Chart.PaletteName = Palette.Five
   Chart.ChartAreaSpacing = 8
   Chart.LegendBox.Template = "%Icon%Name"
   
   ' Modify the x axis labels.
   Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Smart
   Chart.XAxis.TimeScaleLabels.DayFormatString = "o"
   Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month)
   Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM"
   
   
   ' Setup the axes.
   Chart.YAxis.Label.Text = "Price"
   Chart.YAxis.FormatString = "Currency"
   Chart.YAxis.Scale = Scale.Range
   
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getPriceData()
   mySC(0).Type = SeriesTypeFinancial.CandleStick
   Dim mySCV As SeriesCollection = getVolumeData()
   
   ' Add volume chart area.
   Dim volumeArea As New ChartArea()
   volumeArea.YAxis.Label.Text = "Volume"
   volumeArea.SeriesCollection.Add(mySCV)
   volumeArea.HeightPercentage = 25
   Chart.ExtraChartAreas.Add(volumeArea)
   
   
   ' Add the price data.
   Chart.SeriesCollection.Add(mySC)
   
   
   ' Create a new Aroon chart area
   Dim aroonChartArea As New ChartArea()
   aroonChartArea.HeightPercentage = 32
   aroonChartArea.YAxis.Label.Text = "Aroon" + ControlChars.Lf + "Indicators"
   Chart.ExtraChartAreas.Add(aroonChartArea)
   
   ' AroonUpOverPeriod Calculation
   Dim aroonUpOverPeriod As Series = FinancialEngine.AroonUpOverPeriod(mySC(0), 5)
   aroonUpOverPeriod.Name = " Aroon Up"
   aroonUpOverPeriod.Type = SeriesType.Spline
   aroonChartArea.SeriesCollection.Add(aroonUpOverPeriod)
   
   ' AroonDownOverPeriod Calculation
   Dim aroonDownOverPeriod As Series = FinancialEngine.AroonDownOverPeriod(mySC(0), 5)
   aroonDownOverPeriod.Name = " Aroon Down"
   aroonDownOverPeriod.Type = SeriesType.Spline
   aroonChartArea.SeriesCollection.Add(aroonDownOverPeriod)
End Sub 'Page_Load
 



Private Function getPriceData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim i As Integer
   For i = 0 To 0
      Dim s As New Series()
      s.Name = "Price"
      Dim startPrice As Double = 50
      
      Dim startDT As New DateTime(2000, 1, 1)
      Dim b As Integer
      For b = 0 To 74
         Dim e As New Element()
         e.XDateTime = startDT
         startDT = startDT.AddDays(1)
         
         If myR.Next(10) > 5 Then
            startPrice += myR.Next(5)
         Else
            startPrice -= myR.Next(3)
         End If 
         e.Close = startPrice
         
         If myR.Next(10) > 5 Then
            e.Open = startPrice + myR.Next(6)
         Else
            e.Open = startPrice - myR.Next(6)
         End If 
         If e.Open > e.Close Then
            e.High = e.Open + myR.Next(6)
            e.Low = e.Close - myR.Next(6)
         Else
            e.High = e.Close + myR.Next(6)
            e.Low = e.Open - myR.Next(6)
         End If
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next i
   Return SC
End Function 'getPriceData


Private Function getVolumeData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim i As Integer
   For i = 0 To 0
      Dim s As New Series()
      s.Name = "Volume"
      Dim startPrice As Double = 50
      
      Dim startDT As New DateTime(2000, 1, 1)
      Dim b As Integer
      For b = 0 To 74
         Dim e As New Element()
         e.XDateTime = startDT
         startDT = startDT.AddDays(1)
         e.YValue = myR.Next(100)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next i
   Return SC
End Function 'getVolumeData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
