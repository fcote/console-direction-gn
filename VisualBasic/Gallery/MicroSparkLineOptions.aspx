<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates several features of the sparkLine microChart.

		Chart.TempDirectory = "temp"
		Chart.Background.Color = Color.FromArgb(244, 244, 244)

		Chart.LabelChart.Alignment = StringAlignment.Near


		Dim s As String = "<block fStyle='Bold'>Sparkline Types and Options<block><hr>"

		' A plain SparkLine.
		s &= "Plain sparkline <Chart:Sparkline values='5,6,8,23,2,15' >"

		' Providing two colors specifies the color of the line and end point colors.
		s &= "<row>Sparkline with ends <Chart:Sparkline values='5,6,8,23,2,15' colors='Gray,Red'>"

		' Providing three colors specifies the color of the line, end points, and high/low value colors.
		s &= "<row>Sparkline with ends and min/max <Chart:Sparkline values='5,6,8,23,2,15' colors='Gray,Red,Blue'>"

		' By specifying a transparent color for end points they can be omitted.
		s &= "<row>Sparkline with min/max <Chart:Sparkline values='5,6,8,23,2,15' colors='Gray,Transparent,Blue'>"

		' Specifying only the area line color.

		s &= "<row>Colored sparkline <Chart:Sparkline values='5,6,8,23,2,15' color='Blue'>"

		' A reference line can be used by specifying a value in the AxisMarker parameter.
		s &= "<row>Sparkline with AxisMarker line <Chart:Sparkline values='5,6,8,23,2,15' AxisMarker='10'>"

		' A portion of the y axis can be marked by using two values in the AxisMarker parameter and it uses the fourth color in the Colors parameter.
		s &= "<row>Sparkline with colored range AxisMarker <Chart:Sparkline values='5,6,8,23,2,15' AxisMarker='10,20' colors='Gray,Transparent,Transparent,Blue'>"
		Chart.ObjectChart = New dotnetCHARTING.Label(s)

	End Sub


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
