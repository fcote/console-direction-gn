<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " & b
         e.YValue = - 25 + myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   ' Set the title.
   Chart.Title = "My Chart"
   
   ' Set the chart Type
   Chart.Type = ChartType.ComboHorizontal
   
   ' Turn 3D off.
   Chart.Use3D = True
   
   ' Set a default transparency
   Chart.DefaultSeries.DefaultElement.Transparency = 20
   
   ' Set the x axis label
   Chart.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   Chart.YAxis.Label.Text = "Y Axis Label"
   
   
   ' Set the directory where the images will be stored.
   Chart.TempDirectory = "temp"
   
   ' Set the chart size.
   Chart.Size = "600x350"
   
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample (Time Gantt Chart in 2D)</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
