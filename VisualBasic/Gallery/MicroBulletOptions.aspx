<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates several features of the bullet microChart.

		Chart.TempDirectory = "temp"
		Chart.Background.Color = Color.FromArgb(244, 244, 244)
		Chart.LabelChart.Alignment = StringAlignment.Near

		' Bullet types.
		Dim s As String = "<block fStyle='Bold'>Bullet Type Options<block><hr>"
		s &= "Simple Bullet: <Chart:Bullet values='15,10' >"
		s &= "<row>One Shaded Area: <Chart:Bullet values='15,10,8' >"
		s &= "<row>Full Bullet: <Chart:Bullet values='15,10,4,8' >"
		s &= "<row>Reversed shading: <Chart:Bullet values='15,10,8,4' ><hr>"

		' Bullet ShadingEffect modes.
		s &= "<block fStyle='Bold'>Shading Effects<block><hr>"
		s &= "<row>Shading Effect 1: <Chart:Bullet values='15,10,4,8' shading='1' color='CornflowerBlue'>"
		s &= "<row>Shading Effect 2: <Chart:Bullet values='15,10,4,8' shading='2' color='DarkCyan'>"
		s &= "<row>Shading Effect 3: <Chart:Bullet values='15,10,4,8' shading='3' color='Navy'>"
		s &= "<row>Shading Effect 4: <Chart:Bullet values='15,10,4,8' shading='4' color='DarkRed'>"
		s &= "<row>Shading Effect 5: <Chart:Bullet values='15,10,4,8' shading='5' color='YellowGreen'>"
		s &= "<row>Shading Effect 7: <Chart:Bullet values='15,10,4,8' shading='7' color='SeaGreen'><hr>"

		' Specifying colors
		s &= "<block fStyle='Bold'>Coloring<block><hr>"
		s &= "<row>Bullet SimpleColor Blue: <Chart:Bullet values='15,10,4,8' color='Blue'>"
		s &= "<row>Bullet SimpleColor Maroon: <Chart:Bullet values='15,10,4,8' color='Maroon'>"
		s &= "<row>Bullet Full Color Control: <Chart:Bullet values='15,10,4,8' Colors='MidnightBlue,Green,Blue,CornflowerBlue,LightSkyBlue'>"
		s &= "<row>Coloring & Transparent Back: <Chart:Bullet values='15,10,4,8' Colors='MidnightBlue,Red,Blue,CornflowerBlue,Transparent'><hr>"

		' Using negative values
		s &= "<block fStyle='Bold'>Values<block><hr>"
		s &= "<row>Full with scale max: <Chart:Bullet values='15,10,4,8' max='20'>"
		s &= "<row>Full with negative values: <Chart:Bullet values='-15,-10,-4,-8' >"
		s &= "<row>Negative values & one area: <Chart:Bullet values='-15,-10,-8' >"

		Chart.ObjectChart = New dotnetCHARTING.Label(s)
	End Sub


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
