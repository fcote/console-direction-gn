<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates overlapping Pies in 2D where the pies sizes are controled by Element.BubbleSize.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.PiesNested
	Chart.Size = "600x350"
	Chart.Title = ".netCHARTING Sample"
	Chart.YAxis.Scale = Scale.Normal
	Chart.Use3D = True

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	' Pie sizes for each series can be manually specified through DefaultElement.BubbleSize
	Dim mySC As SeriesCollection = getRandomData()
	mySC(0).DefaultElement.BubbleSize = 50
	mySC(1).DefaultElement.BubbleSize = 60
	mySC(2).DefaultElement.BubbleSize = 70
	mySC(3).DefaultElement.BubbleSize = 75

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)


End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(1)
	Dim SC As SeriesCollection = New SeriesCollection()
	        Dim a As Integer
        Dim b As Integer
	For a = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())
		For b = 1 To 4
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
