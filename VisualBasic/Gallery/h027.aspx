<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates a vertical linear gauge with an axis marker.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Gauges
	Chart.Size = "110x220"
	'Chart.Title = ".netCHARTING Sample";
	'Chart.ShadingEffectMode = ShadingEffectMode.Three;

	Chart.ChartArea.Padding = 2
	Chart.Margin = "-3"
   ' Chart.ChartArea.ClearColors();
	Chart.ChartArea.Background.Color = Color.Empty
	Chart.ChartArea.Shadow.Color = Color.Empty
	Chart.ChartArea.Line.Color = Color.Empty
	Chart.DefaultElement.Transparency = 40
	Chart.XAxis.SpacingPercentage = 20

	Dim am As AxisMarker = New AxisMarker("", Color.Red, 30,50)
	Chart.YAxis.Markers.Add(am)
	Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round
	Chart.DefaultSeries.GaugeBorderBox.Padding = 5
	Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox

	Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Left
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()
	'Chart.DefaultElement.ShowValue = true;
	Chart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Bottom
	Chart.DefaultElement.SmartLabel.Text = "%Name - (%YValue)"
	Chart.DefaultSeries.GaugeType = GaugeType.Vertical

	Chart.LegendBox.Visible = False
	' Add the random data.
	Chart.SeriesCollection.Add(mySC)


End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random()
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim a As Integer
	Dim b As Integer
	For a = 1 To 1
		Dim s As Series = New Series("")
		For b = 1 To 1
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = 25+myR.Next(25)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
