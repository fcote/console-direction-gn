<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates several features of the Pie microChart.

		Chart.TempDirectory = "temp"
		Chart.Background.Color = Color.FromArgb(244, 244, 244)
		Chart.LabelChart.Alignment = StringAlignment.Near

		Dim s As String = "<block fStyle='Bold'>Pie & 3D Pie Type Options<block><hr>"
		s &= "Plain Pie: <Chart:Pie values='10,14,5,6'>"

		' Pie using the SimpleColor feature.
		s &= "<row>Pie with SimpleColor Black: <Chart:Pie values='10,14,5,6' color='Black' >"
		s &= "<row>Pie with SimpleColor Blue: <Chart:Pie values='10,14,5,6' color='Blue' >"

		' Using a custom palette
		s &= "<row>Pie with palette: <Chart:Pie values='10,14,5,6' colors='#31FF31,#FFFF00,#FF6331,#009CFF' ><hr>"

		' Shading effect modes are available with 2D pie microCharts.
		s &= "<row>Pie Shading 1 <Chart:Pie values='10,14,5,6' shading='1' size='70'>"
		s &= "<row>Pie Shading 2 <Chart:Pie values='10,14,5,6' shading='2' size='70'>"
		s &= "<row>Pie Shading 3 <Chart:Pie values='10,14,5,6' shading='3' size='70'>"
		s &= "<row>Pie Shading 4 <Chart:Pie values='10,14,5,6' shading='4' size='70'><hr>"

		' 3D pie microCharts.
		s &= "<row>3D Pie: <Chart:Pie3D values='10,14,5,6'>"
		s &= "<row>Larger 3D Pie: <Chart:Pie3D values='10,14,5,6' size='40' >"
		s &= "<row>Larger 3D Pie & Palette: <Chart:Pie3D values='10,14,5,6' size='70' colors='#31FF31,#FFFF00,#FF6331,#009CFF'>"
		Chart.ObjectChart = New dotnetCHARTING.Label(s)
	End Sub


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
