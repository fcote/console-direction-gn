<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' Enable the Navigator.
		Chart.Navigator.Enabled = True

		Chart.DefaultSeries.PaletteName = Palette.Bright

		' Specify the default series type.
		Chart.DefaultSeries.Type = SeriesType.AreaLine

		' Set the x axis label
		Chart.XAxis.Label.Text = "X Axis Label"

		' Set the y axis label
		Chart.YAxis.Label.Text = "Y Axis Label"

		' Set the directory where the images will be stored.
		Chart.TempDirectory = "temp"

		' Set he chart size.
		Chart.Width = 600
		Chart.Height = 350

		' Add the random data.
		Chart.SeriesCollection.Add(getRandomData())

	End Sub

	Function getRandomData() As SeriesCollection
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim myR As Random = New Random()
		For a As Integer = 0 To 0
			Dim s As Series = New Series()
			s.Name = "Series " & a
			For b As Integer = 0 To 9
				Dim e As Element = New Element()
				e.Name = "E " & b
				e.YValue = 20 + myR.Next(30) - b * 3
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a



		Return SC
	End Function



</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Gallery Sample</title>
</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
