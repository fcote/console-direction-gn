<%@ Page Language="VBs" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Palette = New Color() {Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255)}
   
   Chart.Type = ChartType.ComboHorizontal
   Chart.Size = "600x350"
   Chart.Title = ".netCHARTING Sample"
   
   ' This sample demonstrates horizontal cone in 3d with high depth.
   Chart.DefaultSeries.Type = SeriesType.Cone
   Chart.Use3D = True
   Chart.Depth = Unit.Parse(40)
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Help File > Getting Started > Data Tutorials
   ' - DataEngine Class in the help file	
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim myR As New Random(1)
   Dim SC As New SeriesCollection()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series("Series " + a.ToString())
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element("Element " + b.ToString())
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
