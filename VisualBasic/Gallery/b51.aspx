<%@ Page Language="vb" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>


<script runat="server">

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 4
         Dim e As New Element()
         e.Name = "E " & b
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   ' Set 2 series to bars and 2 to lines.
   SC(0).Type = SeriesType.AreaLine
   SC(1).Type = SeriesType.Column
   SC(2).Type = SeriesType.Cylinder
   SC(3).Type = SeriesType.Line
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   ' Set the title.
   Chart.Title = "My Chart"
   Chart.Type = ChartType.ComboHorizontal
   
   
   ' Set 3D
   Chart.Use3D = False
   
   ' set the x axis clustering
   Chart.XAxis.ClusterColumns = False
   
   ' Set a default transparency
   Chart.DefaultSeries.DefaultElement.Transparency = 20
   
   ' Set a default line Width
   Chart.DefaultSeries.Line.Width = 3
   
   ' Set the y Axis Scale
   Chart.YAxis.Scale = Scale.Normal
   
   
   ' Set the x axis label
   Chart.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   Chart.YAxis.Label.Text = "Y Axis Label"
   
   ' Set the directory where the images will be stored.
   Chart.TempDirectory = "temp"
   
   
      ' Set the chart size.
   Chart.Size = "600x350"
   
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
