<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' Enable the Navigator.
		Chart.Navigator.Enabled = True

		' Set a default transparency
		Chart.DefaultSeries.DefaultElement.Transparency = 20

		' Set line new properties
		Chart.DefaultSeries.Line.Width = 5
		Chart.DefaultSeries.Line.DashStyle = DashStyle.Dash

		' Set the Default Series Type
		Chart.DefaultSeries.Type = SeriesType.Line

		' Set the y Axis Scale
		Chart.YAxis.Scale = Scale.Normal

		' Set the x axis label
		Chart.XAxis.Label.Text = "X Axis Label"

		' Set the y axis label
		Chart.YAxis.Label.Text = "Y Axis Label"

		' Set the directory where the images will be stored.
		Chart.TempDirectory = "temp"


		' Set he chart size.
		Chart.Width = 600
		Chart.Height = 350

		' Add the random data.
		Chart.SeriesCollection.Add(getRandomData())

	End Sub

	Function getRandomData() As SeriesCollection
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim myR As Random = New Random()
		For a As Integer = 0 To 3
			Dim s As Series = New Series()
			s.Name = "Series " & a
			For b As Integer = 0 To 9
				Dim e As Element = New Element()
				e.Name = "E " & b
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		' Set Different Colors for our Series
		SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
		SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
		SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
		SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)

		Return SC
	End Function


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Gallery Sample</title>
</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
