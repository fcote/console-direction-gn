<%@ Page Language="vb" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

	Function getRandomData() As SeriesCollection
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim myR As Random = New Random()
		For a As Integer = 0 To 3
			Dim s As Series = New Series()
			s.Name = "Series " & a
			For b As Integer = 0 To 2
				Dim e As Element = New Element()
				e.Name = "Element " & b
				e.YDateTimeStart = New DateTime(2003, 4, 4).AddDays(myR.Next(50))
				e.YDateTime = e.YDateTimeStart.AddDays(myR.Next(50))
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a

		' Set Different Colors for our Series
		SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
		SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
		SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
		SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)

		Return SC
	End Function


Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' Set the title.
	Chart.Title="My Chart"

	' Set the chart Type
	Chart.Type = ChartType.ComboHorizontal

	' Turn 3D off.
	Chart.Use3D = False

	Chart.ShadingEffectMode = ShadingEffectMode.Six

	' Set a default transparency
	Chart.DefaultSeries.DefaultElement.Transparency = 20

	' Set the x axis label
	Chart.XAxis.Label.Text="X Axis Label"

	' Set the y axis label
	Chart.YAxis.Label.Text="Y Axis Label"

	' Set the directory where the images will be stored.
	Chart.TempDirectory="temp"

	' Set the chart size.
	Chart.Width = 600
	Chart.Height = 350

	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData())


End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample (Time Gantt Chart in 2D)</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
