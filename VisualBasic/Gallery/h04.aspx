<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>


<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   ' Set the title.
   Chart.Title = "My Chart"
   
   ' Set the directory where images are temporarily be stored.
   Chart.TempDirectory = "temp"
   
   ' Disable the legend box
   Chart.LegendBox.Position = LegendBoxPosition.None
   
   ' Set a default gauge face background color.
   Chart.DefaultSeries.Background.Color = Color.LightGray
   
   ' Set he chart size.
   Chart.Size = "600x350"
   
   ' Specify the gauges chart type
   Chart.Type = ChartType.Gauges
   
   ' Create a series with a single element
   Dim s1 As New Series("series 1", New Element("", 120))
   
   ' Create a series with a single element
   Dim s2 As New Series("series 2", New Element("", 30))
   
   ' Create a series with a single element
   Dim s3 As New Series("series 3", New Element("", 50))
   
   ' Add the series
   Chart.SeriesCollection.Add(s1)
   Chart.SeriesCollection.Add(s2)
   Chart.SeriesCollection.Add(s3)
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
