<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' Set the size
   Chart.Size = "600x350"
   ' Set the temp directory
   Chart.TempDirectory = "temp"
   ' Debug mode. ( Will show generated errors if any )
   Chart.Debug = True
   Chart.Title = "My Chart"
   
   Chart.DefaultSeries.Type = SeriesType.Spline
   Chart.DefaultSeries.Line.Width = 5
   Chart.DefaultSeries.Line.DashStyle = DashStyle.Dash
   
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " +(a + 1).ToString()
      Dim b As Integer
      For b = 0 To 10
         Dim e As New Element()
         e.Name = "Element " +(b + 1).ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   
   
   ' Set Different Colors for our Series
   SC(0).PaletteName = Palette.Two '.Color = Color.FromArgb(49,255,49);
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
