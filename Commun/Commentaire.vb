Imports Microsoft.VisualBasic
Imports System.drawing
Imports System.text
Imports System.collections.Specialized
Imports System.Web.UI.WebControls

Public Interface ICommentaire
    'Property Introduction() As Composante
    Property Commentaire1() As Composante
    Property Commentaire2() As Composante
    Property Commentaire3() As Composante
    Property MaxLignes() As Integer
    Property CharsParLigne() As Integer
    Function ToString() As String
End Interface

Public Class Commentaire
    Implements ICommentaire
    'Private _note_exam As Composante
    'Private _a_pei As Composante
    Private _forces As Composante
    Private _defis As Composante
    Private _prochEtape As Composante

    Private _pap(6) As String
    Private _comm_pap(4) As String

    Private _cpl As Integer
    Private _maxlines As Integer
    Private _is_struct As Boolean
    Public DimBoiteComm As SizeF = Nothing
    Private _prenom As String = String.Empty
    Private _sexe As String = String.Empty
    Private _max As Integer = 2
    Private MAXCOMPOSANTE As Integer = 3
    Private _mergeResult As Boolean = True
    Private _noteExam As String = ""
    Private _noteSemestre As String = ""
    Private _prefixNote As String = ""
    Private _maxLen As Integer = 0

    Public Const STR_ID_PEIMOD As String = "pei_mod"
    Public Const STR_ID_PEIADAP As String = "pei_adap"
    'Public Const STR_ID_ALFPDF As String = "alf_pdf"
    Public Const STR_ID_ALF As String = "*alf"
    Public Const STR_ID_PDF As String = "*pdf"
    Public Const STR_ID_APD As String = "*apd"
    Public Const STR_ID_NA As String = "*na"

    Public Const STR_COMM_NOTE_SEMESTRE As String = "*NS*"
    Public Const STR_COMM_NOTE_EXAMEN As String = "*NE*"

    ''' <summary>
    ''' Attention: Les TypesPAP doivent �tre dans le m�me ordre que dans la table
    ''' SP_LISTE_PEI_ALF_PDF
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum TypesPAP
        ALF = 1
        PEIAdaptation
        PEIModification
        PDF
        APD
        NA
    End Enum

    Public Property Max() As Integer
        Get
            Return _max
        End Get
        Set(ByVal value As Integer)
            '_a_pei.Max = value
            _max = value
            _forces.Max = value
            _defis.Max = value
            _prochEtape.Max = value
        End Set
    End Property

    Public Property MaxLength() As Integer
        Get
            Return _maxLen
        End Get
        Set(ByVal value As Integer)
            _maxLen = value
        End Set
    End Property

    Public Sub CopierPrefixes(ByVal c As Commentaire)
        Me._pap = c._pap
        'Me._comm_pap = c._comm_pap
        Me._noteExam = c._noteExam
        Me._noteSemestre = c._noteSemestre
    End Sub

    Public Sub SetPAP(ByVal type As TypesPAP, ByVal value As Boolean)
        _pap(CInt(type)) = IIf(value, "x", String.Empty).ToString
    End Sub

    Public Sub SetCommentairesPAP(ByVal type As TypesPAP, ByVal value As String)

        _comm_pap(CInt(type)) = value
        
    End Sub

    Public Function GetPAP(ByVal index As TypesPAP) As String

        'If _pap(index) = "x" Then
        '    Return _comm_pap(index)
        'Else
        '    Return String.Empty
        'End If

        If index >= _pap.Length Then
            Return String.Empty
        Else
            Return IIf(_pap(index) Is Nothing, String.Empty, _pap(index)).ToString
        End If

    End Function

    Public Function GetPAPComment(ByVal index As TypesPAP) As String
        If _pap(index) = "x" Then
            Return _comm_pap(index)
        Else
            Return String.Empty
        End If
    End Function

    Public Sub New(ByVal cpl As Integer, ByVal maxlines As Integer, ByVal structured As Boolean)
        _cpl = cpl
        _maxlines = maxlines
        _is_struct = structured

        If _is_struct Then
            '_a_pei = New Composante(String.Empty, 2, 10)

            'PF 2007-09-19 On ne veut pas que les titres Forces, d�fis, P� apparaissent
            '_forces = New Composante("Forces", 2, 56)
            '_defis = New Composante("D�fis", 2, 56)
            '_prochEtape = New Composante("Prochaine �tape", 250, 56)

            _forces = New Composante(String.Empty, 2, 56)
            _defis = New Composante(String.Empty, 2, 56)
            _prochEtape = New Composante(String.Empty, 250, 56)
        Else

            '_a_pei = New Composante(String.Empty, 250, 56)
            _forces = New Composante(String.Empty, 250, 56)
            _defis = New Composante(String.Empty, 250, 56)
            _prochEtape = New Composante(String.Empty, 250, 56)
        End If

        _forces.Prefix = "F"
        _defis.Prefix = "D"
        _prochEtape.Prefix = "P"

    End Sub

    Public Property Prenom() As String
        Get
            Return _prenom
        End Get
        Set(ByVal value As String)
            _prenom = value
            Commentaire1.Prenom = value
            Commentaire2.Prenom = value
            Commentaire3.Prenom = value
        End Set
    End Property
    Public Property Sexe() As String
        Get
            Return _sexe
        End Get
        Set(ByVal value As String)
            _sexe = value
            Commentaire1.Sexe = value
            Commentaire2.Sexe = value
            Commentaire3.Sexe = value
        End Set
    End Property

    Public Property NoteExamen() As String
        Get
            Return _noteExam
        End Get
        Set(ByVal value As String)
            _noteExam = value
            _prefixNote = String.Empty
        End Set
    End Property

    Public Property NoteSemestre() As String
        Get
            Return _noteSemestre
        End Get
        Set(ByVal value As String)
            _noteSemestre = value
            _prefixNote = String.Empty
        End Set
    End Property


    'Public Property NoteExamenFormatee() As String

    '    Get
    '        If _noteExam = String.Empty Then
    '            Return String.Empty
    '        Else
    '            Return "Note de l'examen: " & _noteExam & "%."
    '        End If
    '    End Get

    '    Set(ByVal value As String)
    '        _noteExam = value
    '    End Set

    'End Property

    'Public Property NoteSemestreFormatee() As String

    '    Get
    '        If _noteExam = String.Empty Then
    '            Return String.Empty
    '        Else
    '            Return "Note du semestre: " & _noteSemestre & "%."
    '        End If
    '    End Get

    '    Set(ByVal value As String)
    '        _noteExam = value
    '    End Set

    'End Property

    Public Property PrefixNotes() As String
        Get
            If _prefixNote = "" Then
                'On doit rafraichir la variable
                Dim sb As StringBuilder = New StringBuilder

                If NoteSemestre <> "" Then
                    sb.Append(_noteSemestre)
                End If

                If NoteExamen <> "" Then

                    If sb.Length > 0 Then
                        sb.Append("  ")
                    End If

                    sb.Append(_noteExam)
                End If

                If sb.Length > 0 Then
                    sb.Append("  ")
                End If



                _prefixNote = sb.ToString

            End If

            Return _prefixNote

        End Get
        Set(ByVal value As String)
            _prefixNote = value
        End Set
    End Property

    Public Function Clone() As Commentaire
        Dim c As Commentaire = New Commentaire(_cpl, _maxlines, _is_struct)
        c.Prenom = _prenom
        c.Sexe = _sexe
        c.DimBoiteComm = DimBoiteComm
        c.Max = _max
        c.MaxLength = _maxLen
        c._pap = _pap
        c._comm_pap = _comm_pap
        c._noteExam = _noteExam
        c._noteSemestre = _noteSemestre
        c.Commentaire1 = Commentaire1.Clone()
        c.Commentaire2 = Commentaire2.Clone()
        c.Commentaire3 = Commentaire3.Clone()
        Return c
    End Function

    Public Sub SetBoxSize(ByVal height As Single, ByVal width As Single)
        DimBoiteComm = New SizeF(width, height)
    End Sub

    Public Property CharsParLigne() As Integer Implements ICommentaire.CharsParLigne
        Get
            Return _cpl
        End Get
        Set(ByVal value As Integer)
            _cpl = value
        End Set
    End Property

    Public Property MaxLignes() As Integer Implements ICommentaire.MaxLignes
        Get
            Return _maxlines
        End Get
        Set(ByVal value As Integer)
            _maxlines = value
        End Set
    End Property

    Public Property Commentaire1() As Composante Implements ICommentaire.Commentaire1
        Get
            Return _forces
        End Get
        Set(ByVal value As Composante)
            _forces = value
        End Set
    End Property

    Public Property Commentaire2() As Composante Implements ICommentaire.Commentaire2
        Get
            Return _defis
        End Get
        Set(ByVal value As Composante)
            _defis = value
        End Set
    End Property

    Public Property Commentaire3() As Composante Implements ICommentaire.Commentaire3
        Get
            Return _prochEtape
        End Get
        Set(ByVal value As Composante)
            _prochEtape = value
        End Set
    End Property

    Public Function GetCommentaire(ByVal index As Integer) As Composante
        Select Case index
            Case 1
                Return Commentaire1
            Case 2
                Return Commentaire2
            Case 3
                Return Commentaire3

            Case Else
                Throw New Exception("L'index des commentaires doit �tre moins que 4.")
        End Select
    End Function

    ''' <summary>
    ''' Combine le commentaire appelant et le commentaire pass� en param�tre.
    ''' Les parties du commentaire pass� en param�tre sont concaten�es aux partie
    ''' du commentaire appelant.
    ''' </summary>
    ''' <param name="comm"></param>
    ''' <returns>Le commentaire combin�.</returns>
    ''' <remarks></remarks>
    Public Function Append(ByVal comm As Commentaire) As Commentaire

        Dim com_ret As Commentaire = Me.Clone()

        For i As Integer = 1 To MAXCOMPOSANTE
            com_ret.GetCommentaire(i).AjouterPartie(_append(Me.GetCommentaire(i).Partie(0), comm.GetCommentaire(i).Partie(0)))
            com_ret.GetCommentaire(i).AjouterPartie(_append(Me.GetCommentaire(i).Partie(1), comm.GetCommentaire(i).Partie(1)))
        Next

        Return com_ret

    End Function

    ''' <summary>
    ''' Combine le commentaire appelant et le commentaire pass� en param�tre.  
    ''' Si une partie du commentaire appelant est vide, alors elle est peupl�e par
    ''' la partie du commentaire pass� en param�tre.
    ''' </summary>
    ''' <param name="comm"></param>
    ''' <returns>Le commentaire combin�.</returns>
    ''' <remarks></remarks>
    Public Function Merge(ByVal comm As Commentaire) As Commentaire

        _mergeResult = True

        'Ici on ne peut pas faire un clone direct;
        'on ne doit pas copier les parties.
        Dim com_ret As Commentaire = Me.Clone()

        'Pour qu'un commentaire soit appliqu� sans avertissement, je dois avoir 
        'le m�me nombre de composantes vides dans Me que de composantes 
        'non-vides dans comm.
        'Validons que le commentaire sera appliqu� sans avertissement

        'V�rifions par composante:
        For i As Integer = 1 To MAXCOMPOSANTE

            Dim mes_vides As Integer = 0
            Dim ses_nonvides As Integer = 0

            'com_ret.GetCommentaire(i).Clear()

            For j As Integer = 0 To _max - 1

                If comm.GetCommentaire(i).Partie(j).Length > 0 Then

                    For k As Integer = 0 To _max - 1
                        If Me.GetCommentaire(i).Partie(k).Length = 0 Then
                            mes_vides += 1
                        End If
                    Next k

                    ses_nonvides += 1

                End If

            Next j

            'S'il y a une composante a merger
            If ses_nonvides > 0 Then
                If mes_vides < ses_nonvides Then
                    _mergeResult = False
                    com_ret._mergeResult = False
                    Exit For
                End If
            End If

        Next

        'Assignation du commentaire
        For i As Integer = 1 To MAXCOMPOSANTE

            Dim j2 As Integer = 0
            For j As Integer = 0 To _max - 1

                If comm.GetCommentaire(i).Partie(j2).Length > 0 Then

                    If Me.GetCommentaire(i).GetPartie(j).ToString().Length = 0 Then
                        com_ret.GetCommentaire(i).AjouterPartie(comm.GetCommentaire(i).Partie(j2))
                        j2 += 1
                    Else
                        'PF 2007-07-17
                        'Le clone en haut popule d�j� le commentaire
                        'avec les composantes de Me, donc pas besoin
                        'de la ligne ci-dessous.
                        'com_ret.GetCommentaire(i).AjouterPartie(Me.GetCommentaire(i).Partie(j))
                    End If

                End If

            Next
        Next

        Return com_ret

    End Function

    ''' <summary>
    ''' Retourne Vrai si le commentaire r�sultant de l'op�ration Merge
    ''' est identique au commentaire donn�, sinon retourne Faux.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property MergeResult() As Boolean
        Get
            Return _mergeResult
        End Get
    End Property

    Private Function _append(ByVal s1 As String, ByVal s2 As String) As String

        Dim s As String = s1.Trim()

        If s.Length > 0 And s2.Trim().Length > 0 Then
            s &= "  "
        End If

        s &= s2.Trim()

        Return s

    End Function

    ''' <summary>
    ''' Indique si un commentaire sera valide apr�s l'ajout
    ''' d'un nouveau commentaire.  Un commentaire est valide
    ''' s'il peut s'afficher compl�tement dans le bulletin imprim�.
    ''' </summary>
    ''' <param name="comm"></param>
    ''' <returns>Vrai si le commentaire est valide, Faux sinon.</returns>
    ''' <remarks></remarks>
    Public Function Valider(ByVal comm As Commentaire) As Boolean

        Dim com_ret As Commentaire = Me.Append(comm)
        Return comm.IsValid()

    End Function

    ''' <summary>
    ''' Indique si un commentaire est valide.  Un commentaire est valide
    ''' s'il peut s'afficher compl�tement dans le bulletin imprim�.
    ''' </summary>
    ''' <returns>Vrai si le commentaire est valide, Faux sinon.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IsValid() As Boolean
        Get

            If _maxLen > 0 Then
                If Me.ToString().Replace(Commentaire.STR_COMM_NOTE_SEMESTRE, "99,9%").Replace(Commentaire.STR_COMM_NOTE_EXAMEN, "99,9%").Length > _maxLen Then
                    Return False
                End If
            End If

            Dim sz As SizeF = Me.Size(UnitType.Mm)

            If sz.Height > DimBoiteComm.Height Then
                Return False
            Else
                Return True
            End If

        End Get

    End Property

    Public Function GetCaracteresDeTrop() As Integer
        Return getExtraChars(Me.ToString, DimBoiteComm)
    End Function

    Private Function getExtraChars(ByVal s As String, ByVal dimBoiteComm As SizeF) As Integer

        Dim length As Integer = CInt(Math.Round(s.Length / 2, System.MidpointRounding.AwayFromZero))
        Dim s1 As String = s.Substring(0, length)

        Dim done As Boolean = False
        Dim last_fit As Integer = 0

        Do While Not done

            If _fits(s1) Then
                last_fit = length
                length = CInt(Math.Round(length + (s.Length - s1.Length) / 2, System.MidpointRounding.AwayFromZero))

            Else

                If (length - last_fit) <= 1 Then
                    done = True
                End If

                length = last_fit + CInt(Math.Round((length - last_fit) / 2, System.MidpointRounding.AwayFromZero))

            End If

            s1 = s.Substring(0, length)

        Loop

        Return s.Length - length

    End Function

    Private Function _fits(ByVal s As String) As Boolean
        Dim fm As FontMetrics = New FontMetrics()
        Dim sz As SizeF = New SizeF(DimBoiteComm)
        Dim sz_comm As SizeF = fm.GetSize(s, sz, UnitType.Mm)

        If sz_comm.Height > DimBoiteComm.Height Then
            Return False
        Else
            Return True
        End If
    End Function

    Public ReadOnly Property Size(ByVal u As UnitType) As SizeF
        Get
            If DimBoiteComm = Nothing Then
                Throw New Exception("Impossible de d�terminer la dimension du commentaire puisque la propri�t� DimBoiteComm a la valeur 'Nothing'.")
            End If

            Dim fm As FontMetrics = New FontMetrics()
            Dim sz As SizeF = New SizeF(DimBoiteComm)
            Dim sz_comm As SizeF = fm.GetSize(Me, sz, u)
            Return sz_comm
        End Get
    End Property

    Public Overrides Function ToString() As String Implements ICommentaire.ToString

        Dim s As StringBuilder = New StringBuilder()

        If _is_struct Then

            If GetPAP(TypesPAP.ALF).Length > 0 Or GetPAP(TypesPAP.PDF).Length > 0 Then
                s.Append(GetPAPComment(TypesPAP.ALF) & "  ")
            End If

            If GetPAP(TypesPAP.PEIAdaptation).Length > 0 Then
                s.Append(GetPAPComment(TypesPAP.PEIAdaptation) & "  ")
            End If

            If GetPAP(TypesPAP.PEIModification).Length > 0 Then
                s.Append(GetPAPComment(TypesPAP.PEIModification) & "  ")
            End If

            Dim pn As String = PrefixNotes

            If pn.Length > 0 Then
                s.Append(PrefixNotes)
            End If

            If Commentaire1.ToString().Length > 0 Then
                s.Append(Commentaire1.ToString())
            End If

            If Commentaire2.ToString().Length > 0 Then
                If s.ToString().Length > 0 Then
                    s.Append("  ")
                End If
                s.Append(Commentaire2.ToString())
            End If

            If Commentaire3.ToString().Length > 0 Then
                If s.ToString().Length > 0 Then
                    s.Append("  ")
                End If
                s.Append(Commentaire3.ToString())
            End If

        Else

            Dim c As String = _append(String.Empty, Commentaire1.ToString())
            c = _append(c, Commentaire2.ToString())
            c = _append(c, Commentaire3.ToString())
            s.Append(c)

        End If

        Return s.ToString

    End Function

End Class

Public Class Composante

    Private _max As Integer
    Private _comp As ArrayList
    Private _titre As String
    Private _prenom As String = String.Empty
    Private _sexe As String = String.Empty
    Private _charsParLigne As Integer
    Private _prefix As String = String.Empty

    Public Sub New(ByVal titre As String, ByVal max As Integer, ByVal charsParLigne As Integer)
        _max = max
        _titre = titre
        _comp = New ArrayList()
        _charsParLigne = charsParLigne
    End Sub

    Public Property Prefix() As String
        Get
            Return _prefix
        End Get
        Set(ByVal value As String)
            _prefix = value
        End Set
    End Property

    Public Sub Clear()
        _comp.Clear()
    End Sub

    Public Function AjouterPartie(ByVal texte As String) As Boolean
        If _comp.Count = _max Then
            Return False
        End If

        If texte.Length > 0 Then
            _comp.Add(texte)
            Return True
        Else
            Return False
        End If

    End Function

    Public Property Max() As Integer
        Get
            Return _max
        End Get
        Set(ByVal value As Integer)
            _max = value
        End Set
    End Property

    Public Function Clone() As Composante
        Dim c As Composante = New Composante(_titre, _max, _charsParLigne)
        c.Prenom = _prenom
        c.Prefix = _prefix
        c.Sexe = _sexe
        c._comp = CType(_comp.Clone(), ArrayList)
        Return c
    End Function

    Public Sub EnleverPartie(ByVal index As Integer)
        If index < _comp.Count Then
            _comp.Remove(index)
        End If
    End Sub

    Public Property Prenom() As String
        Get
            Return _prenom
        End Get
        Set(ByVal value As String)
            _prenom = value
        End Set
    End Property

    Public Property Sexe() As String
        Get
            Return _sexe
        End Get
        Set(ByVal value As String)
            _sexe = value
        End Set
    End Property

    Public Property Partie(ByVal index As Integer) As String
        Get
            If index >= _comp.Count Then
                Return String.Empty
            Else
                If IsDBNull(_comp(index)) Then
                    Return String.Empty
                Else
                    Return _comp(index).ToString
                End If
            End If
        End Get
        Set(ByVal value As String)
            If index >= _comp.Count Then
                Throw New IndexOutOfRangeException()
            Else
                If IsDBNull(_comp(index)) Then
                    _comp.Item(index) = value
                End If
            End If

        End Set
    End Property

    Public Function GetPartie(ByVal index As Integer) As String

        If index >= _comp.Count Then
            Return String.Empty
        Else
            If IsDBNull(_comp(index)) Then
                Return String.Empty
            Else
                Return Tokenize(_comp(index).ToString())
            End If
        End If
    End Function

    Public Overrides Function ToString() As String

        If _comp.Count > 0 Then

            Dim c As String = String.Empty
            Dim i As Integer = 0

            'V�rifier s'il y a au moins un commentaire
            While c.Trim.Length = 0

                If i = _comp.Count Then
                    Return String.Empty
                End If

                c = _comp(i).ToString()

                i += 1

            End While
            
            Dim s As String = String.Empty

            If _titre <> String.Empty Then
                s = _titre & ": "
            End If

            For i = 0 To _comp.Count - 1
                If _comp(i).ToString.Length > 0 Then
                    s &= _comp(i).ToString & "  "
                End If
            Next

            If s.EndsWith("  ") Then
                s = s.Remove(s.Length - 2, 2)
            End If

            Return Tokenize(s)

        Else
            Return String.Empty
        End If

    End Function

    Private Function Tokenize(ByVal s As String) As String

        If _prenom.Length = 0 Then
            s = s.Replace("*N*", "(Sans pr�nom)")
        Else
            s = s.Replace("*N*", _prenom)
        End If

        If _sexe.Length > 0 Then
            s = RemplacerP(s, _sexe)
        End If

        Return s

    End Function

    Private Function RemplacerP(ByVal s As String, ByVal sexe As String) As String

        Dim i As Integer = 0
        Dim ponctuation As String = ".!?:"
        Dim pronom As String
        Dim tag As String = "*P*"

        s = s.TrimStart()

        Select Case sexe
            Case "M"
                pronom = "il"
            Case "F"
                pronom = "elle"
            Case Else
                Return s
        End Select
        
        i = s.IndexOf(tag, i)

        'On loop � travers la composante en rempla�ant tous les *P*
        'par des 
        While i > -1

            If i > 0 Then

                Dim j As Integer = 1
                While s.Substring(i - j, 1) = Space(1)
                    j += 1
                End While

                If ponctuation.Contains(s.Substring(i - j, 1)) Then
                    'C'est une ponctuation - on doit mettre une majuscule
                    s = s.Remove(i, tag.Length)

                    'On met une majuscule
                    s = s.Insert(i, pronom.Substring(0, 1).ToUpper & pronom.Substring(1))
                Else
                    s = s.Remove(i, tag.Length)
                    s = s.Insert(i, pronom)
                End If

            Else
                'C'est une ponctuation - on doit mettre une majuscule
                s = s.Remove(i, tag.Length)

                'On met une majuscule
                s = s.Insert(i, pronom.Substring(0, 1).ToUpper & pronom.Substring(1))
            End If

            i = s.IndexOf(tag, i)

        End While

        Return s

    End Function

End Class

Public Class LineManager

    Private _comp As Composante
    Private _cpl As Integer

    Public Sub New(ByVal comp As Composante, ByVal cpl As Integer)
        _comp = comp
        _cpl = cpl
    End Sub

    Public Function GetNombreLignes() As Integer

        Dim main_s As String = _comp.ToString()

        If main_s.Length > 0 Then
            Return CInt(Math.Ceiling(main_s.Length / _cpl))
        Else
            Return 0
        End If
        
    End Function

End Class

Public Class CommentaireDictionary
    Inherits System.Collections.DictionaryBase

    Default Public Property Item(ByVal key As String) As Commentaire
        Get
            Return CType(Dictionary(key), Commentaire)
        End Get
        Set(ByVal value As Commentaire)
            Dictionary(key) = value
        End Set
    End Property

    Public ReadOnly Property Keys() As ICollection
        Get
            Return Dictionary.Keys
        End Get
    End Property

    Public ReadOnly Property Values() As ICollection
        Get
            Return Dictionary.Values
        End Get
    End Property

    Public Sub Add(ByVal key As String, ByVal value As Commun.Commentaire)
        Dictionary.Add(key, value)
    End Sub 'Add

    Public Function Contains(ByVal key As String) As Boolean
        Return Dictionary.Contains(key)
    End Function 'Contains

    Public Sub Remove(ByVal key As String)
        Dictionary.Remove(key)
    End Sub 'Remove

    Protected Overrides Sub OnInsert(ByVal key As Object, ByVal value As Object)
        If Not GetType(System.String).IsAssignableFrom(key.GetType()) Then
            Throw New ArgumentException("key must be of type String.", "key")
        End If

        If Not GetType(Commun.Commentaire).IsAssignableFrom(value.GetType()) Then
            Throw New ArgumentException("value must be of type String.", "value")
        End If

    End Sub 'OnInsert

    Protected Overrides Sub OnRemove(ByVal key As Object, ByVal value As Object)
        If Not GetType(System.String).IsAssignableFrom(key.GetType()) Then
            Throw New ArgumentException("key must be of type String.", "key")
        End If
    End Sub 'OnRemove

    Protected Overrides Sub OnSet(ByVal key As Object, ByVal oldValue As Object, ByVal newValue As Object)
        If Not GetType(System.String).IsAssignableFrom(key.GetType()) Then
            Throw New ArgumentException("key must be of type String.", "key")
        End If
        If Not GetType(Commun.Commentaire).IsAssignableFrom(newValue.GetType()) Then
            Throw New ArgumentException("newValue must be of type Commun.Commentaire.", "newValue")
        End If
    End Sub 'OnSet

    Protected Overrides Sub OnValidate(ByVal key As Object, ByVal value As Object)
        If Not GetType(System.String).IsAssignableFrom(key.GetType()) Then
            Throw New ArgumentException("key must be of type String.", "key")
        End If
        If Not GetType(Commun.Commentaire).IsAssignableFrom(value.GetType()) Then
            Throw New ArgumentException("value must be of type Commun.Commentaire.", "value")
        End If
    End Sub 'OnValidate 


End Class

