﻿Imports Microsoft.VisualBasic
Imports Commun
Imports System.Data

Public Class WebTools

    Public Sub New()

    End Sub

    Public Shared Function dsIsValid(ByVal ds As DataSet) As Boolean
        If ds IsNot Nothing Then
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                End If
            End If
        End If
        Return False
    End Function

    Public Shared Function isServeur() As Boolean
        Return ((InStr(1, System.Web.HttpContext.Current.Request.Url.ToString(), "portail.csdccs.edu.on.ca", CType(1, CompareMethod)) > 0) Or (InStr(1, System.Web.HttpContext.Current.Request.Url.ToString(), "apps.csdccs.edu.on.ca", CType(1, CompareMethod)) > 0))
    End Function

End Class
