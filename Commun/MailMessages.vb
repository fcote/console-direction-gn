Imports Microsoft.VisualBasic
Imports System.Net.mail
Imports System.data

Namespace Messagerie

    Public Class StatutsMail
        Public Const Normal As String = "X"
        Public Const Efface As String = "E"
        Public Const Retire As String = "R"
    End Class

    Public Enum Typesdevue
        Inbox
        Envoyes
    End Enum

    Public Class MailFilter

        Public Enum FilterTypes
            Cours
            Eleve
        End Enum

        Private _type As FilterTypes
        Private _value As Object

        Public ReadOnly Property Type() As FilterTypes
            Get
                Return _type
            End Get
        End Property

        Public ReadOnly Property Value() As Object
            Get
                Return _value
            End Get
        End Property

        Public Function Serialize() As String

            Dim ser As String

            ser = _type.ToString() & "%"

            If TypeOf (_value) Is Commun.UneClasse Then
                ser &= CType(Value, Commun.UneClasse).Serialize()
            Else
                ser &= Value.ToString()
            End If

            Return ser

        End Function

        Public Sub Deserialize(ByVal ser As String)

            Dim s() As String = ser.Split("%".ToCharArray(0, 1))

            _type = CType(s(0), Commun.Messagerie.MailFilter.FilterTypes)

            If _type = FilterTypes.Cours Then
                Dim c As Commun.UneClasse = New Commun.UneClasse()
                c.Deserialize(s(1))
                _value = c
            Else
                _value = s(1)
            End If

        End Sub

        Public Sub New(ByVal p_type As FilterTypes, ByVal p_value As Object)
            If p_type = FilterTypes.Cours Then
                If Not (TypeOf (p_value) Is Commun.UneClasse) Then
                    Throw New Exception("Le filtre est de type Cours mais la valeur n'est pas une classe.")
                End If
            End If

            _type = p_type
            _value = p_value

        End Sub

        Public Sub New(ByVal serializeInfo As String)
            Deserialize(serializeInfo)
        End Sub

    End Class

    <Serializable()> _
    Public Class Destinataire
        Implements System.Runtime.Serialization.ISerializable

        Public Adresse As MailAddress = New MailAddress("csdccs@csdccs.edu.on.ca")
        Public PersonID As String
        Public ChildID As String
        Public Display As String
        Public Notifier As Boolean
        Public Classe As UneClasse
        Public Type_Person As String

        Public Sub New()

        End Sub

        Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
            Adresse = New MailAddress(info.GetString("Adresse"))
            PersonID = info.GetString("PersonID")
            Display = info.GetString("Display")
            Notifier = info.GetBoolean("Notifier")
            Type_Person = info.GetString("Type_Person")
        End Sub

        Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
            info.AddValue("Adresse", Adresse.Address, System.Type.GetType("System.String"))
            info.AddValue("PersonID", PersonID, System.Type.GetType("System.String"))
            info.AddValue("Display", Display, System.Type.GetType("System.String"))
            info.AddValue("Notifier", Notifier, System.Type.GetType("System.Boolean"))
            info.AddValue("Type_Person", Type_Person, System.Type.GetType("System.String"))
        End Sub

    End Class

    Public NotInheritable Class DestinataireCollection
        Inherits CollectionBase

        Default Public Overloads Property Item(ByVal index As Integer) As Destinataire
            Get
                Return CType(Me.List.Item(index), Destinataire)
            End Get
            Set(ByVal value As Destinataire)
                Me.List.Item(index) = value
            End Set
        End Property
        Default Public Overloads ReadOnly Property Item(ByVal membre_id As String) As Destinataire
            Get
                Return Me(IndexOf(membre_id))
            End Get
        End Property

        Public Function Add(ByVal value As Destinataire) As Integer
            Return Me.List.Add(value)
        End Function

        Public Function Add(ByVal adresse As MailAddress, ByVal membre_id As String) As Destinataire
            Dim p As New Destinataire()
            p.Adresse = adresse
            p.PersonID = membre_id

            Me.Add(p)
            Return p
        End Function

#Region "Collection Manipulation"
        Public Overloads Function Contains(ByVal p As Destinataire) As Boolean
            Return Me.List.Contains(p)
        End Function

        Public Overloads Function Contains(ByVal name As String) As Boolean
            Return (-1 <> IndexOf(name))
        End Function

        Public Overloads Function IndexOf(ByVal p As Destinataire) As Integer
            Return Me.List.IndexOf(p)
        End Function

        Public Overloads Function IndexOf(ByVal membre_id As String) As Integer
            Dim index As Integer = 0
            Dim item As Destinataire
            ' Brute force            
            For Each item In Me
                If item.PersonID = membre_id Then
                    Return index
                End If
                index += 1
            Next
            Return -1
        End Function

        Public Overloads Sub Remove(ByVal p As Destinataire)
            Me.RemoveAt(IndexOf(p.PersonID))
        End Sub

        Public Overloads Sub Remove(ByVal membre_id As String)
            Me.RemoveAt(IndexOf(membre_id))
        End Sub
#End Region

        Protected Overrides Sub OnValidate(ByVal value As Object)
            If Not TypeOf value Is Destinataire Then
                Throw New ArgumentException("On doit ajouter un objet Destinataire")
            End If

        End Sub

    End Class

    Public Class MessagerieMessage

        Public ReadOnly [To] As DestinataireCollection = New DestinataireCollection()
        Public Sender As String
        Public From As String
        Public Subject As String
        Public Body As String

        Public Sub New()

        End Sub
    End Class

    Public Class Utilitaires
        Public Shared Function genererNouvelleFenetre(ByVal dataitemindex As String, ByVal mode As String, ByVal source As String, ByVal parentApp As String, ByVal enregistrer As Boolean, Optional ByVal return_false As Boolean = True) As String

            Dim js As String = "var child = window.open('msg.aspx?dii=" & dataitemindex & "&mode=" & mode & "&source=" & source & "&parentapp=" & parentApp & "', '', 'toolbar=no,width=750,height=700');"

            If return_false Then
                js &= " return false;"
            End If

            Return js

        End Function

        Public Shared Function genererFenetreContacts(ByVal parentApp As String, ByVal enregistrer As Boolean) As String

            Dim js As String = String.Empty

            If parentApp = "AE" Then
                js = "var child = window.open('ChoisirContacts_AE.aspx', '', 'toolbar=no,width=1500,height=1024,scrollbars=yes,resizable=yes');"
            Else
                js = "var child = window.open('ChoisirContacts_PP.aspx', '', 'toolbar=no,width=750,height=700,scrollbars=yes,resizable=yes');"
            End If

            Return js

        End Function

    End Class

End Namespace
