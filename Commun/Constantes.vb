Namespace Securite

    Public Enum Roles
        'cette liste de roles devra �tre d�finit dans Gestion_roles et g�r� a partir de la.
        NonAuthorize = 1
        Eleve = 2
        Enseignant = 4
        Employe = 4
        Direction_ecole = 8
        Direction_service = 16
        Surintendant = 32
        Administrateur = 64
        Administrateur_Formulaire = 256
        Administrateur_srv = 512
        gestionnaire_wf = 1024
        gestionnaire_form = 2048
        public_role = 4096
        acces_rapport = 8192
    End Enum

    Public Class RolesCollection
        Implements IEnumerable

        Private _role As Integer = 1

        Public Sub New()

        End Sub

        Public Sub Add(ByVal role As Roles)

            If role <> Roles.NonAuthorize Then
                Remove(Roles.NonAuthorize)
            Else
                If _role > 1 Then
                    'Ignorer si on a d'autres r�les
                    Return
                End If
            End If

            If Not Contains(role) Then
                _role += role
            End If

        End Sub

        Public Sub Remove(ByVal role As Roles)
            If Contains(role) Then
                _role -= role
            End If
        End Sub

        Public Function Contains(ByVal role As Roles) As Boolean
            Return (_role And role) = role
        End Function

        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Dim r As ArrayList = New ArrayList()

            For i As Integer = 0 To 5
                Dim role As Roles = CType([Enum].Parse(System.Type.GetType("Commun.Securite.Roles"), i.ToString), Roles)
                If Contains(role) Then
                    r.Add(role)
                End If
            Next

            Return r.GetEnumerator()

        End Function

        Public Function GetValue() As Integer

            Return _role

        End Function

        Public Sub SetValue(ByVal value As Integer)
            _role = value
        End Sub

    End Class

End Namespace

Public Enum Paliers
    Elementaire
    Intermediaire
    Secondaire
End Enum

Public Enum Preferences
    Tabulation = 1
    CommunicationActivee = 2
    PublierProfil = 3
    EnvoiResultatsParCourriel = 4
    AfficherGraphiquesDansProfilRendement = 5
    AfficherEvaluationsFormativesDansProfilRendement = 6
    AfficherMoyenneClasse = 7
    RecevoirNotifications = 8
    AfficherElevesQuitte = 9
End Enum

Public Enum ContexteLogin
    StandAlone
    VO
End Enum

Public Enum StatutTransfertNotes
    AucunTransfert
    EnCours
    Complet
End Enum

Public Structure Ponderation
    Public Comp1 As Integer
    Public Comp2 As Integer
    Public Comp3 As Integer
    Public Comp4 As Integer
End Structure

Public Class Constantes
    Public Const MAX_COMP As Integer = 4
    Public Const FORMAT_DATE As String = "dd-MM-yy"
    Public Const GLOBALE As String = "globale"
    Public Const ANALYTIQUE As String = "analytique"
    Public Const PAS_DE_NOTE_DISPLAY As String = ""
    Public Const FORMATIF_DISPLAY As String = "F"
    Public Const MOYENNE_VIDE As String = ""
    Public Const NOTE_MAX_GLOBALE_SEC As Integer = 100
    Public Const NOTE_MAX_GLOBALE_ELE As Integer = 100
    Public Const NIVEAU_MATERNELLE As String = "JK"
    Public Const NIVEAU_JARDIN As String = "SK"
    Public Const MAX_PREFS As Integer = 9
    Public Const NOM_CONSEIL As String = "Conseil Scolaire De District Catholique Centre-Sud"
    Public Const NO_HAB_ELE As Integer = 10
    Public Const NO_HAB_SEC As Integer = 6
    Public Const SUBJECT_CODE_ANGLAIS As String = "9999000000020"
    Public Const TRILL_ALF As String = "ALF"
    Public Const TRILL_PDF As String = "PDF"
    Public Const TRILL_APD As String = "APD"
    Public Const TRILL_NON_APPLICABLE As String = "N/A"
    Public Const TRILL_PEI_TRUE As String = "1"
    Public Const TRILL_PEI_FALSE As String = "0"

    'Public Const NORME_MINISTERE_ELEMENTAIRE As String = "n3-"

    Public Const MAX_ESSAIS_CONNECTION As Integer = 5

    Public Class Competences
        Public Const COMP1_NOM As String = "Connaissance et compr�hension"
        Public Const COMP2_NOM As String = "Habilet� de la pens�e"
        Public Const COMP3_NOM As String = "Communication"
        Public Const COMP4_NOM As String = "Mise en application"

        Public Const COMP1_ABB As String = "CC"
        Public Const COMP2_ABB As String = "HP"
        Public Const COMP3_ABB As String = "CO"
        Public Const COMP4_ABB As String = "MA"

        Public Shared Function GetNom(ByVal index As Integer) As String
            Select Case index
                Case 1
                    Return COMP1_NOM
                Case 2
                    Return COMP2_NOM
                Case 3
                    Return COMP3_NOM
                Case 4
                    Return COMP4_NOM
                Case Else
                    Return String.Empty
            End Select
        End Function

        Public Shared Function GetAbb(ByVal index As Integer) As String
            Select Case index
                Case 1
                    Return COMP1_ABB
                Case 2
                    Return COMP2_ABB
                Case 3
                    Return COMP3_ABB
                Case 4
                    Return COMP4_ABB
                Case Else
                    Return String.Empty
            End Select
        End Function
    End Class

    Public Class LettresPaliers
        Public Const Secondaire As String = "S"
        Public Const Elementaire As String = "E"
        Public Const Intermediaire As String = "I"
    End Class

    Public Class EvaluationsCAP
        Public Const Diagnostic As String = "D"
        Public Const V�rification As String = "V"
        Public Const Aucun As String = "X"
    End Class

    Public Shared ReadOnly Property PAS_DE_NOTE() As Object
        Get
            Return System.DBNull.Value
        End Get
    End Property

    Public Sub New()

    End Sub
End Class

Public Class ConstRepDB
    Public Const OUI As String = "O"
    Public Const NON As String = "N"
    Public Const GLOBALE As String = "G"
    Public Const ANALYTIQUE As String = "A"
    Public Const MASCULIN As String = "M"
    Public Const FEMININ As String = "F"

    Public Class Paliers
        Public Const ELEMENTAIRE As String = "E"
        Public Const SECONDAIRE As String = "S"
    End Class

    'Public Class TypesEvaluationCAP
    '    Public Const DIAGNOSTIQUE As String = "D"
    '    Public Const VERIFICATION As String = "V"
    '    Public Const AUCUN As String = "X"
    'End Class


End Class

Public Class ConstantesTrillium
    Public Class SubjectCodes
        Public Const ENSEIGNEMENT_RELIGIEUX As String = "9999000000076"
        Public Const ANGLAIS As String = "9999000000020"
    End Class
End Class