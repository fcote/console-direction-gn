Imports Microsoft.VisualBasic
Imports System.Text
Imports System.drawing
Imports System.Web.UI.WebControls

Public Class FontMetrics

    Public Function GetSize(ByVal comm As Commentaire, ByVal blocksz As SizeF, ByVal u As UnitType) As SizeF

        Dim v As Drawing.Bitmap = New Bitmap(1, 1, Imaging.PixelFormat.Format24bppRgb)

        Dim g As Graphics = Graphics.FromImage(v)

        Dim ff As Font = New Font("arial", 8)

        'Convertir le block size en pixels:
        Dim blocksz_px As SizeF = _mm_to_px(blocksz, g)

        Dim strf As StringFormat = New StringFormat(StringFormatFlags.NoClip)
        strf.Alignment = StringAlignment.Far
        Dim sf As SizeF = g.MeasureString(comm.ToString().Replace(Commentaire.STR_COMM_NOTE_SEMESTRE, "99,9%").Replace(Commentaire.STR_COMM_NOTE_EXAMEN, "99,9%"), ff, CInt(Math.Floor(blocksz_px.Width)), strf)

        Dim sf_cm As SizeF = sf

        If u = UnitType.Mm Then
            'Convertir le bloc retourné en cm:
            sf_cm = _px_to_mm(sf, g)
        ElseIf u = UnitType.Pixel Then
            'Ne rien faire
        Else
            Throw New Exception("Unit type " & u.ToString() & " not supported.")
        End If

        'Se débarasser des objets pu bons
        g.Dispose()
        v.Dispose()

        Return sf_cm

        'If sf_cm.Height > blocksz.Height Then
        '    Return False
        'Else
        '    Return True
        'End If

    End Function

    Public Function GetSize(ByVal s As String, ByVal blocksz As SizeF, ByVal u As UnitType) As SizeF

        Dim v As Drawing.Bitmap = New Bitmap(1, 1, Imaging.PixelFormat.Format24bppRgb)

        Dim g As Graphics = Graphics.FromImage(v)

        Dim ff As Font = New Font("arial", 8)

        'Convertir le block size en pixels:
        Dim blocksz_px As SizeF = _mm_to_px(blocksz, g)

        Dim strf As StringFormat = New StringFormat(StringFormatFlags.NoClip)
        strf.Alignment = StringAlignment.Far
        Dim sf As SizeF = g.MeasureString(s.Replace(Commentaire.STR_COMM_NOTE_SEMESTRE, "99,9%").Replace(Commentaire.STR_COMM_NOTE_EXAMEN, "99,9%"), ff, CInt(Math.Floor(blocksz_px.Width)), strf)

        Dim sf_cm As SizeF = sf

        If u = UnitType.Mm Then
            'Convertir le bloc retourné en cm:
            sf_cm = _px_to_mm(sf, g)
        ElseIf u = UnitType.Pixel Then
            'Ne rien faire
        Else
            Throw New Exception("Unit type " & u.ToString() & " not supported.")
        End If

        'Se débarasser des objets pu bons
        g.Dispose()
        v.Dispose()

        Return sf_cm

        'If sf_cm.Height > blocksz.Height Then
        '    Return False
        'Else
        '    Return True
        'End If

    End Function

    Private Function _px_to_mm(ByVal px As SizeF, ByVal g As Graphics) As SizeF
        'Obtenons la grandeur en pouces
        Dim ht_pouces As Single = px.Height / g.DpiY
        Dim wt_pouces As Single = px.Width / g.DpiX

        'Maintenant calculons la grandeur en cm
        Dim ht_cm As Single = System.Convert.ToSingle(ht_pouces * 2.54)
        Dim wt_cm As Single = System.Convert.ToSingle(wt_pouces * 2.54)

        Dim sz As SizeF = New SizeF(wt_cm * 10, ht_cm * 10)

        Return sz

    End Function

    Private Function _mm_to_px(ByVal mm As SizeF, ByVal g As Graphics) As SizeF
        Dim ht_pouces As Single = System.Convert.ToSingle(mm.Height / 10 / 2.54)
        Dim wt_pouces As Single = System.Convert.ToSingle(mm.Width / 10 / 2.54)

        Dim sz As SizeF = New SizeF()
        sz.Height = ht_pouces * g.DpiY
        sz.Width = wt_pouces * g.DpiX

        Return sz
    End Function

End Class
