Imports System.Data

Namespace ObjetsRapports

    Public Class TableParams
        Inherits DataTable

        Public Sub New()
            Me.TableName = "Parametres_Rapport"
            Me.Columns.Add("Nom", System.Type.GetType("System.String"))
            Me.Columns.Add("Valeur", System.Type.GetType("System.String"))
            Dim pk(0) As DataColumn
            pk(0) = Me.Columns("nom")
            Me.PrimaryKey = pk
        End Sub

        Public Sub AjouterParametre(ByVal nom As String, ByVal valeur As String)
            Dim dr As DataRow = Me.NewRow()
            dr("Nom") = nom
            dr("Valeur") = valeur
            Me.Rows.Add(dr)
        End Sub

    End Class

End Namespace
