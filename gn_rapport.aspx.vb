﻿Imports System.Data
Imports Oracle.DataAccess.Client
Imports System.Drawing

Partial Class gn_rapport
    Inherits System.Web.UI.Page
    Dim eval_reel As Integer = 0
    Dim nb_eval_tot As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Session("role") <> "administrateur" And Session("role") <> "directeur" And Session("role") <> "enseignant" Then
            Response.Redirect("~/index.aspx")
        End If

        If Not Page.IsPostBack Then
            If Session("role") = "administrateur" Or Session("role") = "directeur" Then
                SqlDataSource4.SelectCommand = " SELECT DISTINCT gne2.m_ecole as ecole, gne2.nom_ecole " & _
                                                " FROM GN_VW_ENSEIGNANTS GNE2 " & _
                                                " WHERE GNE2.M_MEMBRE_ID = :ID_USER"
            Else
                SqlDataSource4.SelectCommand = " select ecole from SP_ACCES_CONSOLE where login = :ID_USER"
            End If
            Load_data()
        Else
            update_req_rg(Session("eleve_id"))
            RadGrid1.ExportSettings.FileName = "Rapport_eleve" & Date.Today & "_" & DropDownList3.SelectedValue.ToString
            RadGrid1.ExportSettings.IgnorePaging = True
            RadGrid1.ExportSettings.OpenInNewWindow = True
            RadGrid1.MasterTableView.CommandItemSettings.RefreshText = "Rafraîchir"
        End If


    End Sub


    

    Public Sub Load_data()

        DropDownList3.DataBind()
        update_req_rg("")
        RadGrid1.Rebind()

    End Sub

    Public Sub update_req_rg(ByVal eleve_id As String)


        Dim ds As DataSet = New OracleHelperAE().getDataSet("select school_year_track from school_year_tracks  where school_code = '" & DropDownList3.SelectedValue & "' and school_year =  gn.fc_val_parametres ('ANNEESCOLAIRE')")
        If ds IsNot Nothing Then

            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("school_year_track") <> "Second" Then
                    SqlDataSource2.SelectCommand = "select p.preferred_name_upper ,s.e_classe_code, s.e_codecours , e_date,  e_periode_ref, e_description, e_moyenne, e_compteur_eleve, e_ponderation, e_examen, "
                    SqlDataSource2.SelectCommand &= "(select count(*) from sp_notes where n_evaluation_id = e_evaluation_id   and n_note <= 50) as R, "
                    SqlDataSource2.SelectCommand &= "(select count(*) from sp_notes where n_evaluation_id = e_evaluation_id   and n_note <= 62.5 and n_note >=50.0000001) as D, "
                    SqlDataSource2.SelectCommand &= "(select count(*) from sp_notes where n_evaluation_id = e_evaluation_id   and n_note <= 75 and n_note >=62.0000001) as C "
                    SqlDataSource2.SelectCommand &= "from sp_evaluations s, persons p "
                    SqlDataSource2.SelectCommand &= "where s.e_ecole = '" & DropDownList3.SelectedValue & "' and s.e_anneescolaire = gn.fc_val_parametres ('ANNEESCOLAIRE') "
                    SqlDataSource2.SelectCommand &= "and p.person_id = s.e_membre_id"
                Else
                    SqlDataSource2.SelectCommand = "select p.preferred_name_upper ,s.e_classe_code, s.e_codecours , e_date, e_description, e_moyenne, e_compteur_eleve, e_ponderation, e_examen, "
                    SqlDataSource2.SelectCommand &= "(select count(*) from sp_notes where n_evaluation_id = e_evaluation_id   and n_note <= 44.499999) as R2, "
                    SqlDataSource2.SelectCommand &= "(select count(*) from sp_notes where n_evaluation_id = e_evaluation_id   and n_note <= 49.499999 and n_note >=44.5) as R1, "
                    SqlDataSource2.SelectCommand &= "(select count(*) from sp_notes where n_evaluation_id = e_evaluation_id   and n_note <= 59.499999 and n_note >=49.5) as N1 "
                    SqlDataSource2.SelectCommand &= "from sp_evaluations s, persons p "
                    SqlDataSource2.SelectCommand &= "where s.e_ecole = '" & DropDownList3.SelectedValue & "' and s.e_anneescolaire = gn.fc_val_parametres ('ANNEESCOLAIRE') "
                    SqlDataSource2.SelectCommand &= "and p.person_id = s.e_membre_id "
                End If
            End If

        End If

    End Sub

    Protected Sub DropDownList3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList3.SelectedIndexChanged
        Load_data()
    End Sub




End Class


