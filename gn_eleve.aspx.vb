﻿Imports dotnetCHARTING
Imports System.Data
Imports Oracle.DataAccess.Client
Imports System.Drawing

Partial Class gn_eleve
    Inherits System.Web.UI.Page
    Dim eval_reel As Integer = 0
    Dim nb_eval_tot As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Session("role") <> "administrateur" And Session("role") <> "directeur" And Session("role") <> "enseignant" Then
            Response.Redirect("~/index.aspx")
        End If

        If Not Page.IsPostBack Then
            If Session("role") = "administrateur" Or Session("role") = "directeur" Then
                SqlDataSource4.SelectCommand = " SELECT DISTINCT gne2.m_ecole as ecole, gne2.nom_ecole " & _
                                                " FROM GN_VW_ENSEIGNANTS GNE2 " & _
                                                " WHERE GNE2.M_MEMBRE_ID = :ID_USER"
            Else
                SqlDataSource4.SelectCommand = " select ecole from SP_ACCES_CONSOLE where login = :ID_USER"
            End If
            Load_data()
        Else
            update_req_rg(Session("eleve_id"))
            RadGrid1.ExportSettings.FileName = "Élèves_en_échecs_" & Date.Today & "_" & DropDownList3.SelectedValue.ToString
            RadGrid1.ExportSettings.IgnorePaging = True
            RadGrid1.ExportSettings.OpenInNewWindow = True
            RadGrid1.MasterTableView.CommandItemSettings.RefreshText = "Rafraîchir"
        End If


        'A FINIR: a ajouter au bonne endroit
        If Session("m_ecole") <> "" Then
            If Not Page.IsPostBack Then
                Me.RadGrid1.MasterTableView.GroupsDefaultExpanded = False
                'Dim expression1 As Telerik.Web.UI.GridGroupByExpression = Telerik.Web.UI.GridGroupByExpression.Parse("e_classe_code [Cours], count(e_classe_code) Items [Évaluation], avg(note_sur_100) [Moyenne] Group By E_CLASSE_CODE")
                Dim expression1 As Telerik.Web.UI.GridGroupByExpression = Telerik.Web.UI.GridGroupByExpression.Parse("e_classe_code [Cours], count(e_classe_code) Items [Évaluation] Group By E_CLASSE_CODE")
                Me.RadGrid1.MasterTableView.GroupByExpressions.Add(expression1)
                'Me.RadGrid1.MasterTableView.FilterExpression.
                RadGrid1.Rebind()

                ''filtre example:
                'RadGrid1.MasterTableView.FilterExpression = "([Country] LIKE '<see cref="Germany"/>') "
                'Dim column As GridColumn = RadGrid1.MasterTableView.GetColumnSafe("Country")
                'column.CurrentFilterFunction = GridKnownFunction.Contains
                'column.CurrentFilterValue = "Germany"
            End If
            'RadGrid1.Rebind()

        End If
    End Sub

    Protected Sub DropDownList1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.DataBound
        DropDownList1_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        'Load_data(DropDownList1.SelectedValue)
        If DropDownList1.SelectedValue <> "" Then
            Session("eleve_id") = DropDownList1.SelectedValue
            Session("nom_eleve") = DropDownList1.SelectedItem.Text
            update_req_rg(DropDownList1.SelectedValue)
            RadGrid1.Rebind()
        End If
        chart.Visible = False
    End Sub

    Public Sub Load_data()

        DropDownList3.DataBind()

      	
        'SqlDataSource1.SelectCommand = "SELECT DISTINCT GNE1.M_NOM || ', ' || GNE1.M_PRENOM || ' - ' || '(' || GNE1.NIVEAU || ')' AS NOM, GNE1.MEMBRE_ID " & _
        '"FROM GN_VW_ELEVES_SEC  GNE1 " & _
        '"WHERE GNE1.ECOLE = '" & DropDownList3.SelectedValue & "' " & _
        '"ORDER BY NOM"

        'SqlDataSource3.SelectCommand = "SELECT DISTINCT GNE1.M_NOM || ', ' || GNE1.M_PRENOM || ' - ' || '(' || GNE1.NIVEAU || ')' AS NOM, GNE1.MEMBRE_ID " & _
        '"FROM GN_VW_ELEVES_ELE  GNE1 " & _
        '"WHERE GNE1.ECOLE = '" & DropDownList3.SelectedValue & "' " & _
        '"ORDER BY NOM"

        
        SqlDataSource3.SelectCommand = "SELECT DISTINCT p.legal_surname || ', ' || p.legal_first_name || ' - ' || '(' || sr.grade || ')' AS NOM, p.person_id membre_id " & _
        " FROM trill02.student_program_class_tracks spct,  trill02.persons p,  trill02.student_registrations sr,  trill02.class_courses cc " & _
        " WHERE p.person_id    = sr.person_id " & _
        " AND spct.person_id   = sr.person_id " & _
        " AND spct.school_code = sr.school_code " & _
        " AND spct.school_year = sr.school_year " & _
        " AND sr.status_indicator_code IN ('Active', 'PreReg', 'Inactive') " & _
        " AND sr.school_year            = '" & DropDownList6.SelectedValue & "' " & _
        " AND cc.school_code            = spct.school_code " & _
        " AND cc.school_year            = spct.school_year " & _
        " AND cc.class_code             = spct.class_code " & _
        " AND cc.grade                  = sr.grade " & _
        " AND spct.school_code = '" & DropDownList3.SelectedValue & "' " & _
        " AND sr.school_code           IN " & _
"           (SELECT school_code " & _
          " FROM trill02.schools " & _
          " WHERE default_school_bsid IN " & _
"             (SELECT organization_id " & _
            " FROM trill02.organizations " & _
            " WHERE board_bsid     = 'B67318' " & _
            " AND school_type_code = 'Elem' " & _
            " ) " & _
          " AND active_flag = 'x' " & _
          " ) " & _
        " AND spct.start_date = " & _
"           (SELECT MAX (spct01.start_date) " & _
          " FROM trill02.student_program_class_tracks spct01 " & _
          " WHERE spct01.person_id = spct.person_id " & _
          " AND spct01.school_code = spct.school_code " & _
          " AND spct01.school_year = spct.school_year " & _
          " AND spct01.class_code  = spct.class_code " & _
          " ) ORDER BY NOM" 
        
        
        
        
        
        SqlDataSource1.SelectCommand = "SELECT DISTINCT p.legal_surname || ', ' || p.legal_first_name || ' - ' || '(' || sr.grade || ')' AS NOM, p.person_id membre_id " & _
        " FROM TRILL02.persons p, TRILL02.student_registrations sr, TRILL02.student_program_class_tracks spct " & _
        " WHERE p.person_id    = sr.person_id " & _
        " AND spct.person_id   = sr.person_id " & _
        " AND spct.school_code = sr.school_code " & _
        " AND spct.school_year = sr.school_year " & _
        " AND sr.status_indicator_code IN ('Active', 'PreReg', 'Inactive') " & _
        " AND spct.school_year_track    = sr.school_year_track " & _
        " AND sr.school_year            = '" & DropDownList6.SelectedValue & "' " & _
        " AND spct.school_code = '" & DropDownList3.SelectedValue & "' " & _
        " AND sr.school_code           IN " & _
"           (SELECT school_code " & _
          " FROM TRILL02.schools " & _
          " WHERE default_school_bsid IN " & _
"             (SELECT organization_id " & _
            " FROM TRILL02.organizations " & _
            " WHERE board_bsid                                                 = 'B67318' " & _
            " AND DECODE (school_type_code, 'ConEd', 'Sec', school_type_code) = 'Sec' " & _
            " ) " & _
          " AND active_flag = 'x' " & _
          " ) " & _
        " AND spct.start_date = " & _
"           (SELECT MAX (spct01.start_date) " & _
          " FROM TRILL02.student_program_class_tracks spct01 " & _
          " WHERE spct01.person_id = spct.person_id " & _
          " AND spct01.school_code = spct.school_code " & _
          " AND spct01.school_year = spct.school_year " & _
          " AND spct01.class_code  = spct.class_code " & _
          " ) ORDER BY NOM" 
        

		
		
        DropDownList1.DataBind()
        DropDownList2.DataBind()

        If DropDownList1.Items.Count > 0 Then
            DropDownList2.Visible = False
            DropDownList4.Visible = False
            DropDownList1.Visible = True
            DropDownList5.Visible = True
        Else
            DropDownList2.Visible = True
            DropDownList4.Visible = True
            DropDownList1.Visible = False
            DropDownList5.Visible = False
        End If

    End Sub

    Public Sub update_req_rg(ByVal eleve_id As String)


        'elementaire = 0  DECODE ('0','0', 
        'intermediare = 1  DECODE ('1','0', 
        'secondaire = 2   DECODE ('2','0', 
        Dim palier As String = " DECODE('1', '0', 'E', '1', 'S') "
        Dim p As String = " '1' "
        Dim p_niv As String = " 'E' " ' pas besoin
        Dim p_ref As String = " '" & DropDownList4.SelectedValue & "' " ' 
        Dim semestre As String = ""
        Dim sem_table As String = ""
        If DropDownList1.Items.Count > 0 Then
            palier = " DECODE('2', '0', 'E', '1', 'S') "
            p = " '2' "
            p_niv = " 'S' " ' pas besoin
            p_ref = " '00' "
            semestre = " AND t.school_code = er.m_ecole "
            semestre &= " AND t.school_year = er.m_anneescolaire "
            semestre &= " AND t.class_code = er.m_classe_code "
            semestre &= " AND t.semester = '" & DropDownList5.SelectedValue & "' "
            semestre &= " AND t.term = '2' "
            'semestre &= " AND t.term = '" & DropDownList6.SelectedValue & "' "
            sem_table = " , school_class_term t "
        End If


        'periode_ref I P ou F (en realité I ou F) 

        SqlDataSource2.SelectCommand = " " & _
        "SELECT   " & _
        "e.e_classe_code, " & _
        "e.e_description, " & _
        "TO_CHAR (e.e_date, 'dd-MM-yy') AS e_date, " & _
        "gne.m_nom || ' ' || gne.m_prenom as Enseignant, " & _
        "er.m_prenom as Prenom, er.m_nom as nom, m_a_quitte, " & _
        "pkg_sp.fc_niveau_a_afficher (n_comp1_sur100," & DropDownList6.SelectedValue  & ", " & palier & ") AS CC, " & _
        "pkg_sp.fc_niveau_a_afficher (n_comp2_sur100," & DropDownList6.SelectedValue  & ", " & palier & ") AS HP, " & _
        "pkg_sp.fc_niveau_a_afficher (n_comp3_sur100," & DropDownList6.SelectedValue  & ", " & palier & ") AS CO, " & _
        "pkg_sp.fc_niveau_a_afficher (n_comp4_sur100," & DropDownList6.SelectedValue  & ", " & palier & ") AS MA, " & _
        "pkg_sp.fc_niveau_a_afficher (n_note, " & DropDownList6.SelectedValue  & ", " & palier & ") AS note, " & _
        "ROUND (n_note, 1) as note_sur_100, n_abs as abs, " & _
        " DECODE ( " & p & " ,'0', (SELECT b_niveau_bulletin " & _
        "            FROM sp_baremes " & _
        "           WHERE b_niveau_rendement = pkg_sp.fc_niveau_a_afficher(ROUND (NVL (m_note_ajustee,m_note),1), " & _
        "             " & DropDownList6.SelectedValue  & "," & palier & ") " & _
        "             AND b_anneescolaire = " & DropDownList6.SelectedValue  & " " & _
        "             AND b_niveau = " & palier & " " & _
        "             AND b_type_eval = 'G'), " & _
        "   '1', ROUND (NVL (m_note_ajustee, m_note), 1), " & _
        "   '2', ROUND (NVL (m_note_ajustee, m_note), 1) " & _
        "   ) AS note_bulletin " & _
        "FROM sp_eleves_resultats er,  " & _
        "    sp_notes,  " & _
        "    sp_evaluations e, gn_vw_enseignants gne " & _
        " " & sem_table & " " & _
        "WHERE er.m_ecole = '" & DropDownList3.SelectedValue & "' " & _
        "AND e.E_ECOLE = er.m_ecole " & _
        "  AND er.m_membre_id = '" & eleve_id & "' " & _
        "AND er.m_periode_ref = " & p_ref & " " & _
        "  AND er.m_classe_code = e.e_classe_code " & _
        "AND er.m_codecours = e.e_codecours " & _
        "AND er.m_strand_code = e.e_strand_code  " & _
        "     AND er.m_anneescolaire = " & DropDownList6.SelectedValue  & " " & _
        "     AND er.m_membre_id = n_membre_id " & _
        "     and e.e_evaluation_id = n_evaluation_id " & _
        "     and gne.m_membre_id = e.e_membre_id " & _
        "     and gne.m_ecole = er.m_ecole " & _
        " " & semestre & " " & _
        " ORDER BY n_evaluation_id, er.m_nom, er.m_prenom"


    End Sub

    Protected Sub DropDownList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList2.SelectedIndexChanged
        'Load_data(DropDownList2.SelectedValue)
        If DropDownList2.SelectedValue <> "" Then
            Session("eleve_id") = DropDownList2.SelectedValue
            Session("nom_eleve") = DropDownList2.SelectedItem.Text
            update_req_rg(DropDownList2.SelectedValue)
            RadGrid1.Rebind()

        End If
        chart.Visible = False
    End Sub

    Protected Sub DropDownList2_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList2.DataBound
        DropDownList2_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub DropDownList3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList3.SelectedIndexChanged
        Load_data()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        If Session("eleve_id") <> "" Then

            chart.Visible = True
            Dim de As New DataEngine()

            Dim conf As String = "2"
            If conf = "1" Then
                Dim ds As DataSet = New OracleHelperAE().getDataSet(Chart_ee())
                de.Data = ds
                Dim sc As SeriesCollection = de.GetSeries()
                chart.SeriesCollection.Add(sc)
            ElseIf conf = "2" Then
                'ajout des données
                Dim data As SeriesCollection = getData()
                If Not data Is Nothing Then
                    chart.SeriesCollection.Add(data)
                End If

            End If


            ' "effet" visuel 3D
            'Chart.ShadingEffectMode = ShadingEffectMode.Four

            '3D avec ombre
            'chart.Use3D = True
            'chart.ShadingEffect = True

            'nom du graphique
            Chart.Series.Name = "Note par évaluation par cours"
            chart.TitleBox.Label.Text = "Note par évaluation par cours:" & ControlChars.Lf & Session("nom_eleve")
            'taille du graphique
            'Chart.Width = Unit.Parse(1200)
            Chart.Height = Unit.Parse(800)
            'enleve les couleur de fond du graphique
            Chart.ChartArea.ClearColors()
            'stack mode 
            Chart.YAxis.Scale = Scale.Stacked
            'nom des axes
            chart.XAxis.Label.Text = "Cours / matières"
            'taille des bars en largeur
            chart.XAxis.StaticColumnWidth = 50
            'nom du fichier de l'image
            chart.FileName = "Notes_" & Session("nom_eleve").ToString.Replace(" ", "_") & "_" & Date.Today.Second
            'chemin où sont généré les fichier image temporaire:
            chart.TempDirectory = "~/temp"
            ''annotation spécifique à un point précis
            'Dim an As New Annotation("Moyenne de tous les cours")
            'an.Position = New Point(320, 20)
            'chart.Annotations.Add(an)
            ''ajout d'un marker (ligne)
            'Dim am As AxisMarker = New AxisMarker("60", New Line(Color.Blue, 2), 60)
            'am.Label.Alignment = StringAlignment.Near
            'Chart.YAxis.Markers.Add(am)
            'Format de la légende: icone et nom 
            chart.LegendBox.Template = "%Icon %Name"

            'range marker: couleur en degradé dans le background
            'A FAIRE: utiliser les données de trillium
            Dim am1 As New AxisMarker("4+", New Background(Color.Green, Color.Green, 90), 94.5, 100)
            Dim am1b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 94.5)
            Dim am2 As New AxisMarker("4", New Background(Color.Green, Color.Green, 90), 86.5, 94.5)
            Dim am2b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 86.5)
            Dim am3 As New AxisMarker("4-", New Background(Color.Green, Color.GreenYellow, 90), 79.5, 86.5)
            Dim am3b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 79.5)
            Dim am4 As New AxisMarker("3+", Color.GreenYellow, 76.5, 79.5)
            Dim am4b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 76.5)
            Dim am5 As New AxisMarker("3", Color.GreenYellow, 72.5, 76.5)
            Dim am5b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 72.5)
            Dim am6 As New AxisMarker("3-", New Background(Color.GreenYellow, Color.Yellow, 90), 69.5, 72.5)
            Dim am6b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 69.5)
            Dim am7 As New AxisMarker("2+", Color.Yellow, 66.5, 69.5)
            Dim am7b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 66.5)
            Dim am8 As New AxisMarker("2", Color.Yellow, 62.5, 66.5)
            Dim am8b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 62.5)
            Dim am9 As New AxisMarker("2-", New Background(Color.Yellow, Color.Orange, 90), 59.5, 62.5)
            Dim am9b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 59.5)
            Dim am10 As New AxisMarker("1+", Color.Orange, 56.5, 59.5)
            Dim am10b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 56.5)
            Dim am11 As New AxisMarker("1", Color.Orange, 52.5, 56.5)
            Dim am11b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 52.5)
            Dim am12 As New AxisMarker("1-", New Background(Color.Orange, Color.Salmon, 90), 49.5, 52.5)
            Dim am12b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 49.5)
            Dim am13 As New AxisMarker("R1", Color.Salmon, 44.5, 49.5)
            Dim am13b As AxisMarker = New AxisMarker("", New Line(Color.Blue, 2), 44.5)
            Dim am14 As New AxisMarker("R2", Color.Salmon, 0, 44.5)



            'chart.YAxis.Markers.Add(am1, am1b, am2, am2b, am3, am3b, am4, am4b, am5, am5b, am6, am6b, am7, am7b, am8, am8b, am9, am9b, am10, am10b, am11, am11b, am12, am12b, am13, am13b, am14)
            chart.YAxis.Markers.Add(am1, am2, am3, am4, am5, am6, am7, am8, am9, am10, am11, am12, am13, am14, am1b, am2b, am3b, am4b, am5b, am6b, am7b, am8b, am9b, am10b, am11b, am12b, am13b)

            Dim i As Integer
            For i = 0 To chart.YAxis.Markers.Count - 1
                chart.YAxis.Markers(i).LegendEntry.Visible = False
            Next
            For i = 14 To chart.YAxis.Markers.Count - 1
                chart.YAxis.Markers(i).Line.DashStyle = Drawing2D.DashStyle.Dash
                chart.YAxis.Markers(i).Line.Width = 1
            Next

        End If

        chart.Width = Unit.Parse(300 + nb_eval_tot * 150 + (eval_reel - nb_eval_tot) * 75)

    End Sub


    Function getEntry(ByVal s As Series, ByVal index As Integer) As LegendEntry
        Dim le As LegendEntry = New LegendEntry()

        Dim sql As String = "select distinct e_description,  matiere from sp_evaluations "
        sql &= "LEFT JOIN GN_VW_CLASSES_SEC ON e_codecours = codecours "
        sql &= "where e_evaluation_id = '" & s.Name & "'"

        Dim ds As DataSet = New OracleHelperAE().getDataSet(sql.ToString)
        If ds.Tables(0).Rows.Count > 0 Then
            le.Name = ds.Tables(0).Rows(0)("e_description").ToString
        Else
            'le.Name = s.Name
            le.Name = "Pas de nom"
        End If


        le.SeriesType = SeriesType.Column
        le.Background.Color = Chart.Palette(index)
        Return le
    End Function

    Private Function Chart_ee() As String
        Dim Sql As String = " "
        If Session("eleve_id") <> "" Then
            Sql &= "SELECT   "
            'Sql &= "er.m_prenom || ' ' || er.m_nom as eleve, "
            '--Sql &= "e.e_classe_code, "
            '---Sql &= "e.e_classe_code || chr(13) || chr(10) || e.e_description, "
            Sql &= "e.e_evaluation_id, "
            'Sql &= "e.e_description, "
            Sql &= "ROUND (n_note, 1) as note_sur_100  "
            '---Sql &= "e.e_evaluation_id "
            '"TO_CHAR (e.e_date, 'dd-MM-yy') AS e_date, " & _
            ''"gne.m_nom || ' ' || gne.m_prenom as Enseignant, " & _
            '"er.m_prenom as Prenom, er.m_nom as nom, " & _
            '--Sql &= "e.e_description "
            '"m_a_quitte, " & _"
            '"pkg_sp.fc_niveau_a_afficher (n_comp1_sur100," & DropDownList6.SelectedValue  & ",'S') AS CC, " & _
            '"pkg_sp.fc_niveau_a_afficher (n_comp2_sur100," & DropDownList6.SelectedValue  & ",'S') AS HP, " & _
            '"pkg_sp.fc_niveau_a_afficher (n_comp3_sur100," & DropDownList6.SelectedValue  & ",'S') AS CO, " & _
            '"pkg_sp.fc_niveau_a_afficher (n_comp4_sur100," & DropDownList6.SelectedValue  & ",'S') AS MA, " & _
            '"pkg_sp.fc_niveau_a_afficher (n_note, " & DropDownList6.SelectedValue  & ", 'S') AS note, " & _

            '"n_abs as abs, " & _
            '" DECODE ('2','0', (SELECT b_niveau_bulletin " & _
            '"            FROM sp_baremes " & _
            '"           WHERE b_niveau_rendement = pkg_sp.fc_niveau_a_afficher(ROUND (NVL (m_note_ajustee,m_note),1), " & _
            '"             " & DropDownList6.SelectedValue  & ",DECODE ('2','0','E','1', 'S')) " & _
            '"             AND b_anneescolaire = " & DropDownList6.SelectedValue  & " " & _
            '"             AND b_niveau = DECODE ('2', '0', 'E', '1', 'S') " & _
            '"             AND b_type_eval = 'G'), " & _
            '"   '1', ROUND (NVL (m_note_ajustee, m_note), 1), " & _
            '"   '2', ROUND (NVL (m_note_ajustee, m_note), 1) " & _
            '"   ) AS note_bulletin " & _
            Sql &= "FROM sp_eleves_resultats er,  "
            Sql &= "    sp_notes,  "
            Sql &= "    sp_evaluations e --, gn_vw_enseignants gne "
            Sql &= "WHERE er.m_ecole = '" & DropDownList3.SelectedValue & "' "
            Sql &= "  AND er.m_membre_id = '" & Session("eleve_id") & "' "
            Sql &= "  AND er.m_classe_code = e.e_classe_code "
            Sql &= "     AND er.m_anneescolaire = " & DropDownList6.SelectedValue  & " "
            Sql &= "     AND er.m_membre_id = n_membre_id "
            Sql &= "     and e.e_evaluation_id = n_evaluation_id "
            ''Sql &= "     and gne.m_membre_id = e.e_membre_id "
            ''Sql &= "     and gne.m_ecole = er.m_ecole "
            ''Sql &= " ORDER BY e.e_classe_code "
            'Sql &= "GROUP BY e.e_classe_code, e.e_evaluation_id, n_note "
            'Sql &= "GROUP BY e.e_evaluation_id,e.e_classe_code, n_note, e.e_description "
            'Sql &= "GROUP BY e.e_classe_code "
        End If
        Return Sql

    End Function

    Public Function getData() As SeriesCollection
        Try


            Dim SC As New SeriesCollection()
            'Dim SC2 As New SeriesCollection()
            Dim myR As New Random()

            Dim s1 As New Series()
            s1.Name = "Connaissance et compréhension"
            's1.DefaultElement.HatchStyle = Drawing2D.HatchStyle.BackwardDiagonal 'ne pas oublier la couleur
            's1.DefaultElement.HatchColor = Color.Azure
            s1.DefaultElement.ShowValue = True
            s1.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
            Dim s2 As New Series()
            s2.Name = "Habileté de la pensée"
            's2.DefaultElement.HatchStyle = Drawing2D.HatchStyle.DarkHorizontal 'ne pas oublier la couleur
            's2.DefaultElement.HatchColor = Color.Azure
            s2.DefaultElement.ShowValue = True
            s2.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
            Dim s3 As New Series()
            s3.Name = "Communication"
            ' s3.DefaultElement.HatchStyle = Drawing2D.HatchStyle.DarkVertical 'ne pas oublier la couleur
            ' s3.DefaultElement.HatchColor = Color.Azure
            s3.DefaultElement.ShowValue = True
            s3.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
            Dim s4 As New Series()
            s4.Name = "Mise en application"
            's4.DefaultElement.HatchStyle = Drawing2D.HatchStyle.DiagonalCross 'ne pas oublier la couleur
            's4.DefaultElement.HatchColor = Color.Azure
            s4.DefaultElement.ShowValue = True
            s4.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
            Dim s5 As New Series()
            s5.Name = "Moyenne"
            s5.DefaultElement.ShowValue = True
            s5.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
            Dim s6 As New Series()
            s6.Name = "Ajustement"
            s6.DefaultElement.ShowValue = True
            s6.DefaultElement.SmartLabel.OutlineColor = Color.White
            s6.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
            Dim s7 As New Series()
            s7.Name = "Moyenne classe"
            s7.Type = SeriesType.Marker
            s7.DefaultElement.ShowValue = True
            s7.DefaultElement.SmartLabel.OutlineColor = Color.White
            s7.DefaultElement.Marker.Type = ElementMarkerType.FivePointStar

            Dim palier As String = " DECODE('1', '0', 'E', '1', 'S') "
            Dim p As String = " '1' "
            Dim p_niv As String = " 'E' " ' pas besoin
            Dim p_ref As String = " '" & DropDownList4.SelectedValue & "' " ' 
            If DropDownList1.Items.Count > 0 Then 'secondaire
                palier = " DECODE('2', '0', 'E', '1', 'S') "
                p = " '2' "
                p_niv = " 'S' " ' pas besoin
                p_ref = " '00' "
            End If

            Dim Sql As String = " "
            Sql &= "SELECT   "
            Sql &= "e.e_evaluation_id as id, "
            Sql &= "decode(n_note, null, '0',ROUND (n_note, 1)) as note_sur_100,  "
            Sql &= "gne.m_prenom || ' ' || gne.m_nom  as enseignant, "
            Sql &= "decode(n_comp1_sur100, null, '0',n_comp1_sur100)  AS CC, "
            Sql &= "decode(n_comp2_sur100, null, '0',n_comp2_sur100) AS HP, "
            Sql &= "decode(n_comp3_sur100, null, '0',n_comp3_sur100) AS CO, "
            Sql &= "decode(n_comp4_sur100, null, '0',n_comp4_sur100) AS MA, "
            Sql &= "decode(er.m_note_semestre, null, '0',er.m_note_semestre) as n, "
            Sql &= "decode(er.m_note_semestre_ajustement, null, 'Double.NaN',er.m_note_semestre_ajustement) as a, "
            Sql &= "decode(er.m_note_ajustee, null, '0',er.m_note_ajustee) as na, "
            Sql &= "er.m_classe_code as classe_code, "
            Sql &= "e.e_comp1_utilisation as c1_utilisation, "
            Sql &= "e.e_comp2_utilisation as c2_utilisation, "
            Sql &= "e.e_comp3_utilisation as c3_utilisation, "
            Sql &= "e.e_comp4_utilisation as c4_utilisation, "
            Sql &= "c.c_comp1_ponderation as c1_pond, "
            Sql &= "c.c_comp2_ponderation as c2_pond, "
            Sql &= "c.c_comp3_ponderation as c3_pond, "
            Sql &= "c.c_comp4_ponderation as c4_pond, "
            Sql &= "c.c_description as c_description, "
            Sql &= "e.e_codecours as cours, "
            Sql &= "e.E_DESCRIPTION as description, "
            Sql &= "e.E_PONDERATION as ponderation, "
            Sql &= "decode(e.e_moyenne, null, '0',e.e_moyenne) as moyenne_classe, "
            Sql &= "pkg_sp.fc_niveau_a_afficher (n_note, " & DropDownList6.SelectedValue  & ", 'S') AS niveau_note "
            Sql &= "FROM sp_eleves_resultats er,  "
            Sql &= "sp_notes n,  "
            Sql &= "sp_evaluations e, gn_vw_enseignants gne, sp_classes c "
            Sql &= "WHERE er.m_ecole = '" & DropDownList3.SelectedValue & "' "
            Sql &= "AND er.m_membre_id = '" & Session("eleve_id") & "' "
            Sql &= "AND er.m_classe_code = e.e_classe_code "
            Sql &= "AND er.m_anneescolaire = " & DropDownList6.SelectedValue  & " "
            Sql &= "AND er.m_membre_id = n_membre_id "
            Sql &= "and e.e_evaluation_id = n_evaluation_id "
            Sql &= "and e.e_ecole = er.m_ecole " 'aj
            Sql &= "and e.e_codecours = er.m_codecours " 'aj
            Sql &= "and gne.m_membre_id = e.e_membre_id "
            Sql &= "and gne.m_ecole = er.m_ecole "
            'ajouter pour gérer les coeff
            Sql &= "and c.c_ecole = er.m_ecole "
            Sql &= "and c.c_anneescolaire = er.m_anneescolaire "
            Sql &= "and c.c_classe_code = e.e_classe_code "
            Sql &= "and c.c_enseignant_id = gne.m_membre_id "
            If DropDownList1.Items.Count > 0 Then 'secondaire
                Sql &= "and c.c_strand_code = 'xxx' " 'secondaire seulement
                Sql &= "and c.c_subject_code = 'x' " 'secondaire seulement
            Else 'elementaire
                Sql &= "and c.c_strand_code= er.m_strand_code " ' 3 caractères
                Sql &= "and e.e_strand_code = er.m_strand_code " 'aj
                'Sql &= "and c.c_subject_code= 'x' " '
            End If
            Sql &= "and c.c_periode_ref = " & p_ref & " "
            Sql &= "and er.m_periode_ref = c.c_periode_ref "
            'pour l'élémentaire a finir


            Sql &= " ORDER BY e.e_classe_code "


            Dim ds As DataSet = New OracleHelperAE().getDataSet(Sql.ToString)
            Dim classe_code_old As String = ""
            Dim classe_code_cour As String = ""
            Dim add_eval As Boolean = False
            Dim name As String = ""
            Dim annotation As String = ""
            'pour test
            nb_eval_tot = ds.Tables(0).Rows.Count
            If ds.Tables(0).Rows.Count > 0 Then
                Dim nb_eval As Integer
                eval_reel = 1 'ds.Tables(0).Rows.Count
                Dim nb_moyenne_classe As Integer = 0
                Dim moyenne_classe_cours As Integer = 0
                For nb_eval = 0 To ds.Tables(0).Rows.Count - 1
                    'on remplit la serie s1
                    If classe_code_old = "" Then
                        classe_code_old = ds.Tables(0).Rows(nb_eval)("classe_code")
                    End If

                    classe_code_cour = ds.Tables(0).Rows(nb_eval)("classe_code")

                    If classe_code_cour <> classe_code_old Then
                        add_eval = True
                        classe_code_old = classe_code_cour
                    Else
                        add_eval = False
                    End If

                    If add_eval Then
                        moyenne_eleve(ds, nb_eval, eval_reel, moyenne_classe_cours, nb_moyenne_classe, s1, s2, s3, s4, s5, s6, s7)
                    End If


                    If CType(ds.Tables(0).Rows(nb_eval)("note_sur_100"), Integer) > 0 Then
                        add_series(ds, nb_eval, eval_reel, moyenne_classe_cours, nb_moyenne_classe, s1, s2, s3, s4, s5, s6, s7)
                    End If
                    'fin cours

                Next

                'moyenne du dernier cour
                If eval_reel > 0 Then
                    'seulement au secondaire
                    If DropDownList1.Items.Count > 0 Then
                        moyenne_eleve(ds, nb_eval, eval_reel, moyenne_classe_cours, nb_moyenne_classe, s1, s2, s3, s4, s5, s6, s7)
                    End If
                End If

                SC.Add(s1) 'CC
                SC.Add(s2) 'HP
                SC.Add(s3) 'COMM
                SC.Add(s4) 'MA
                SC.Add(s5) 'note
                SC.Add(s6) 'ajustement
                SC.Add(s7) 'moyenne classe

            End If

            ' Set Different Colors for our Series
            SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49) 'vert
            SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0) 'jaune
            SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49) 'orange
            SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255) 'bleu
            'SC(4).DefaultElement.Color = Color.FromArgb(0, 10, 10) 'note sur 100
            'SC(5).DefaultElement.Color = Color.White 'ajustement
            'SC(6).DefaultElement.Color = Color.Black 'moyenne classe
            Return SC

        Catch ex As Exception

        End Try
        Return Nothing

    End Function

    Sub moyenne_eleve(ByVal ds As DataSet, ByRef nb_eval As Integer, ByRef eval_reel As Integer, ByRef moyenne_classe_cours As Integer, ByRef nb_moyenne_classe As Integer, ByVal s1 As Series, ByVal s2 As Series, ByVal s3 As Series, ByVal s4 As Series, ByVal s5 As Series, ByVal s6 As Series, ByVal s7 As Series)

        If CType(ds.Tables(0).Rows(nb_eval - 1)("n"), Integer) > 0 Then

            Dim element_name As String = ds.Tables(0).Rows(nb_eval - 1)("cours") & " Moyenne"

            Dim a As Annotation
            Dim annotation As String = "Moyenne " & ds.Tables(0).Rows(nb_eval - 1)("n") + ds.Tables(0).Rows(nb_eval - 1)("a").ToString
            a = New Annotation(annotation)
            a.Orientation = dotnetCHARTING.Orientation.Top

            Dim e1b As New Element()
            e1b.Name = element_name
            e1b.YValue = 0
            s1.Elements.Add(e1b)
            Dim e1c As New Element()
            e1c.Name = element_name
            e1c.YValue = 0
            s2.Elements.Add(e1c)
            Dim e1d As New Element()
            e1d.Name = element_name
            e1d.YValue = 0
            s3.Elements.Add(e1d)
            Dim e1e As New Element()
            e1e.Name = element_name
            e1e.YValue = 0
            s4.Elements.Add(e1e)

            Dim e6 As New Element()
            e6.Name = element_name
            e6.YValue = CType(ds.Tables(0).Rows(nb_eval - 1)("a"), Integer)
            e6.Annotation = a
            s6.Elements.Add(e6)
            eval_reel = eval_reel + 1

            'moyenne classe
            Dim e7 As New Element()
            e7.Name = element_name
            'e7.YValue = ds.Tables(0).Rows(nb_eval - 1)("moyenne_classe")
            e7.YValue = 0
            If moyenne_classe_cours > 0 And nb_moyenne_classe > 0 Then
                e7.YValue = moyenne_classe_cours / nb_moyenne_classe
            End If
            nb_moyenne_classe = 0
            moyenne_classe_cours = 0
            s7.Elements.Add(e7)

            Dim e5 As New Element()
            e5.Name = element_name
            e5.YValue = CType(ds.Tables(0).Rows(nb_eval - 1)("n"), Integer)
            e5.ToolTip = "Moyenne de la classe: " & CType(e7.YValue, Integer)
            s5.Elements.Add(e5)

        End If

    End Sub


    Sub add_series(ByVal ds As DataSet, ByRef nb_eval As Integer, ByRef eval_reel As Integer, ByRef moyenne_classe_cours As Integer, ByRef nb_moyenne_classe As Integer, ByVal s1 As Series, ByVal s2 As Series, ByVal s3 As Series, ByVal s4 As Series, ByVal s5 As Series, ByVal s6 As Series, ByVal s7 As Series)

        Dim name As String
        'pour le secondaire
        'Dim sql_note As String = "select distinct e_description,  matiere from sp_evaluations "
        'sql_note &= "LEFT JOIN GN_VW_CLASSES_SEC ON e_codecours = codecours "
        'sql_note &= "where e_evaluation_id = '" & ds.Tables(0).Rows(nb_eval)("ID") & "'"

        'ajout des annotations
        'Dim ds2 As DataSet = New OracleHelperAE().getDataSet(sql_note.ToString)
        Dim a As Annotation
        Dim annotation As String = ""
        'If ds2.Tables(0).Rows.Count > 0 Then
        '   annotation = ds2.Tables(0).Rows(0)("matiere").ToString
        annotation = ds.Tables(0).Rows(nb_eval)("c_description").ToString
        annotation += ControlChars.Lf & ds.Tables(0).Rows(nb_eval)("enseignant").ToString
        annotation += ControlChars.Lf & ControlChars.Lf & ds.Tables(0).Rows(nb_eval)("description").ToString
        annotation += ControlChars.Lf & "Pondération: " & ds.Tables(0).Rows(nb_eval)("ponderation").ToString
        annotation += ControlChars.Lf & "Note: " & ds.Tables(0).Rows(nb_eval)("note_sur_100").ToString
        annotation += " / " & ds.Tables(0).Rows(nb_eval)("niveau_note").ToString
        a = New Annotation(annotation)
        'a.ToolTip = "on peut ajouter d'autres infos icite"
        a.Size = New System.Drawing.Size(130, 100)
        a.Orientation = dotnetCHARTING.Orientation.Top
        'Else
        'a = New Annotation("")
        'a.Orientation = dotnetCHARTING.Orientation.Top
        'End If


        'Name = ds.Tables(0).Rows(nb_eval)("cours") & " E" & eval_reel & ControlChars.Lf & ds.Tables(0).Rows(nb_eval)("description")
        'old
        name = ds.Tables(0).Rows(nb_eval)("cours") & " E" & eval_reel


        'coef et dom évalué:
        Dim c1_pond As Integer = CType(ds.Tables(0).Rows(nb_eval)("c1_pond"), Integer)
        Dim c2_pond As Integer = CType(ds.Tables(0).Rows(nb_eval)("c2_pond"), Integer)
        Dim c3_pond As Integer = CType(ds.Tables(0).Rows(nb_eval)("c3_pond"), Integer)
        Dim c4_pond As Integer = CType(ds.Tables(0).Rows(nb_eval)("c4_pond"), Integer)

        Dim nb_c As Integer = 0
        If ds.Tables(0).Rows(nb_eval)("c1_utilisation") = "O" Then
            nb_c += 1
        Else
            c1_pond = 0
        End If
        If ds.Tables(0).Rows(nb_eval)("c2_utilisation") = "O" Then
            nb_c += 1
        Else
            c2_pond = 0
        End If
        If ds.Tables(0).Rows(nb_eval)("c3_utilisation") = "O" Then
            nb_c += 1
        Else
            c3_pond = 0
        End If
        If ds.Tables(0).Rows(nb_eval)("c4_utilisation") = "O" Then
            nb_c += 1
        Else
            c4_pond = 0
        End If





        Dim tot_pond As Integer = c1_pond + c2_pond + c3_pond + c4_pond

        Dim e1 As New Element()
        e1.Name = name
        e1.YValue = CType(ds.Tables(0).Rows(nb_eval)("CC"), Integer) * (c1_pond * 100 / tot_pond) / 100
        s1.Elements.Add(e1)

        Dim e2 As New Element()
        e2.Name = name
        e2.YValue = CType(ds.Tables(0).Rows(nb_eval)("HP"), Integer) * (c2_pond * 100 / tot_pond) / 100
        s2.Elements.Add(e2)

        Dim e3 As New Element()
        e3.Name = name
        e3.YValue = CType(ds.Tables(0).Rows(nb_eval)("CO"), Integer) * (c3_pond * 100 / tot_pond) / 100
        s3.Elements.Add(e3)

        Dim e4 As New Element()
        e4.Name = name
        e4.YValue = CType(ds.Tables(0).Rows(nb_eval)("MA"), Integer) * (c4_pond * 100 / tot_pond) / 100
        'ajout de l'anotation sur la dernière série
        e4.Annotation = a
        e4.ToolTip = "Moyenne de la classe: " & CType(ds.Tables(0).Rows(nb_eval)("moyenne_classe"), Integer)
        s4.Elements.Add(e4)

        'a mettre a part car represente une serie differente = note + ajustement
        'mettre i dans un tableau a chaque fois que cours change et en dehors.

        'moyenne classe
        Dim e7 As New Element()
        e7.Name = name
        e7.YValue = ds.Tables(0).Rows(nb_eval)("moyenne_classe")
        s7.Elements.Add(e7)

        If e7.YValue > 0 Then 'Tester la pondération
            moyenne_classe_cours += e7.YValue * ds.Tables(0).Rows(nb_eval)("ponderation")
            'moyenne_classe_cours += e7.YValue
            nb_moyenne_classe += ds.Tables(0).Rows(nb_eval)("ponderation")
            'nb_moyenne_classe += 1
        End If


        eval_reel = eval_reel + 1
    End Sub

    Protected Sub DropDownList5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList5.SelectedIndexChanged
        update_req_rg(Session("eleve_id"))
        RadGrid1.Rebind()
        chart.Visible = False
    End Sub
	Protected Sub DropDownList6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList6.SelectedIndexChanged
		Load_data()
        update_req_rg(Session("eleve_id"))
        RadGrid1.Rebind()
        chart.Visible = False
    End Sub
End Class


