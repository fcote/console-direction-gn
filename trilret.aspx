<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="trilret.aspx.vb" Inherits="trilret" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label ID="Label1" runat="server" Font-Names="Berlin Sans FB Demi" Font-Size="XX-Large"
Text="Trillium: renseignements enseignants" Width="100%"></asp:Label><br />

<br />

<asp:DropDownList ID="DropDownList1" runat="server"  DataTextField="school_name" DataValueField="school_code"  DataSourceID="SqlDataSource8" AutoPostBack="True" Visible="true">
</asp:DropDownList>

<asp:SqlDataSource ID="SqlDataSource8" runat="server" 
ConnectionString="<%$ ConnectionStrings:CS_Trillium %>" 
ProviderName="<%$ ConnectionStrings:CS_Trillium.ProviderName %>" 
SelectCommand=" 
SELECT DISTINCT school_name, school_code FROM schools ORDER BY school_name ">
</asp:SqlDataSource>


<asp:DropDownList ID="DropDownList2" runat="server"  DataTextField="nom" DataValueField="membre_id"  DataSourceID="SqlDataSource2" AutoPostBack="True" Visible="true">
</asp:DropDownList>

<asp:SqlDataSource ID="SqlDataSource2" runat="server" 
ConnectionString="<%$ ConnectionStrings:CS_Trillium %>" 
ProviderName="<%$ ConnectionStrings:CS_Trillium.ProviderName %>" 
SelectCommand=" 
SELECT DISTINCT membre_id, m_nom || ' ' || m_prenom as nom FROM gn.gn_vw_eleves_ele
where ecole = :school_code 
union
SELECT DISTINCT membre_id, m_nom || ' ' || m_prenom as nom FROM gn.gn_vw_eleves_sec
where ecole = :school_code ORDER BY nom 
">
<SelectParameters>
<asp:ControlParameter ControlID="DropDownList1" Type="String" Name="school_code"  DefaultValue="000" />
</SelectParameters>
</asp:SqlDataSource>


<telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource1" GridLines="None" AllowFilteringByColumn="True" AllowSorting="True" PageSize="15" ShowGroupPanel="True" Skin="Office2007">
<MasterTableView DataSourceID="SqlDataSource1" GroupsDefaultExpanded="False" 
AutoGenerateColumns="True">
               
</MasterTableView>
<ClientSettings AllowDragToGroup="True">
</ClientSettings>
</telerik:RadGrid>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CS_Trillium %>"
ProviderName="<%$ ConnectionStrings:CS_Trillium.ProviderName %>" 
SelectCommand="
select pa.SCHOOL_CODE,
s.school_name
,pa.SCHOOL_YEAR         
,pa.SCHOOL_YEAR_TRACK   
,pa.PERSON_ID           
,to_char(pa.CALENDAR_DATE,'ddmmyyyy') CAL_DATE
,pa.ATTENDANCE_FROM_TIME
,pa.ATTENDANCE_TO_TIME  
,pa.SCHOOL_PERIOD       
,pa.SCHOOL_TIMELINE_CODE
,pa.REASON_CODE         
,pa.ATTENDANCE_CODE     
,pa.GUARDIAN_CONTACT_FLAG
,pa.NOTE_RECEIVED_FLAG   
,pa.EXPLANATION          
,pa.CLASS_CODE
FROM period_attendance pa, schools s
WHERE pa.school_year                 = '20122013'
AND s.school_code = pa.school_code
AND person_id=:person_id
            
">

<SelectParameters>
<asp:ControlParameter ControlID="DropDownList2" Type="String" Name="person_id"  DefaultValue="000" />
</SelectParameters>
</asp:SqlDataSource>

</asp:Content>
