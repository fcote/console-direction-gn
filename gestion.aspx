﻿<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="gestion.aspx.vb" Inherits="gestion" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
    <div style="font-family: 'Berlin Sans FB Demi'; font-size: x-large">Gestion des enseignants</div>
    </div>
    <div align="left">
<%--     <% If Session("role") = "administrateur" Then%>
        <asp:DropDownList ID="ddl_dir" runat="server" 
            DataSourceID="SqlDataSource5" DataTextField="NOM" 
            DataValueField="M_MEMBRE_ID" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand=" 
            SELECT M_NOM || ', ' || M_PRENOM || ' - ' || M_ECOLE AS NOM, M_MEMBRE_ID 
            FROM GN_VW_ENSEIGNANTS WHERE 
            TYPE_PERSON = 'D' ORDER BY M_NOM, M_PRENOM ">
        </asp:SqlDataSource><br/>
     
     <% End If%>--%>
        <asp:DropDownList ID="DropDownList1" runat="server" 
            DataSourceID="SqlDataSource1" DataTextField="NOM" 
            DataValueField="M_MEMBRE_ID" >
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" >
       <%--<SelectParameters>
            <asp:SessionParameter Name="ID_Direction" SessionField="ID_Direction" Type="String" DefaultValue="" />
        </SelectParameters>--%>
        </asp:SqlDataSource>
        
        <asp:DropDownList ID="DropDownList3" runat="server" 
            DataSourceID="SqlDataSource4" DataTextField="NOM_ECOLE" 
            DataValueField="M_ECOLE">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" >
<%--            <SelectParameters>
                <asp:SessionParameter Name="ID_Direction" SessionField="ID_Direction" Type="String" DefaultValue="" />
            </SelectParameters>--%>
        </asp:SqlDataSource>
        <asp:Button ID="Button1" runat="server" Text="Ajouter" /><br/>
       
    <br />
    
        <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource2" 
            GridLines="None" AutoGenerateColumns="False" Skin="Hay">
            <MasterTableView DataSourceID="SqlDataSource2">
                <Columns>
                    <telerik:GridBoundColumn DataField="NOM" HeaderText="NOM" SortExpression="NOM" UniqueName="NOM">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="sp_ecole" HeaderText="ECOLE" SortExpression="sp_ecole" UniqueName="sp_ecole">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" 
            >
<%--<SelectParameters>
                <asp:SessionParameter Name="ID_Direction" SessionField="ID_Direction" Type="String" DefaultValue="" />
            </SelectParameters>--%>
</asp:SqlDataSource>
    
    
     <br/>
        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource3"
                DataTextField="NOM" DataValueField="ens_ecole_id">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:CS_GN %>"
                ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" >
<%--                <SelectParameters>
                    <asp:SessionParameter Name="ID_Direction" SessionField="ID_Direction" Type="String" DefaultValue="" />
                </SelectParameters>--%>
            </asp:SqlDataSource>
&nbsp;<asp:Button ID="Button2" runat="server" Text="Supprimer" />
    </div>
</asp:content>