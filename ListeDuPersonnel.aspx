﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ListeDuPersonnel.aspx.vb" Inherits="ListeDuPersonnel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:DropDownList ID="DropDownList1" runat="server" 
            DataTextField="service" DataValueField="service" AutoPostBack="True">
            <asp:ListItem Value="bda">Bureau des affaires</asp:ListItem>
            <asp:ListItem Value="spaie">Service de la paye</asp:ListItem>
            <asp:ListItem Value="sdp">Service de la plannification</asp:ListItem>
            <asp:ListItem Value="ddle">Service des relations corporative</asp:ListItem>
            <asp:ListItem Value="srf">Service des ressources finançières</asp:ListItem>
            <asp:ListItem Value="srh">Service des ressources humaines</asp:ListItem>
            <asp:ListItem Selected="True" Value="sri">Service des ressources informatiques</asp:ListItem>
            <asp:ListItem Value="srm">Service des ressources matérielles</asp:ListItem>
            <asp:ListItem Value="st">Service du transport</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Button ID="btnInitBdd" runat="server" 
            Text="Afficher liste" style="height: 26px" />
    
    </div>
    </form>
</body>
</html>
