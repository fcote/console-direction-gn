﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="radgrid.aspx.vb" Inherits="radgrid" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" 
            AllowPaging="True" AllowSorting="True" AutoGenerateEditColumn="True" 
            DataSourceID="SqlDataSource2" GridLines="None" ShowGroupPanel="True">
<MasterTableView autogeneratecolumns="False" datakeynames="id" 
                datasourceid="SqlDataSource2">
<CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn DataField="id" DataType="System.Int32" HeaderText="id" 
            ReadOnly="True" SortExpression="id" UniqueName="id">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="login" HeaderText="login" 
            SortExpression="login" UniqueName="login">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="domaine" HeaderText="domaine" 
            SortExpression="domaine" UniqueName="domaine">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="description" HeaderText="description" 
            SortExpression="description" UniqueName="description">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="position" DataType="System.Int32" 
            HeaderText="position" SortExpression="position" UniqueName="position">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="parent" DataType="System.Int32" 
            HeaderText="parent" SortExpression="parent" UniqueName="parent">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="postetel" HeaderText="postetel" 
            SortExpression="postetel" UniqueName="postetel">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="service" HeaderText="service" 
            SortExpression="service" UniqueName="service">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="actif" DataType="System.Int32" 
            HeaderText="actif" SortExpression="actif" UniqueName="actif">
        </telerik:GridBoundColumn>
    </Columns>
</MasterTableView>
            <ClientSettings AllowDragToGroup="True">
            </ClientSettings>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:OrganigrammeConnectionString %>" 
            SelectCommand="SELECT * FROM [Informations]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" 
            SelectCommand="SELECT * FROM &quot;AAS&quot;"></asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
