﻿Imports System.Data
Imports Telerik.Web.UI

Partial Class Main
    Inherits System.Web.UI.MasterPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim urlAbsolue As String = Request.Url.ToString()
        If Not urlAbsolue.Contains("index.aspx") Then
            If Session("role") <> "administrateur" And Session("role") <> "directeur" And Session("role") <> "enseignant" Then
                Response.Redirect("~/index.aspx")
            End If
        End If
    End Sub
End Class

