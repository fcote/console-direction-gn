﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ConfigurationAssiduite.aspx.vb" Inherits="ConfigurationAssiduite" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" >
    <title></title>

    <style type="text/css">
    <!--
    @media print
    {
     .noprint {display:none;}
    }
    -->
    </style>
    <script>
        function printpage() {

            var prtContent = document.getElementById("print");
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


            //window.print();
        }
    </script>
</head>
<body>
<form id="form1" runat="server">

    <div class="noprint">

        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
         <script type="text/javascript">
             function onRequestStart(sender, args) {
                 if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                        args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                     args.set_enableAjax(false);
                 }
             }
        </script>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
           <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlEcole" >
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlEcole" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="ddlEleve" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="RadListView1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlEleve" >
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlEleve" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="RadListView1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            
            
              <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
           
         
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default" />





     <div style="width:1280px;">
        <center style="font-size:x-large">Configuration de l'assiduité</center>

        <asp:Label ID="lblUsagerInfo" runat="server" Text="" ></asp:Label>
        <br/><br/>

   

        <br/>
        École:
        <asp:DropDownList ID="ddlEcole" runat="server"  DataTextField="SCHOOL_NAME" DataValueField="SCHOOL_CODE" AutoPostBack="True" DataSourceID="SqlDataSource2"></asp:DropDownList>

        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
            SelectCommand=" 
                SELECT '000' as SCHOOL_CODE, 'Veuillez choisir une école' as SCHOOL_NAME, '000' as SCHOOL_YEAR_TRACK, '000' as PALIER from dual
                UNION ALL
                SELECT DISTINCT S.SCHOOL_CODE, SCHOOL_NAME, SCHOOL_YEAR_TRACK, 
                DECODE(SCHOOL_YEAR_TRACK,'7-8', 'I','Elem', 'I','Élem', 'I', 'Sec', 'S', 'SOIR', 'S', 'E') AS PALIER FROM SCHOOLS S, SCHOOL_YEAR_TRACKS Y, GN_VW_ENSEIGNANTS WHERE ACTIVE_FLAG = 'x' 
                AND S.SCHOOL_CODE = M_ECOLE 
                AND M_MEMBRE_ID = :MEMBRE_ID AND TYPE_PERSON in ('A','D') 
                AND S.SCHOOL_CODE = Y.SCHOOL_CODE 
                AND Y.SCHOOL_YEAR = FC_VAL_PARAMETRES('ANNEESCOLAIRE') 
                ORDER BY SCHOOL_NAME 
                ">
            <SelectParameters>
                <asp:SessionParameter Name="MEMBRE_ID" Type="String" SessionField="ID_Direction"  />
            </SelectParameters>
        </asp:SqlDataSource>


        <br/>
    
        <telerik:RadGrid ID="RadGrid1" DataSourceID="SqlDataSource4" runat="server" CellSpacing="0" 
                     GridLines="None" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" 
                    AutoGenerateEditColumn="True" AllowAutomaticUpdates="True" AutoGenerateColumns="false"  >
            <MasterTableView DataSourceID="SqlDataSource4" AutoGenerateColumns="False" DataKeyNames="ID">
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="ID" HeaderText="ID" ReadOnly=true FilterControlAltText="Filter ID column" SortExpression="ID" UniqueName="ID"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SCHOOL_CODE" ReadOnly=true HeaderText="SCHOOL_CODE" FilterControlAltText="Filter SCHOOL_CODE column" SortExpression="SCHOOL_CODE" UniqueName="SCHOOL_CODE"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PERIODE" ReadOnly=true HeaderText="PERIODE" FilterControlAltText="Filter PERIODE column" SortExpression="PERIODE" UniqueName="PERIODE"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RETARD" HeaderText="Nombre de minutes avant retard" FilterControlAltText="Filter RETARD column" SortExpression="RETARD" UniqueName="RETARD"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ABSENCE" HeaderText="Nombre de minutes avant absence" FilterControlAltText="Filter ABSENCE column" SortExpression="ABSENCE" UniqueName="ABSENCE"></telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False"></FilterMenu>
        </telerik:RadGrid>



        <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
            UpdateCommand=" UPDATE GN.SP_ASSIDUITE_CONFIGURATION SET ABSENCE=:ABSENCE , RETARD=:RETARD  WHERE ID = :ID "
            SelectCommand=" SELECT ID, SCHOOL_CODE, PERIODE, RETARD, ABSENCE FROM GN.SP_ASSIDUITE_CONFIGURATION WHERE SCHOOL_CODE = :SCHOOL_CODE ORDER BY PERIODE " >
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlEcole" Type="String" Name="SCHOOL_CODE"  DefaultValue="000" />
            </SelectParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="RadGrid1" Name="ID" Type="String"  />
                <asp:ControlParameter ControlID="RadGrid1" Name="RETARD" Type="String"  />
                <asp:ControlParameter ControlID="RadGrid1" Name="ABSENCE" Type="String"  />
            </UpdateParameters>
        </asp:SqlDataSource>




        <br/>
        Élèves:
        <asp:DropDownList ID="ddlEleve" runat="server"  DataTextField="M_NOM" DataValueField="PERSON_ID" AutoPostBack="True" DataSourceID="SqlDataSource1"></asp:DropDownList>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
            SelectCommand=" 
                select p.legal_surname || ' ' ||  p.legal_first_name m_nom, p.person_id 
                -- p.student_no CODE_BAR
                FROM trill02.student_registrations s,persons p where s.school_year = gn.fc_val_parametres ('ANNEESCOLAIRE')
                and s.school_code = :SCHOOL_CODE
                and p.person_id    = s.person_id order by p.legal_surname
            ">
            <SelectParameters>
            <asp:ControlParameter ControlID="ddlEcole" Type="String" Name="SCHOOL_CODE"  DefaultValue="000" />
             </SelectParameters>
        </asp:SqlDataSource>

    </div>



    <div class="print" id="print" style="float: left;">
    
        <telerik:RadListView ID="RadListView1" DataSourceID="SqlDataSource3" runat="server"
            ItemPlaceholderID="ProductsContainer">
            <LayoutTemplate>
                <fieldset style="float: left; width: 200px; ">
                    <asp:PlaceHolder ID="ProductsContainer" runat="server"></asp:PlaceHolder>
                </fieldset>
            </LayoutTemplate>
            <ItemTemplate>
                <center>
                <%--<fieldset style="float: left; width: 90%; ">--%>
                    <center> <%# Eval("M_NOM")%>  <%# Eval("M_PRENOM")%><br/>
                    <telerik:RadBarcode ID="RadBarcode1" runat="server" Type="Code11" Text='<%#Eval("STUDENT_NO")%>'
                        ShowText="true" ShortLinesLengthPercentage="80" Width="150px" Height="60px" Style="font-size: 15px;">
                    </telerik:RadBarcode></center>
                <%--</fieldset>--%>
                </center>
            </ItemTemplate>
        </telerik:RadListView>
    </div>

    <div class="noprint">
    
    <br/><br/><br/><br/><br/><br/><input type="button" value="Imprimer le code bar" onclick="printpage()"  />

    </div>

    <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
        SelectCommand=" SELECT student_no, legal_surname m_nom, legal_first_name m_prenom FROM persons WHERE person_id = :PERSON_ID " >
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlEleve" Type="String" Name="PERSON_ID"  DefaultValue="000" />
        </SelectParameters>
    </asp:SqlDataSource>

    <br />
    </div>
</form>
</body>
</html>
