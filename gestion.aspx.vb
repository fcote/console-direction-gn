﻿Imports Oracle.DataAccess.Client
Imports System.Data

Partial Class gestion
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("role") <> "administrateur" And Session("role") <> "directeur" Then
            Response.Redirect("~/index.aspx")
        End If

        'If Not Page.IsPostBack Then
        '    If Session("role") = "administrateur" Then
        '        Session("ID_Direction") = ddl_dir.SelectedValue
        '    End If
        'End If

        Dim var As String = ""

        '" & session("ID_Direction") & "'
        var &= " SELECT DISTINCT GNE1.M_NOM || ', ' || GNE1.M_PRENOM AS NOM, GNE1.M_MEMBRE_ID "
        var &= " FROM GN_VW_ENSEIGNANTS GNE1"
        var &= " LEFT OUTER JOIN GN_VW_ENSEIGNANTS GNE2 "
        var &= " ON GNE2.M_MEMBRE_ID = '" & Session("ID_Direction") & "'"
        var &= " WHERE (GNE1.TYPE_PERSON = 'E' OR GNE1.TYPE_PERSON = 'A')"
        var &= " AND GNE1.M_ECOLE = GNE2.M_ECOLE"
        var &= " ORDER BY NOM"
        SqlDataSource1.SelectCommand = var

        var = " SELECT DISTINCT gne2.m_ecole, gne2.nom_ecole"
        var &= " FROM GN_VW_ENSEIGNANTS GNE2 "
        var &= " WHERE GNE2.M_MEMBRE_ID = '" & Session("ID_Direction") & "'"
        SqlDataSource4.SelectCommand = var

        var = " select distinct GNE1.M_NOM || ', ' || GNE1.M_PRENOM AS NOM, sp.ecole as sp_ecole"
        var &= " from SP_ACCES_CONSOLE sp, GN_VW_ENSEIGNANTS gne1"
        var &= " where(gne1.m_membre_id = sp.login)"
        var &= " and gne1.anneescolaire = fc_val_parametres('ANNEESCOLAIRE')"
        var &= " and sp.ecole in (select m_ecole from GN_VW_ENSEIGNANTS where m_membre_id = '" & Session("ID_Direction") & "') order by nom"
        SqlDataSource2.SelectCommand = var
        RadGrid1.Rebind()

        var = " select distinct GNE1.M_NOM || ', ' || GNE1.M_PRENOM || ' - ' || sp.ECOLE AS NOM, login || sp.ECOLE as ens_ecole_id"
        var &= " from SP_ACCES_CONSOLE sp, GN_VW_ENSEIGNANTS gne1"
        var &= " where(gne1.m_membre_id = sp.login)"
        'var &= " --and gne1.m_ecole = sp.ecole "
        var &= " and gne1.anneescolaire = fc_val_parametres('ANNEESCOLAIRE')"
        var &= " and sp.ecole in (select m_ecole from GN_VW_ENSEIGNANTS where m_membre_id = '" & Session("ID_Direction") & "')"
        var &= " order by nom"
        SqlDataSource3.SelectCommand = var

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim enseignant_id As String = DropDownList1.SelectedValue
        Dim ecole_id As String = DropDownList3.SelectedValue
        Dim sql As String = ""

        Try
            sql = "insert into GN.SP_ACCES_CONSOLE (login, ecole) values ('" & enseignant_id & "', '" & ecole_id & "')"
            Dim ds As DataSet = New OracleHelperAE().getDataSet(sql.ToString)
        Catch ex As Exception
            'Response.Write("Cette enseignant existe déjà")
        End Try

        RadGrid1.Rebind()
        DropDownList2.DataBind()

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim enseignant_ecole_id As String = DropDownList2.SelectedValue
        Dim sql As String = ""
        Try
            sql = "delete from SP_ACCES_CONSOLE where (login || ecole) = '" & enseignant_ecole_id & "'"
            Dim ds As DataSet = New OracleHelperAE().getDataSet(Sql.ToString)
        Catch ex As Exception
            'Response.Write("Cette enseignant existe déjà")
        End Try

        RadGrid1.Rebind()
        DropDownList2.DataBind()

    End Sub

    'Protected Sub ddl_dir_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_dir.SelectedIndexChanged
    '    Session("ID_Direction") = ddl_dir.SelectedValue
    'End Sub
End Class
