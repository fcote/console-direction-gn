﻿// Fichier JScript

//
        
    var warningshown = false;
    var posted_back = false;
    var gtimer;
    var hold_timer = false;
        
    function GetRadWindow()
    {
        var oWindow = null;
        if (window.radWindow) oWindow = window.radWindow;//Will work in Moz in all cases, including clasic dialog
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;//IE (and Moz az well)
        return oWindow;
    }  
    /*
    function GetRadWindowManager()
    {
        return $find('<%=RadWindowManager1.ClientID%>');   
    }
    */
    function Timeout(arg)
    {
        
        if (arg)
        {
            ResetTimer();
        }
        else
        {
            logout();
        }
    }
    
    function ResetTimer()
    {
       PageMethods.ResetTimer(ResetTimerSuccess, ResetTimerError);
    }
    
    function ResetTimerSuccess()
    {
        timer = start;
        hold_timer = false;
        warningshown = false;
    }
    
    function ResetTimerError(error)
    {
        alert(error.get_message());
    }

    function unGrayOut()
    {
        var gout = document.getElementById('darkenScreenObject');
        
        if (gout){
            gout.style.display = 'none';
            document.body.style.cursor = "default";
        }
    }

     function grayOut(vis, options, msg) {  
            // Pass true to gray out screen, false to ungray  
            // options are optional.  This is a JSON object with the following (optional) properties  
            // opacity:0-100         
            // Lower number = less grayout higher = more of a blackout   
            // zindex: #             
            // HTML elements with a higher zindex appear on top of the gray out  
            // bgcolor: (#xxxxxx)    
            // Standard RGB Hex color code  
            // grayOut(true, {'zindex':'50', 'bgcolor':'#0000FF', 'opacity':'70'});  
            // Because options is JSON opacity/zindex/bgcolor are all optional and can appear  
            // in any order.  Pass only the properties you need to set.  
            document.body.style.cursor = "wait";
            var options = options || {};   
            //var zindex = options.zindex || 1000; 
            var zindex = 1000000;
            var opacity = options.opacity || 50;  
            var opaque = (opacity / 100);  
            var bgcolor = options.bgcolor || '#B0C4DE';  
            var dark=document.getElementById('darkenScreenObject');  
            
            if (!dark) {    
                // The dark layer doesn't exist, it's never been created.  So we'll    
                // create it here and apply some basic styles.    
                // If you are getting errors in IE see: http://support.microsoft.com/default.aspx/kb/927917   
                var tbody = document.getElementsByTagName("body")[0];    
                var tnode = document.createElement('div');           
                // Create the layer.        
                tnode.style.position='absolute';                 
                // Position absolutely        
                tnode.style.top='8px'; // '0px'                          
                // In the top        
                tnode.style.left='8px'; // '0px'                          
                // Left corner of the page        
                tnode.style.overflow='hidden';                   
                // Try to avoid making scroll bars                    
                tnode.style.display='none';        
                // Start out Hidden        
                tnode.id='darkenScreenObject';                  
                // Name it so we can find it later    
                tbody.appendChild(tnode);                            
                // Add it to the web page    
                dark=document.getElementById('darkenScreenObject'); 
           
                // Get the object.  
                }  
                
                if (vis) {    
                    // Calculate the page width and height     
                    if( document.body && ( document.body.scrollWidth || document.body.scrollHeight ) ) 
                    {        
                        var pageWidth = document.body.scrollWidth+'px';        
                        var pageHeight = document.body.scrollHeight+'px';    
                    } else if( document.body.offsetWidth ) {      
                        var pageWidth = document.body.offsetWidth+'px';      
                        var pageHeight = document.body.offsetHeight+'px';    
                    } else {       
                        var pageWidth='100%';       
                        var pageHeight='100%';    
                    }       
                    //set the shader to cover the entire page and make it visible.    
                    dark.style.opacity=opaque;                          
                    dark.style.MozOpacity=opaque;                       
                    dark.style.filter='alpha(opacity='+opacity+')';     
                    dark.style.zIndex=zindex;            
                    dark.style.backgroundColor=bgcolor;      
                    dark.style.width= pageWidth;    
                    dark.style.height= pageHeight;    
                    
                    var wind = document.createElement('div');
                    wind.style.position = 'absolute';
//                    wind.style.height = "100px";
//                    wind.style.width = "100px";
                    
                    wind.style.top = ((parseInt(screen.height) / 2) - 20) + "px";
                    //wind.style.left = ((parseInt(dark.style.width) / 2) - 20) + "px";
                    wind.style.left = ((parseInt(dark.style.width) / 2) -50 ) + "px";
                    wind.style.backgroundColor='transparent';
                     // Set alignment
                    wind.style.align = 'center';     
                    
                    dark.appendChild(wind);
                    
                    
                    
                     var sp = document.getElementById('spanProcessing');
                   
                   if (!sp){
                    sp = document.createElement('span');
                    sp.id='spanProcessing';
                    wind.appendChild(sp);
                   }
                    
                    sp.innerHTML = '';
                    var img = document.getElementById('imgProcessing');
                    if (!img){
                      //  // Add processing image
                      //  img = document.createElement('img');
                      //  img.id='imgProcessing';
                      //  img.src = img_path;
                      //  // Add to div
                      //  wind.appendChild(img);
                        
                         sp.innerHTML+='<center><img src=' + img_path + '>&nbsp;&nbsp;&nbsp;</center>';
                   }

 
                    //Maintenant le texte
                    if (msg)
                    {
                        sp.innerHTML+='<br/>' + msg ;
                    }
                    else
                    {
                        sp.innerHTML+='<br/><center>Chargement en cours...</center>';
                    }

                   
                                       
                    dark.style.display='block';      
                    wind.style.display='block';
                    
                                         
                } else {     
                    dark.style.display='none';  
                }
           }
           
                
    function logout()
    {
        __doPostBack('LOGOUT', true);
        posted_back = true;
    }
    
    function blockPrompt(text, mozEvent, oWidth, oHeight, callerObj, oTitle) 
    { 
        
        var ev = mozEvent ? mozEvent : window.event; //Moz support requires passing the event argument manually 
        //Cancel the event 
        ev.cancelBubble = true; 
        ev.returnValue = false; 
        if (ev.stopPropagation) ev.stopPropagation(); 
        if (ev.preventDefault) ev.preventDefault(); 
           
        //Determine who is the caller 
        var callerObj = ev.srcElement ? ev.srcElement : ev.target; 

        //Call the original radconfirm and pass it all necessary parameters 
        if (callerObj) 
        { 
            //Show the confirm, then when it is closing, if returned value was true, automatically call the caller's click method again. 
            var callBackFn = function (arg) 
            { 
                if (!arg)
                {
                    return false;
                }
                
                if (arg.toLowerCase() == 'supprimer') 
                { 
                    callerObj["onclick"] = ""; 
                    if (callerObj.click) callerObj.click(); //Works fine every time in IE, but does not work for links in Moz 
                    else if (callerObj.tagName == "A") //We assume it is a link button! 
                    { 
                        try 
                        { 
                            eval(callerObj.href) 
                        } 
                        catch(e){} 
                    } 
                } 
            } 

            radprompt(text, callBackFn, oWidth, oHeight, callerObj, oTitle, ''); 
        } 
        return false; 
    } 
    
    function promptDelete(type, name, event)
    {
        
        var str_type
        var str_type_part2
        
        if (type == "E")
        {
            str_type = "l\'évaluation";
            str_type_part2 = "";
        }
        else if (type == "L")
        {
            str_type = "la leçon";
            str_type_part2 = " ainsi que les pièces qui y sont jointes ";
            
        }
        else if (type=="ATT")
        {
            str_type = "la pièce jointe"
            str_type_part2 = "";
            
        }
        
        return blockPrompt('<b><font color=red size="16pt">Attention!</font></b><br/><br/>Êtes-vous certain de vouloir supprimer ' + str_type + ' <b>' + name + '</b>' + str_type_part2 + '?<br/>Si oui, veuillez taper "supprimer" dans la boîte ci-dessous.', event, 330, 100,'','Confirmation');
    
    }
    
    


