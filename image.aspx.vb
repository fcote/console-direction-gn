﻿Imports System.IO
Imports System.Configuration
Imports System.Data

Partial Class Classe_image
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, _
       ByVal e As System.EventArgs) Handles MyBase.Load
        Dim fileName As String = Request.QueryString("src")
        If fileName = "" Then
            fileName = "ndisponible.gif"
        Else
            If fileName.Length > 4 Then
                fileName = fileName.Substring(0, fileName.Length - 4)
                Dim Sql As String = " SELECT NUMERO_ELEVE FROM GN_VW_ELEVE_EED WHERE membre_id = '" & fileName & "'"
                Dim ds As DataSet = New OracleHelperAE().getDataSet(Sql.ToString)
                fileName = ds.Tables(0).Rows(0)("NUMERO_ELEVE") & ".jpg"
            End If
        End If

        'System.Diagnostics.Debug.Print(fileName)

        'in your web.config file be sure to have an 
        'appsetting similar to the following.
        'the "d:\assets\" is where the secured images are 
        'being stored inaccessible from a browser directly
        '<appSettings>
        '<add key="PathImages" value="mon_repertoire" />    
        '</appSettings>
        Dim apr As AppSettingsReader = New AppSettingsReader()
        Dim path As String = apr.GetValue("PathImagesLocal", System.Type.GetType("System.String"))
        Dim fileStream As FileStream
        Dim fileSize As Long

        fileName = verifPhoto(path & fileName, path)

        Try
            fileStream = New FileStream(fileName, FileMode.Open, FileAccess.Read)
        Catch ex As FileNotFoundException
            Return
        Catch ex As DirectoryNotFoundException
            Return
        Catch ex As System.IO.DriveNotFoundException
            Return
        Catch ex As System.IO.IOException
            Return
        End Try

        fileSize = fileStream.Length

        Dim Buffer(CInt(fileSize)) As Byte
        fileStream.Read(Buffer, 0, CInt(fileSize))
        fileStream.Close()
        'Response.Write(path & " - " & fileName)
        Response.BinaryWrite(Buffer)
    End Sub

    Public Function verifPhoto(ByVal chemin As String, ByVal pathPhotos As String) As String
        Dim affichage As String
        'Dim lefichier As New FileInfo(chemin)
        'If lefichier.Exists Then
        If File.Exists(chemin) Then
            affichage = chemin
            'Response.Write("Trouvé " & chemin)
        Else
            affichage = pathPhotos & "ndisponible.jpg"
            'Response.Write("Non-trouvé." & chemin)
        End If

        Return affichage
    End Function

End Class
