﻿<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="gn.aspx.vb" Inherits="gn" EnableEventValidation="false" MaintainScrollPositionOnPostback="true"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Height="75px"    
    Width="75px" Transparency="50">     
    <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading2.gif") %>'    
        style="border: 0px;" />    
</telerik:RadAjaxLoadingPanel>   

    <br/>
        <span lang="fr-ca" style="font-family: 'Berlin Sans FB Demi'; font-size: xx-large">Notes par cours</span>
    <br />
    <div align="left">
    
        <br />
        <asp:DropDownList ID="DropDownList4" runat="server" DataSourceID="SqlDataSource7"
            DataTextField="ECOLE" DataValueField="ECOLE" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:CS_GN %>"
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="">
            <SelectParameters>
                <asp:SessionParameter Name="ID_USER" SessionField="ID_USER" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:DropDownList ID="DropDownList1" runat="server" 
            DataSourceID="SqlDataSource1" DataTextField="NOM" 
            DataValueField="M_MEMBRE_ID" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="SELECT DISTINCT GNE1.M_NOM || ', ' || GNE1.M_PRENOM || ' - ' || GNE1.M_ECOLE AS NOM, GNE1.M_MEMBRE_ID 
            FROM GN_VW_ENSEIGNANTS GNE1
            LEFT OUTER JOIN GN_VW_ENSEIGNANTS GNE2 
                ON GNE2.M_MEMBRE_ID = :ID_USER
            WHERE (GNE1.TYPE_PERSON = 'E' OR GNE1.TYPE_PERSON = 'A')
            AND GNE1.M_ECOLE = :ECOLE
            ORDER BY NOM">
        <SelectParameters>
            <asp:SessionParameter Name="ID_USER" SessionField="ID_USER" Type="String" />
            <asp:ControlParameter ControlID="DropDownList4" Name="ECOLE" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <%--Classes élémentaires:<br />
        <br />
        <br />--%>
        
        Classes secondaires:
        <asp:DropDownList ID="DropDownList2" runat="server" 
            DataSourceID="SqlDataSource2" DataTextField="LISTE_CLASSES" 
            DataValueField="UNIQUEID" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="SELECT DISTINCT ecole
                || codecours
                || cours_section
                || classe_code
                || strand_code
                || report_period AS uniqueid,
                semester, ecole, codecours, classe_code,
                'Semestre ' || semester|| ' -&gt; ' ||
                'Cours '|| matiere || '(' || codecours || ')'|| ' -&gt; ' || 
                classe_code || ' -&gt; ' ||
                'Nb évalutions ' || pkg_sp.fc_nbr_evaluation (ecole,
                                          anneescolaire,
                                          classe_code,
                                          codecours,
                                          report_period,
                                          strand_code
                                         ) AS Liste_Classes
                                   
           FROM gn_vw_classes_sec c,
                organizations o,
                schools s,
                school_class_term t
          WHERE c.anneescolaire = fc_val_parametres('ANNEESCOLAIRE')
            AND c.membre_id = :M_MEMBRE_ID
            AND s.default_school_bsid = o.organization_id
            AND s.school_code = ecole
            AND t.school_code = s.school_code
            AND t.school_year = c.anneescolaire
            AND t.class_code = c.classe_code
       ORDER BY semester, ecole, codecours, classe_code ">
        <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="M_MEMBRE_ID" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
       </asp:SqlDataSource>

        <br />
        <telerik:radgrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource3" 
            GridLines="None" AllowSorting="True" AllowFilteringByColumn="True"  
            ShowGroupPanel="True" Skin="Hay" >
   <ExportSettings HideStructureColumns="true" ExportOnlyData="True" >
            <Excel Format="ExcelML" />
        </ExportSettings>
        <%--<ClientSettings><Scrolling AllowScroll="true" UseStaticHeaders="true" /></ClientSettings>  --%>
        
        <MasterTableView Width="100%" CommandItemDisplay="Top"  
            AutoGenerateColumns="False" DataSourceID="SqlDataSource3" >
    <Columns>
        <telerik:gridboundcolumn DataField="PRÉNOM" HeaderText="PRÉNOM" 
            SortExpression="PRÉNOM" UniqueName="PRÉNOM">
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="NOM" HeaderText="NOM" 
            SortExpression="NOM" UniqueName="NOM">
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="SEXE" HeaderText="SEXE" 
            SortExpression="SEXE" UniqueName="SEXE" FilterControlWidth="40">
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="PEI" HeaderText="PEI" 
            SortExpression="PEI" UniqueName="PEI" FilterControlWidth="40">
        </telerik:GridBoundColumn>
      
        <telerik:gridboundcolumn DataField="NOTE_BULLETIN" HeaderText="NOTE BULLETIN" HeaderStyle-Width="60"
            SortExpression="NOTE_BULLETIN" UniqueName="NOTE_BULLETIN" DataType="System.Decimal" FilterControlWidth="40">
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="CC" DataType="System.Decimal" 
            HeaderText="CC" SortExpression="CC" AllowFiltering="False"
            UniqueName="CC" Visible="false">
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="HP" DataType="System.Decimal" 
            HeaderText="HP" SortExpression="HP" AllowFiltering="False"
            UniqueName="HP" Visible="false">
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="CO" DataType="System.Decimal" 
            HeaderText="CO" SortExpression="CO" AllowFiltering="False"
            UniqueName="CO" Visible="false">
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="MA" DataType="System.Decimal" 
            HeaderText="MA" SortExpression="MA" AllowFiltering="False"
            UniqueName="MA" Visible="false">
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="CC_N" 
            HeaderText="CC_N" SortExpression="CC_N" AllowFiltering="False"
            UniqueName="CC_N">
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="HP_N" 
            HeaderText="HP_N" SortExpression="HP_N" AllowFiltering="False"
            UniqueName="HP_N">
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="CO_N" 
            HeaderText="CO_N" SortExpression="CO_N" AllowFiltering="False"
            UniqueName="CO_N">
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="MA_N" 
            HeaderText="MA_N" SortExpression="MA_N" AllowFiltering="False"
            UniqueName="MA_N">
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="EXAMEN" DataType="System.Decimal" 
            HeaderText="EXAMEN" SortExpression="EXAMEN" AllowFiltering="False"
            UniqueName="EXAMEN">
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="EXAM_N" 
            HeaderText="EXAM_N" SortExpression="EXAM_N" 
            UniqueName="EXAM_N" FilterControlWidth="40">
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="SEMESTRE" DataType="System.Decimal" 
            HeaderText="SEMESTRE" SortExpression="SEMESTRE" 
            UniqueName="SEMESTRE" FilterControlWidth="40">
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="SEMESTRE_N" 
            HeaderText="SEMESTRE_N" SortExpression="SEMESTRE_N" 
            UniqueName="SEMESTRE_N" AllowFiltering="False" >
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="MOYENNE" DataType="System.Decimal" AllowFiltering="False"
            HeaderText="MOYENNE" SortExpression="MOYENNE" UniqueName="MOYENNE">
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="AJUSTEMENT" DataType="System.Decimal" 
            HeaderText="AJUSTEMENT" SortExpression="AJUSTEMENT" AllowFiltering="False"
            UniqueName="AJUSTEMENT">
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="MOYENNE_AJUSTEE" DataType="System.Decimal" 
            HeaderText="MOYENNE AJUSTÉE" SortExpression="MOYENNE_AJUSTEE" AllowFiltering="False"
            UniqueName="MOYENNE_AJUSTEE" HeaderStyle-Width="80" >
        </telerik:GridBoundColumn>
       
        <telerik:gridboundcolumn DataField="MOYENNE_N" HeaderText="MOYENNE_N" 
            SortExpression="MOYENNE_N" UniqueName="MOYENNE_N" FilterControlWidth="40">
        </telerik:GridBoundColumn>
        
        <telerik:gridboundcolumn DataField="MOYENNE_AJUSTEE_N" 
            HeaderText="MOYENNE AJUSTÉE_N"  HeaderStyle-Width="80" HeaderStyle-Wrap="true" SortExpression="MOYENNE_AJUSTEE_N" 
            UniqueName="MOYENNE_AJUSTEE_N" FilterControlWidth="40">
        </telerik:GridBoundColumn>
        <telerik:gridboundcolumn DataField="M_A_QUITTE" HeaderText="M_A_QUITTE" 
            SortExpression="M_A_QUITTE" UniqueName="M_A_QUITTE" FilterControlWidth="40">
        </telerik:GridBoundColumn>
    </Columns>
    <CommandItemSettings  ShowExportToWordButton="true" ShowExportToExcelButton="true" ShowExportToCsvButton="true" 
            ShowExportToPdfButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
</MasterTableView>
            <ClientSettings AllowDragToGroup="True"></ClientSettings>
        </telerik:radgrid>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="SELECT   m.m_prenom as Prénom, m.m_nom as nom, p.gender sexe,
         pkg_sp.fc_eleve_pei (m.m_membre_id, m.m_anneescolaire, m.m_ecole ) Pei,
         DECODE ('2', '0', (SELECT b_niveau_bulletin FROM sp_baremes 
WHERE b_niveau_rendement = pkg_sp.fc_niveau_a_afficher (ROUND (NVL (m_note_ajustee, m_note ), 1), fc_val_parametres('ANNEESCOLAIRE'),DECODE ('2', '0', 'E', '1', 'S' ) )  
AND b_anneescolaire = fc_val_parametres('ANNEESCOLAIRE')   
AND b_niveau = DECODE ('2', '0', 'E', '1', 'S')  
 AND b_type_eval = 'G'),'1', ROUND (NVL (m_note_ajustee, m_note), 1), '2', ROUND (NVL (m_note_ajustee, m_note), 1) ) AS note_bulletin,
         ROUND (m_comp1_moyenne, 1) CC,
         ROUND (m_comp2_moyenne, 1) HP,
         ROUND (m_comp3_moyenne, 1) CO,
         ROUND (m_comp4_moyenne, 1) MA,
         pkg_sp.fc_niveau_a_afficher(ROUND (m_comp1_moyenne, 1),fc_val_parametres('ANNEESCOLAIRE'),'S') AS CC_N,
         pkg_sp.fc_niveau_a_afficher(ROUND (m_comp2_moyenne, 1),fc_val_parametres('ANNEESCOLAIRE'),'S') AS HP_N,
         pkg_sp.fc_niveau_a_afficher(ROUND (m_comp3_moyenne, 1),fc_val_parametres('ANNEESCOLAIRE'),'S') AS CO_N,
         pkg_sp.fc_niveau_a_afficher(ROUND (m_comp4_moyenne, 1),fc_val_parametres('ANNEESCOLAIRE'),'S') AS MA_N,
         ROUND (m_note_examen, 1) AS Examen,
         pkg_sp.fc_niveau_a_afficher(ROUND (m_note_examen, 1),fc_val_parametres('ANNEESCOLAIRE'),'S') AS Exam_N,
         ROUND (m_note_semestre, 1) AS Semestre,
         pkg_sp.fc_niveau_a_afficher(ROUND (m_note_semestre, 1),fc_val_parametres('ANNEESCOLAIRE'),'S') AS Semestre_N,
         ROUND (m_note, 1) AS moyenne,
         m_note_semestre_ajustement AS ajustement,
         ROUND (NVL (m_note_ajustee, m_note), 1) AS moyenne_ajustee,
         pkg_sp.fc_niveau_a_afficher (ROUND (m_note, 1),fc_val_parametres('ANNEESCOLAIRE'),'S') AS moyenne_n,
         pkg_sp.fc_niveau_a_afficher(ROUND (NVL (m_note_ajustee, m_note), 1),fc_val_parametres('ANNEESCOLAIRE'),'S') AS moyenne_ajustee_N,
         m_a_quitte--,
    FROM sp_classes c, trill02.persons p, sp_eleves_resultats m
   WHERE m.m_ecole = :m_ecole
     AND m.m_anneescolaire = fc_val_parametres('ANNEESCOLAIRE')
     AND m.m_classe_code = :m_classe_code
     AND m.m_codecours = :m_codecours
     AND m.m_periode_ref = :m_periode_ref
     AND m.m_strand_code = :m_strand_code
     AND m.m_membre_id = p.person_id
     AND c.c_ecole = m.m_ecole
     AND c.c_anneescolaire = m.m_anneescolaire
     AND c.c_classe_code = m.m_classe_code
     AND c.c_codecours = m.m_codecours
     AND c.c_periode_ref = m.m_periode_ref
     AND c.c_strand_code = m.m_strand_code
ORDER BY m_a_quitte, m_nom, m_prenom">
        <SelectParameters>
            <asp:SessionParameter Name="m_ecole" SessionField="m_ecole" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_classe_code" SessionField="m_classe_code" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_codecours" SessionField="m_codecours" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_periode_ref" SessionField="m_periode_ref" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_strand_code" SessionField="m_strand_code" Type="String" DefaultValue="" />
        </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        
       <span lang="fr-ca">Liste des évaluations:<asp:DropDownList ID="DropDownList3" runat="server" 
            DataSourceID="SqlDataSource6" DataTextField="DESCRIPTION" 
            DataValueField="EVAL_ID" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="SELECT   e_evaluation_id as eval_id, e_date AS d_date,
         e_description, e_date || ' ' || e_description as description
    FROM sp_evaluations LEFT JOIN sp_grilles ON e_evaluation_id = evaluation_id
   WHERE e_ecole = :m_ecole
     AND e_anneescolaire = fc_val_parametres('ANNEESCOLAIRE')
     AND e_classe_code = :m_classe_code
     AND e_codecours = :m_codecours
     AND e_strand_code = :m_strand_code
ORDER BY d_date ASC, e_description ASC">

        <SelectParameters>
            <asp:SessionParameter Name="m_ecole" SessionField="m_ecole" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_classe_code" SessionField="m_classe_code" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_codecours" SessionField="m_codecours" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_strand_code" SessionField="m_strand_code" Type="String" DefaultValue="" />
        </SelectParameters>
</asp:SqlDataSource>
        </span>
        
        <telerik:RadGrid ID="RadGrid2" runat="server" AllowFilteringByColumn="True" 
            AllowSorting="True" DataSourceID="SqlDataSource4" GridLines="None" 
            ShowGroupPanel="True" Skin="Hay">
               <ExportSettings HideStructureColumns="true" ExportOnlyData="True" >
            <Excel Format="ExcelML" />
        </ExportSettings>
<MasterTableView AutoGenerateColumns="False" CommandItemDisplay="Top" DataSourceID="SqlDataSource4">
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn DataField="E_DATE" HeaderText="E_DATE" 
            SortExpression="E_DATE" UniqueName="E_DATE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_DESCRIPTION" HeaderText="E_DESCRIPTION" 
            SortExpression="E_DESCRIPTION" UniqueName="E_DESCRIPTION">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_MOYENNE" DataType="System.Decimal" 
            HeaderText="E_MOYENNE" SortExpression="E_MOYENNE" UniqueName="E_MOYENNE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_PONDERATION" DataType="System.Decimal" 
            HeaderText="E_PONDERATION" SortExpression="E_PONDERATION" 
            UniqueName="E_PONDERATION">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_EXAMEN" HeaderText="E_EXAMEN" 
            SortExpression="E_EXAMEN" UniqueName="E_EXAMEN">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="MOYENNE_GR" HeaderText="MOYENNE_GR" 
            SortExpression="MOYENNE_GR" UniqueName="MOYENNE_GR">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="MOYENNE_AFF" HeaderText="MOYENNE_AFF" 
            SortExpression="MOYENNE_AFF" UniqueName="MOYENNE_AFF">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_COMP1_STYLE" HeaderText="COMP1 STYLE" AllowFiltering="False" HeaderStyle-Width="60"
            SortExpression="E_COMP1_STYLE" UniqueName="E_COMP1_STYLE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_COMP2_STYLE" HeaderText="COMP2 STYLE" AllowFiltering="False" HeaderStyle-Width="60"
            SortExpression="E_COMP2_STYLE" UniqueName="E_COMP2_STYLE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_COMP3_STYLE" HeaderText="COMP3 STYLE" AllowFiltering="False" HeaderStyle-Width="60"
            SortExpression="E_COMP3_STYLE" UniqueName="E_COMP3_STYLE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_COMP4_STYLE" HeaderText="COMP4 STYLE" AllowFiltering="False" HeaderStyle-Width="60"
            SortExpression="E_COMP4_STYLE" UniqueName="E_COMP4_STYLE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="STATUT" HeaderText="STATUT" FilterControlWidth="40"
            SortExpression="STATUT" UniqueName="STATUT">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NO_ELEVES" DataType="System.Decimal" FilterControlWidth="40"
            HeaderText="NO_ELEVES" SortExpression="NO_ELEVES" UniqueName="NO_ELEVES">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NO_VIDES" DataType="System.Decimal" FilterControlWidth="40"
            HeaderText="NO_VIDES" SortExpression="NO_VIDES" UniqueName="NO_VIDES">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NO_ABSENTS" DataType="System.Decimal" 
            HeaderText="NO_ABSENTS" SortExpression="NO_ABSENTS" UniqueName="NO_ABSENTS">
        </telerik:GridBoundColumn>
    </Columns>
        <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true" ShowExportToCsvButton="true" 
            ShowExportToPdfButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
</MasterTableView>
            <ClientSettings AllowDragToGroup="True">
            </ClientSettings>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="SELECT  
            -- e_evaluation_id,
            -- e_date AS d_date,
         TO_CHAR (e_date, 'dd-MM-yy')as e_date , e_description , e_moyenne,
         e_ponderation, e_examen, --gr_id AS grille_id, e_date_affichage,
         --e_retard_remise,
         pkg_sp.fc_niveau_a_afficher (e_moyenne,fc_val_parametres('ANNEESCOLAIRE'),'S') AS moyenne_gr,ROUND (e_moyenne, 1)
         || ' / '
         || pkg_sp.fc_niveau_a_afficher (e_moyenne, fc_val_parametres('ANNEESCOLAIRE'), 'S') AS moyenne_aff,
         e_comp1_style, e_comp2_style, e_comp3_style, e_comp4_style,
         pkg_sp.fc_status_evaluation (e_evaluation_id) AS statut,
         (SELECT COUNT (n_membre_id)
            FROM sp_notes, sp_eleves_resultats
           WHERE n_evaluation_id = e_evaluation_id
             AND m_membre_id = n_membre_id
             AND m_codecours = e_codecours
             AND m_classe_code = e_classe_code
             AND m_ecole = e_ecole
             AND m_periode_ref = e_periode_ref
             AND m_anneescolaire = e_anneescolaire
             AND m_a_quitte = 'N') AS no_eleves,
         (SELECT COUNT (n_membre_id)
            FROM sp_notes, sp_eleves_resultats
           WHERE n_abs = 'N'
             AND n_comp1_sur100 IS NULL
             AND n_comp2_sur100 IS NULL
             AND n_comp3_sur100 IS NULL
             AND n_comp4_sur100 IS NULL
             AND m_membre_id = n_membre_id
             AND m_codecours = e_codecours
             AND m_classe_code = e_classe_code
             AND m_ecole = e_ecole
             AND m_periode_ref = e_periode_ref
             AND m_anneescolaire = e_anneescolaire
             AND m_a_quitte = 'N'
             AND n_evaluation_id = e_evaluation_id) AS no_vides,
         (SELECT COUNT (n_membre_id)
            FROM sp_notes, sp_eleves_resultats
           WHERE n_evaluation_id = e_evaluation_id
             AND n_abs = 'O'
             AND m_membre_id = n_membre_id
             AND m_codecours = e_codecours
             AND m_classe_code = e_classe_code
             AND m_ecole = e_ecole
             AND m_periode_ref = e_periode_ref
             AND m_anneescolaire = e_anneescolaire
             AND m_a_quitte = 'N') AS no_absents
    FROM sp_evaluations LEFT JOIN sp_grilles ON e_evaluation_id = evaluation_id
   WHERE e_ecole = :m_ecole
     AND e_anneescolaire = fc_val_parametres('ANNEESCOLAIRE')
     AND e_classe_code = :m_classe_code
     AND e_codecours = :m_codecours
     AND e_strand_code = :m_strand_code
ORDER BY e_date ASC, e_description ASC">
        <SelectParameters>
            <asp:SessionParameter Name="m_ecole" SessionField="m_ecole" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_classe_code" SessionField="m_classe_code" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_codecours" SessionField="m_codecours" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_strand_code" SessionField="m_strand_code" Type="String" DefaultValue="" />
        </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
 
        <telerik:RadGrid ID="RadGrid3" runat="server" AllowFilteringByColumn="True" 
            AllowSorting="True" DataSourceID="SqlDataSource5" GridLines="None" 
            ShowGroupPanel="True" Skin="Hay">
             <ExportSettings HideStructureColumns="true" ExportOnlyData="True" >
            <Excel Format="ExcelML" />
        </ExportSettings>
        <%--<ClientSettings><Scrolling AllowScroll="true" UseStaticHeaders="true" /></ClientSettings>      --%>
<MasterTableView AutoGenerateColumns="False" DataSourceID="SqlDataSource5" CommandItemDisplay="Top">
    <RowIndicatorColumn>
    <HeaderStyle Width="20px"></HeaderStyle>
    </RowIndicatorColumn>
    <ExpandCollapseColumn>
    <HeaderStyle Width="20px"></HeaderStyle>
    </ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn DataField="PRENOM" HeaderText="PRENOM" 
            SortExpression="PRENOM" UniqueName="PRENOM">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NOM" HeaderText="NOM" SortExpression="NOM" 
            UniqueName="NOM">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="M_A_QUITTE" HeaderText="M_A_QUITTE" 
            SortExpression="M_A_QUITTE" UniqueName="M_A_QUITTE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CC" HeaderText="CC" SortExpression="CC" AllowFiltering="False"
            UniqueName="CC">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="HP" HeaderText="HP" SortExpression="HP" AllowFiltering="False"
            UniqueName="HP">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CO" HeaderText="CO" SortExpression="CO" AllowFiltering="False"
            UniqueName="CO">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="MA" HeaderText="MA" SortExpression="MA" AllowFiltering="False"
            UniqueName="MA">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NOTE" HeaderText="NOTE" 
            SortExpression="NOTE" UniqueName="NOTE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NOTE_SUR_100" DataType="System.Decimal" 
            HeaderText="NOTE_SUR_100" SortExpression="NOTE_SUR_100" 
            UniqueName="NOTE_SUR_100">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ABS" HeaderText="ABS" SortExpression="ABS" 
            UniqueName="ABS">
        </telerik:GridBoundColumn>
    </Columns>
    <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true" ShowExportToCsvButton="true" 
            ShowExportToPdfButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
</MasterTableView>
            <ClientSettings AllowDragToGroup="True">
            </ClientSettings>
        </telerik:RadGrid>
       <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="SELECT  
         m_prenom as Prenom, m_nom as nom, m_a_quitte,
         pkg_sp.fc_niveau_a_afficher (n_comp1_sur100,fc_val_parametres('ANNEESCOLAIRE'),'S') AS CC,
         pkg_sp.fc_niveau_a_afficher (n_comp2_sur100,fc_val_parametres('ANNEESCOLAIRE'),'S') AS HP,
         pkg_sp.fc_niveau_a_afficher (n_comp3_sur100,fc_val_parametres('ANNEESCOLAIRE'),'S') AS CO,
         pkg_sp.fc_niveau_a_afficher (n_comp4_sur100,fc_val_parametres('ANNEESCOLAIRE'),'S') AS MA,
         pkg_sp.fc_niveau_a_afficher (n_note, fc_val_parametres('ANNEESCOLAIRE'), 'S') AS note,
         ROUND (n_note, 1) as note_sur_100, n_abs as abs
    FROM sp_eleves_resultats, sp_notes LEFT JOIN sp_documents_note
         ON dn_evaluation_id = n_evaluation_id AND dn_membre_id = n_membre_id
   WHERE n_evaluation_id = :EVAL_ID
     AND m_ecole = :m_ecole
     AND m_periode_ref = :m_periode_ref
     AND m_classe_code = :m_classe_code
     AND m_codecours = :m_codecours
     AND m_strand_code = :m_strand_code
     AND m_anneescolaire = fc_val_parametres('ANNEESCOLAIRE')
     AND m_membre_id = n_membre_id
     AND (   dn_date_remise =
                (SELECT MAX (dn_date_remise)
                   FROM sp_documents_note n2
                  WHERE n2.dn_evaluation_id = n_evaluation_id
                    AND n2.dn_membre_id = n_membre_id)
          OR dn_date_remise IS NULL
         )
ORDER BY m_nom, m_prenom">

        <SelectParameters>
            <asp:SessionParameter Name="m_ecole" SessionField="m_ecole" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_classe_code" SessionField="m_classe_code" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_codecours" SessionField="m_codecours" Type="String" DefaultValue="" />
            <asp:SessionParameter Name="m_strand_code" SessionField="m_strand_code" Type="String" DefaultValue="" />
            <asp:ControlParameter ControlID="DropDownList3" Name="EVAL_ID" PropertyName="SelectedValue" Type="String" />
            <asp:SessionParameter Name="m_periode_ref" SessionField="m_periode_ref" Type="String" DefaultValue="" />
        </SelectParameters>

</asp:SqlDataSource>

<%--    A FINIR    <br />
        <br />
        Evaluation : RG tableau group by (nbre évaluation totale + moyenne classe dans 
        le group by comme SMS)<br />
        Moyenne: Ajustement 1 er tableau du GN<br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        Bulletin: commentaire &amp; compétences<br />
        -- bulletins + commentaires + habilletés + transferé à trillium oui ou non </span></div>--%>
        

 </asp:content>
